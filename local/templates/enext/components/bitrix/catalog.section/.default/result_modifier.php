<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

//DISABLE_BASKET//
$arSettings = CEnext::GetFrontParametrsValues(SITE_ID);
$arParams['DISABLE_BASKET'] = false;
if($arSettings['DISABLE_BASKET'] == 'Y') {
	$arParams['DISABLE_BASKET'] = true;
	if($arParams['USE_PRODUCT_QUANTITY'])
		$arParams['USE_PRODUCT_QUANTITY'] = false;
}

//SHOW_SUBSCRIBE//
if($arParams['PRODUCT_SUBSCRIPTION'] == 'Y') {
	$saleNotifyOption = Bitrix\Main\Config\Option::get('sale', 'subscribe_prod');
	if(strlen($saleNotifyOption) > 0)
		$saleNotifyOption = unserialize($saleNotifyOption);
	$saleNotifyOption = is_array($saleNotifyOption) ? $saleNotifyOption : array();
	foreach($saleNotifyOption as $siteId => $data) {
		if($siteId == SITE_ID && $data['use'] != 'Y')
			$arParams['PRODUCT_SUBSCRIPTION'] = 'N';
	}
}

//MEASURE//
$measureIds = $arMeasureList = array();

foreach($arResult['ITEMS'] as $item) {
	if(!empty($item['OFFERS'])) {
		foreach($item['OFFERS'] as $arOffer) {
			$measureIds[] = $arOffer['ITEM_MEASURE']['ID'];
		}
		unset($arOffer);
	} else {
		$measureIds[] = $item['ITEM_MEASURE']['ID'];
	}
}
unset($item);

if(count($measureIds) > 0) {
	$rsMeasures = CCatalogMeasure::getList(array(), array('ID' => array_unique($measureIds)), false, false, array('ID', 'SYMBOL_INTL'));
	while($arMeasure = $rsMeasures->GetNext()) {
		$arMeasureList[$arMeasure['ID']] = $arMeasure['SYMBOL_INTL'];
	}
	unset($arMeasure, $rsMeasures);

	foreach($arResult['ITEMS'] as $key => &$item) {
		if(!empty($item['OFFERS'])) {
			foreach($item['OFFERS'] as $keyOffer => &$arOffer) {
				if(array_key_exists($arOffer['ITEM_MEASURE']['ID'], $arMeasureList)) {
					$arOffer['ITEM_MEASURE']['SYMBOL_INTL'] = $arMeasureList[$arOffer['ITEM_MEASURE']['ID']];
					$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['ITEM_MEASURE'] = $arOffer['ITEM_MEASURE'];
				}
			}
			unset($keyOffer, $arOffer);
		} else {
			if(array_key_exists($item['ITEM_MEASURE']['ID'], $arMeasureList))
				$item['ITEM_MEASURE']['SYMBOL_INTL'] = $arMeasureList[$item['ITEM_MEASURE']['ID']];
		}
	}
	unset($key, $item);
}
unset($arMeasureList, $measureIds);

//PRICE//
foreach($arResult['ITEMS'] as $key => &$item) {
	if(!empty($item['PROPERTIES']['M2_COUNT']['VALUE'])) {
		$sqMCount = str_replace(',', '.', $item['PROPERTIES']['M2_COUNT']['VALUE']);	
		if(!empty($item['OFFERS'])) {
			foreach($item['OFFERS'] as $keyOffer => &$arOffer) {
				$measureRatio = $arOffer['ITEM_MEASURE_RATIOS'][$arOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
				if($arOffer['ITEM_MEASURE']['SYMBOL_INTL'] == 'pc. 1') {
					$arOffer['PC_MAX_QUANTITY'] = $arOffer['CATALOG_QUANTITY'];
					$arOffer['PC_STEP_QUANTITY'] = $measureRatio;				
					$arOffer['SQ_M_MAX_QUANTITY'] = round($arOffer['CATALOG_QUANTITY'] / $sqMCount, 2);
					$arOffer['SQ_M_STEP_QUANTITY'] = round($measureRatio / $sqMCount, 2);

					$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['PC_MAX_QUANTITY'] = $arOffer['PC_MAX_QUANTITY'];
					$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['PC_STEP_QUANTITY'] = $arOffer['PC_STEP_QUANTITY'];				
					$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['SQ_M_MAX_QUANTITY'] = $arOffer['SQ_M_MAX_QUANTITY'];
					$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['SQ_M_STEP_QUANTITY'] = $arOffer['SQ_M_STEP_QUANTITY'];
					
					if(Bitrix\Main\Loader::includeModule('catalog') && Bitrix\Main\Loader::includeModule('currency')) {
						foreach($arOffer['ITEM_PRICES'] as $keyPrice => &$arPrice) {
							$arPrice['SQ_M_BASE_PRICE'] = Bitrix\Catalog\Product\Price::roundPrice($arPrice['PRICE_TYPE_ID'], $arPrice['BASE_PRICE'] * $sqMCount, $arPrice['CURRENCY']);
							$arPrice['SQ_M_PRINT_BASE_PRICE'] = CCurrencyLang::CurrencyFormat($arPrice['BASE_PRICE'] * $sqMCount, $arPrice['CURRENCY'], true);
							$arPrice['SQ_M_PRICE'] = Bitrix\Catalog\Product\Price::roundPrice($arPrice['PRICE_TYPE_ID'], $arPrice['PRICE'] * $sqMCount, $arPrice['CURRENCY']);	
							$arPrice['SQ_M_PRINT_PRICE'] = CCurrencyLang::CurrencyFormat($arPrice['PRICE'] * $sqMCount, $arPrice['CURRENCY'], true);
							$arPrice['SQ_M_DISCOUNT'] = Bitrix\Catalog\Product\Price::roundPrice($arPrice['PRICE_TYPE_ID'], $arPrice['BASE_PRICE'] * $sqMCount - $arPrice['PRICE'] * $sqMCount, $arPrice['CURRENCY']);
							$arPrice['SQ_M_PRINT_DISCOUNT'] = CCurrencyLang::CurrencyFormat($arPrice['BASE_PRICE'] * $sqMCount - $arPrice['PRICE'] * $sqMCount, $arPrice['CURRENCY'], true);
							$arPrice['PC_MIN_QUANTITY'] = $arPrice['MIN_QUANTITY'];
							$arPrice['SQ_M_MIN_QUANTITY'] = round($arPrice['MIN_QUANTITY'] / $sqMCount, 2);

							$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['ITEM_PRICES'][$keyPrice]['SQ_M_BASE_PRICE'] = $arPrice['SQ_M_BASE_PRICE'];
							$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['ITEM_PRICES'][$keyPrice]['SQ_M_PRINT_BASE_PRICE'] = $arPrice['SQ_M_PRINT_BASE_PRICE'];
							$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['ITEM_PRICES'][$keyPrice]['SQ_M_PRICE'] = $arPrice['SQ_M_PRICE'];
							$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['ITEM_PRICES'][$keyPrice]['SQ_M_PRINT_PRICE'] = $arPrice['SQ_M_PRINT_PRICE'];
							$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['ITEM_PRICES'][$keyPrice]['SQ_M_DISCOUNT'] = $arPrice['SQ_M_DISCOUNT'];
							$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['ITEM_PRICES'][$keyPrice]['SQ_M_PRINT_DISCOUNT'] = $arPrice['SQ_M_PRINT_DISCOUNT'];
							$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['ITEM_PRICES'][$keyPrice]['PC_MIN_QUANTITY'] = $arPrice['PC_MIN_QUANTITY'];
							$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['ITEM_PRICES'][$keyPrice]['SQ_M_MIN_QUANTITY'] = $arPrice['SQ_M_MIN_QUANTITY'];
						}
						unset($keyPrice, $arPrice);
					}
				} elseif($arOffer['ITEM_MEASURE']['SYMBOL_INTL'] == 'm2') {
					$arOffer['PC_MAX_QUANTITY'] = floor($arOffer['CATALOG_QUANTITY'] / $measureRatio);
					$arOffer['PC_STEP_QUANTITY'] = 1;
					$arOffer['SQ_M_MAX_QUANTITY'] = $arOffer['CATALOG_QUANTITY'];
					$arOffer['SQ_M_STEP_QUANTITY'] = $measureRatio;

					$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['PC_MAX_QUANTITY'] = $arOffer['PC_MAX_QUANTITY'];
					$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['PC_STEP_QUANTITY'] = $arOffer['PC_STEP_QUANTITY'];
					$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['SQ_M_MAX_QUANTITY'] = $arOffer['SQ_M_MAX_QUANTITY'];
					$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['SQ_M_STEP_QUANTITY'] = $arOffer['SQ_M_STEP_QUANTITY'];

					foreach($arOffer['ITEM_PRICES'] as $keyPrice => &$arPrice) {
						$arPrice['PC_MIN_QUANTITY'] = 1;
						$arPrice['SQ_M_MIN_QUANTITY'] = $arPrice['MIN_QUANTITY'];

						$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['ITEM_PRICES'][$keyPrice]['PC_MIN_QUANTITY'] = $arPrice['PC_MIN_QUANTITY'];
						$arResult['ITEMS'][$key]['JS_OFFERS'][$keyOffer]['ITEM_PRICES'][$keyPrice]['SQ_M_MIN_QUANTITY'] = $arPrice['SQ_M_MIN_QUANTITY'];
					}
					unset($keyPrice, $arPrice);
				}
			}
			unset($keyOffer, $arOffer, $measureRatio);
		} else {
			if($item['ITEM_MEASURE']['SYMBOL_INTL'] == 'pc. 1') {
				if(Bitrix\Main\Loader::includeModule('catalog') && Bitrix\Main\Loader::includeModule('currency')) {
					foreach($item['ITEM_PRICES'] as &$arPrice) {
						$arPrice['SQ_M_BASE_PRICE'] = Bitrix\Catalog\Product\Price::roundPrice($arPrice['PRICE_TYPE_ID'], $arPrice['BASE_PRICE'] * $sqMCount, $arPrice['CURRENCY']);
						$arPrice['SQ_M_PRINT_BASE_PRICE'] = CCurrencyLang::CurrencyFormat($arPrice['BASE_PRICE'] * $sqMCount, $arPrice['CURRENCY'], true);
						$arPrice['SQ_M_PRICE'] = Bitrix\Catalog\Product\Price::roundPrice($arPrice['PRICE_TYPE_ID'], $arPrice['PRICE'] * $sqMCount, $arPrice['CURRENCY']);	
						$arPrice['SQ_M_PRINT_PRICE'] = CCurrencyLang::CurrencyFormat($arPrice['PRICE'] * $sqMCount, $arPrice['CURRENCY'], true);
						$arPrice['SQ_M_DISCOUNT'] = Bitrix\Catalog\Product\Price::roundPrice($arPrice['PRICE_TYPE_ID'], $arPrice['BASE_PRICE'] * $sqMCount - $arPrice['PRICE'] * $sqMCount, $arPrice['CURRENCY']);
						$arPrice['SQ_M_PRINT_DISCOUNT'] = CCurrencyLang::CurrencyFormat($arPrice['BASE_PRICE'] * $sqMCount - $arPrice['PRICE'] * $sqMCount, $arPrice['CURRENCY'], true);
						$arPrice['PC_MIN_QUANTITY'] = $arPrice['MIN_QUANTITY'];
						$arPrice['SQ_M_MIN_QUANTITY'] = round($arPrice['MIN_QUANTITY'] / $sqMCount, 2);
					}
					unset($arPrice);
				}
			} elseif($item['ITEM_MEASURE']['SYMBOL_INTL'] == 'm2') {
				foreach($item['ITEM_PRICES'] as &$arPrice) {
					$arPrice['PC_MIN_QUANTITY'] = 1;
					$arPrice['SQ_M_MIN_QUANTITY'] = $arPrice['MIN_QUANTITY'];
				}
				unset($arPrice);
			}
		}
	}
	unset($sqMCount);
}
unset($item);

//MARKERS_BRAND//
foreach($arResult['ITEMS'] as $item) {
	foreach($item['PROPERTIES'] as $prop) {
		if($prop['CODE'] == 'MARKER' && !empty($prop['VALUE'])) {
			if(!is_array($prop['VALUE'])) {
				$markersIds[] = $prop['VALUE'];
			} else {
				foreach($prop['VALUE'] as $val) {
					$markersIds[] = $val;
				}
				unset($val);
			}
		} elseif($prop['CODE'] == 'BRAND' && !empty($prop['VALUE'])) {
			$brandIds[] = $prop['VALUE'];
		}
	}
	unset($prop);
}
unset($item);

//MARKERS//
if(!empty($markersIds)) {	
	$rsElements = CIBlockElement::GetList(array(), array('ID' => array_unique($markersIds)), false, false, array('ID', 'IBLOCK_ID', 'NAME', 'SORT'));	
	while($obElement = $rsElements->GetNextElement()) {
		$arElement = $obElement->GetFields();
		$arElement['PROPERTIES'] = $obElement->GetProperties();
		
		$arMarkers[$arElement['ID']] = array(
			'NAME' => $arElement['NAME'],
			'SORT' => $arElement['SORT'],
			'BACKGROUND_1' => $arElement['PROPERTIES']['BACKGROUND_1']['VALUE'],
			'BACKGROUND_2' => $arElement['PROPERTIES']['BACKGROUND_2']['VALUE'],
			'ICON' => $arElement['PROPERTIES']['ICON']['VALUE'],
			'FONT_SIZE' => $arElement['PROPERTIES']['FONT_SIZE']['VALUE_XML_ID']
		);
	}
	unset($arElement, $obElement, $rsElements);

	if(!empty($arMarkers)) {
		foreach($arResult['ITEMS'] as &$item) {
			foreach($item['PROPERTIES'] as &$prop) {
				if($prop['CODE'] == 'MARKER' && !empty($prop['VALUE'])) {
					if(!is_array($prop['VALUE'])) {
						if(array_key_exists($prop['VALUE'], $arMarkers))
							$prop['FULL_VALUE'][] = $arMarkers[$prop['VALUE']];
					} else {
						foreach($prop['VALUE'] as $val) {
							if(array_key_exists($val, $arMarkers))
								$prop['FULL_VALUE'][] = $arMarkers[$val];
						}
						unset($val);
					}
					
					if(!empty($prop['FULL_VALUE']))
						Bitrix\Main\Type\Collection::sortByColumn($prop['FULL_VALUE'], array('SORT' => SORT_NUMERIC, 'NAME' => SORT_ASC));
				}
			}
			unset($prop);
		}
		unset($item);
	}
	unset($arMarkers);
}
unset($markersIds);

//BRANDS//
if(!empty($brandIds)) {
	$rsElements = CIBlockElement::GetList(array(), array('ID' => array_unique($brandIds)), false, false, array('ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_PICTURE'));
	while($arElement = $rsElements->GetNext()) {
		$arBrands[$arElement['ID']] = array(
			'NAME' => $arElement['NAME'],
			'PREVIEW_PICTURE' => $arElement['PREVIEW_PICTURE'] > 0 ? CFile::GetFileArray($arElement['PREVIEW_PICTURE']) : array()
		);
	}
	unset($arElement, $rsElements);
	
	if(!empty($arBrands)) {
		foreach($arResult['ITEMS'] as &$item) {		
			foreach($item['PROPERTIES'] as &$prop) {
				if($prop['CODE'] == 'BRAND' && !empty($prop['VALUE'])) {
					if(array_key_exists($prop['VALUE'], $arBrands))
						$prop['FULL_VALUE'] = $arBrands[$prop['VALUE']];
				}
			}
			unset($prop);
		}
		unset($item);
	}
	unset($arBrands);
}
unset($brandIds);

//UF_CODE//
$isSkuProps = false;
foreach($arResult['ITEMS'] as $item) {
	if(!empty($item['OFFERS']) && !empty($item['OFFERS_PROP'])) {
		$isSkuProps = true;
		break;
	}
}
unset($item);

if($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && !!$isSkuProps) {
	foreach($arResult['SKU_PROPS'][$arResult['IBLOCK_ID']] as &$skuProperty) {
		if($skuProperty['SHOW_MODE'] == 'PICT') {
			$entity = $skuProperty['USER_TYPE_SETTINGS']['ENTITY'];
			if(!($entity instanceof Bitrix\Main\Entity\Base))
				continue;

			$entityFields = $entity->getFields();
			if(!array_key_exists('UF_CODE', $entityFields))
				continue;

			$entityDataClass = $entity->getDataClass();
			
			$directorySelect = array('ID', 'UF_CODE');
			$directoryOrder = array();
			
			$entityGetList = array(
				'select' => $directorySelect,
				'order' => $directoryOrder
			);
			$propEnums = $entityDataClass::getList($entityGetList);
			while($oneEnum = $propEnums->fetch()) {
				$values[$oneEnum['ID']] = $oneEnum['UF_CODE'];
			}

			foreach($skuProperty['VALUES'] as &$val) {				
				if(isset($values[$val['ID']]))
					$val['CODE'] = $values[$val['ID']];
			}
			unset($val, $values);
		}
	}
	unset($skuProperty);
}
unset($isSkuProps);

//RATING_REVIEWS_COUNT//
if($arParams['USE_REVIEW'] != 'N' && intval($arParams['REVIEWS_IBLOCK_ID']) > 0) {
	foreach($arResult['ITEMS'] as $item) {
		$itemIds[] = $item['ID'];
		
		$ratingSum[$item['ID']] = 0;
		$reviewsCount[$item['ID']] = 0;
	}
	unset($item);

	if(count($itemIds) > 0) {
		$rsElements = CIBlockElement::GetList(array(), array('ACTIVE' => 'Y', 'IBLOCK_ID' => $arParams['REVIEWS_IBLOCK_ID'], "PROPERTY_PRODUCT_ID" => array_unique($itemIds)), false, false, array('ID', 'IBLOCK_ID'));
		while($obElement = $rsElements->GetNextElement()) {
			$arElement = $obElement->GetFields();
			$arProps = $obElement->GetProperties();

			$ratingSum[$arProps['PRODUCT_ID']['VALUE']] += $arProps['RATING']['VALUE_XML_ID'];
			
			$reviewsCount[$arProps['PRODUCT_ID']['VALUE']]++;
		}
		unset($arProps, $arElement, $obElement, $rsElements);

		foreach($arResult['ITEMS'] as &$item) {
			$item['RATING_VALUE'] = $reviewsCount[$item['ID']] > 0 ? sprintf('%.1f', round($ratingSum[$item['ID']] / $reviewsCount[$item['ID']], 1)) : 0;
			$item['REVIEWS_COUNT'] = $reviewsCount[$item['ID']];
		}
		unset($reviewsCount, $ratingSum, $item);
	}
	unset($itemIds);
}