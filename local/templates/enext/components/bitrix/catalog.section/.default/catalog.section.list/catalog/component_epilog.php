<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$title = $arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]; // Расширенное название раздела
if (empty($title)) $title = $arResult["SECTION"]["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]; // Короткое название раздела
if (empty($title)) $title = $APPLICATION->GetTitle(); // если ничего нет, то Каталог товаров
$APPLICATION->SetTitle($title);
$GLOBALS['SECTIONS_COUNT'] = $arResult['SECTIONS_COUNT'];
?>