<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
	Bitrix\Main\ModuleManager,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();

if($isFilter) {
	//SECTION_FILTER//?>
	<?$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arCurSection["ID"],
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SAVE_IN_SESSION" => "N",				
			"XML_EXPORT" => "Y",
			"SECTION_TITLE" => "NAME",
			"SECTION_DESCRIPTION" => "DESCRIPTION",
			"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],				
			"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
			"CURRENCY_ID" => $arParams["CURRENCY_ID"],
			"SEF_MODE" => $arParams["SEF_MODE"],
			"SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
			"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
			"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
			"INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
		),
		$component,
		array("HIDE_ICONS" => "Y")
	);?>
<?}

//COLLECTIONS_IDS//
if($arParams["SHOW_COLLECTIONS"] != "N") {
	$arCollectFilter = array("ACTIVE" => "Y", "IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => $arCurSection["ID"], "INCLUDE_SUBSECTIONS" => "Y", "SECTION_GLOBAL_ACTIVE" => "Y", "!PROPERTY_COLLECTION" => false);

	if(!empty($GLOBALS[$arParams["FILTER_NAME"]]))
		$arCollectFilter = array_merge($arCollectFilter, $GLOBALS[$arParams["FILTER_NAME"]]);

	$obCache = new CPHPCache();
	if($obCache->InitCache($arParams["CACHE_TIME"], serialize($arCollectFilter), "/iblock/catalog")) {
		$collectionsIds = $obCache->GetVars();
	} elseif($obCache->StartDataCache()) {
		if(Bitrix\Main\Loader::includeModule("iblock")) {	
			$rsElements = CIBlockElement::GetList(array(), $arCollectFilter, false, false, array("ID", "IBLOCK_ID"));	
			if(defined("BX_COMP_MANAGED_CACHE")) {
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");
				$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
				while($obElement = $rsElements->GetNextElement()) {
					$arElement = $obElement->GetFields();
					$arElement["PROPERTIES"] = $obElement->GetProperties();
					
					if(!isset($collectionsIds) || !in_array($arElement["PROPERTIES"]["COLLECTION"]["VALUE"], $collectionsIds))
						$collectionsIds[] = $arElement["PROPERTIES"]["COLLECTION"]["VALUE"];
				}
				unset($arElement, $obElement, $rsElements);
				$CACHE_MANAGER->EndTagCache();
			}
		}
		$obCache->EndDataCache($collectionsIds);
	}
	unset($arCollectFilter);
}?>

<div class="catalog-section-container">
	<?//SECTION_LIST//			
	$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "catalog",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
			"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
			"TOP_DEPTH" => "1",
			"SECTION_FIELDS" => array(),
			"SECTION_USER_FIELDS" => array(
				"UF_ICON",
				"UF_NAME_ADDITIONAL"
			),
			"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
			"ADD_SECTIONS_CHAIN" => "N"
		),
		$component,
		array("HIDE_ICONS" => "Y")
	);
	
	//SECTION_PREVIEW//
	if(!empty($arCurSection["UF_PREVIEW"])) {
		if(!$_REQUEST["PAGEN_1"] || $_REQUEST["PAGEN_1"] <= 1) {?>
			<div class="catalog-section-prev"><?=$arCurSection["UF_PREVIEW"];?></div>
		<?}
	}

	//SECTION_VIEW//
	if(!empty($collectionsIds)) {
		$arAvailableView = array("items", "collections");
		
		$catalogViewField = $APPLICATION->get_cookie("ELEMENT_VIEW") ? $APPLICATION->get_cookie("ELEMENT_VIEW") : "items";
		$getView = $request->get("view");
		
		if(!empty($getView) && in_array($getView, $arAvailableView)) {
			$APPLICATION->set_cookie("ELEMENT_VIEW", $getView, false, "/", SITE_SERVER_NAME);
			$catalogView = $getView;
		} elseif(!empty($catalogViewField) && in_array($catalogViewField, $arAvailableView)) {
			$catalogView = $catalogViewField;
		}
	}

	//SECTION_SORT//
	if($isSort && ((!empty($collectionsIds) && $catalogView != "collections") || (!isset($collectionsIds) || empty($collectionsIds)))) {
		$arAvailableSort = array(
			"default" => array(					
				"FIELD" => (intval($GLOBALS['SECTIONS_COUNT']) > 0) ? $arParams["ELEMENT_SORT_FIELD"] : "SORT",
				"ORDER" => (intval($GLOBALS['SECTIONS_COUNT']) > 0) ? $arParams["ELEMENT_SORT_ORDER"] : "ASC",
				"FIELD2" => (intval($GLOBALS['SECTIONS_COUNT']) > 0) ? $arParams["ELEMENT_SORT_FIELD"] : "PROPERTY_MINIMUM_PRICE_1",
				"ORDER2" => (intval($GLOBALS['SECTIONS_COUNT']) > 0) ? $arParams["ELEMENT_SORT_ORDER"] : "ASC",
				"VALUE" => Loc::getMessage("CATALOG_SORT_DEFAULT")
			),
			"cheap" => array(					
				"FIELD" => "PROPERTY_MINIMUM_PRICE_1",
				"ORDER" => "ASC",
				"FIELD2" => "PROPERTY_MINIMUM_PRICE_1",
				"ORDER2" => "ASC",
				"VALUE" => Loc::getMessage("CATALOG_SORT_CHEAP")
			),
			"expensive" => array(
				"FIELD" => "PROPERTY_MAXIMUM_PRICE_1",
				"ORDER" => "DESC",
				"FIELD2" => "PROPERTY_MAXIMUM_PRICE_1",
				"ORDER2" => "DESC",
				"VALUE" => Loc::getMessage("CATALOG_SORT_EXPENSIVE")
			)
		);

		$catalogSortField = $APPLICATION->get_cookie("ELEMENT_SORT") ? $APPLICATION->get_cookie("ELEMENT_SORT") : "default";
		$getSort = $request->get("sort");

		if(!empty($getSort) && !empty($arAvailableSort[$getSort])) {
			$APPLICATION->set_cookie("ELEMENT_SORT", $getSort, false, "/", SITE_SERVER_NAME);
			$arParams["ELEMENT_SORT_FIELD"] = $arAvailableSort[$getSort]["FIELD"];
			$arParams["ELEMENT_SORT_ORDER"] = $arAvailableSort[$getSort]["ORDER"];
			$arParams["ELEMENT_SORT_FIELD2"] = $arAvailableSort[$getSort]["FIELD2"];
			$arParams["ELEMENT_SORT_ORDER2"] = $arAvailableSort[$getSort]["ORDER2"];
			$arAvailableSort[$getSort]["CHECKED"] = "Y";
		} elseif(!empty($catalogSortField) && !empty($arAvailableSort[$catalogSortField])) {
			$arParams["ELEMENT_SORT_FIELD"] = $arAvailableSort[$catalogSortField]["FIELD"];
			$arParams["ELEMENT_SORT_ORDER"] = $arAvailableSort[$catalogSortField]["ORDER"];
			$arParams["ELEMENT_SORT_FIELD2"] = $arAvailableSort[$catalogSortField]["FIELD2"];
			$arParams["ELEMENT_SORT_ORDER2"] = $arAvailableSort[$catalogSortField]["ORDER2"];
			$arAvailableSort[$catalogSortField]["CHECKED"] = "Y";
		}
	}
	
	if($GLOBALS['USER']->IsAdmin())
	{
		echo "<div style='background: #fee; outline: 1px solid red; padding: 10px;'>";
		echo "<div>Если есть подразделы, то сортируем по индексу сортировки по возрастанию, затем по минимальной цене по возрастанию. Если подразделов нет, то сортируем по счетчику просмотров детальных страниц.</div>";
		echo "<div>Кол-во подразделов: ";print_r($GLOBALS['SECTIONS_COUNT']);echo "</div>";
		echo "<div>Выбранная сортировка: ";print_r(!empty($getSort) ? $getSort : $catalogSortField);echo "</div>";
		echo "<div>Поле, по которому фактически сортируем: ";print_r($arParams["ELEMENT_SORT_FIELD"]);echo "</div>";
		echo "<div>Порядок, по которому фактически сортируем: ";print_r($arParams["ELEMENT_SORT_ORDER"]);echo "</div>";
		echo "<div>Поле, по которому вторично фактически сортируем: ";print_r($arParams["ELEMENT_SORT_FIELD2"]);echo "</div>";
		echo "<div>Порядок, по которому вторично фактически сортируем: ";print_r($arParams["ELEMENT_SORT_ORDER2"]);echo "</div>";
		echo "</div>";
	}

	if(!empty($collectionsIds) || $isFilter || !empty($arAvailableSort)) {
		ob_start();?>
		<div class="catalog-section-panel-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="catalog-section-panel">
							<div class="catalog-section-panel-block">
								<?if(!empty($collectionsIds) || $isFilter) {?>
									<div class="catalog-section-toggle-filter">
										<?if(!empty($collectionsIds)) {?>
											<div class="catalog-section-toggle hidden-xs hidden-sm">
												<input type="checkbox" id="catalog-section-toggle" name="toggle"<?=($catalogView == "collections" ? " checked='checked'" : "")?> />
												<span><?=GetMessage("CATALOG_COLLECTIONS")?></span>
												<label for="catalog-section-toggle"></label>
												<span><?=GetMessage("CATALOG_ELEMENTS")?></span>
											</div>
										<?}
										if($isFilter) {?>
											<div class="catalog-section-filter-container">
												<div class="catalog-section-filter" data-entity="showFilter" data-id="<?=$arCurSection['ID']?>">
													<div class="catalog-section-filter-block"><i class="icon-sliders"></i><span><?=GetMessage("CATALOG_FILTER")?></span></div>
												</div>
											</div>
										<?}?>
									</div>
								<?}
								if(!empty($arAvailableSort)) {?>
									<div class="catalog-section-sort-container">
										<div class="catalog-section-sort" data-role="catalogSectionSort">
											<div class="catalog-section-sort-block">
												<div class="catalog-section-sort-text"><?=Loc::getMessage("CATALOG_SORT");?></div>
												<div class="catalog-section-sort-arrow"><i class="icon-arrow-down"></i></div>
											</div>
											<div class="catalog-section-sort-popup" data-role="dropdownContent" style="display: none;">
												<ul>
													<?foreach($arAvailableSort as $val => $ar) {?>
														<li<?=($ar["CHECKED"] ? " class='active'" : "")?>>
															<a href="<?=$APPLICATION->GetCurPageParam('sort='.$val, array('sort'))?>"><?=$ar["VALUE"]?></a>
														</li>
													<?}?>
												</ul>
											</div>
										</div>							
									</div>
								<?}?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?$APPLICATION->AddViewContent("CATALOG_SECTION_PANEL", ob_get_contents());
		ob_end_clean();
	}
	
	if(!empty($collectionsIds) && $catalogView == "collections") {
		//COLLECTIONS//?>
		<div class="catalog-section-collections">
			<?$GLOBALS["arCatalogCollectFilter"] = array("ID" => $collectionsIds);?>
			<?$APPLICATION->IncludeComponent("bitrix:news.list", "collections",
				array(
					"IBLOCK_TYPE" => $arParams["COLLECTIONS_IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["COLLECTIONS_IBLOCK_ID"],
					"NEWS_COUNT" => $arParams["COLLECTIONS_NEWS_COUNT"],
					"SORT_BY1" => $arParams["COLLECTIONS_SORT_BY1"],
					"SORT_ORDER1" => $arParams["COLLECTIONS_SORT_ORDER1"],
					"SORT_BY2" => $arParams["COLLECTIONS_SORT_BY2"],
					"SORT_ORDER2" => $arParams["COLLECTIONS_SORT_ORDER2"],
					"FILTER_NAME" => "arCatalogCollectFilter",
					"FIELD_CODE" => array(),
					"PROPERTY_CODE" => $arParams["COLLECTIONS_PROPERTY_CODE"],
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "",
					"AJAX_OPTION_SHADOW" => "",
					"AJAX_OPTION_JUMP" => "",
					"AJAX_OPTION_STYLE" => "",
					"AJAX_OPTION_HISTORY" => "",
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_FILTER" => $arParams["CACHE_FILTER"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "",
					"DISPLAY_PANEL" => "",
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "",
					"HIDE_LINK_WHEN_NO_DETAIL" => "",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"DISPLAY_NAME" => "",
					"DISPLAY_DATE" => "",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_SHOW_ALWAYS" => "",
					"PAGER_TEMPLATE" => "arrows",
					"PAGER_DESC_NUMBERING" => "",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "",
					"PAGER_SHOW_ALL" => "",
					"AJAX_OPTION_ADDITIONAL" => "",
					"SHOW_MIN_PRICE" => $arParams["COLLECTIONS_SHOW_MIN_PRICE"],
					"CATALOG_IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"CATALOG_IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"CATALOG_PRICE_CODE" => $arParams["PRICE_CODE"],		
					"CATALOG_PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
					"CATALOG_CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
					"CURRENCY_ID" => $arParams["CURRENCY_ID"]
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);?>
		</div>

		<?//SECTION_DESCRIPTION//
		if(!empty($arCurSection["DESCRIPTION"])) {
			if(!$_REQUEST["PAGEN_1"] || $_REQUEST["PAGEN_1"] <= 1) {?>
				<div class="catalog-section-desc"><?=$arCurSection["DESCRIPTION"];?></div>
			<?}
		}
		
		//SECTION_TITLE//
		if($arParams["SET_TITLE"] && !empty($arCurSection["TITLE"]))
			$APPLICATION->SetTitle($arCurSection["TITLE"]);
		
		//SECTION_BROWSER_TITLE//
		if($arParams["SET_BROWSER_TITLE"] != "N" && !empty($arCurSection["IPROPERTY_VALUES"]["SECTION_META_TITLE"]))
			$APPLICATION->SetPageProperty("title", $arCurSection["IPROPERTY_VALUES"]["SECTION_META_TITLE"]);
		
		//SECTION_META_KEYWORDS//
		if($arParams["SET_META_KEYWORDS"] != "N" && !empty($arCurSection["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"]))
			$APPLICATION->SetPageProperty("keywords", $arCurSection["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"]);
		
		//SECTION_META_DESCRIPTION//
		if($arParams["SET_META_DESCRIPTION"] != "N" && !empty($arCurSection["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"]))
			$APPLICATION->SetPageProperty("description", $arCurSection["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"]);
	} else {	
		//SECTION//
		if(isset($arParams["USE_COMMON_SETTINGS_BASKET_POPUP"]) && $arParams["USE_COMMON_SETTINGS_BASKET_POPUP"] == "Y") {
			$basketAction = isset($arParams["COMMON_ADD_TO_BASKET_ACTION"]) ? $arParams["COMMON_ADD_TO_BASKET_ACTION"] : "";
		} else {
			$basketAction = isset($arParams["SECTION_ADD_TO_BASKET_ACTION"]) ? $arParams["SECTION_ADD_TO_BASKET_ACTION"] : "";
		}

        Zverushki\Seofilter\Custom::setParams($arParams);

		$intSectionID = $APPLICATION->IncludeComponent("bitrix:catalog.section", "",
			array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
				"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
				"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
				"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
				"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],						
				"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
				"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
				"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
				"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
				"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],					
				"BASKET_URL" => $arParams["BASKET_URL"],
				"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
				"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
				"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
				"FILTER_NAME" => $arParams["FILTER_NAME"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_FILTER" => $arParams["CACHE_FILTER"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"SET_TITLE" => $arParams["SET_TITLE"],
				"MESSAGE_404" => $arParams["~MESSAGE_404"],
				"SET_STATUS_404" => $arParams["SET_STATUS_404"],
				"SHOW_404" => $arParams["SHOW_404"],
				"FILE_404" => $arParams["FILE_404"],						
				"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
				"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
				"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
				"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
				"USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
				"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ""),
				"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ""),
				"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

				"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
				"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
				"PAGER_TITLE" => $arParams["PAGER_TITLE"],
				"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
				"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
				"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
				"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
				"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
				"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
				"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
				"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
				"LAZY_LOAD" => $arParams["LAZY_LOAD"],
				"MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
				"LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

				"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
				"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
				"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
				"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
				"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
				"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
				"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
				"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

				"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
				"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
				"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
				"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],					
				"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
				"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
				"CURRENCY_ID" => $arParams["CURRENCY_ID"],
				"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
				"HIDE_NOT_AVAILABLE_OFFERS" => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
				
				"PRODUCT_DISPLAY_MODE" => $arParams["PRODUCT_DISPLAY_MODE"],						
				"PRODUCT_ROW_VARIANTS" => $arParams["LIST_PRODUCT_ROW_VARIANTS"],
				
				"OFFER_TREE_PROPS" => $arParams["OFFER_TREE_PROPS"],
				"PRODUCT_SUBSCRIPTION" => $arParams["PRODUCT_SUBSCRIPTION"],
				"SHOW_DISCOUNT_PERCENT" => $arParams["SHOW_DISCOUNT_PERCENT"],						
				"SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"],
				"SHOW_MAX_QUANTITY" => $arParams["SHOW_MAX_QUANTITY"],
				"MESS_SHOW_MAX_QUANTITY" => (isset($arParams["~MESS_SHOW_MAX_QUANTITY"]) ? $arParams["~MESS_SHOW_MAX_QUANTITY"] : ""),
				"RELATIVE_QUANTITY_FACTOR" => (isset($arParams["RELATIVE_QUANTITY_FACTOR"]) ? $arParams["RELATIVE_QUANTITY_FACTOR"] : ""),
				"MESS_RELATIVE_QUANTITY_MANY" => (isset($arParams["~MESS_RELATIVE_QUANTITY_MANY"]) ? $arParams["~MESS_RELATIVE_QUANTITY_MANY"] : ""),
				"MESS_RELATIVE_QUANTITY_FEW" => (isset($arParams["~MESS_RELATIVE_QUANTITY_FEW"]) ? $arParams["~MESS_RELATIVE_QUANTITY_FEW"] : ""),
				"MESS_BTN_BUY" => (isset($arParams["~MESS_BTN_BUY"]) ? $arParams["~MESS_BTN_BUY"] : ""),
				"MESS_BTN_ADD_TO_BASKET" => (isset($arParams["~MESS_BTN_ADD_TO_BASKET"]) ? $arParams["~MESS_BTN_ADD_TO_BASKET"] : ""),
				"MESS_BTN_SUBSCRIBE" => (isset($arParams["~MESS_BTN_SUBSCRIBE"]) ? $arParams["~MESS_BTN_SUBSCRIBE"] : ""),
				"MESS_BTN_DETAIL" => (isset($arParams["~MESS_BTN_DETAIL"]) ? $arParams["~MESS_BTN_DETAIL"] : ""),
				"MESS_NOT_AVAILABLE" => (isset($arParams["~MESS_NOT_AVAILABLE"]) ? $arParams["~MESS_NOT_AVAILABLE"] : ""),
				"MESS_BTN_COMPARE" => (isset($arParams["~MESS_BTN_COMPARE"]) ? $arParams["~MESS_BTN_COMPARE"] : ""),
					
				"USE_ENHANCED_ECOMMERCE" => (isset($arParams["USE_ENHANCED_ECOMMERCE"]) ? $arParams["USE_ENHANCED_ECOMMERCE"] : ""),
				"DATA_LAYER_NAME" => (isset($arParams["DATA_LAYER_NAME"]) ? $arParams["DATA_LAYER_NAME"] : ""),
				"BRAND_PROPERTY" => (isset($arParams["BRAND_PROPERTY"]) ? $arParams["BRAND_PROPERTY"] : ""),
				
				"ADD_SECTIONS_CHAIN" => "N",
				"ADD_TO_BASKET_ACTION" => $basketAction,
				"COMPARE_PATH" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
				"COMPARE_NAME" => $arParams["COMPARE_NAME"],
				"BACKGROUND_IMAGE" => (isset($arParams["SECTION_BACKGROUND_IMAGE"]) ? $arParams["SECTION_BACKGROUND_IMAGE"] : ""),
				"COMPATIBLE_MODE" => (isset($arParams["COMPATIBLE_MODE"]) ? $arParams["COMPATIBLE_MODE"] : ""),
				"DISABLE_INIT_JS_IN_COMPONENT" => (isset($arParams["DISABLE_INIT_JS_IN_COMPONENT"]) ? $arParams["DISABLE_INIT_JS_IN_COMPONENT"] : ""),

				"SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
				"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
				"INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],

				"USE_REVIEW" => $arParams["USE_REVIEW"],
				"REVIEWS_IBLOCK_ID" => $arParams["REVIEWS_IBLOCK_ID"]
			),
			$component
		);?>
		<?$GLOBALS["CATALOG_CURRENT_SECTION_ID"] = $intSectionID;
	}?>
</div>

<?//GIFTS//
if($arParams["USE_GIFTS_SECTION"] == "Y" && ModuleManager::isModuleInstalled("sale")) {?>
	<div class="catalog-section-gifts" data-entity="parent-container" style="display: none;">
		<?if($arParams["GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE"] !== "Y") {?>
			<div class="h2" data-entity="header" data-showed="false" style="display: none; opacity: 0;"><?=($arParams["GIFTS_SECTION_LIST_BLOCK_TITLE"] ?: Loc::getMessage("CATALOG_GIFTS"))?></div>
		<?}
		CBitrixComponent::includeComponentClass("bitrix:sale.products.gift.section");?>
		<?$APPLICATION->IncludeComponent("bitrix:sale.products.gift.section", ".default",
			array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
				"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
				"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action")."_spgs",
				"PRODUCT_ROW_VARIANTS" => Bitrix\Main\Web\Json::encode(SaleProductsGiftSectionComponent::predictRowVariants(4, $arParams["GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT"])),
				"PAGE_ELEMENT_COUNT" => $arParams["GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT"],
				"DEFERRED_PRODUCT_ROW_VARIANTS" => "",
				"DEFERRED_PAGE_ELEMENT_COUNT" => 0,
				"PRODUCT_DISPLAY_MODE" => $arParams["PRODUCT_DISPLAY_MODE"],
				"TEXT_LABEL_GIFT" => $arParams["GIFTS_SECTION_LIST_TEXT_LABEL_GIFT"],
				"ADD_TO_BASKET_ACTION" => $basketAction,
				"MESS_BTN_BUY" => $arParams["~GIFTS_MESS_BTN_BUY"],
				"MESS_BTN_ADD_TO_BASKET" => $arParams["~GIFTS_MESS_BTN_BUY"],
				"MESS_BTN_DETAIL" => $arParams["~MESS_BTN_DETAIL"],
				"MESS_BTN_SUBSCRIBE" => $arParams["~MESS_BTN_SUBSCRIBE"],
				"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
				"ADD_PICT_PROP" => $arParams["ADD_PICT_PROP"],
				"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
				"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
				"OFFER_TREE_PROPS" => $arParams["OFFER_TREE_PROPS"],
				"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
				"OFFER_ADD_PICT_PROP" => $arParams["OFFER_ADD_PICT_PROP"],
				"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
				"HIDE_NOT_AVAILABLE_OFFERS" => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
				"PRODUCT_SUBSCRIPTION" => $arParams["PRODUCT_SUBSCRIPTION"],				
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
				"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
				"CURRENCY_ID" => $arParams["CURRENCY_ID"],
				"BASKET_URL" => $arParams["BASKET_URL"],
				"ADD_PROPERTIES_TO_BASKET" => $arParams["ADD_PROPERTIES_TO_BASKET"],
				"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
				"PARTIAL_PRODUCT_PROPERTIES" => $arParams["PARTIAL_PRODUCT_PROPERTIES"],
				"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
				"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"USE_ENHANCED_ECOMMERCE" => (isset($arParams["USE_ENHANCED_ECOMMERCE"]) ? $arParams["USE_ENHANCED_ECOMMERCE"] : ""),
				"DATA_LAYER_NAME" => (isset($arParams["DATA_LAYER_NAME"]) ? $arParams["DATA_LAYER_NAME"] : ""),
				"BRAND_PROPERTY" => (isset($arParams["BRAND_PROPERTY"]) ? $arParams["BRAND_PROPERTY"] : ""),
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);?>
		<?unset($basketAction);?>
	</div>
<?}?>