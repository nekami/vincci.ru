BX.checkSectionPanel = function() {
	BX.sectionPanelWrapper = document.body.querySelector('.catalog-section-panel-wrapper');
	BX.sectionPanel = !!BX.sectionPanelWrapper && BX.sectionPanelWrapper.querySelector('.catalog-section-panel');
	
	if(!!BX.sectionPanel) {				
		BX.sectionPanelFixed = false;
		BX.sectionPanelScrolled = false;
		BX.lastScrollTop = 0;
		
		BX.bind(window, 'scroll', BX.proxy(BX.checkSectionPanelScroll, BX));
		BX.bind(window, 'resize', BX.proxy(BX.checkSectionPanelResize, BX));
	}
}

BX.checkSectionPanelScroll = function() {
	var topPanel = document.body.querySelector('.top-panel'),
		topPanelHeight = 0,				
		topPanelThead = !!topPanel && topPanel.querySelector('.top-panel__thead'),
		topPanelTfoot = !!topPanel && topPanel.querySelector('.top-panel__tfoot'),				
		sectionPanelWrapperTop = BX.pos(BX.sectionPanelWrapper).top,
		sectionPanel = BX.sectionPanel,				
		sectionPanelHeight = sectionPanel.offsetHeight,
		scrollTop = BX.GetWindowScrollPos().scrollTop;
	
	if(window.innerWidth < 992) {
		if(!!topPanelThead && !!BX.hasClass(topPanelThead, 'fixed')) {
			topPanelHeight = topPanelThead.offsetHeight;
			if(!!topPanelTfoot && !!BX.hasClass(topPanelTfoot, 'visible'))
				topPanelHeight += topPanelTfoot.offsetHeight;
		}
		
		if(scrollTop + topPanelHeight >= sectionPanelWrapperTop) {
			if(!BX.sectionPanelFixed) {
				BX.sectionPanelFixed = true;
				BX.style(BX.sectionPanelWrapper, 'height', sectionPanelHeight + 'px');
				BX.style(sectionPanel, 'top', topPanelHeight + 'px');							
				BX.addClass(sectionPanel, 'fixed');
			} else {
				if(!BX.sectionPanelScrolled && topPanelHeight > 0 && scrollTop < BX.lastScrollTop) {
					BX.sectionPanelScrolled = true;
					var sectionPanelScrolled = BX.sectionPanelScrolled;
					new BX.easing({
						duration: 300,
						start: {top: Math.abs(parseInt(BX.style(sectionPanel, 'top'), 10))},
						finish: {top: topPanelHeight},
						transition: BX.easing.transitions.linear,
						step: function(state) {
							if(!!sectionPanelScrolled)
								BX.style(sectionPanel, 'top', state.top + 'px');								
						}
					}).animate();
				} else if(!!BX.sectionPanelScrolled && topPanelHeight > 0 && scrollTop > BX.lastScrollTop) {								
					BX.sectionPanelScrolled = false;
					new BX.easing({
						duration: 300,
						start: {top: Math.abs(parseInt(BX.style(sectionPanel, 'top'), 10))},
						finish: {top: topPanelHeight},
						transition: BX.easing.transitions.linear,
						step: function(state) {
							BX.style(sectionPanel, 'top', state.top + 'px');								
						}
					}).animate();
				}
			}
		} else if(!!BX.sectionPanelFixed && (scrollTop + topPanelHeight < sectionPanelWrapperTop)) {
			BX.sectionPanelFixed = false;
			BX.sectionPanelScrolled = false;
			BX.sectionPanelWrapper.removeAttribute('style');
			sectionPanel.removeAttribute('style');
			BX.removeClass(sectionPanel, 'fixed');
		}
	} else {
		if(!!topPanel && !!BX.hasClass(topPanel, 'fixed'))
			topPanelHeight = topPanel.offsetHeight;
		
		if(!BX.sectionPanelFixed && (scrollTop + topPanelHeight >= sectionPanelWrapperTop)) {
			BX.sectionPanelFixed = true;
			BX.style(BX.sectionPanelWrapper, 'height', sectionPanelHeight + 'px');
			BX.style(sectionPanel, 'top', topPanelHeight + 'px');							
			BX.addClass(sectionPanel, 'fixed');
		} else if(!!BX.sectionPanelFixed && (scrollTop + topPanelHeight < sectionPanelWrapperTop)) {
			BX.sectionPanelFixed = false;
			BX.sectionPanelWrapper.removeAttribute('style');
			sectionPanel.removeAttribute('style');
			BX.removeClass(sectionPanel, 'fixed');
		}
	}
	BX.lastScrollTop = scrollTop;
}

BX.checkSectionPanelResize = function() {
	if(!!BX.hasClass(BX.sectionPanel, 'fixed')) {
		var topPanel = document.body.querySelector('.top-panel'),
			topPanelHeight = 0,
			topPanelThead = !!topPanel && topPanel.querySelector('.top-panel__thead'),
			topPanelTfoot = !!topPanel && topPanel.querySelector('.top-panel__tfoot');					
		
		if(window.innerWidth < 992) {
			if(!!topPanelThead && !!BX.hasClass(topPanelThead, 'fixed')) {
				topPanelHeight = topPanelThead.offsetHeight;
				if(!!topPanelTfoot && !!BX.hasClass(topPanelTfoot, 'visible'))
					topPanelHeight += topPanelTfoot.offsetHeight;
			}
		} else {
			if(!!topPanel && !!BX.hasClass(topPanel, 'fixed'))
				topPanelHeight = topPanel.offsetHeight;
			BX.sectionPanelScrolled = false;
		}
		
		BX.style(BX.sectionPanel, 'top', topPanelHeight + 'px');
	}
}

BX.changeSectionToogle = function() {
	var url = new URL(window.location.href);
	
	url.searchParams.set('view', BX.toogle.checked ? 'collections' : 'items');
	window.location.href = url.toString();
}

BX.adjustSectionToogle = function() {
	var toogle = BX.toogle.parentNode,
		tooglePanelWrapper = document.body.querySelector('.catalog-section-toggle-wrapper'),
		sectionPanelToogleFilter = document.body.querySelector('.catalog-section-toggle-filter');
	
	if(window.innerWidth < 992) {
		if(!BX.toogleAdjusted) {
			if(!tooglePanelWrapper)
				tooglePanelWrapper = BX.create('DIV', {props: {className: 'catalog-section-toggle-wrapper'}});
			
			if(!!tooglePanelWrapper && !!BX.sectionPanelWrapper) {
				tooglePanelWrapper.appendChild(BX.removeClass(toogle, 'hidden-xs hidden-sm'));
				BX.sectionPanelWrapper.parentNode.insertBefore(tooglePanelWrapper, BX.sectionPanelWrapper);
				BX.toogleAdjusted = true;
			}
		}
	} else {
		if(BX.toogleAdjusted && !!sectionPanelToogleFilter && !!tooglePanelWrapper) {
			BX.prepend(BX.addClass(toogle, 'hidden-xs hidden-sm'), sectionPanelToogleFilter);
			BX.remove(tooglePanelWrapper);
			BX.toogleAdjusted = false;
		}		
	}
}

BX.showSectionSortDropDownPopup = function() {
	if(BX.isNodeHidden(BX.sectionSortPopup)) {
		BX.style(BX.sectionSortPopup, 'display', '');
		BX.addClass(BX.sectionSortContainer, 'active');
	} else {
		BX.style(BX.sectionSortPopup, 'display', 'none');
		BX.removeClass(BX.sectionSortContainer, 'active');
	}
}

BX.hideSectionSortDropDownPopup = function(event) {			
	var target = BX.getEventTarget(event);
	if(!BX.findParent(target, {attr: {'data-role': 'catalogSectionSort'}}, false) && target.getAttribute('data-role') != 'catalogSectionSort') {
		BX.style(BX.sectionSortPopup, 'display', 'none');
		BX.removeClass(BX.sectionSortContainer, 'active');
	}
}
	
BX.ready(function() {
	BX.checkSectionPanel();

	BX.toogle = BX('catalog-section-toggle');
	if(!!BX.toogle) {
		BX.bind(BX.toogle, 'bxchange', BX.proxy(BX.changeSectionToogle, BX));
		
		BX.toogleAdjusted = false;
		BX.adjustSectionToogle();
		BX.bind(window, 'resize', BX.proxy(BX.adjustSectionToogle, BX));
	}
	
	BX.sectionSortContainer = document.body.querySelector('[data-role="catalogSectionSort"]');
	BX.sectionSortPopup = BX.sectionSortContainer && BX.sectionSortContainer.querySelector('[data-role="dropdownContent"]');
	if(BX.sectionSortContainer && BX.sectionSortPopup) {
		BX.bind(BX.sectionSortContainer, 'click', BX.proxy(BX.showSectionSortDropDownPopup, BX));
		BX.bind(document, 'click', BX.proxy(BX.hideSectionSortDropDownPopup, BX));
	}
});