<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\ModuleManager,
    Bitrix\Iblock;

$this->setFrameMode(true);

$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');
$isFilter = ($arParams['USE_FILTER'] == 'Y');
$isSort = true;
$isElement = false;

//CUR_SECTION//
$arFilter = array(
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "ACTIVE" => "Y",
    "GLOBAL_ACTIVE" => "Y",
    "ELEMENT_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
    "CNT_ACTIVE" => "Y"
);
if(0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
    $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
elseif('' != $arResult["VARIABLES"]["SECTION_CODE"])
    $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

$obCache = new CPHPCache();
if($obCache->InitCache($arParams["CACHE_TIME"], serialize($arFilter), "/iblock/catalog")) {
    $arCurSection = $obCache->GetVars();
    if(!$arCurSection) {
        $isElement = true;
        include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/element.php");
    }
} elseif($obCache->StartDataCache()) {
    $arCurSection = array();
    if(Loader::includeModule("iblock")) {
        $dbRes = CIBlockSection::GetList(array(), $arFilter, true, array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "NAME", "PICTURE", "DESCRIPTION", "DEPTH_LEVEL", "UF_BACKGROUND_IMAGE", "UF_BANNER", "UF_BANNER_URL", "UF_PREVIEW"));

        if(defined("BX_COMP_MANAGED_CACHE")) {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache("/iblock/catalog");

            if($arCurSection = $dbRes->Fetch()) {
                $CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);

                //SECTION_FILTER_SORT//
                $arCurSection["IS_FILTER"] = $isFilter;
                $arCurSection["IS_SORT"] = $isSort;
                if($arCurSection["ELEMENT_CNT"] <= 0) {
                    $arCurSection["IS_FILTER"] = false;
                    $arCurSection["IS_SORT"] = false;
                }

                //SECTION_PICTURE//
                if($arCurSection["PICTURE"] > 0)
                    $arCurSection["PICTURE"] = CFile::GetFileArray($arCurSection["PICTURE"]);

                //SECTION_BACKGROUND_IMAGE//
                if($arCurSection["UF_BACKGROUND_IMAGE"] <= 0 && $arCurSection["DEPTH_LEVEL"] > 1) {
                    if($arCurSection["DEPTH_LEVEL"] > 2) {
                        $rsPath = CIBlockSection::GetNavChain(
                            $arCurSection["IBLOCK_ID"],
                            $arCurSection["IBLOCK_SECTION_ID"],
                            array('ID', 'IBLOCK_ID')
                        );
                        while($path = $rsPath->GetNext()) {
                            $pathIds[] = $path["ID"];
                        }
                        unset($path, $rsPath);
                    } else {
                        $pathIds = $arCurSection["IBLOCK_SECTION_ID"];
                    }
                    if(!empty($pathIds)) {
                        $rsSections = CIBlockSection::GetList(
                            array("DEPTH_LEVEL" => "DESC"),
                            array("IBLOCK_ID" => $arCurSection["IBLOCK_ID"], "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y", "ID" => $pathIds),
                            false,
                            array("ID", "IBLOCK_ID", "NAME", "UF_BACKGROUND_IMAGE")
                        );
                        while($arSection = $rsSections->GetNext()) {
                            if($arCurSection["UF_BACKGROUND_IMAGE"] <= 0 && $arSection["UF_BACKGROUND_IMAGE"] > 0) {
                                $arCurSection["UF_BACKGROUND_IMAGE"] = CFile::GetFileArray($arSection["UF_BACKGROUND_IMAGE"]);
                                break;
                            }
                        }
                        unset($arSection, $rsSections);
                    }
                    unset($pathIds);
                }

                //SECTION_BANNER//
                if($arCurSection["UF_BANNER"] > 0)
                    $arCurSection["UF_BANNER"] = CFile::GetFileArray($arCurSection["UF_BANNER"]);

                //SECTION_TITLE//
                $ipropValues = new Iblock\InheritedProperty\SectionValues($arCurSection["IBLOCK_ID"], $arCurSection["ID"]);
                $arCurSection["IPROPERTY_VALUES"] = $ipropValues->getValues();
                $arCurSection['TITLE'] = !empty($arCurSection['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'])
                    ? $arCurSection['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']
                    : $arCurSection['NAME'];
                unset($ipropValues);

                //SECTION_BREADCRUMBS//
                if($arParams["ADD_SECTIONS_CHAIN"] == "Y") {
                    $arCurSection['PATH'] = array();
                    $rsPath = CIBlockSection::GetNavChain(
                        $arCurSection["IBLOCK_ID"],
                        $arCurSection["ID"],
                        array('ID', 'IBLOCK_ID', 'NAME', 'SECTION_PAGE_URL')
                    );
                    while($path = $rsPath->GetNext()) {
                        $ipropValues = new Iblock\InheritedProperty\SectionValues($arCurSection['IBLOCK_ID'], $path['ID']);
                        $path['IPROPERTY_VALUES'] = $ipropValues->getValues();
                        $arCurSection['PATH'][$path['ID']] = $path;
                    }
                    unset($ipropValues, $path, $rsPath);
                    if(!empty($arCurSection['PATH'])) {
                        $rsSections = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arCurSection["IBLOCK_ID"], "ID" => array_keys($arCurSection['PATH'])), false, array("ID", "IBLOCK_ID", "UF_BREADCRUMB_TITLE"));
                        while($arSection = $rsSections->GetNext()) {
                            $arCurSection['PATH'][$arSection['ID']]['BREADCRUMB_TITLE'] = $arSection['UF_BREADCRUMB_TITLE'];
                        }
                        unset($arSection, $rsSections);
                    }
                }
            }
            else {
                $isElement = true;
                include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/element.php");
            }

            $CACHE_MANAGER->EndTagCache();
        } else {
            if(!$arCurSection = $dbRes->Fetch())
                $arCurSection = array();
        }
    }
    $obCache->EndDataCache($arCurSection);
}

if(!$isElement) {
    if(!isset($arCurSection))
        $arCurSection = array();

    //SECTION_FILTER//
    if(!$arCurSection["IS_FILTER"])
        $isFilter = false;

    //SECTION_SORT//
    if(!$arCurSection["IS_SORT"])
        $isSort = false;

    //SECTION_BANNER//
    if(is_array($arCurSection["UF_BANNER"])) {
        ob_start();?>
        <div class="catalog-section-pic">
            <a href="<?=!empty($arCurSection["UF_BANNER_URL"]) ? $arCurSection["UF_BANNER_URL"] : 'javascript:void(0)'?>">
                <img src="<?=$arCurSection['UF_BANNER']['SRC']?>" width="<?=$arCurSection['UF_BANNER']['WIDTH']?>" height="<?=$arCurSection['UF_BANNER']['HEIGHT']?>" alt="" title="" />
            </a>
        </div>
        <?$APPLICATION->AddViewContent("UF_BANNER", ob_get_contents());
        ob_end_clean();
    }

    include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/section_vertical.php");

    //SECTION_BACKGROUND_IMAGE//
    if(is_array($arCurSection["UF_BACKGROUND_IMAGE"])) {
        $APPLICATION->SetPageProperty(
            "backgroundImage",
            'style="background-image:url(\''.CHTTP::urnEncode($arCurSection['UF_BACKGROUND_IMAGE']['SRC'], 'UTF-8').'\')"'
        );
    }

    //SECTION_BREADCRUMBS//
    if(!empty($arCurSection['PATH'])) {
        foreach($arCurSection['PATH'] as $path) {
            if(!empty($path["BREADCRUMB_TITLE"]))
                $APPLICATION->AddChainItem($path["BREADCRUMB_TITLE"], $path['~SECTION_PAGE_URL']);
            elseif(!empty($path['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']))
                $APPLICATION->AddChainItem($path['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'], $path['~SECTION_PAGE_URL']);
            else
                $APPLICATION->AddChainItem($path['NAME'], $path['~SECTION_PAGE_URL']);
        }
    }

    //SECTION_PAGE_TITLE//
    if(!empty($_REQUEST["PAGEN_1"]) && $_REQUEST["PAGEN_1"] > 1) {
        $APPLICATION->SetPageProperty("title", $arCurSection['TITLE']." | ".Loc::getMessage("CATALOG_PAGE")." ".$_REQUEST["PAGEN_1"]);
        $APPLICATION->SetPageProperty("keywords", "");
        $APPLICATION->SetPageProperty("description", "");
    }

    //SECTION_META_PROPERTY//
    $APPLICATION->AddHeadString("<meta property='og:title' content='".$arCurSection['TITLE']."' />", true);
    if(!empty($arCurSection["UF_PREVIEW"])) {
        $APPLICATION->AddHeadString("<meta property='og:description' content='".strip_tags($arCurSection["UF_PREVIEW"])."' />", true);
    }
    $ogScheme = CMain::IsHTTPS() ? "https" : "http";
    $APPLICATION->AddHeadString("<meta property='og:url' content='".$ogScheme."://".SITE_SERVER_NAME.$APPLICATION->GetCurPage()."' />", true);
    if(is_array($arCurSection["PICTURE"])) {
        $APPLICATION->AddHeadString("<meta property='og:image' content='".$ogScheme."://".SITE_SERVER_NAME.$arCurSection["PICTURE"]["SRC"]."' />", true);
        $APPLICATION->AddHeadString("<meta property='og:image:width' content='".$arCurSection["PICTURE"]["WIDTH"]."' />", true);
        $APPLICATION->AddHeadString("<meta property='og:image:height' content='".$arCurSection["PICTURE"]["HEIGHT"]."' />", true);
        $APPLICATION->AddHeadString("<link rel='image_src' href='".$ogScheme."://".SITE_SERVER_NAME.$arCurSection["PICTURE"]["SRC"]."' />", true);
    } elseif(is_array($arCurSection["UF_BANNER"])) {
        $APPLICATION->AddHeadString("<meta property='og:image' content='".$ogScheme."://".SITE_SERVER_NAME.$arCurSection["UF_BANNER"]["SRC"]."' />", true);
        $APPLICATION->AddHeadString("<meta property='og:image:width' content='".$arCurSection["UF_BANNER"]["WIDTH"]."' />", true);
        $APPLICATION->AddHeadString("<meta property='og:image:height' content='".$arCurSection["UF_BANNER"]["HEIGHT"]."' />", true);
        $APPLICATION->AddHeadString("<link rel='image_src' href='".$ogScheme."://".SITE_SERVER_NAME.$arCurSection["UF_BANNER"]["SRC"]."' />", true);
    }
}