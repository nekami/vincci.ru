<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;

if(empty($arResult))
	return;?>

<div class="hidden-print personal-menu-wrapper">
	<div class="personal-menu">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="personal-menu-cols">
						<?foreach($arResult as $itemIdex => $arItem) {
							if($arItem["DEPTH_LEVEL"] == "1") {?>							
								<a class="personal-menu-col<?=($arItem['SELECTED'] ? ' selected' : '')?>" href="<?=$arItem['LINK']?>" title="<?=$arItem['TEXT']?>">
									<div class="personal-menu-col-block">
										<?if(!empty($arItem['PARAMS']['ICON'])) {?>
											<div class="personal-menu-col-icon"><i class="<?=$arItem['PARAMS']['ICON']?>"></i></div>
										<?}?>
										<div class="hidden-xs hidden-sm personal-menu-col-name"><span><?=htmlspecialcharsbx($arItem["TEXT"])?></span></div>
									</div>
									<?if(isset($arItem["COUNT"])) {?>
										<div class="personal-menu-col-count"><?=$arItem["COUNT"]?></div>
									<?}?>
								</a>
							<?}
						}?>
						<a class="personal-menu-col" href="<?=$APPLICATION->GetCurPageParam('logout=yes', array('logout'))?>" title="<?=Loc::getMessage('PERSONAL_MENU_EXIT')?>">
							<div class="personal-menu-col-block">
								<div class="personal-menu-col-icon"><i class="icon-logout"></i></div>
								<div class="hidden-xs hidden-sm personal-menu-col-name"><span><?=Loc::getMessage("PERSONAL_MENU_EXIT")?></span></div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>