<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
	Bitrix\Sale,
	Bitrix\Catalog;

if(!Loader::includeModule("sale") || !Loader::includeModule("catalog"))
	return;

global $USER;

foreach($arResult as $itemIdex => $arItem) {
	if($arItem["PARAMS"]["CODE"] == "ORDERS") {
		//ORDERS_COUNT//
		$arResult[$itemIdex]["COUNT"] = 0;
		$arFilter = array(
			"select" => array("ID", "STATUS_ID"),
			"filter" => array(
				"USER_ID" => $USER->GetID(),
				"LID" => SITE_ID,
				"CANCELED" => "N"
			)
		);
		$dbOrders = Sale\Order::getList($arFilter);
		while($arOrder = $dbOrders->fetch()) {
			if($arOrder["STATUS_ID"] != "F")
				$arResult[$itemIdex]["COUNT"]++;
		}
		unset($arOrder, $dbOrders, $arFilter);
	} elseif($arItem["PARAMS"]["CODE"] == "BASKET") {
		//BASKET_ITEMS_COUNT//
		$arResult[$itemIdex]["COUNT"] = (int)Sale\BasketComponentHelper::getFUserBasketQuantity(Sale\Fuser::getId(true), SITE_ID);
	} elseif($arItem["PARAMS"]["CODE"] == "SUBSCRIBE") {
		//SUBSCRIBE_ITEMS_COUNT//
		$arFilter = array(
			"USER_ID" => $USER->GetID(),
			"=SITE_ID" => SITE_ID,
			array(
				"LOGIC" => "OR",
				array("=DATE_TO" => false),
				array(">DATE_TO" => date($DB->dateFormatToPHP(\CLang::getDateFormat("FULL")), time()))
			)
		);
		$countQuery = Catalog\SubscribeTable::getList(
			array(		
				"filter" => $arFilter,
				"select" => array(new Bitrix\Main\Entity\ExpressionField("CNT", "COUNT(1)"))
			)
		);
		$totalCount = $countQuery->fetch();
		$arResult[$itemIdex]["COUNT"] = (int)$totalCount["CNT"];
		unset($totalCount, $countQuery, $arFilter);
	}
}