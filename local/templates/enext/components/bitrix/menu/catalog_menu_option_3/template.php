<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);

$isSiteClosed = false;
if(COption::GetOptionString("main", "site_stopped") == "Y" && !$USER->CanDoOperation("edit_other_settings"))
	$isSiteClosed = true;

$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($this->randString()));
$containerName = 'catalog-menu-'.$obName;

if(!$isSiteClosed && !empty($arResult)) {?>
	<div class="hidden-xs hidden-sm top-panel__col top-panel__catalog-menu">	
		<ul class="catalog-menu scrollbar-inner" id="<?=$containerName?>" data-entity="dropdown-menu">
			<?$previousLevel = 0;					
			foreach($arResult as $arItem) {			
				if($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel)
					echo str_repeat("</ul></div></div></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
				if($arItem["IS_PARENT"]) {?>
					<li<?=($arItem["SELECTED"] ? " class='active'" : "")?> data-entity="dropdown">
						<a href="<?=$arItem['LINK']?>">
							<?if(!empty($arItem["PARAMS"]["ICON"])) {?>
								<span class="<?=($arItem['DEPTH_LEVEL'] > 2 ? 'hidden-md hidden-lg ' : '')?>catalog-menu-icon"><i class="<?=$arItem['PARAMS']['ICON']?>"></i></span>
							<?} elseif(is_array($arItem["PARAMS"]["PICTURE"])) {?>
								<span class="<?=($arItem['DEPTH_LEVEL'] != 2 ? 'hidden-md hidden-lg ' : '')?>catalog-menu-pic">
									<img src="<?=$arItem['PARAMS']['PICTURE']['SRC']?>" width="<?=$arItem['PARAMS']['PICTURE']['WIDTH']?>" height="<?=$arItem['PARAMS']['PICTURE']['HEIGHT']?>" alt="<?=$arItem['PARAMS']['PICTURE']['ALT']?>" title="<?=$arItem['PARAMS']['PICTURE']['TITLE']?>" />
								</span>
							<?}?>
							<span class="catalog-menu-text"><?=($arItem["DEPTH_LEVEL"] == 1 ? preg_replace("/\s/", "<span></span>", $arItem["TEXT"], 1) : $arItem["TEXT"])?></span>
							<i class="hidden-xs hidden-sm<?=($arItem['DEPTH_LEVEL'] > 1 ? ' hidden-md hidden-lg' : '')?> icon-arrow-down"></i>
							<i class="hidden-md hidden-lg icon-arrow-right"></i>
						</a>
						<div class="catalog-menu-dropdown-menu scrollbar-inner" data-entity="dropdown-menu">
							<div class="catalog-menu-dropdown-menu-list">
								<ul>
									<li class="hidden-md hidden-lg" data-entity="title">
										<i class="icon-arrow-left catalog-menu-back"></i>
										<span class="catalog-menu-title"><?=$arItem["TEXT"]?></span>
										<i class="icon-close catalog-menu-close"></i>
									</li>
				<?} else {?>
					<li<?=$arItem["SELECTED"] ? " class='active'" : ""?>>
						<a href="<?=$arItem['LINK']?>">
							<?if(!empty($arItem["PARAMS"]["ICON"])) {?>
								<span class="<?=($arItem['DEPTH_LEVEL'] > 2 ? 'hidden-md hidden-lg ' : '')?>catalog-menu-icon"><i class="<?=$arItem['PARAMS']['ICON']?>"></i></span>
							<?} elseif(is_array($arItem["PARAMS"]["PICTURE"])) {?>
								<span class="<?=($arItem['DEPTH_LEVEL'] != 2 ? 'hidden-md hidden-lg ' : '')?>catalog-menu-pic">
									<img src="<?=$arItem['PARAMS']['PICTURE']['SRC']?>" width="<?=$arItem['PARAMS']['PICTURE']['WIDTH']?>" height="<?=$arItem['PARAMS']['PICTURE']['HEIGHT']?>" alt="<?=$arItem['PARAMS']['PICTURE']['ALT']?>" title="<?=$arItem['PARAMS']['PICTURE']['TITLE']?>" />
								</span>
							<?}?>
							<span class="catalog-menu-text"><?=($arItem["DEPTH_LEVEL"] == 1 ? preg_replace("/\s/", "<span></span>", $arItem["TEXT"], 1) : $arItem["TEXT"])?></span>
							<?if($arItem["PARAMS"]["ELEMENT_CNT"] > 0) {?>
								<span class="hidden-md hidden-lg catalog-menu-count"><?=$arItem["PARAMS"]["ELEMENT_CNT"]?></span>
							<?}?>
						</a>
					</li>
				<?}
				$previousLevel = $arItem["DEPTH_LEVEL"];						
			}
			if($previousLevel > 1)
				echo str_repeat("</ul></div></div></li>", ($previousLevel - 1));?>
		</ul>
		<script type="text/javascript">
			BX.message({
				MAIN_MENU: '<?=GetMessageJS("BM_MAIN_MENU")?>'
			});
			var <?=$obName?> = new JCCatalogMenu({		
				container: '<?=$containerName?>'
			});
		</script>
	</div>
<?}?>