<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);

$isSiteClosed = false;
if(COption::GetOptionString("main", "site_stopped") == "Y" && !$USER->CanDoOperation("edit_other_settings"))
	$isSiteClosed = true;

$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($this->randString()));
$containerName = 'slide-menu-'.$obName;

if(!$isSiteClosed && !empty($arResult)) {?>
	<ul class="slide-menu scrollbar-inner" id="<?=$containerName?>">
		<?$previousLevel = 0;					
		foreach($arResult as $arItem) {
			if($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel)
				echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
			if($arItem["IS_PARENT"]) {?>
				<li class="slide-menu-item<?=$arItem["SELECTED"] ? " active" : ""?> itm_<?=$arItem['ITEM_INDEX']?>" data-entity="dropdown" data-ids="<?=$arItem["ID"];?>">
					<a href="<?=$arItem['LINK']?>"<?=($arItem["DEPTH_LEVEL"] == 1 ? " title='".$arItem["TEXT"]."'" : "")?>					
					<? if ($arItem["DEPTH_LEVEL"] == 1) { ?> class="slide-menu-link" <? } ?>>
						<?if(!empty($arItem["PARAMS"]["ICON"])) {?>
							<span class="slide-menu-icon">
								<i class="<?=$arItem['PARAMS']['ICON']?>"></i>
							</span>
						<?} elseif(is_array($arItem["PARAMS"]["PICTURE"])) {?>
							<span class="slide-menu-pic">
								<?if($arItem["PARAMS"]['BACKLIGHT']) {?>
								<span style="background-image: url('<?= $arItem["PARAMS"]['BACKLIGHT'] ?>');
								<? if(!empty($arItem["PARAMS"]["POSITIONING_SHADE"])) { ?> 
									  background-position-y: <?=$arItem["PARAMS"]["POSITIONING_SHADE"]?>px <? } ?>"></span>
								<?}?>
								<img class="lazy-custom-2" data-original="<?=$arItem['PARAMS']['PICTURE']['SRC']?>" src="<?=$arItem['PARAMS']['PICTURE']['SRC']?>" width="<?=$arItem['PARAMS']['PICTURE']['WIDTH']?>" height="<?=$arItem['PARAMS']['PICTURE']['HEIGHT']?>" alt="<?=$arItem['PARAMS']['PICTURE']['ALT']?>" title="<?=$arItem['PARAMS']['PICTURE']['TITLE']?>" />
							</span>
						<?}?>
						<span class="slide-menu-text"><?=$arItem["TEXT"]?></span>
						<i class="icon-arrow-right"></i>
					</a>
					<ul class="slide-menu-dropdown-menu scrollbar-inner" data-entity="dropdown-menu">
						<li class="hidden-md hidden-lg" data-entity="title">
							<i class="icon-arrow-left slide-menu-back"></i>
							<span class="slide-menu-title"><?=$arItem["TEXT"]?></span>
							<i class="icon-close slide-menu-close"></i>
						</li>
			<?} else {?>
				<li class="slide-menu-item<?=$arItem["SELECTED"] ? " active" : ""?>">
					<a href="<?=$arItem['LINK']?>"<?=($arItem["DEPTH_LEVEL"] == 1 ? " title='".$arItem["TEXT"]."'" : "")?>>
						<?if(!empty($arItem["PARAMS"]["ICON"])) {?>
							<span class="slide-menu-icon">
								<i class="<?=$arItem['PARAMS']['ICON']?>"></i>
							</span>
						<?} elseif(is_array($arItem["PARAMS"]["PICTURE"])) {?>
							<span class="slide-menu-pic">
								<span></span>
								<img class="lazy-custom" data-original="<?=$arItem['PARAMS']['PICTURE']['SRC']?>" width="<?=$arItem['PARAMS']['PICTURE']['WIDTH']?>" height="<?=$arItem['PARAMS']['PICTURE']['HEIGHT']?>" alt="<?=$arItem['PARAMS']['PICTURE']['ALT']?>" title="<?=$arItem['PARAMS']['PICTURE']['TITLE']?>" />
							</span>
						<?}?>
						<span class="slide-menu-text"><?=$arItem["TEXT"]?></span>
						<?if($arItem["PARAMS"]["ELEMENT_CNT"] > 0) {?>
							<span class="slide-menu-count"><?=$arItem["PARAMS"]["ELEMENT_CNT"]?></span>
						<?}?>
					</a>
				</li>
			<?}
			$previousLevel = $arItem["DEPTH_LEVEL"];						
		}
		if($previousLevel > 1)
			echo str_repeat("</ul></li>", ($previousLevel - 1));?>
	</ul>	
	<script type="text/javascript">
	if(window.innerWidth < 1024) {
		BX.message({
			MAIN_MENU: '<?=GetMessageJS("BM_MAIN_MENU")?>'
		});
		var <?=$obName?> = new JCSlideMenuHover({
			container: '<?=$containerName?>'
		});
		
	}
	// $(window).resize(function() {

		// if(window.innerWidth < 1024) {
			// BX.message({
				// MAIN_MENU: '<?=GetMessageJS("BM_MAIN_MENU")?>'
			// });
			// var <?=$obName?> = new JCSlideMenuHover({
				// container: '<?=$containerName?>'
			// });
		// } else {
			 // var <?=$obName?> = new JCSlideMenuHover({
				// container: '<?=$containerName?>'
			 // });
			 // <?=$obName?>.slideMenuLoaded = false;
			// $('.slide-menu.scrollbar-inner').removeClass('scroll-content').attr('style',false).closest('.scroll-wrapper ').removeClass('scroll-wrapper');
		// }
	// });
	</script>	
<?}?>