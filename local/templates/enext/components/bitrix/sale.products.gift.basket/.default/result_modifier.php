<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//DISABLE_BASKET//
$arSettings = CEnext::GetFrontParametrsValues(SITE_ID);
$arParams["DISABLE_BASKET"] = false;
if($arSettings["DISABLE_BASKET"] == "Y")
	$arParams["DISABLE_BASKET"] = true;

//SHOW_SUBSCRIBE//
if($arParams["PRODUCT_SUBSCRIPTION"] == "Y") {
	$saleNotifyOption = Bitrix\Main\Config\Option::get("sale", "subscribe_prod");
	if(strlen($saleNotifyOption) > 0)
		$saleNotifyOption = unserialize($saleNotifyOption);
	$saleNotifyOption = is_array($saleNotifyOption) ? $saleNotifyOption : array();
	foreach($saleNotifyOption as $siteId => $data) {
		if($siteId == SITE_ID && $data["use"] != "Y")
			$arParams["PRODUCT_SUBSCRIPTION"] = "N";
	}
}

//BRANDS//
foreach($arResult["ITEMS"] as $item) {
	foreach($item["PROPERTIES"] as $prop) {
		if($prop["CODE"] == "BRAND" && !empty($prop["VALUE"]))
			$brandIds[] = $prop["VALUE"];
	}
	unset($prop);
}
unset($item);

if(!empty($brandIds)) {
	$rsElements = CIBlockElement::GetList(array(), array("ID" => array_unique($brandIds)), false, false, array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE"));
	while($arElement = $rsElements->GetNext()) {
		$arBrands[$arElement["ID"]] = array(
			"NAME" => $arElement["NAME"],
			"PREVIEW_PICTURE" => $arElement["PREVIEW_PICTURE"] > 0 ? CFile::GetFileArray($arElement["PREVIEW_PICTURE"]) : array()
		);
	}
	unset($arElement, $rsElements);
	
	if(!empty($arBrands)) {
		foreach($arResult["ITEMS"] as &$item) {		
			foreach($item["PROPERTIES"] as &$prop) {
				if($prop["CODE"] == "BRAND" && !empty($prop["VALUE"])) {
					if(array_key_exists($prop["VALUE"], $arBrands))
						$prop["FULL_VALUE"] = $arBrands[$prop["VALUE"]];
				}
			}
			unset($prop);
		}
		unset($item);
	}
	unset($arBrands);
}
unset($brandIds);

//PRODUCT_PROPERTIES//
if($arParams["ADD_PROPERTIES_TO_BASKET"] == "Y" && !empty($arParams["PRODUCT_PROPERTIES"])) {
	foreach($arResult["ITEMS"] as &$item) {
		if(!isset($item["PRODUCT_PROPERTIES"]) || empty($item["PRODUCT_PROPERTIES"]))
			$item["PRODUCT_PROPERTIES"] = CIBlockPriceTools::GetProductProperties($item["IBLOCK_ID"], $item["ID"], $arParams["PRODUCT_PROPERTIES"], $item["PROPERTIES"]);
	}
	unset($item);
}

//UF_CODE//
$isSkuProps = false;
foreach($arResult["ITEMS"] as $item) {
	if(!empty($item["OFFERS"]) && !empty($item["OFFERS_PROP"])) {
		$isSkuProps = true;
		break;
	}
}
unset($item);

if($arParams["PRODUCT_DISPLAY_MODE"] === "Y" && !!$isSkuProps) {
	foreach($arResult["SKU_PROPS"] as &$skuProps) {		
		foreach($skuProps as &$skuProperty) {
			if($skuProperty["SHOW_MODE"] == "PICT") {
				$entity = $skuProperty["USER_TYPE_SETTINGS"]["ENTITY"];
				if(!($entity instanceof Bitrix\Main\Entity\Base))
					continue;

				$entityFields = $entity->getFields();
				if(!array_key_exists("UF_CODE", $entityFields))
					continue;

				$entityDataClass = $entity->getDataClass();
				
				$directorySelect = array("ID", "UF_CODE");
				$directoryOrder = array();
				
				$entityGetList = array(
					"select" => $directorySelect,
					"order" => $directoryOrder
				);
				$propEnums = $entityDataClass::getList($entityGetList);
				while($oneEnum = $propEnums->fetch()) {
					$values[$oneEnum["ID"]] = $oneEnum["UF_CODE"];
				}

				foreach($skuProperty["VALUES"] as &$val) {				
					if(isset($values[$val["ID"]]))
						$val["CODE"] = $values[$val["ID"]];
				}
				unset($val, $values);
			}
		}
		unset($skuProperty);	
	}
	unset($skuProps);
}
unset($isSkuProps);