<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);

$this->addExternalCss(SITE_TEMPLATE_PATH."/components/bitrix/catalog.product.subscribe/.default/style.css");

$templateLibrary = array("popup", "ajax", "fx");
$currencyList = "";
if(!empty($arResult["CURRENCIES"])) {
	$templateLibrary[] = "currency";
	$currencyList = CUtil::PhpToJSObject($arResult["CURRENCIES"], false, true, true);
}

$templateData = array(
	"TEMPLATE_LIBRARY" => $templateLibrary,
	"CURRENCIES" => $currencyList
);
unset($currencyList, $templateLibrary);

$elementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
$elementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
$elementDeleteParams = array("CONFIRM" => GetMessage("CT_SPG_TPL_ELEMENT_DELETE_CONFIRM"));

$arParams["MESS_BTN_BUY"] = $arParams["MESS_BTN_BUY"] ?: Loc::getMessage("CT_SPG_TPL_MESS_BTN_BUY");
$arParams["MESS_BTN_DETAIL"] = $arParams["MESS_BTN_DETAIL"] ?: Loc::getMessage("CT_SPG_TPL_MESS_BTN_DETAIL");
$arParams["MESS_BTN_SUBSCRIBE"] = $arParams["MESS_BTN_SUBSCRIBE"] ?: Loc::getMessage("CT_SPG_TPL_MESS_BTN_SUBSCRIBE");
$arParams["MESS_BTN_ADD_TO_BASKET"] = $arParams["MESS_BTN_ADD_TO_BASKET"] ?: Loc::getMessage("CT_SPG_TPL_MESS_BTN_ADD_TO_BASKET");

$arParams["TEXT_LABEL_GIFT"] = $arParams["TEXT_LABEL_GIFT"] ?: Loc::getMessage("CT_SPG_TPL_TEXT_LABEL_GIFT");

$generalParams = array(
	"PRODUCT_DISPLAY_MODE" => $arParams["PRODUCT_DISPLAY_MODE"],
	"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
	"DISABLE_BASKET" => $arParams["DISABLE_BASKET"],
	"ADD_TO_BASKET_ACTION" => $arParams["ADD_TO_BASKET_ACTION"],
	"ADD_PROPERTIES_TO_BASKET" => $arParams["ADD_PROPERTIES_TO_BASKET"],
	"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
	"PRODUCT_SUBSCRIPTION" => $arParams["PRODUCT_SUBSCRIPTION"],
	"~ADD_URL_TEMPLATE" => $arResult["~ADD_URL_TEMPLATE"],
	"~BUY_URL_TEMPLATE" => $arResult["~BUY_URL_TEMPLATE"],
	"USE_ENHANCED_ECOMMERCE" => $arParams["USE_ENHANCED_ECOMMERCE"],
	"DATA_LAYER_NAME" => $arParams["DATA_LAYER_NAME"],
	"BRAND_PROPERTY" => $arParams["BRAND_PROPERTY"],
	"MESS_BTN_BUY" => $arParams["MESS_BTN_BUY"],
	"MESS_BTN_DETAIL" => $arParams["MESS_BTN_DETAIL"],
	"MESS_BTN_SUBSCRIBE" => $arParams["MESS_BTN_SUBSCRIBE"],
	"MESS_BTN_ADD_TO_BASKET" => $arParams["MESS_BTN_ADD_TO_BASKET"],
	"TEXT_LABEL_GIFT" => $arParams["TEXT_LABEL_GIFT"]
);

$obName = "ob".preg_replace("/[^a-zA-Z0-9_]/", "x", $this->GetEditAreaId($this->randString()));
$containerName = "sale-products-gift-container";?>

<div class="sale-products-gift" data-entity="<?=$containerName?>">
	<?if(!empty($arResult["ITEMS"]) && !empty($arResult["ITEM_ROWS"])) {
		$areaIds = array();
		foreach($arResult["ITEMS"] as &$item) {
			$uniqueId = $item["ID"]."_".md5($this->randString().$component->getAction());
			$areaIds[$item["ID"]] = $this->GetEditAreaId($uniqueId);
			$this->AddEditAction($uniqueId, $item["EDIT_LINK"], $elementEdit);
			$this->AddDeleteAction($uniqueId, $item["DELETE_LINK"], $elementDelete, $elementDeleteParams);
		}
		unset($item);?>		
		<!-- items-container -->
		<?foreach($arResult["ITEM_ROWS"] as $rowData) {
			$rowItems = array_splice($arResult["ITEMS"], 0, $rowData["COUNT"]);?>
			<div class="row product-item-row" data-entity="items-row">
				<?foreach($rowItems as $item) {?>
					<div class="col-xs-12 <?=($rowData['VARIANT'] == 2 ? 'col-md-4' : 'col-md-3')?>">
						<?$APPLICATION->IncludeComponent("bitrix:catalog.item", "gift",
							array(
								"RESULT" => array(
									"ITEM" => $item,
									"AREA_ID" => $areaIds[$item["ID"]],
									"TYPE" => $rowData["TYPE"]
								),
								"PARAMS" => $generalParams + array("SKU_PROPS" => $arResult["SKU_PROPS"][$item["IBLOCK_ID"]])
							),
							$component,
							array("HIDE_ICONS" => "Y")
						);?>
					</div>
				<?}?>
			</div>
		<?}
		unset($generalParams, $rowItems);?>
		<!-- items-container -->
	<?} else {
		//load css for bigData/deferred load
		$APPLICATION->IncludeComponent("bitrix:catalog.item", "",
			array(),
			$component,
			array("HIDE_ICONS" => "Y")
		);
	}?>	
</div>

<?$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedTemplate = $signer->sign($templateName, "sale.products.gift");
$signedParams = $signer->sign(base64_encode(serialize($arResult["ORIGINAL_PARAMETERS"])), "sale.products.gift");?>

<script type="text/javascript">
	BX.message({
		BASKET_URL: '<?=$arParams["BASKET_URL"]?>',
		ADD_BASKET_MESSAGE: '<?=($arParams["ADD_TO_BASKET_ACTION"] == "BUY" ? $arParams["MESS_BTN_BUY"] : $arParams["MESS_BTN_ADD_TO_BASKET"])?>',
		ADD_BASKET_OK_MESSAGE: '<?=GetMessageJS("CT_SPG_TPL_MESS_BTN_ADD_TO_BASKET_OK")?>',		
		SITE_ID: '<?=SITE_ID?>'
	});
	var <?=$obName?> = new JCSaleProductsGiftComponent({
		siteId: '<?=CUtil::JSEscape($component->getSiteId())?>',
		componentPath: '<?=CUtil::JSEscape($componentPath)?>',
		deferredLoad: true,
		initiallyShowHeader: '<?=!empty($arResult["ITEM_ROWS"])?>',
		currentProductId: <?=CUtil::JSEscape((int)$arResult['POTENTIAL_PRODUCT_TO_BUY']['ID'])?>,
		template: '<?=CUtil::JSEscape($signedTemplate)?>',
		parameters: '<?=CUtil::JSEscape($signedParams)?>',
		container: '<?=$containerName?>'
	});
</script>