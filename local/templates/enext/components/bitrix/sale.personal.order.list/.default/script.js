BX.ready(function() {
	var tabs = document.querySelector('.sale-order-tabs-list');
	if(!!tabs) {
		BX.addClass(tabs, 'owl-carousel');
		$(tabs).owlCarousel({								
			autoWidth: true,
			nav: true,
			navText: ['<i class=\"icon-arrow-left\"></i>', '<i class=\"icon-arrow-right\"></i>'],
			navContainer: '.sale-order-tabs-scroll',
			dots: false,			
		});
	}
});