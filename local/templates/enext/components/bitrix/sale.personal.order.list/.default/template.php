<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

global $USER;
if(!$USER->IsAuthorized())
	return;

CJSCore::Init(array("clipboard", "fx"));

$this->addExternalCss(SITE_TEMPLATE_PATH."/js/owlCarousel/owl.carousel.css");
$this->addExternalJS(SITE_TEMPLATE_PATH."/js/owlCarousel/owl.carousel.min.js");

Loc::loadMessages(__FILE__);

if(!empty($arResult["ERRORS"]["FATAL"])) {
	foreach($arResult["ERRORS"]["FATAL"] as $error)
		ShowError($error);
		
	$component = $this->__component;
	if($arParams["AUTH_FORM_IN_TEMPLATE"] && isset($arResult["ERRORS"]["FATAL"][$component::E_NOT_AUTHORIZED]))
		$APPLICATION->AuthForm("", false, false, "N", false);
} else {?>
	<div class="row sale-order-list">
		<?if($arParams["ONLY_LAST_ORDERS"] != "Y") {?>
			<div class="col-xs-12">
				<div class="sale-order-tabs-block">
					<div class="sale-order-tabs-scroll">
						<ul class="sale-order-tabs-list">
							<?$nothing = !isset($_REQUEST["filter_history"]) && !isset($_REQUEST["show_all"]);
							$clearFromLink = array("filter_history", "filter_status", "show_all", "show_canceled");?>
							<li class="sale-order-tab<?=($nothing ? ' active': '')?>">
								<a href="<?=$APPLICATION->GetCurPageParam("", $clearFromLink, false)?>" class="sale-order-tab-link">
									<span><?=Loc::getMessage("SPOL_TPL_CUR_ORDERS")?></span>
									<span class="sale-order-tab-count"><?=$arResult["COUNT_ORDERS"]["order"]?></span>
								</a>
							</li>
							<li class="sale-order-tab<?=($_REQUEST["filter_history"] == "Y" && $_REQUEST["show_canceled"] != "Y"? ' active': '')?>">
								<a href="<?=$APPLICATION->GetCurPageParam("filter_history=Y", $clearFromLink, false)?>" class="sale-order-tab-link">
									<span><?=Loc::getMessage("SPOL_TPL_VIEW_ORDERS_HISTORY")?></span>
									<span class="sale-order-tab-count"><?=$arResult["COUNT_ORDERS"]["history"]?></span>
								</a>
							</li>
							<li class="sale-order-tab<?=($_REQUEST["filter_history"] == "Y" && $_REQUEST["show_canceled"] == "Y"? ' active': '')?>">
								<a href="<?=$APPLICATION->GetCurPageParam("filter_history=Y&show_canceled=Y", $clearFromLink, false)?>" class="sale-order-tab-link">
									<span><?=Loc::getMessage("SPOL_TPL_VIEW_ORDERS_CANCELED")?></span>
									<span class="sale-order-tab-count"><?=$arResult["COUNT_ORDERS"]["cancel"]?></span>
								</a>
							</li>
							<div class="clearfix"></div>
						</ul>
					</div>
				</div>
			</div>
		<?}
		if(!empty($arResult["ERRORS"]["NONFATAL"])) {?>
			<div class="col-xs-12 sale-order-alert">
				<?foreach($arResult["ERRORS"]["NONFATAL"] as $error)
					ShowError($error);?>
			</div>
		<?}
		if(!count($arResult["ORDERS"])) {?>
			<div class="col-xs-12 sale-order-alert">
				<?if($_REQUEST["filter_history"] == "Y") {
					if($_REQUEST["show_canceled"] == "Y") {
						ShowNote(Loc::getMessage("SPOL_TPL_EMPTY_CANCELED_ORDER"), "warning");
					} else {
						ShowNote(Loc::getMessage("SPOL_TPL_EMPTY_HISTORY_ORDER_LIST"), "warning");
					}
				} else {
					ShowNote(Loc::getMessage("SPOL_TPL_EMPTY_ORDER_LIST"), "warning");
				}?>
			</div>
		<?}
		foreach($arResult["ORDERS"] as $key => $order) {?>			
			<div class="col-xs-12 col-md-3 sale-order-item-container" style="height: auto;">
				<div class="sale-order-item">	
					<div class="sale-order-item-image-wrapper">		
						<a class="sale-order-item-image" href="<?=htmlspecialcharsbx($order['ORDER']['URL_TO_DETAIL'])?>" title="<?=Loc::getMessage("SPOL_TPL_ORDER_TITLE", array("#ACCOUNT_NUMBER#" => $order['ORDER']['ACCOUNT_NUMBER']))?>">
							<?if(is_array($order["PREVIEW_PICTURE"])) {?>
								<img src="<?=$order['PREVIEW_PICTURE']['SRC']?>" width="<?=$order['PREVIEW_PICTURE']['WIDTH']?>" height="<?=$order['PREVIEW_PICTURE']['HEIGHT']?>" alt="<?=$order['PREVIEW_PICTURE']['ALT']?>" />
							<?}?>
							<div class="sale-order-item-sticker-wrap">
								<span class="sale-order-item-sticker <?=($order['ORDER']['CANCELED'] !== 'N' ? 'canceled' : mb_strtolower($order['ORDER']['STATUS_ID']))?>" title="<?=($order['ORDER']['CANCELED'] !== "N" ? Loc::getMessage('SPOL_TPL_ORDER_CANCELED_STICKER') : $arResult['INFO']['STATUS'][$order['ORDER']['STATUS_ID']]['NAME'])?>">
									<?=($order["ORDER"]["CANCELED"] !== "N" ? Loc::getMessage("SPOL_TPL_ORDER_CANCELED_STICKER") : $arResult["INFO"]["STATUS"][$order["ORDER"]["STATUS_ID"]]["NAME"])?>
								</span>
							</div>
						</a>			
						<div class="sale-order-item-icons-container">
							<div class="sale-order-item-repeat" title="<?=Loc::getMessage('SPOL_TPL_REPEAT_ORDER')?>" onClick="location.href='<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>'">
								<i class="icon-repeat"></i>
							</div>
							<?if($order["ORDER"]["CAN_CANCEL"] == "Y") {?>
								<div class="sale-order-item-cancel" title="<?=Loc::getMessage('SPOL_TPL_CANCEL_ORDER')?>" onClick="location.href='<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_CANCEL"])?>'">
									<i class="icon-close"></i>
								</div>
							<?}?>
						</div>
					</div>					
					<div class="sale-order-item-info-container">
						<a href="<?=htmlspecialcharsbx($order['ORDER']['URL_TO_DETAIL'])?>">
							<div class="sale-order-item-date">
								<?=$order["ORDER"]["DATE_INSERT_FORMATED"]?>
							</div>
							<div class="sale-order-item-title">
								<?=Loc::getMessage("SPOL_TPL_ORDER_TITLE", array("#ACCOUNT_NUMBER#" => $order["ORDER"]["ACCOUNT_NUMBER"]))?>
							</div>
							<div class="sale-order-item-info">
								<?=count($order["BASKET_ITEMS"]);?>
								<?$count = count($order["BASKET_ITEMS"]) % 10;
								if($count == "1")
									echo Loc::getMessage("SPOL_TPL_GOOD");
								elseif($count >= "2" && $count <= "4")
									echo Loc::getMessage("SPOL_TPL_TWO_GOODS");
								else
									echo Loc::getMessage("SPOL_TPL_GOODS");?>
								<?=Loc::getMessage("SPOL_TPL_SUMOF")." ".$order["ORDER"]["FORMATED_PRICE"];?>
							</div>
						</a>
					</div>					
					<div class="sale-order-item-button-container">
						<div class="sale-order-item-button">
							<?if($order["ORDER"]["IS_ALLOW_PAY"] !== "N" && $order["ORDER"]["CANCELED"] === "N" && count($order["PAYMENT"]) <= 1 && $order["ORDER"]["PAYED"] !== "Y" && $order["PAYMENT"][0]["NEW_WINDOW"] === "Y") {?>
								<a class="btn btn-buy" title="<?=Loc::getMessage('SPOL_TPL_PAY')?>" href="<?=htmlspecialcharsbx($order['PAYMENT'][0]['PSA_ACTION_FILE'])?>" target="_blank"><i class="fa fa-credit-card hidden-md hidden-lg" aria-hidden="true"></i><span class="hidden-xs hidden-sm"><?=Loc::getMessage("SPOL_TPL_PAY")?></span></a>
							<?}?>
						</div>
						<div class="sale-order-item-button">
							<a class="btn btn-default" title="<?=Loc::getMessage('SPOL_TPL_MORE_ON_ORDER')?>" href="<?=htmlspecialcharsbx($order['ORDER']['URL_TO_DETAIL'])?>"><i class="icon-arrow-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		<?}
		if($arParams["ONLY_LAST_ORDERS"] != "Y")
			echo $arResult["NAV_STRING"];?>
	</div>
<?}?>