<?
$MESS['PROFILE_DATA_SAVED'] = "Изменения сохранены";
$MESS['MP_TITLE_PERSONAL_DATA'] = "Личные данные";
$MESS['LAST_UPDATE'] = "Дата обновления:";
$MESS['LAST_LOGIN'] = "Последняя авторизация:";
$MESS['MP_NAME'] = "Имя";
$MESS['MP_LAST_NAME'] = "Фамилия";
$MESS['MP_SECOND_NAME'] = "Отчество";
$MESS['MP_EMAIL'] = "E-Mail";
$MESS['MP_USER_PHOTO'] = "Изображение профиля";
$MESS['MP_SELECT_PHOTO'] = "Выбрать";
$MESS['MP_NOT_PHOTO'] = "Файл не выбран";
$MESS['MP_DELETE_PHOTO'] = "Удалить изображение";
$MESS['MP_TITLE_CHANGE_PASSWORD'] = "Изменить пароль";
$MESS['MP_NEW_PASSWORD_REQ'] = "Новый пароль";
$MESS['MP_NEW_PASSWORD_CONFIRM'] = "Подтверждение нового пароля";
$MESS['MP_MAIN_SAVE'] = "Сохранить";
$MESS['MP_MAIN_RESET'] = "Сбросить";
$MESS['MP_MAIN_RESET'] = "Отмена";
$MESS['MP_TITLE_SOCIAL_BLOCK'] = "Социальные сети";
?>
