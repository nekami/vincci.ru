<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<span class="alert alert-<?=($arParams["STYLE"] == "success" ? "success" : ($arParams["STYLE"] == "warning" ? "warning" : "error"));?>" style="display: block;"><?=$arParams["MESSAGE"]?></span>