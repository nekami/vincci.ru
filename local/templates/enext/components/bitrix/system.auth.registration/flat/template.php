<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;?>

<div class="bx-authform">
	<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) && $arParams["AUTH_RESULT"]["TYPE"] === "OK") {
		ShowNote(Loc::getMessage("AUTH_EMAIL_SENT"), "success");
	} else {?>
		<noindex>
			<div class="bx-authform-title-container">
				<div class="bx-authform-title">
					<div class="bx-authform-title-icon"><i class="fa fa-user-plus" aria-hidden="true"></i></div>
					<div class="bx-authform-title-val"><?=Loc::getMessage("AUTH_FORM_TITLE")?></div>
				</div>
			</div>
			<form method="post" action="<?=$arResult['AUTH_URL']?>" name="bform" enctype="multipart/form-data">
				<div class="bx-authform-content-container">
					<div class="bx-authform-content col-xs-12 col-md-4">
						<?if(!empty($arParams["~AUTH_RESULT"]))
							if(is_array($arParams["~AUTH_RESULT"]))
								ShowNote($arParams["~AUTH_RESULT"]["MESSAGE"], ($arParams["~AUTH_RESULT"]["TYPE"] == "OK" ? "success" : "error"));
							else
								ShowMessage($arParams["~AUTH_RESULT"]);

						if($arResult["USE_EMAIL_CONFIRMATION"] === "Y")
							ShowMessage(Loc::getMessage("AUTH_EMAIL_WILL_BE_SENT"));
						
						if($arResult["BACKURL"] <> "") {?>
							<input type="hidden" name="backurl" value="<?=$arResult['BACKURL']?>">
						<?}?>
						<input type="hidden" name="AUTH_FORM" value="Y">
						<input type="hidden" name="TYPE" value="REGISTRATION">
						<div class="bx-authform-formgroup-container">
							<div class="bx-authform-label-container"><?=Loc::getMessage("AUTH_NAME")?></div>
							<div class="bx-authform-input-container">
								<input type="text" name="USER_NAME" maxlength="255" value="<?=$arResult['USER_NAME']?>" class="form-control">
							</div>
						</div>
						<div class="bx-authform-formgroup-container">
							<div class="bx-authform-label-container"><?=Loc::getMessage("AUTH_LAST_NAME")?></div>
							<div class="bx-authform-input-container">
								<input type="text" name="USER_LAST_NAME" maxlength="255" value="<?=$arResult['USER_LAST_NAME']?>" class="form-control">
							</div>
						</div>
						<div class="bx-authform-formgroup-container">
							<div class="bx-authform-label-container"><?=Loc::getMessage("AUTH_LOGIN_MIN")?> <span class="bx-authform-starrequired">*</span></div>
							<div class="bx-authform-input-container">
								<input type="text" name="USER_LOGIN" maxlength="255" value="<?=$arResult['USER_LOGIN']?>" class="form-control">
							</div>
						</div>
						<div class="bx-authform-formgroup-container">
							<div class="bx-authform-label-container"><?=Loc::getMessage("AUTH_PASSWORD_REQ")?> (<?=$arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?>) <span class="bx-authform-starrequired">*</span></div>
							<div class="bx-authform-input-container">								
								<?if($arResult["SECURE_AUTH"]) {?>								
									<div class="bx-authform-psw-protected" id="bx_auth_secure" style="display: none;">
										<div class="bx-authform-psw-protected-desc"><?=Loc::getMessage("AUTH_SECURE_NOTE")?></div>
									</div>
									<script type="text/javascript">
										document.getElementById("bx_auth_secure").style.display = "";
									</script>								
								<?}?>								
								<input type="password" name="USER_PASSWORD" maxlength="255" value="<?=$arResult['USER_PASSWORD']?>" autocomplete="off" class="form-control">
							</div>
						</div>
						<div class="bx-authform-formgroup-container">
							<div class="bx-authform-label-container"><?=Loc::getMessage("AUTH_CONFIRM")?> <span class="bx-authform-starrequired">*</span></div>
							<div class="bx-authform-input-container">								
								<?if($arResult["SECURE_AUTH"]) {?>								
									<div class="bx-authform-psw-protected" id="bx_auth_secure_conf" style="display: none;">
										<div class="bx-authform-psw-protected-desc"><?=Loc::getMessage("AUTH_SECURE_NOTE")?></div>
									</div>
									<script type="text/javascript">
										document.getElementById("bx_auth_secure_conf").style.display = "";
									</script>								
								<?}?>								
								<input type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult['USER_CONFIRM_PASSWORD']?>" autocomplete="off" class="form-control">
							</div>
						</div>
						<div class="bx-authform-formgroup-container">
							<div class="bx-authform-label-container"><?=Loc::getMessage("AUTH_EMAIL").($arResult["EMAIL_REQUIRED"] ? " <span class='bx-authform-starrequired'>*</span>" : "");?></div>
							<div class="bx-authform-input-container">
								<input type="text" name="USER_EMAIL" maxlength="255" value="<?=$arResult['USER_EMAIL']?>" class="form-control">
							</div>
						</div>
						<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y") {
							foreach($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField) {?>
								<div class="bx-authform-formgroup-container">
									<div class="bx-authform-label-container"><?=$arUserField["EDIT_FORM_LABEL"].($arUserField["MANDATORY"] == "Y" ? " <span class='bx-authform-starrequired'>*</span>" : "");?></div>
									<div class="bx-authform-input-container">
										<?$APPLICATION->IncludeComponent("bitrix:system.field.edit", $arUserField["USER_TYPE"]["USER_TYPE_ID"],
											array(
												"bVarsFromForm" => $arResult["bVarsFromForm"],
												"arUserField" => $arUserField,
												"form_name" => "bform"
											),
											null,
											array("HIDE_ICONS" => "Y")
										);?>
									</div>
								</div>
							<?}
						}
						if($arResult["USE_CAPTCHA"] == "Y") {?>
							<div>
                                <input type="hidden" name="captcha_sid" value="<?=$arResult['CAPTCHA_CODE']?>">
                                <img src="" alt="" style="display: none">
                                <input type="hidden" name="g_recaptcha_response" value="" />
							</div>
						<?}?>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="bx-authform-formgroup-container">
					<div class="bx-authform-buttons-container">
						<button type="submit" class="btn btn-buy" name="Register" value="<?=Loc::getMessage('AUTH_REGISTER')?>"><span><?=Loc::getMessage("AUTH_REGISTER")?></span></button>
						<a href="<?=$arResult['AUTH_AUTH_URL']?>" rel="nofollow" class="btn btn-default"><?=Loc::getMessage("AUTH_AUTH")?></a>
					</div>
				</div>
			</form>
		</noindex>
		<script type="text/javascript">
			document.bform.USER_NAME.focus();
		</script>
	<?}?>
</div>