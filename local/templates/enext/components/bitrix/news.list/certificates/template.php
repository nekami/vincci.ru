<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if(count($arResult["ITEMS"]) < 1)
	return;

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('GALLERY_ITEM_DELETE_CONFIRM'));

$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($this->randString()));
$containerName = 'gallery-'.$obName;?>

<div class="row gallery gallery_certificates" id="<?=$containerName?>">			
	<?foreach($arResult["ITEMS"] as $arItem) {
		$arFilePath = CFile::GetByID($arItem['PROPERTIES']['PDF']['VALUE'])->fetch();
		$filePath = $arFilePath ? '/upload/'.$arFilePath['SUBDIR'].'/'.$arFilePath['FILE_NAME'] : $arItem['DETAIL_PICTURE']['SRC'];
		$this->AddEditAction($arItem["ID"], $arItem["EDIT_LINK"], $elementEdit);
		$this->AddDeleteAction($arItem["ID"], $arItem["DELETE_LINK"], $elementDelete, $elementDeleteParams);?>
		<div class="col-xs-6 col-md-3" id="<?=$this->GetEditAreaId($arItem['ID'])?>">				
			<a class="gallery-item gallery-item fancypdf" title="<?=$arItem['NAME']?>" href="<?=$filePath?>" data-fancybox-group="gallery">
				<span class="gallery-item__image lazy-load"<?=(!empty($arItem["PREVIEW_PICTURE"]) ? " data-src='".$arItem["PREVIEW_PICTURE"]["SRC"]."'" : "");?>></span>
				<span class="gallery-item__caption-wrap">
					<span class="gallery-item__caption">
						<span class="gallery-item__title"><?=$arItem["NAME"]?></span>
						<?=(!empty($arItem["PREVIEW_TEXT"]) ? "<span class='gallery-item__text'>".$arItem["PREVIEW_TEXT"]."</span>" : "");?>
					</span>
				</span>
			</a>
		</div>
	<?}
	if($arParams["DISPLAY_BOTTOM_PAGER"]) {
		if(!empty($arResult["NAV_STRING"])) {?>
			<div class="col-xs-12">
				<?=$arResult["NAV_STRING"];?>
			</div>
		<?}
	}?>
</div>

<script type="text/javascript">
	var <?=$obName?> = new JCNewsListGallery({
		container: '<?=$containerName?>'
	});
</script>