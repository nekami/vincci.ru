<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if(count($arResult["ITEMS"]) < 1)
	return;

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('NEWS_ITEM_DELETE_CONFIRM'));?>

<div class="row news">	
	<?foreach($arResult["ITEMS"] as $arItem) {
		$this->AddEditAction($arItem["ID"], $arItem["EDIT_LINK"], $elementEdit);
		$this->AddDeleteAction($arItem["ID"], $arItem["DELETE_LINK"], $elementDelete, $elementDeleteParams);?><!--
		--><div class="col-xs-12 col-md-12" id="<?=$this->GetEditAreaId($arItem['ID'])?>">
			<div class="news-item"> <!--
				<div class="news-item__pic-wrapper">
					<div class="news-item__pic">
						<?if(!empty($arItem["PREVIEW_PICTURE"])) {?>
							<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>" />
						<?}?>
					</div>
				</div> -->
				<div class="news-item__caption">
					<h2 class="news-item__title"><?=$arItem["NAME"]?></h2>
					<p class="news-item__text"><?=$arItem["DETAIL_TEXT"]?></p>
				</div>
			</div>
		</div><!--				
	--><?}
	if($arParams["DISPLAY_BOTTOM_PAGER"]) {
		if(!empty($arResult["NAV_STRING"])) {?>
			<div class="col-xs-12">
				<?=$arResult["NAV_STRING"];?>
			</div>
		<?}
	}?>
</div>