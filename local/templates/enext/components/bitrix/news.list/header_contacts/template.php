<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);

if(count($arResult["ITEMS"]) < 1)
	return;

global $arSettings;

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('HEADER_CONTACTS_ITEM_DELETE_CONFIRM'));

foreach($arResult["ITEMS"] as $arItem) {
	$this->AddEditAction($arItem["ID"], $arItem["EDIT_LINK"], $elementEdit);
	$this->AddDeleteAction($arItem["ID"], $arItem["DELETE_LINK"], $elementDelete, $elementDeleteParams);
	
	$strMainID = $this->GetEditAreaId($arItem["ID"]);	
	$strObName = "ob".preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);?>

	<div class="top-panel__col top-panel__contacts">
	<a class="top-panel__contacts-block" href="tel:88007758183">
<span class="top-panel__contacts-icon"><i class="icon-phone-call"></i></span>
<span class="top-panel__contacts-caption hidden-xs hidden-sm">
<span class="top-panel__contacts-title"><span>8 (800)</span> 775-81-83 </span>
<span class="top-panel__contacts-descr">Звонок бесплатный</span>
</span>
</a> 
		<a class="top-panel__contacts-block" id="<?=$strMainID?>" href="javascript:void(0)">
			<span class="top-panel__contacts-icon"><i class="icon-phone-call"></i></span>
			<span class="top-panel__contacts-caption hidden-xs hidden-sm<?=($arSettings['CATALOG_MENU']['VALUE'] == 'OPTION-3' ? ' hidden-md' : '')?>">
				<?if(!empty($arItem["PROPERTIES"]["HEADER_PHONE"]["~VALUE"]["TEXT"])) {?>
					<span class="top-panel__contacts-title"><?=$arItem["PROPERTIES"]["HEADER_PHONE"]["~VALUE"]["TEXT"]?></span>
				<?}
				if(!empty($arItem["PROPERTIES"]["HEADER_WORK_SCHED"]["~VALUE"]["TEXT"])) {?>
					<span class="top-panel__contacts-descr"><?=$arItem["PROPERTIES"]["HEADER_WORK_SCHED"]["~VALUE"]["TEXT"]?></span>
				<?}?>
			</span>
			<span class="top-panel__contacts-icon hidden-xs hidden-sm<?=($arSettings['CATALOG_MENU']['VALUE'] == 'OPTION-3' ? ' hidden-md' : '')?>"><i class="icon-arrow-down"></i></span>
		</a>
		<?$arJSParams = array(				
			"ITEM" => array(
				"ADDRESS" => $arItem["ADDRESS"],
				"TIMEZONE" => $arItem["TIMEZONE"],
				"WORKING_HOURS" => $arItem["WORKING_HOURS"],			
				"PHONE" => $arItem["PHONE"],								
				"EMAIL" => $arItem["EMAIL"],
				"SKYPE" => $arItem["SKYPE"]
			),
			"VISUAL" => array(
				"ID" => $strMainID
			)
		);?>
		<script>
			BX.message({		
				HEADER_CONTACTS_TITLE: '<?=GetMessageJS("HEADER_CONTACTS_TITLE");?>',
				HEADER_CONTACTS_ITEM_TODAY: '<?=GetMessageJS("HEADER_CONTACTS_ITEM_TODAY");?>',
				HEADER_CONTACTS_ITEM_24_HOURS: '<?=GetMessageJS("HEADER_CONTACTS_ITEM_24_HOURS");?>',
				HEADER_CONTACTS_ITEM_OFF: '<?=GetMessageJS("HEADER_CONTACTS_ITEM_OFF");?>',
				HEADER_CONTACTS_ITEM_BREAK: '<?=GetMessageJS("HEADER_CONTACTS_ITEM_BREAK");?>',
				HEADER_CONTACTS_TEMPLATE_PATH: '<?=CUtil::JSEscape($templateFolder)?>'
			});
			var <?=$strObName;?> = new JCNewsListHeaderContacts(<?=CUtil::PhpToJSObject($arJSParams, false, true);?>);

			$(function() {
				BX.HEADER_AJAX_ID = "<?=$arParams["AJAX_ID"]?>";
				$("div #comp_"+BX.HEADER_AJAX_ID).addClass('top-panel__col top-panel__contacts top-panel__contacts__add');

				window.updateContacts = function() {
					var url = window.location.href + '?';
					url = url.substr(0, url.indexOf('?'));
					BX.ajax.insertToNode(url+'?bxajaxid='+BX.HEADER_AJAX_ID, 'comp_'+BX.HEADER_AJAX_ID);
				}

				if(window.CONTACTS_HANDLER_ADDED !== true)
					BX.addCustomEvent("UpdateCity", window.updateContacts);
				
				window.CONTACTS_HANDLER_ADDED = true;
			});
		</script>
	</div>
<?}?>