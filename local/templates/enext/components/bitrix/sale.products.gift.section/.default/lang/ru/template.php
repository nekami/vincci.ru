<?
$MESS["CT_SPGS_TPL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["CT_SPGS_TPL_TEXT_LABEL_GIFT"] = "Подарок";
$MESS["CT_SPGS_TPL_MESS_BTN_BUY"] = "Выбрать";
$MESS["CT_SPGS_TPL_MESS_BTN_ADD_TO_BASKET"] = "Выбрать";
$MESS["CT_SPGS_TPL_MESS_BTN_ADD_TO_BASKET_OK"] = "В корзине";
$MESS["CT_SPGS_TPL_MESS_BTN_DETAIL"] = "Подробнее";
$MESS["CT_SPGS_TPL_MESS_BTN_SUBSCRIBE"] = "Подписаться";
?>