<?
$MESS["AUTH_FORM_TITLE"] = "Форма авторизации";
$MESS["AUTH_LOGIN_SOCSERV"] = "Войти как пользователь";
$MESS["AUTH_LOGIN"] = "Логин";
$MESS["AUTH_PASSWORD"] = "Пароль";
$MESS["AUTH_REMEMBER_ME"] = "Запомнить меня на этом компьютере";
$MESS["AUTH_AUTHORIZE"] = "Войти";
$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["AUTH_FORGOT_PASSWORD"] = "Напомнить пароль";
$MESS["AUTH_CAPTCHA_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_SECURE_NOTE"] = "Перед отправкой формы авторизации пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";
?>