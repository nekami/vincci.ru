<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;?>

<div class="product-item" itemscope itemtype="http://schema.org/Product">
    <div class="product-item-image-wrapper" data-entity="image-wrapper">
        <?//PREVIEW_PICTURE//?>
        <a class="product-item-image" id="<?=$itemIds['PICT_ID']?>" href="<?=$item['DETAIL_PAGE_URL']?>" title="<?=$imgTitle?>">
            <?if(is_array($item['PREVIEW_PICTURE'])) {?>
                <img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" width="<?=$item['PREVIEW_PICTURE']['WIDTH']?>" height="<?=$item['PREVIEW_PICTURE']['HEIGHT']?>" alt="<?=$imgAlt?>" title="<?=$imgTitle?>" itemprop="image" />
            <?} else {?>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/no_photo.png" width="222" height="222" alt="<?=$imgAlt?>" title="<?=$imgTitle?>" itemprop="image" />
            <?}
            //MARKERS//?>
            <div class="product-item-markers<?=(!$object && !$partnersUrl && (!$haveOffers || $arParams['PRODUCT_DISPLAY_MODE'] === 'Y') ? ' product-item-markers-icons' : '')?>">
                <?if($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y') {?>
                    <span class="product-item-marker-container<?=($price['PERCENT'] > 0 ? '' : ' product-item-marker-container-hidden')?>" id="<?=$itemIds['DISCOUNT_PERCENT_ID']?>">
						<span class="product-item-marker product-item-marker-discount product-item-marker-14px"><span data-entity="dsc-perc-val"><?=-$price['PERCENT']?>%</span></span>
					</span>
                <?}


                $prop=CIBlockElement::GetByID($item['ID'])->GetNextElement()->GetProperties();

                if(!empty($prop['WARRANTY']['VALUE']) && $prop['WARRANTY']['VALUE'] == '5 лет') {
                    ?>

                    <span class="product-item-marker-container">
							
								<div class="icon_war"><?=$prop['WARRANTY']['VALUE'];?></div>
							
							</span>


                    <?
                }
				if(!empty($prop['WARRANTY']['VALUE']) && $prop['WARRANTY']['VALUE'] == '7 лет') {
                    ?>

                    <span class="product-item-marker-container">
							
								<div class="icon_war"><?=$prop['WARRANTY']['VALUE'];?></div>
							
							</span>


                    <?
                }

                if(!empty($prop['LIGHT_STREAM']['VALUE']) && !empty($prop['POWER']['VALUE'])) {

                    $power = str_replace("Вт", "", $prop['POWER']['VALUE']);
                    $stream = str_replace("лм", "", $prop['LIGHT_STREAM']['VALUE']);

                    $res = $stream * 1 / ($power*1);


                    ?>
                    <span class="product-item-marker-container">
							<?



                            if($res >= 138 && $res <  147) { echo '<div class="icon_lvt"> 140<br/>лт/вт</div>';}
                            if($res >= 148 && $res < 157) { echo '<div class="icon_lvt">150<br/>лт/вт</div>';}
                            if($res >= 158) { echo '<div class="icon_lvt">160 <br/>лт/вт</div>'; }

                            ?>
						</span>
                    <?
                }


                if(!empty($item['PROPERTIES']['MARKER']['FULL_VALUE'])) {
                    foreach($item['PROPERTIES']['MARKER']['FULL_VALUE'] as $key => $arMarker) {
                        if($key <= 4) {?>
                            <span class="product-item-marker-container">
								<span class="product-item-marker<?=(!empty($arMarker['FONT_SIZE']) ? ' product-item-marker-'.$arMarker['FONT_SIZE'] : '')?>"<?=(!empty($arMarker['BACKGROUND_1']) && !empty($arMarker['BACKGROUND_2']) ? ' style="background: '.$arMarker['BACKGROUND_2'].'; background: -webkit-linear-gradient(left, '.$arMarker['BACKGROUND_1'].', '.$arMarker['BACKGROUND_2'].'); background: -moz-linear-gradient(left, '.$arMarker['BACKGROUND_1'].', '.$arMarker['BACKGROUND_2'].'); background: -o-linear-gradient(left, '.$arMarker['BACKGROUND_1'].', '.$arMarker['BACKGROUND_2'].'); background: -ms-linear-gradient(left, '.$arMarker['BACKGROUND_1'].', '.$arMarker['BACKGROUND_2'].'); background: linear-gradient(to right, '.$arMarker['BACKGROUND_1'].', '.$arMarker['BACKGROUND_2'].');"' : (!empty($arMarker['BACKGROUND_1']) && empty($arMarker['BACKGROUND_2']) ? ' style="background: '.$arMarker['BACKGROUND_1'].';"' : (empty($arMarker['BACKGROUND_1']) && !empty($arMarker['BACKGROUND_2']) ? ' style="background: '.$arMarker['BACKGROUND_2'].';"' : '')))?>><?=(!empty($arMarker['ICON']) ? '<i class="'.$arMarker['ICON'].'"></i>' : '')?><span><?=$arMarker['NAME']?></span></span>
							</span>
                        <?} else {
                            break;
                        }
                    }
                    unset($key, $arMarker);
                }?>
            </div>
            <?//BRAND//
            if(!empty($item['PROPERTIES']['BRAND']['FULL_VALUE']['PREVIEW_PICTURE'])) {?>
                <div class="product-item-brand" itemprop="brand" itemscope itemtype="http://schema.org/Brand">
                    <meta itemprop="name" content="<?=$item['PROPERTIES']['BRAND']['FULL_VALUE']['NAME']?>" />
                    <img itemprop="logo" src="<?=$item['PROPERTIES']['BRAND']['FULL_VALUE']['PREVIEW_PICTURE']['SRC']?>" width="<?=$item['PROPERTIES']['BRAND']['FULL_VALUE']['PREVIEW_PICTURE']['WIDTH']?>" height="<?=$item['PROPERTIES']['BRAND']['FULL_VALUE']['PREVIEW_PICTURE']['HEIGHT']?>" alt="<?=$item['PROPERTIES']['BRAND']['FULL_VALUE']['NAME']?>" title="<?=$item['PROPERTIES']['BRAND']['FULL_VALUE']['NAME']?>" />
                </div>
            <?}?>
        </a>
        <?//DELAY//
        if(!$object && !$partnersUrl && (!$haveOffers || $arParams['PRODUCT_DISPLAY_MODE'] === 'Y')) {?>
            <div class="visible-md visible-lg product-item-icons-container">
                <div class="product-item-delay" id="<?=$itemIds['DELAY_LINK']?>" title="<?=$arParams['MESS_BTN_DELAY']?>" style="display: <?=($actualItem['CAN_BUY'] && $price['RATIO_PRICE'] > 0 ? '' : 'none')?>;">
                    <i class="icon-star" data-entity="delay-icon"></i>
                </div>
            </div>
        <?}?>
    </div>
    <?//META//?>
    <meta itemprop="name" content="<?=$productTitle?>" />
    <meta itemprop="description" content="<?= !empty($item["~PREVIEW_TEXT"]) ? strip_tags($item["~PREVIEW_TEXT"]) : $productTitle ?>" />
    <? if(empty($item["PROPERTIES"]["BRAND"]["FULL_VALUE"]["PREVIEW_PICTURE"]) && !empty($item["PROPERTIES"]["BRAND"]["FULL_VALUE"])) {?>
        <meta itemprop="brand" content="<?=$item['PROPERTIES']['BRAND']['FULL_VALUE']['NAME']?>" />
    <?}?>
    <?//TITLE//?>
    <div class="product-item-title">
        <a href="<?=$item['DETAIL_PAGE_URL']?>" title="<?=$productTitle?>"><?=$productTitle?></a>
    </div>
    <?//RATING//
    if(isset($item['REVIEWS_COUNT'])) {?>
        <div class="product-item-rating<?=($item['REVIEWS_COUNT'] < 1 ? ' hidden-xs hidden-sm' : '')?>">
            <?if($item['REVIEWS_COUNT'] > 0) {?>
                <div class="product-item-rating-val"<?=($item['RATING_VALUE'] <= 4.4 ? ' data-rate="'.intval($item['RATING_VALUE']).'"' : '')?>><?=$item['RATING_VALUE']?></div>
                <?$arReviewsDeclension = new Bitrix\Main\Grid\Declension(Loc::getMessage('CT_BCI_TPL_MESS_REVIEW'), Loc::getMessage('CT_BCI_TPL_MESS_REVIEWS_1'), Loc::getMessage('CT_BCI_TPL_MESS_REVIEWS_2'));?>
                <div class="product-item-rating-reviews-count"><?=$item['REVIEWS_COUNT'].' '.$arReviewsDeclension->get($item['REVIEWS_COUNT'])?></div>
                <?unset($arReviewsDeclension);
            }?>
        </div>
    <?}?>
    <div class="product-item-info-container">
        <div class="product-item-info-block">
            <?//SKU//
            if(!$object && !$partnersUrl && $haveOffers && $arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && !empty($item['OFFERS_PROP'])) {?>
                <div id="<?=$itemIds['TREE_ID']?>">
                    <?foreach($arParams['SKU_PROPS'] as $skuProperty) {
                        $propertyId = $skuProperty['ID'];
                        $skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
                        if(!isset($item['SKU_TREE_VALUES'][$propertyId]))
                            continue;?>
                        <div class="product-item-hidden" data-entity="sku-block">
                            <div class="product-item-scu-container" data-entity="sku-line-block">
                                <div class="product-item-scu-title"><?=$skuProperty['NAME']?></div>
                                <div class="product-item-scu-block">
                                    <div class="product-item-scu-list">
                                        <ul class="product-item-scu-item-list">
                                            <?foreach($skuProperty['VALUES'] as $value) {
                                                if(!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
                                                    continue;

                                                $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                                if($skuProperty['SHOW_MODE'] === 'PICT') {?>
                                                    <li class="product-item-scu-item-color" title="<?=$value['NAME']?>" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>" style="<?=(!empty($value['CODE']) ? 'background-color: #'.$value['CODE'].';' : (!empty($value['PICT']) ? 'background-image: url('.$value['PICT']['SRC'].');' : ''));?>"></li>
                                                <?} else {?>
                                                    <li class="product-item-scu-item-text" title="<?=$value['NAME']?>" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
                                                        <?=$value['NAME']?>
                                                    </li>
                                                <?}
                                            }?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?}?>
                </div>
                <?foreach($arParams['SKU_PROPS'] as $skuProperty) {
                    if(!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
                        continue;

                    $skuProps[] = array(
                        'ID' => $skuProperty['ID'],
                        'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                        'VALUES' => $skuProperty['VALUES'],
                        'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                    );
                }
                unset($skuProperty, $value);
            }
            //BASKET_PROPERTIES//
            if(!$object && !$partnersUrl && !$haveOffers) {
                if($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !empty($item['PRODUCT_PROPERTIES'])) {?>
                    <div class="product-item-hidden" id="<?=$itemIds['BASKET_PROP_DIV']?>">
                        <?if(!empty($item['PRODUCT_PROPERTIES_FILL'])) {
                            foreach($item['PRODUCT_PROPERTIES_FILL'] as $propId => $propInfo) {?>
                                <input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=htmlspecialcharsbx($propInfo['ID'])?>" />
                                <?unset($item['PRODUCT_PROPERTIES'][$propID]);
                            }
                        }
                        if(!empty($item['PRODUCT_PROPERTIES'])) {
                            foreach($item['PRODUCT_PROPERTIES'] as $propId => $propInfo) {?>
                                <div class="product-item-basket-props-container">
                                    <div class="product-item-basket-props-title"><?=$item['PROPERTIES'][$propId]['NAME']?></div>
                                    <div class="product-item-basket-props-block">
                                        <?if($item['PROPERTIES'][$propId]['PROPERTY_TYPE'] === 'L' && $item['PROPERTIES'][$propId]['LIST_TYPE'] === 'C') {?>
                                            <div class="product-item-basket-props-input-radio">
                                                <?foreach($propInfo['VALUES'] as $valueId => $value) {?>
                                                    <label>
                                                        <input type="radio" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=$valueId?>"<?=($valueId == $propInfo['SELECTED'] ? ' checked="checked"' : '');?> />
                                                        <span class="check-container">
															<span class="check"><i class="fa fa-check"></i></span>
														</span>
                                                        <span class="text" title="<?=$value?>"><?=$value?></span>
                                                    </label>
                                                <?}?>
                                            </div>
                                        <?} else {?>
                                            <div class="product-item-basket-props-drop-down" onclick="<?=$obName?>.showBasketPropsDropDownPopup(this, '<?=$propId?>');">
                                                <?$currId = $currVal = false;
                                                foreach($propInfo['VALUES'] as $valueId => $value) {
                                                    if($valueId == $propInfo['SELECTED']) {
                                                        $currId = $valueId;
                                                        $currVal = $value;
                                                    }
                                                }
                                                unset($value);?>
                                                <input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=(!empty($currId) ? $currId : '');?>" />
                                                <div class="drop-down-text" data-entity="current-option"><?=(!empty($currVal) ? $currVal : '');?></div>
                                                <?unset($currVal, $currId);?>
                                                <div class="drop-down-arrow"><i class="icon-arrow-down"></i></div>
                                                <div class="drop-down-popup" data-entity="dropdownContent" style="display: none;">
                                                    <ul>
                                                        <?foreach($propInfo['VALUES'] as $valueId => $value) {?>
                                                            <li><span onclick="<?=$obName?>.selectBasketPropsDropDownPopupItem(this, '<?=$valueId?>');"><?=$value?></span></li>
                                                        <?}
                                                        unset($value);?>
                                                    </ul>
                                                </div>
                                            </div>
                                        <?}?>
                                    </div>
                                </div>
                            <?}
                        }?>
                    </div>
                <?}
            }?>
            <div class="product-item-info">
                <div class="product-item-blocks" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                    <?//PRICE//?>
                    <div class="product-item-price-container" data-entity="price-block">
                        <div id="<?=$itemIds['PRICE_ID']?>">
                            <?if(!empty($price)) {
                                if($haveOffers && ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' || $object || $partnersUrl)) {?>
                                    <?if($item['PROPERTIES']['OPT_PRICE']['VALUE']){?>
                                        опт. <span class="product-item-price-current"><?=CurrencyFormat($item['PROPERTIES']['OPT_PRICE']['VALUE'], "RUB")?></span>
                                        <meta itemprop="price" content="<?=$item['PROPERTIES']['OPT_PRICE']['VALUE']?>" />
                                    <?}?>
                                    <span class="product-item-price-from"><?=Loc::getMessage('CT_BCI_TPL_MESS_PRICE_FROM')?></span>
                                    <span class="product-item-price-current" <?if(!$item['PROPERTIES']['OPT_PRICE']['VALUE']){?>itemprop="price"<?}?> content="<?= $price['PRICE'] ?>"><?=($price['SQ_M_PRICE'] > 0 ? $price['SQ_M_PRINT_PRICE'] : $price['PRINT_PRICE'])?><meta itemprop="priceCurrency" content="<?= $price['CURRENCY'] ?>" /></span>
                                    <span class="product-item-price-measure">/<?=($price['SQ_M_PRICE'] > 0 ? Loc::getMessage('CT_BCI_TPL_MESS_PRICE_MEASURE_SQ_M') : $minOffer['ITEM_MEASURE']['TITLE'])?></span>
                                <?} else {?>
                                    <?if($item['PROPERTIES']['OPT_PRICE']['VALUE']){?>
                                        опт. <span class="product-item-price-current"><?=CurrencyFormat($item['PROPERTIES']['OPT_PRICE']['VALUE'], "RUB")?></span>
                                        <meta itemprop="price" content="<?=$item['PROPERTIES']['OPT_PRICE']['VALUE']?>" />
                                        <span class="product-item-price-not-set" data-entity="price-current-not-set"<?=($price['SQ_M_PRICE'] > 0 ? ' style="display:none;"' : ($price['PRICE'] > 0 ? ' style="display:none;"' : ''))?>><?=Loc::getMessage('CT_BCI_TPL_MESS_PRICE_NOT_SET')?></span>
                                        <span class="product-item-price-measure" data-entity="price-measure"<?=($price['SQ_M_PRICE'] > 0 ? '' : ($price['PRICE'] > 0 ? '' : ' style="display:none;"'))?>>/<?=($price['SQ_M_PRICE'] > 0 ? Loc::getMessage('CT_BCI_TPL_MESS_PRICE_MEASURE_SQ_M') : $actualItem['ITEM_MEASURE']['TITLE'])?></span>
                                    <?}else{?>
                                        <span class="product-item-price-not-set" data-entity="price-current-not-set"<?=($price['SQ_M_PRICE'] > 0 ? ' style="display:none;"' : ($price['PRICE'] > 0 ? ' style="display:none;"' : ''))?>><?=Loc::getMessage('CT_BCI_TPL_MESS_PRICE_NOT_SET')?></span>
                                        <span class="product-item-price-current <?if($item['PROPERTIES']['OPT_PRICE']['VALUE']){?>small<?}?>" data-entity="price-current"<?=($price['SQ_M_PRICE'] > 0 ? '' : ($price['PRICE'] > 0 ? '' : ' style="display:none;"'))?> <?if(!$item['PROPERTIES']['OPT_PRICE']['VALUE']){?>itemprop="price"<?}?> content="<?= $price['PRICE'] ?>"><?=($price['SQ_M_PRICE'] > 0 ? $price['SQ_M_PRINT_PRICE'] : $price['PRINT_PRICE'])?><meta itemprop="priceCurrency" content="<?= $price['CURRENCY'] ?>" /></span>
                                        <span class="product-item-price-measure" data-entity="price-measure"<?=($price['SQ_M_PRICE'] > 0 ? '' : ($price['PRICE'] > 0 ? '' : ' style="display:none;"'))?>>/<?=($price['SQ_M_PRICE'] > 0 ? Loc::getMessage('CT_BCI_TPL_MESS_PRICE_MEASURE_SQ_M') : $actualItem['ITEM_MEASURE']['TITLE'])?></span>
                                    <?}?>
                                <?}
                            }?>
                        </div>
                        <?if($arParams['SHOW_OLD_PRICE'] === 'Y') {?>
                            <div class="product-item-price-old" id="<?=$itemIds['OLD_PRICE_ID']?>"<?=($price['PERCENT'] > 0 ? '' : ' style="display:none;"')?>><?=($price['PERCENT'] > 0 ? ($price['SQ_M_BASE_PRICE'] > 0 ? $price['SQ_M_PRINT_BASE_PRICE'] : $price['PRINT_BASE_PRICE']) : '')?></div>
                            <div class="product-item-price-economy" id="<?=$itemIds['DISCOUNT_PRICE_ID']?>"<?=($price['PERCENT'] > 0 ? '' : ' style="display:none;"')?>><?=($price['PERCENT'] > 0 ? Loc::getMessage('CT_BCI_TPL_MESS_PRICE_ECONOMY', array('#ECONOMY#' => ($price['SQ_M_DISCOUNT'] > 0 ? $price['SQ_M_PRINT_DISCOUNT'] : $price['PRINT_DISCOUNT']))) : '')?></div>
                        <?}?>
                    </div>
                    <?//QUANTITY_LIMIT//
                    if($arParams['SHOW_MAX_QUANTITY'] !== 'N') {
                        if($haveOffers) {
                            if($arParams['PRODUCT_DISPLAY_MODE'] === 'Y') {?>
                                <div class="product-item-hidden" id="<?=$itemIds['QUANTITY_LIMIT']?>" style="display: none;">
                                    <div class="product-item-quantity">
                                        <i class="icon-ok-b product-item-quantity-icon"></i>
                                        <span class="product-item-quantity-val">
											<?=$arParams['MESS_SHOW_MAX_QUANTITY'].'&nbsp;'?>
											<span data-entity="quantity-limit-value"></span>
											<link itemprop="availability" href="http://schema.org/InStock" />
										</span>
                                    </div>
                                </div>
                                <div class="product-item-hidden" id="<?=$itemIds['QUANTITY_LIMIT_NOT_AVAILABLE']?>" style="display: none;">
                                    <div class="product-item-quantity product-item-quantity-not-avl">
                                        <i class="icon-close-b product-item-quantity-icon"></i>
                                        <span class="product-item-quantity-val">
											<?=$arParams['MESS_NOT_AVAILABLE']?>
											<link itemprop="availability" href="http://schema.org/SoldOut" />
										</span>
                                    </div>
                                </div>
                            <?}
                        } else {?>
                            <div class="product-item-hidden" id="<?=$itemIds['QUANTITY_LIMIT']?>">
                                <div class="product-item-quantity<?=($actualItem['CAN_BUY'] ? '' : ' product-item-quantity-not-avl')?>">
                                    <i class="icon-<?=($actualItem['CAN_BUY'] ? 'ok' : 'close')?>-b product-item-quantity-icon"></i>
                                    <span class="product-item-quantity-val">
										<?if($actualItem['CAN_BUY']) {
                                            echo $arParams['MESS_SHOW_MAX_QUANTITY'].'&nbsp;';
                                            if($measureRatio && (float)$actualItem['CATALOG_QUANTITY'] > 0 && $actualItem['CATALOG_QUANTITY_TRACE'] === 'Y' && $actualItem['CATALOG_CAN_BUY_ZERO'] === 'N') {
                                                if($arParams['SHOW_MAX_QUANTITY'] === 'M') {
                                                    if((float)$actualItem['CATALOG_QUANTITY'] / $measureRatio >= $arParams['RELATIVE_QUANTITY_FACTOR']) {
                                                        echo $arParams['MESS_RELATIVE_QUANTITY_MANY'];
                                                        echo '<link itemprop="availability" href="http://schema.org/InStock" />';
                                                    } else {
                                                        echo $arParams['MESS_RELATIVE_QUANTITY_FEW'];
                                                        echo '<link itemprop="availability" href="http://schema.org/LimitedAvailability" />';
                                                    }
                                                } else {
                                                    echo $actualItem['CATALOG_QUANTITY'];
                                                }
                                            }?>
                                        <?} else {
                                            echo $arParams['MESS_NOT_AVAILABLE'];
                                            echo '<link itemprop="availability" href="http://schema.org/SoldOut" />';
                                        }?>
									</span>
                                </div>
                            </div>
                        <?}
                    }
                    //QUANTITY//
                    if($arParams['USE_PRODUCT_QUANTITY'] && !$object && !$partnersUrl && ((!$haveOffers && $actualItem['CAN_BUY']) || ($haveOffers && $arParams['PRODUCT_DISPLAY_MODE'] === 'Y'))) {?>
                        <div class="product-item-hidden" data-entity="quantity-block">
                            <?if(!empty($item['PROPERTIES']['M2_COUNT']['VALUE'])) {?>
                                <div class="product-item-amount"<?=($isMeasurePc || $isMeasureSqM ? '' : ' style="display: none;"')?>>
                                    <a class="product-item-amount-btn-minus" id="<?=$itemIds['PC_QUANTITY_DOWN_ID']?>" href="javascript:void(0)" rel="nofollow">-</a>
                                    <input class="product-item-amount-input" id="<?=$itemIds['PC_QUANTITY_ID']?>" type="tel" value="<?=$price['PC_MIN_QUANTITY']?>" />
                                    <a class="product-item-amount-btn-plus" id="<?=$itemIds['PC_QUANTITY_UP_ID']?>" href="javascript:void(0)" rel="nofollow">+</a>
                                    <div class="product-item-amount-measure"><?=Loc::getMessage('CT_BCI_TPL_MESS_MEASURE_PC')?></div>
                                </div>
                                <div class="product-item-amount"<?=($isMeasurePc || $isMeasureSqM ? '' : ' style="display: none;"')?>>
                                    <a class="product-item-amount-btn-minus" id="<?=$itemIds['SQ_M_QUANTITY_DOWN_ID']?>" href="javascript:void(0)" rel="nofollow">-</a>
                                    <input class="product-item-amount-input" id="<?=$itemIds['SQ_M_QUANTITY_ID']?>" type="tel" value="<?=$price['SQ_M_MIN_QUANTITY']?>" />
                                    <a class="product-item-amount-btn-plus" id="<?=$itemIds['SQ_M_QUANTITY_UP_ID']?>" href="javascript:void(0)" rel="nofollow">+</a>
                                    <div class="product-item-amount-measure"><?=Loc::getMessage('CT_BCI_TPL_MESS_MEASURE_SQ_M')?></div>
                                </div>
                                <?if($haveOffers) {?>
                                    <div class="product-item-amount"<?=($isMeasurePc || $isMeasureSqM ? ' style="display: none;"' : '')?>>
                                        <a class="product-item-amount-btn-minus" id="<?=$itemIds['QUANTITY_DOWN_ID']?>" href="javascript:void(0)" rel="nofollow">-</a>
                                        <input class="product-item-amount-input" id="<?=$itemIds['QUANTITY_ID']?>" type="tel" value="<?=$price['MIN_QUANTITY']?>" />
                                        <a class="product-item-amount-btn-plus" id="<?=$itemIds['QUANTITY_UP_ID']?>" href="javascript:void(0)" rel="nofollow">+</a>
                                        <div class="product-item-amount-measure" id="<?=$itemIds['QUANTITY_MEASURE']?>"><?=$actualItem["ITEM_MEASURE"]["TITLE"]?></div>
                                    </div>
                                <?}
                            } else {?>
                                <div class="product-item-amount">
                                    <a class="product-item-amount-btn-minus" id="<?=$itemIds['QUANTITY_DOWN_ID']?>" href="javascript:void(0)" rel="nofollow">-</a>
                                    <input class="product-item-amount-input" id="<?=$itemIds['QUANTITY_ID']?>" type="tel" name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>" value="<?=$price['MIN_QUANTITY']?>" />
                                    <a class="product-item-amount-btn-plus" id="<?=$itemIds['QUANTITY_UP_ID']?>" href="javascript:void(0)" rel="nofollow">+</a>
                                    <div class="product-item-amount-measure" id="<?=$itemIds['QUANTITY_MEASURE']?>"><?=$actualItem['ITEM_MEASURE']['TITLE']?></div>
                                </div>
                            <?}?>
                        </div>
                    <?}?>
                </div>
                <?//BUTTONS//?>
                <div class="product-item-button-container" data-entity="buttons-block">
                    <?if($haveOffers && ($arParams['PRODUCT_DISPLAY_MODE'] != 'Y' || $object || $partnersUrl) || (($object || $partnersUrl) && !$haveOffers) || $arParams['DISABLE_BASKET']) {?>
                    <a<?=(($object || $partnersUrl) ? ' target="_blank"' : '')?> class="btn btn-buy" href="<?=$item['DETAIL_PAGE_URL']?>" title="<?=$arParams['MESS_BTN_DETAIL']?>"><i class="icon-arrow-right"></i></a>
                        <?} else {
                            if(!$haveOffers) {
                                if($actualItem['CAN_BUY'] || (!$actualItem['CAN_BUY'] && !$showSubscribe)) {?>
                                    <div id="<?=$itemIds['BASKET_ACTIONS_ID']?>">
                                        <button type="button" class="btn btn-buy" id="<?=$itemIds['BUY_LINK']?>" title="<?=($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET'])?>"<?=($actualItem['CAN_BUY'] && $price['RATIO_PRICE'] > 0 ? '' : ' disabled="disabled"')?>><i class="icon-cart"></i></button>
                                    </div>
                                <?} elseif(!$actualItem['CAN_BUY'] && $showSubscribe) {?>
                                    <?$APPLICATION->IncludeComponent('bitrix:catalog.product.subscribe', '',
                                        array(
                                            'PRODUCT_ID' => $actualItem['ID'],
                                            'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                            'BUTTON_CLASS' => 'btn btn-buy',
                                            'DEFAULT_DISPLAY' => true,
                                            'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                                        ),
                                        $component,
                                        array('HIDE_ICONS' => 'Y')
                                    );?>
                                <?}
                            } else {?>
                                <div id="<?=$itemIds['BASKET_ACTIONS_ID']?>">
                                    <button type="button" class="btn btn-buy" id="<?=$itemIds['BUY_LINK']?>" title="<?=($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET'])?>"<?=($actualItem['CAN_BUY'] ? ($price['RATIO_PRICE'] > 0 ? '' : ' disabled="disabled"') : ($showSubscribe ? ' style="display: none;"' : ' disabled="disabled"'))?>><i class="icon-cart"></i></button>
                                </div>
                                <?if($showSubscribe) {?>
                                    <?$APPLICATION->IncludeComponent('bitrix:catalog.product.subscribe', '',
                                        array(
                                            'PRODUCT_ID' => $actualItem['ID'],
                                            'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                            'BUTTON_CLASS' => 'btn btn-buy',
                                            'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                                            'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                                        ),
                                        $component,
                                        array('HIDE_ICONS' => 'Y')
                                    );?>
                                <?}
                            }
                        }?>
                </div>
            </div>
        </div>
    </div>
    <?//TOTAL_COST//
    if($arParams['USE_PRODUCT_QUANTITY'] && !$object && !$partnersUrl && ((!$haveOffers && $actualItem['CAN_BUY']) || ($haveOffers && $arParams['PRODUCT_DISPLAY_MODE'] === 'Y'))) {?>
        <div class="product-item-total-cost product-item-hidden" id="<?=$itemIds['TOTAL_COST_ID']?>"<?=($price['MIN_QUANTITY'] != 1 || (!empty($item['PROPERTIES']['M2_COUNT']['VALUE']) && ($price['PC_MIN_QUANTITY'] != 1 || $price['SQ_M_MIN_QUANTITY'] != 1)) ? '' : ' style="display:none;"')?>><?=Loc::getMessage('CT_BCI_TPL_MESS_TOTAL_COST')?><span data-entity="total-cost"><?=($price['MIN_QUANTITY'] != 1 || (!empty($item['PROPERTIES']['M2_COUNT']['VALUE']) && ($price['PC_MIN_QUANTITY'] != 1 || $price['SQ_M_MIN_QUANTITY'] != 1)) ? $price['PRINT_RATIO_PRICE'] : '')?></span></div>
    <?}
    //COMPARE//
    if($arParams['DISPLAY_COMPARE']) {?>
        <div class="product-item-compare product-item-hidden">
            <label id="<?=$itemIds['COMPARE_LINK']?>">
                <input type="checkbox" data-entity="compare-checkbox">
                <span class="product-item-compare-checkbox"><i class="icon-ok-b"></i></span>
                <span class="product-item-compare-title" data-entity="compare-title"><?=$arParams["MESS_BTN_COMPARE"]?></span>
            </label>
        </div>
    <?}?>
</div>