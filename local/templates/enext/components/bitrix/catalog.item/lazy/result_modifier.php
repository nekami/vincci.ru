<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//ITEM_START_PRICE//
if(!empty($arResult['ITEM']['OFFERS']) && ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' || !empty($arResult['ITEM']['PROPERTIES']['OBJECT']['VALUE']) || !empty($arResult['ITEM']['PROPERTIES']['PARTNERS_URL']['VALUE']))) {
	$arResult['ITEM']['ITEM_START_PRICE'] = null;
	$arResult['ITEM']['ITEM_START_PRICE_SELECTED'] = null;
	
	$minPrice = null;
	$minPriceIndex = null;
	foreach($arResult['ITEM']['OFFERS'] as $key => $arOffer) {
		if(!$arOffer['CAN_BUY'] || $arOffer['ITEM_PRICE_SELECTED'] === null)
			continue;

		$priceScale = $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]['RATIO_PRICE'];		
		if($priceScale <= 0)
			continue;
		
		if($minPrice === null || $minPrice > $priceScale) {
			$minPrice = $priceScale;
			$minPriceIndex = $key;
		}
		unset($priceScale);
	}
	unset($arOffer, $key);
	
	if($minPriceIndex !== null) {
		$minOffer = $arResult['ITEM']['OFFERS'][$minPriceIndex];
		$arResult['ITEM']['ITEM_START_PRICE_SELECTED'] = $minPriceIndex;
		$arResult['ITEM']['ITEM_START_PRICE'] = $minOffer['ITEM_PRICES'][$minOffer['ITEM_PRICE_SELECTED']];
	}
	unset($minOffer, $minPriceIndex, $minPrice);
}