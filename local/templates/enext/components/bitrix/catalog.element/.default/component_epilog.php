<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->SetTitle($arResult["NAME"]);

use Bitrix\Main\Loader,
	Bitrix\Sale;

//CURRENCIES//
if(!empty($templateData['TEMPLATE_LIBRARY'])) {
	$loadCurrency = false;
	if(!empty($templateData['CURRENCIES'])) {
		$loadCurrency = Loader::includeModule('currency');
	}
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if($loadCurrency) {?>
		<script type="text/javascript">
			BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
		</script>
	<?}
}

if(isset($templateData['JS_OBJ'])) {
	$item = $templateData['ITEM'];

	//CHECK_COMPARED//
	if($arParams["DISPLAY_COMPARE"]) {
		$compared = false;
		$comparedIds = array();		
		
		if(!empty($_SESSION[$arParams["COMPARE_NAME"]][$item["IBLOCK_ID"]])) {
			if(!empty($item["JS_OFFERS"])) {
				foreach($item["JS_OFFERS"] as $key => $offer) {
					if(array_key_exists($offer["ID"], $_SESSION[$arParams["COMPARE_NAME"]][$item["IBLOCK_ID"]]["ITEMS"])) {
						if($key == $item["OFFERS_SELECTED"]) {
							$compared = true;
						}
						$comparedIds[] = $offer["ID"];
					}
				}
			} elseif(array_key_exists($item["ID"], $_SESSION[$arParams["COMPARE_NAME"]][$item["IBLOCK_ID"]]["ITEMS"])) {
				$compared = true;
			}
		}
	}
	
	//CHECK_DELAYED_BUYED_ADDED//
	$delayed = $buyedAdded = false;
	$delayedIds = $buyedAddedIds = array();
	
	if(Loader::includeModule('sale')) {
		$fuserId = Sale\Fuser::getId(true);
		$dbItems = CSaleBasket::GetList(
			array("NAME" => "ASC", "ID" => "ASC"),
			array(			
				"LID" => SITE_ID,			
				"CAN_BUY" => "Y",
				"FUSER_ID" => $fuserId,
				"ORDER_ID" => "NULL"
			),
			false,
			false,
			array("ID", "PRODUCT_ID", "DELAY")
		);
		while($arItem = $dbItems->GetNext()) {
			if(CSaleBasketHelper::isSetItem($arItem))
				continue;
			if(!empty($item['JS_OFFERS'])) {
				foreach($item['JS_OFFERS'] as $key => $offer) {
					if($offer['ID'] == $arItem["PRODUCT_ID"]) {
						if($key == $item['OFFERS_SELECTED']) {
							if($arItem["DELAY"] == "Y")
								$delayed = true;
							else
								$buyedAdded = true;
						}					
						if($arItem["DELAY"] == "Y")
							$delayedIds[] = $offer['ID'];
						else
							$buyedAddedIds[] = $offer['ID'];
					}
				}
			} elseif($item['ID'] == $arItem["PRODUCT_ID"]) {
				if($arItem["DELAY"] == "Y")
					$delayed = true;
				else
					$buyedAdded = true;
			}
		}
	}
	
	//SELECT_TARGET_OFFER//
	$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
	$offerNum = false;
	$offerId = (int)$this->request->get('OFFER_ID');
	$offerCode = $this->request->get('OFFER_CODE');

	if($offerId > 0 && !empty($templateData['OFFER_IDS']) && is_array($templateData['OFFER_IDS'])) {
		$offerNum = array_search($offerId, $templateData['OFFER_IDS']);
	} elseif(!empty($offerCode) && !empty($templateData['OFFER_CODES']) && is_array($templateData['OFFER_CODES'])) {
		$offerNum = array_search($offerCode, $templateData['OFFER_CODES']);
	}
	
	//JS//?>
	<script type="text/javascript">
		BX.ready(BX.defer(function() {
			if(!!window.<?=$templateData['JS_OBJ']?>) {
				//VIEWED_COUNT//
				window.<?=$templateData['JS_OBJ']?>.allowViewedCount(true);

				//CHECK_COMPARED//
				<?if($arParams["DISPLAY_COMPARE"]) {?>
					window.<?=$templateData['JS_OBJ']?>.setCompared('<?=$compared?>');
					<?if(!empty($comparedIds)) {?>
						window.<?=$templateData['JS_OBJ']?>.setCompareInfo(<?=CUtil::PhpToJSObject($comparedIds, false, true)?>);
					<?}
				}?>
				
				//CHECK_DELAYED//
				window.<?=$templateData['JS_OBJ']?>.setDelayed('<?=$delayed?>');
				<?if(!empty($delayedIds)) {?>
					window.<?=$templateData['JS_OBJ']?>.setDelayInfo(<?=CUtil::PhpToJSObject($delayedIds, false, true)?>);
				<?}?>
				
				//CHECK_BUYED_ADDED//
				window.<?=$templateData['JS_OBJ']?>.setBuyedAdded('<?=$buyedAdded?>');
				<?if(!empty($buyedAddedIds)) {?>
					window.<?=$templateData['JS_OBJ']?>.setBuyAddInfo(<?=CUtil::PhpToJSObject($buyedAddedIds, false, true)?>);
				<?}
				
				//SELECT_TARGET_OFFER//				
				if(!empty($offerNum) || $offerNum === 0) {?>
					window.<?=$templateData['JS_OBJ']?>.setOffer(<?=$offerNum?>);
				<?}?>
			}
		}));
	</script>
<?}

//SECTION_PATH//
$bgImage = !empty($arResult['BACKGROUND_IMAGE']) && is_array($arResult['BACKGROUND_IMAGE']);
$addSectionsChain = $arParams["ADD_SECTIONS_CHAIN_EPILOG"] == "Y";
$addElementChain = $arParams["ADD_ELEMENT_CHAIN_EPILOG"] == "Y";
if(!$bgImage || $addSectionsChain) {
	foreach($arResult['SECTION']['PATH'] as $path) {
		$ipropValues = new Bitrix\Iblock\InheritedProperty\SectionValues($arResult['IBLOCK_ID'], $path['ID']);
		$path['IPROPERTY_VALUES'] = $ipropValues->getValues();
		$arCurElement['SECTION_PATH'][$path['ID']] = $path;
	}
	unset($ipropValues, $path);

	$arFilter = array(
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'ACTIVE' => 'Y',
		'GLOBAL_ACTIVE' => 'Y',
		'ID' => array_keys($arCurElement['SECTION_PATH'])
	);

	$arSelect = array('ID', 'IBLOCK_ID');
	if(!$bgImage)
		$arSelect[] = 'UF_BACKGROUND_IMAGE';
	if($addSectionsChain)
		$arSelect[] = 'UF_BREADCRUMB_TITLE';

	$obCache = new CPHPCache();
	if($obCache->InitCache($arParams["CACHE_TIME"], serialize($arFilter), "/iblock/catalog")) {
		$arCurElement = $obCache->GetVars();
	} elseif($obCache->StartDataCache()) {		
		if(Loader::includeModule("iblock")) {	
			$rsSections = CIBlockSection::GetList(
				array('DEPTH_LEVEL' => 'DESC'),	
				$arFilter,
				false,
				$arSelect
			);
			global $CACHE_MANAGER;
			$CACHE_MANAGER->StartTagCache("/iblock/catalog");
			$CACHE_MANAGER->RegisterTag("iblock_id_".$arResult["IBLOCK_ID"]);
			while($arSection = $rsSections->GetNext()) {
				if(!$bgImage && !isset($arCurElement['BACKGROUND_IMAGE']) && $arSection['UF_BACKGROUND_IMAGE'] > 0) {
					$arCurElement['BACKGROUND_IMAGE'] = CFile::GetFileArray($arSection['UF_BACKGROUND_IMAGE']);
				}
				if($addSectionsChain)
					$arCurElement['SECTION_PATH'][$arSection['ID']]['BREADCRUMB_TITLE'] = $arSection['UF_BREADCRUMB_TITLE'];
			}
			unset($arSection, $rsSections);
			$CACHE_MANAGER->EndTagCache();
		}
		$obCache->EndDataCache($arCurElement);
	}
}

//BACKGROUND_IMAGE//
if(!$bgImage && is_array($arCurElement['BACKGROUND_IMAGE'])) {
	$APPLICATION->SetPageProperty(
		'backgroundImage',
		'style="background-image:url(\''.CHTTP::urnEncode($arCurElement['BACKGROUND_IMAGE']['SRC'], 'UTF-8').'\')"'
	);
}
unset($bgImage);

//BREADCRUMBS//
if($addSectionsChain) {
	foreach($arCurElement['SECTION_PATH'] as $path) {
		if(!empty($path["BREADCRUMB_TITLE"]))
			$APPLICATION->AddChainItem($path["BREADCRUMB_TITLE"], $path['~SECTION_PAGE_URL']);
		elseif(!empty($path['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']))
			$APPLICATION->AddChainItem($path['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'], $path['~SECTION_PAGE_URL']);
		else
			$APPLICATION->AddChainItem($path['NAME'], $path['~SECTION_PAGE_URL']);
	}
}
unset($addSectionsChain);

if($addElementChain) {
	if(!empty($arResult["BREADCRUMB_TITLE"]))
		$APPLICATION->AddChainItem($arResult["BREADCRUMB_TITLE"], $arResult['~DETAIL_PAGE_URL']);
	elseif(!empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']))
		$APPLICATION->AddChainItem($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'], $arResult['~DETAIL_PAGE_URL']);
	else
		$APPLICATION->AddChainItem($arResult['NAME'], $arResult['~DETAIL_PAGE_URL']);
}
unset($addElementChain);

//META_PROPERTY//
$APPLICATION->AddHeadString("<meta property='og:type' content='product' />", true);

$ogTitle = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) ? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arResult['NAME'];
$APPLICATION->AddHeadString("<meta property='og:title' content='".$ogTitle."' />", true);
unset($ogTitle);

$ogPreviewText = $arResult['PREVIEW_TEXT'];
if(!empty($ogPreviewText))
	$APPLICATION->AddHeadString("<meta property='og:description' content='".strip_tags($ogPreviewText)."' />", true);
unset($ogPreviewText);

$ogScheme = CMain::IsHTTPS() ? "https" : "http";
$ogPhoto = $arResult['DETAIL_PICTURE_EPILOG'];
$APPLICATION->AddHeadString("<meta property='og:url' content='".$ogScheme."://".SITE_SERVER_NAME.$APPLICATION->GetCurPage()."' />", true);
if(!empty($ogPhoto)) {
	$APPLICATION->AddHeadString("<meta property='og:image' content='".$ogScheme."://".SITE_SERVER_NAME.$ogPhoto['SRC']."' />", true);
	$APPLICATION->AddHeadString("<meta property='og:image:width' content='".$ogPhoto['WIDTH']."' />", true);
	$APPLICATION->AddHeadString("<meta property='og:image:height' content='".$ogPhoto['HEIGHT']."' />", true);
	$APPLICATION->AddHeadString("<link rel='image_src' href='".$ogScheme."://".SITE_SERVER_NAME.$ogPhoto['SRC']."' />", true);
}
unset($ogPhoto, $ogScheme);