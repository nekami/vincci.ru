(function(window){
	'use strict';

	if(window.JCCatalogElement)
		return;
	
	window.JCCatalogElement = function(arParams) {
		this.productType = 0;

		this.config = {
			useCatalog: true,
			showQuantity: true,
			showPrice: true,
			showAbsent: true,
			showOldPrice: false,
			showPercent: false,
			showSkuProps: false,
			showOfferGroup: false,
			useCompare: false,
			useSubscribe: false,	
			usePopup: false,
			useMagnifier: false,
			usePriceRanges: false,
			basketAction: ['BUY'],			
			showSlider: false,
			sliderInterval: 5000,
			useEnhancedEcommerce: false,
			dataLayerName: 'dataLayer',
			brandProperty: false,
			alt: '',
			title: '',
			magnifierZoomPercent: 200
		};

		this.checkQuantity = false;
		this.maxQuantity = 0;
		this.maxPcQuantity = 0;
		this.maxSqMQuantity = 0;
		this.minQuantity = 0;
		this.minPcQuantity = 0;
		this.minSqMQuantity = 0;
		this.stepQuantity = 1;
		this.stepPcQuantity = 1;
		this.stepSqMQuantity = 0.01;
		this.isDblQuantity = false;
		this.canBuy = true;
		this.canSubscription = true;
		this.currentIsSet = false;
		this.updateViewedCount = false;

		this.currentPriceMode = '';
		this.currentPrices = [];
		this.currentPriceSelected = 0;
		this.currentQuantityRanges = [];
		this.currentQuantityRangeSelected = 0;
		this.currentMeasure = [];

		this.precision = 6;
		this.precisionFactor = Math.pow(10, this.precision);

		this.visual = {};
		this.basketMode = '';
		this.product = {
			checkQuantity: false,
			maxQuantity: 0,
			maxPcQuantity: 0,
			maxSqMQuantity: 0,
			stepQuantity: 1,
			stepPcQuantity: 1,
			stepSqMQuantity: 0.01,			
			isDblQuantity: false,
			canBuy: true,
			canSubscription: true,
			name: '',
			pict: {},
			id: 0,
			addUrl: '',
			buyUrl: '',
			slider: {},
			sliderCount: 0,			
			useSlider: false,
			sliderPict: []
		};
		this.mess = {};

		this.basketData = {			
			quantity: 'quantity',
			props: 'prop',
			basketUrl: '',
			sku_props: '',
			sku_props_var: 'basket_props',
			add_url: '',
			buy_url: ''
		};
		this.delayData = {
			delayPath: ''
		};
		this.compareData = {
			compareUrl: '',
			compareDeleteUrl: '',
			comparePath: ''
		};

		this.object = {
			id: 0,
			name: '',
			address: '',
			timezone: '',
			workingHours: {},
			phone: {},				
			email: {},
			skype: {},
			callbackForm: false
		};

		this.moreProducts = {
			ids: []
		};

		this.defaultPict = {
			preview: null,
			detail: null
		};

		this.offers = [];
		this.offerNum = 0;
		this.treeProps = [];
		this.selectedValues = {};

		this.mouseTimer = null;
		this.isTouchDevice = BX.hasClass(document.documentElement, 'bx-touch');
		this.touch = null;
		this.slider = {
			interval: null,
			progress: null,
			paused: null,
			controls: []
		};

		this.obProduct = null;
		this.obQuantity = null;
		this.obQuantityUp = null;
		this.obQuantityDown = null;
		this.obPcQuantity = null;
		this.obPcQuantityUp = null;
		this.obPcQuantityDown = null;
		this.obSqMQuantity = null;
		this.obSqMQuantityUp = null;
		this.obSqMQuantityDown = null;
		this.obPrice = null;
		this.obPriceNotSet = null;
		this.obPriceCurrent = null;		
		this.obPriceOld = null;
		this.obPriceDiscount = null;
		this.obPricePercent = null;
		this.obPricePercentVal = null;
		this.obTotalCost = null;
		this.obTotalCostVal = null;
		
		this.obTree = null;
		this.obPriceRanges = null;		
		this.obBuyBtn = null;
		this.obAddToBasketBtn = null;		
		this.obBasketActions = null;
		this.obPartnersBtn = null;
		this.obAskPrice = null;
		this.obNotAvail = null;
		this.obSubscribe = null;
		this.obQuickOrder = null;
		this.obArticle = {};
		this.obSkuProps = null;
		this.obMainSkuProps = null;
		this.obBigSlider = null;
		this.obMeasure = null;
		this.obQuantityLimit = {
			all: null,
			value: null
		};
		this.obQuantityLimitNotAvl = {};		
		this.obDelay = null;
		this.obCompare = null;
		this.obConstructor = null;

		this.node = {};
		this.shortCardNodes = {};
		
		this.magnify = {
			enabled: false,
			obBigImg: null,
			obBigSlider: null,
			height: 0,
			width: 0,
			timer: 0
		};
		this.currentImg = {
			id: 0,
			src: '',
			width: 0,
			height: 0
		};
		this.viewedCounter = {
			path: '/bitrix/components/bitrix/catalog.element/ajax.php',
			params: {
				AJAX: 'Y',
				SITE_ID: '',
				PRODUCT_ID: 0,
				PARENT_ID: 0
			}
		};

		this.obPopupWin = null;
		this.basketUrl = '';
		this.basketParams = {};

		this.errorCode = 0;

		if(typeof arParams === 'object') {
			this.params = arParams;
			this.initConfig();

			if(this.params.MESS) {
				this.mess = this.params.MESS;
			}

			switch(this.productType) {
				case 0: //no catalog
				case 1: //product
				case 2: //set
					this.initProductData();
					break;
				case 3: //sku
					this.initOffersData();
					break;
				default:
					this.errorCode = -1;
			}

			this.initBasketData();			
			this.initDelayData();
			this.initCompareData();

			this.initObjectData();

			this.initMoreProductsData();
		}

		if(this.errorCode === 0) {
			BX.ready(BX.delegate(this.init, this));
		}

		this.params = {};
	};

	window.JCCatalogElement.prototype = {
		getEntity: function(parent, entity, additionalFilter) {
			if(!parent || !entity)
				return null;

			additionalFilter = additionalFilter || '';

			return parent.querySelector(additionalFilter + '[data-entity="' + entity + '"]');
		},

		getEntities: function(parent, entity, additionalFilter) {
			if(!parent || !entity)
				return {length: 0};

			additionalFilter = additionalFilter || '';

			return parent.querySelectorAll(additionalFilter + '[data-entity="' + entity + '"]');
		},
			
		setOffer: function(offerNum) {
			this.offerNum = parseInt(offerNum);
			this.setCurrent();
		},

		init: function() {
			var i = 0,
				j = 0,
				treeItems = null;

			this.obProduct = BX(this.visual.ID);
			if(!this.obProduct) {
				this.errorCode = -1;
			}

			this.obBigSlider = BX(this.visual.BIG_SLIDER_ID);
			this.node.videoImageContainer = this.getEntity(this.obProduct, 'videos-images-container');			
			this.node.sliderProgressBar = this.getEntity(this.obProduct, 'slider-progress-bar');
			this.node.sliderControlLeft = this.getEntity(this.obBigSlider, 'slider-control-left');
			this.node.sliderControlRight = this.getEntity(this.obBigSlider, 'slider-control-right');
			this.node.sliderMagnifier = this.getEntity(this.obBigSlider, 'slider-magnifier');

			if(!this.obBigSlider || !this.node.videoImageContainer) {
				this.errorCode = -2;
			}

			if(this.config.showPrice) {
				this.obPrice = BX(this.visual.PRICE_ID);
				this.obPriceNotSet = this.getEntity(this.obPrice, 'price-current-not-set');
				this.obPriceCurrent = this.getEntity(this.obPrice, 'price-current');
				this.obPriceMeasure = this.getEntity(this.obPrice, 'price-measure');
				if(!this.obPrice && this.config.useCatalog) {
					this.errorCode = -16;
				} else {
					if(this.config.showOldPrice) {
						this.obPriceOld = BX(this.visual.OLD_PRICE_ID);
						this.obPriceDiscount = BX(this.visual.DISCOUNT_PRICE_ID);

						if(!this.obPriceOld || !this.obPriceDiscount) {
							this.config.showOldPrice = false;
						}
					}

					if(this.config.showPercent) {
						this.obPricePercent = BX(this.visual.DISCOUNT_PERCENT_ID);
						this.obPricePercentVal = this.getEntity(this.obPricePercent, 'dsc-perc-val');						
						if(!this.obPricePercent) {
							this.config.showPercent = false;
						}
					}
				}

				this.obBasketActions = BX(this.visual.BASKET_ACTIONS_ID);
				if(this.obBasketActions) {
					if(BX.util.in_array('BUY', this.config.basketAction)) {
						this.obBuyBtn = BX(this.visual.BUY_LINK);
					}
					if(BX.util.in_array('ADD', this.config.basketAction)) {
						this.obAddToBasketBtn = BX(this.visual.ADD_BASKET_LINK);
					}
				}
				this.obPartnersBtn = BX(this.visual.PARTNERS_LINK);
				this.obAskPrice = BX(this.visual.ASK_PRICE_LINK);
				this.obNotAvail = BX(this.visual.NOT_AVAILABLE_MESS);
				this.obQuickOrder = BX(this.visual.QUICK_ORDER_LINK);
				this.obWholesaler = BX(this.visual.WHOLESALER_LINK);
				this.obConsultation = BX(this.visual.CONSULTATION_LINK);
				this.obOneclick = BX(this.visual.ONECLICK_LINK);
				this.obRequestPrice = BX(this.visual.REQUEST_PRICE_LINK);
			}

			if(this.config.showQuantity) {				
				this.node.quantity = this.getEntity(this.obProduct, 'quantity-block');
				
				this.obQuantity = BX(this.visual.QUANTITY_ID);
				if(this.visual.QUANTITY_UP_ID) {
					this.obQuantityUp = BX(this.visual.QUANTITY_UP_ID);
				}
				if(this.visual.QUANTITY_DOWN_ID) {
					this.obQuantityDown = BX(this.visual.QUANTITY_DOWN_ID);
				}
				
				this.obPcQuantity = BX(this.visual.PC_QUANTITY_ID);				
				if(this.visual.PC_QUANTITY_UP_ID) {
					this.obPcQuantityUp = BX(this.visual.PC_QUANTITY_UP_ID);
				}
				if(this.visual.PC_QUANTITY_DOWN_ID) {
					this.obPcQuantityDown = BX(this.visual.PC_QUANTITY_DOWN_ID);
				}

				this.obSqMQuantity = BX(this.visual.SQ_M_QUANTITY_ID);				
				if(this.visual.SQ_M_QUANTITY_UP_ID) {
					this.obSqMQuantityUp = BX(this.visual.SQ_M_QUANTITY_UP_ID);
				}
				if(this.visual.SQ_M_QUANTITY_DOWN_ID) {
					this.obSqMQuantityDown = BX(this.visual.SQ_M_QUANTITY_DOWN_ID);
				}
				
				this.obTotalCost = BX(this.visual.TOTAL_COST_ID);
				this.obTotalCostVal = !!this.obTotalCost && this.getEntity(this.obTotalCost, 'total-cost');
			}

			if(this.productType === 3) {
				if(this.visual.TREE_ID) {
					this.obTree = BX(this.visual.TREE_ID);
					if(!this.obTree) {
						this.errorCode = -256;
					}
				}

				if(this.visual.QUANTITY_MEASURE) {
					this.obMeasure = BX(this.visual.QUANTITY_MEASURE);
				}

				if(this.visual.QUANTITY_LIMIT && this.config.showMaxQuantity !== 'N') {
					this.obQuantityLimit.all = BX(this.visual.QUANTITY_LIMIT);
					if(this.obQuantityLimit.all) {
						this.obQuantityLimit.value = this.getEntity(this.obQuantityLimit.all, 'quantity-limit-value');
						if(!this.obQuantityLimit.value) {
							this.obQuantityLimit.all = null;
						}
					}
				}

				if(this.visual.QUANTITY_LIMIT_NOT_AVAILABLE && this.config.showMaxQuantity !== 'N') {
					this.obQuantityLimitNotAvl.all = BX(this.visual.QUANTITY_LIMIT_NOT_AVAILABLE);
				}

				if(this.config.usePriceRanges) {
					this.obPriceRanges = this.getEntity(this.obProduct, 'price-ranges-block');
				}
			}

			if(this.visual.ARTICLE_ID) {
				this.obArticle.all = BX(this.visual.ARTICLE_ID);
				if(this.obArticle.all) {
					this.obArticle.value = this.getEntity(this.obArticle.all, 'article-value');
				}
			}

			if(this.config.showSkuProps) {
				this.obSkuProps = BX(this.visual.DISPLAY_PROP_DIV);
				this.obMainSkuProps = BX(this.visual.DISPLAY_MAIN_PROP_DIV);
			}
			
			this.obDelay = BX(this.visual.DELAY_LINK);

			if(this.config.useCompare) {
				this.obCompare = BX(this.visual.COMPARE_LINK);
			}

			if(this.config.useSubscribe) {
				this.obSubscribe = BX(this.visual.SUBSCRIBE_LINK);
			}
			
			this.obTabs = BX(this.visual.TABS_ID);
			this.obTabsBlock = this.getEntity(this.obTabs, 'tabs');
			this.obTabContainers = BX(this.visual.TAB_CONTAINERS_ID);			
			
			this.initPopup();
			this.initTabs();
			
			if(this.obTabsBlock) {
				this.tabsPanelFixed = false;
				this.tabsPanelScrolled = false;
				this.lastScrollTop = 0;
				this.checkTopTabsBlockScroll();
				BX.bind(window, 'scroll', BX.proxy(this.checkTopTabsBlockScroll, this));
				BX.bind(window, 'resize', BX.proxy(this.checkTopTabsBlockResize, this));

				this.checkActiveTabsBlock();
				BX.bind(window, 'scroll', BX.proxy(this.checkActiveTabsBlock, this));
				BX.bind(window, 'resize', BX.proxy(this.checkActiveTabsBlock, this));
			}

			this.obPayBlock = this.obProduct.querySelector('.product-item-detail-pay-block');
			if(this.obPayBlock) {
				this.shortCardNodes.picture = this.getEntity(this.obPayBlock, 'short-card-picture');
				
				this.payBlockFixed = false;
				this.payBlockHidden = false;
				this.checkTopPayBlockScroll();
				BX.bind(window, 'scroll', BX.proxy(this.checkTopPayBlockScroll, this));
				
				this.payBlockMoved = false;
				this.checkTopPayBlockResize();
				BX.bind(window, 'resize', BX.proxy(this.checkTopPayBlockResize, this));
			}

			this.obObjectBtn = this.obProduct.querySelector('.product-item-detail-object-btn');
			
			if(this.config.showOfferGroup)
				this.obConstructor = BX(this.visual.CONSTRUCTOR_ID);
			
			this.obMoreProductsSectionsLinks = this.getEntity(this.obProduct, 'moreProductsSectionsLinks');
			
			if(this.errorCode === 0) {
				//product slider events
				if(this.config.showSlider && !this.isTouchDevice) {
					BX.bind(this.obBigSlider, 'mouseenter', BX.proxy(this.stopSlider, this));
					BX.bind(this.obBigSlider, 'mouseleave', BX.proxy(this.cycleSlider, this));
				}

				if(this.isTouchDevice) {
					BX.bind(this.node.videoImageContainer, 'touchstart', BX.proxy(this.touchStartEvent, this));
					BX.bind(this.node.videoImageContainer, 'touchend', BX.proxy(this.touchEndEvent, this));
					BX.bind(this.node.videoImageContainer, 'touchcancel', BX.proxy(this.touchEndEvent, this));
				}

				BX.bind(this.node.sliderControlLeft, 'click', BX.proxy(this.slidePrev, this));
				BX.bind(this.node.sliderControlRight, 'click', BX.proxy(this.slideNext, this));

				if(this.config.showQuantity) {
					if(this.obQuantityUp) {
						BX.bind(this.obQuantityUp, 'click', BX.delegate(this.quantityUp, this));
					}
					if(this.obQuantityDown) {
						BX.bind(this.obQuantityDown, 'click', BX.delegate(this.quantityDown, this));
					}
					if(this.obQuantity) {
						BX.bind(this.obQuantity, 'change', BX.delegate(this.quantityChange, this));
					}
					
					if(this.obPcQuantityUp) {
						BX.bind(this.obPcQuantityUp, 'click', BX.delegate(this.quantityUp, this));
					}
					if(this.obPcQuantityDown) {
						BX.bind(this.obPcQuantityDown, 'click', BX.delegate(this.quantityDown, this));
					}
					if(this.obPcQuantity) {
						BX.bind(this.obPcQuantity, 'change', BX.delegate(this.pcQuantityChange, this));
					}

					if(this.obSqMQuantityUp) {
						BX.bind(this.obSqMQuantityUp, 'click', BX.delegate(this.quantityUp, this));
					}
					if(this.obSqMQuantityDown) {
						BX.bind(this.obSqMQuantityDown, 'click', BX.delegate(this.quantityDown, this));
					}
					if(this.obSqMQuantity) {
						BX.bind(this.obSqMQuantity, 'change', BX.delegate(this.sqMQuantityChange, this));
					}
				}

				switch(this.productType) {
					case 0: //no catalog
					case 1: //product
					case 2: //set
						if(this.product.useSlider) {
							this.product.slider = {
								ID: this.visual.SLIDER_CONT_ID,
								CONT: BX(this.visual.SLIDER_CONT_ID),
								COUNT: this.product.sliderCount
							};
							this.product.slider.ITEMS = this.getEntities(this.product.slider.CONT, 'slider-control');
							for(j = 0; j < this.product.slider.ITEMS.length; j++) {								
								BX.bind(this.product.slider.ITEMS[j], 'click', BX.delegate(this.selectSliderImg, this));
							}
							
							var i = 0;
							for(j = 0; j < this.product.sliderPict.length; j++) {
								if(!!this.product.sliderPict[i].VALUE && this.product.sliderPict[i].VALUE != '')
									i++;
							}
							this.setCurrentImg(this.product.sliderPict[i], true, true);							
							
							this.checkSliderControls(this.product.sliderCount);

							if(this.product.slider.ITEMS.length > 1) {
								this.initSlider();
							}
						}

						this.checkQuantityControls();						
						this.setAnalyticsDataLayer('showDetail');
						break;
					case 3: //sku
						treeItems = this.obTree.querySelectorAll('li');
						for(i = 0; i < treeItems.length; i++) {
							BX.bind(treeItems[i], 'click', BX.delegate(this.selectOfferProp, this));
						}

						for(i = 0; i < this.offers.length; i++) {
							this.offers[i].SLIDER_COUNT = parseInt(this.offers[i].SLIDER_COUNT, 10) || 0;

							if(this.offers[i].SLIDER_COUNT === 0) {
								this.slider.controls[i] = {
									ID: '',
									COUNT: this.offers[i].SLIDER_COUNT,
									ITEMS: []
								};
							} else {
								for(j = 0; j < this.offers[i].SLIDER.length; j++) {
									this.offers[i].SLIDER[j].WIDTH = parseInt(this.offers[i].SLIDER[j].WIDTH, 10);
									this.offers[i].SLIDER[j].HEIGHT = parseInt(this.offers[i].SLIDER[j].HEIGHT, 10);
								}

								this.slider.controls[i] = {
									ID: this.visual.SLIDER_CONT_OF_ID + this.offers[i].ID,
									OFFER_ID: this.offers[i].ID,
									CONT: BX(this.visual.SLIDER_CONT_OF_ID + this.offers[i].ID),
									COUNT: this.offers[i].SLIDER_COUNT
								};

								this.slider.controls[i].ITEMS = this.getEntities(this.slider.controls[i].CONT, 'slider-control');
								for(j = 0; j < this.slider.controls[i].ITEMS.length; j++) {									
									BX.bind(this.slider.controls[i].ITEMS[j], 'click', BX.delegate(this.selectSliderImg, this));
								}
							}
						}

						this.setCurrent();
						break;
				}
				
				this.obBuyBtn && BX.bind(this.obBuyBtn, 'click', BX.proxy(this.buyBasket, this));				

				this.obAddToBasketBtn && BX.bind(this.obAddToBasketBtn, 'click', BX.proxy(this.add2Basket, this));

				this.obPartnersBtn && BX.bind(this.obPartnersBtn, 'click', BX.proxy(this.partnerSiteRedirect, this));

				this.sPanel = document.body.querySelector('.slide-panel');
				
				this.obAskPrice && BX.bind(this.obAskPrice, 'click', BX.proxy(this.sPanelForm, this));

				this.obNotAvail && BX.bind(this.obNotAvail, 'click', BX.proxy(this.sPanelForm, this));
				
				this.obWholesaler && BX.bind(this.obWholesaler, 'click', BX.proxy(this.sPanelForm, this));
				
				this.obConsultation && BX.bind(this.obConsultation, 'click', BX.proxy(this.sPanelForm, this));

				this.obOneclick && BX.bind(this.obOneclick, 'click', BX.proxy(this.sPanelForm, this));
				
				this.obRequestPrice && BX.bind(this.obRequestPrice, 'click', BX.proxy(this.sPanelForm, this));

				if(this.obDelay)
					BX.bind(this.obDelay, 'click', BX.proxy(this.delay, this));

				if(this.obCompare) {
					BX.bind(this.obCompare, 'click', BX.proxy(this.compare, this));
					BX.addCustomEvent('onCatalogDeleteCompare', BX.proxy(this.checkDeletedCompare, this));
				}
				
				this.obObjectBtn && BX.bind(this.obObjectBtn, 'click', BX.proxy(this.getObjectWorkingHoursToday, this));

				BX.addCustomEvent(this, 'objectWorkingHoursTodayReceived', BX.proxy(this.adjustObjectContacts, this));
				BX.addCustomEvent(this, 'objectContactsAdjusted', BX.proxy(this.object.callbackForm ? this.objectContactsForm : this.objectContacts, this));

				BX.bind(document, 'click', BX.delegate(function(e) {
					if(BX.hasClass(this.sPanel, 'active') && BX.findParent(e.target, {attrs: {id: this.visual.ID + '_contacts'}}) && BX.hasClass(e.target, 'icon-arrow-down')) {
						var workingHoursToday = BX.findParent(e.target, {attrs: {'data-entity': 'working-hours-today'}});
						if(!!workingHoursToday)
							BX.style(workingHoursToday, 'display', 'none');
						
						var workingHours = BX(this.visual.ID + '_contacts').querySelector('[data-entity="working-hours"]');
						if(!!workingHours)
							BX.style(workingHours, 'display', '');
						
						e.stopPropagation();
					}
				}, this));
				BX.bind(document, 'click', BX.delegate(function(e) {
					if(BX.hasClass(this.sPanel, 'active') && BX.findParent(e.target, {attrs: {id: this.visual.ID + '_contacts'}}) && BX.hasClass(e.target, 'icon-arrow-up')) {
						var workingHours = BX.findParent(e.target, {attrs: {'data-entity': 'working-hours'}});
						if(!!workingHours)
							BX.style(workingHours, 'display', 'none');
						
						var workingHoursToday = BX(this.visual.ID + '_contacts').querySelector('[data-entity="working-hours-today"]');
						if(!!workingHoursToday)
							BX.style(workingHoursToday, 'display', '');
						
						e.stopPropagation();
					}
				}, this));

				this.obMoreProductsSectionsLinks && BX.bind(this.obMoreProductsSectionsLinks, 'click', BX.proxy(this.changeMoreProductsSectionLink, this));

				BX.addCustomEvent('onAjaxSuccess', BX.proxy(this.removeNodes, this));
			}
		},

		initConfig: function() {
			this.productType = parseInt(this.params.PRODUCT_TYPE, 10);

			if(this.params.CONFIG.USE_CATALOG !== 'undefined' && BX.type.isBoolean(this.params.CONFIG.USE_CATALOG)) {
				this.config.useCatalog = this.params.CONFIG.USE_CATALOG;
			}

			this.config.showQuantity = this.params.CONFIG.SHOW_QUANTITY;
			this.config.showPrice = this.params.CONFIG.SHOW_PRICE;
			this.config.showPercent = this.params.CONFIG.SHOW_DISCOUNT_PERCENT;
			this.config.showOldPrice = this.params.CONFIG.SHOW_OLD_PRICE;
			this.config.showSkuProps = this.params.CONFIG.SHOW_SKU_PROPS;
			this.config.showOfferGroup = this.params.CONFIG.OFFER_GROUP;
			this.config.useCompare = this.params.CONFIG.DISPLAY_COMPARE;
			this.config.useSubscribe = this.params.CONFIG.USE_SUBSCRIBE;
			this.config.showMaxQuantity = this.params.CONFIG.SHOW_MAX_QUANTITY;
			this.config.relativeQuantityFactor = parseInt(this.params.CONFIG.RELATIVE_QUANTITY_FACTOR);
			this.config.usePriceRanges = this.params.CONFIG.USE_PRICE_COUNT;

			if(this.params.CONFIG.MAIN_PICTURE_MODE) {
				this.config.usePopup = BX.util.in_array('POPUP', this.params.CONFIG.MAIN_PICTURE_MODE);
				this.config.useMagnifier = BX.util.in_array('MAGNIFIER', this.params.CONFIG.MAIN_PICTURE_MODE);
			}

			if(this.params.CONFIG.ADD_TO_BASKET_ACTION) {
				this.config.basketAction = this.params.CONFIG.ADD_TO_BASKET_ACTION;
			}
			
			this.config.showSlider = this.params.CONFIG.SHOW_SLIDER === 'Y';

			if(this.config.showSlider && !this.isTouchDevice) {
				this.config.sliderInterval = parseInt(this.params.CONFIG.SLIDER_INTERVAL) || 5000;
			} else {
				this.config.sliderInterval = false;
			}

			this.config.useEnhancedEcommerce = this.params.CONFIG.USE_ENHANCED_ECOMMERCE === 'Y';
			this.config.dataLayerName = this.params.CONFIG.DATA_LAYER_NAME;
			this.config.brandProperty = this.params.CONFIG.BRAND_PROPERTY;

			this.config.alt = this.params.CONFIG.ALT || '';
			this.config.title = this.params.CONFIG.TITLE || '';

			this.config.magnifierZoomPercent = parseInt(this.params.CONFIG.MAGNIFIER_ZOOM_PERCENT) || 200;

			if(!this.params.VISUAL || typeof this.params.VISUAL !== 'object' || !this.params.VISUAL.ID) {
				this.errorCode = -1;
				return;
			}

			this.visual = this.params.VISUAL;
		},

		initProductData: function() {
			var i = 0,
				j = 0;

			if(this.params.PRODUCT && typeof this.params.PRODUCT === 'object') {
				if(this.config.showPrice) {
					this.currentPriceMode = this.params.PRODUCT.ITEM_PRICE_MODE;
					this.currentPrices = this.params.PRODUCT.ITEM_PRICES;
					this.currentPriceSelected = this.params.PRODUCT.ITEM_PRICE_SELECTED;
					this.currentQuantityRanges = this.params.PRODUCT.ITEM_QUANTITY_RANGES;
					this.currentQuantityRangeSelected = this.params.PRODUCT.ITEM_QUANTITY_RANGE_SELECTED;
				}
				
				if(this.config.showQuantity) {
					this.currentMeasure = this.params.PRODUCT.ITEM_MEASURE;

					this.product.checkQuantity = this.params.PRODUCT.CHECK_QUANTITY;
					this.product.isDblQuantity = this.params.PRODUCT.QUANTITY_FLOAT;
					
					if(this.product.checkQuantity) {
						this.product.maxQuantity = this.product.isDblQuantity
							? parseFloat(this.params.PRODUCT.MAX_QUANTITY)
							: parseInt(this.params.PRODUCT.MAX_QUANTITY, 10);
						this.product.maxPcQuantity = parseInt(this.params.PRODUCT.PC_MAX_QUANTITY, 10);
						this.product.maxSqMQuantity = parseFloat(this.params.PRODUCT.SQ_M_MAX_QUANTITY);
					}

					this.product.stepQuantity = this.product.isDblQuantity
						? parseFloat(this.params.PRODUCT.STEP_QUANTITY)
						: parseInt(this.params.PRODUCT.STEP_QUANTITY, 10);
					this.product.stepPcQuantity = parseInt(this.params.PRODUCT.PC_STEP_QUANTITY, 10);
					this.product.stepSqMQuantity = parseFloat(this.params.PRODUCT.SQ_M_STEP_QUANTITY);
					this.checkQuantity = this.product.checkQuantity;
					this.isDblQuantity = this.product.isDblQuantity;					
					this.stepQuantity = this.product.stepQuantity;
					this.stepPcQuantity = this.product.stepPcQuantity;
					this.stepSqMQuantity = this.product.stepSqMQuantity;
					this.maxQuantity = this.product.maxQuantity;
					this.maxPcQuantity = this.product.maxPcQuantity;
					this.maxSqMQuantity = this.product.maxSqMQuantity;
					this.minQuantity = this.currentPriceMode === 'Q' ? parseFloat(this.currentPrices[this.currentPriceSelected].MIN_QUANTITY) : this.stepQuantity;
					this.minPcQuantity = this.stepPcQuantity;
					this.minSqMQuantity = this.currentPriceMode === 'Q' ? parseFloat(this.currentPrices[this.currentPriceSelected].SQ_M_MIN_QUANTITY) : this.stepSqMQuantity;
					
					if(this.isDblQuantity) {
						this.stepQuantity = Math.round(this.stepQuantity * this.precisionFactor) / this.precisionFactor;
					}
					this.stepSqMQuantity = Math.round(this.stepSqMQuantity * this.precisionFactor) / this.precisionFactor;
				}

				this.product.canBuy = this.params.PRODUCT.CAN_BUY;
				this.canSubscription = this.product.canSubscription = this.params.PRODUCT.SUBSCRIPTION;

				this.product.name = this.params.PRODUCT.NAME;
				this.product.pict = this.params.PRODUCT.PICT;
				this.product.id = this.params.PRODUCT.ID;
				this.product.category = this.params.PRODUCT.CATEGORY;

				if(this.params.PRODUCT.ADD_URL) {
					this.product.addUrl = this.params.PRODUCT.ADD_URL;
				}

				if(this.params.PRODUCT.BUY_URL) {
					this.product.buyUrl = this.params.PRODUCT.BUY_URL;
				}

				if(this.params.PRODUCT.SLIDER_COUNT) {
					this.product.sliderCount = parseInt(this.params.PRODUCT.SLIDER_COUNT, 10) || 0;

					if(this.product.sliderCount > 0 && this.params.PRODUCT.SLIDER.length) {
						for(j = 0; j < this.params.PRODUCT.SLIDER.length; j++) {
							this.product.useSlider = true;
							this.params.PRODUCT.SLIDER[j].WIDTH = parseInt(this.params.PRODUCT.SLIDER[j].WIDTH, 10);
							this.params.PRODUCT.SLIDER[j].HEIGHT = parseInt(this.params.PRODUCT.SLIDER[j].HEIGHT, 10);
						}

						this.product.sliderPict = this.params.PRODUCT.SLIDER;
						i = 0;
						for(j = 0; j < this.product.sliderPict.length; j++) {
							if(!!this.product.sliderPict[i].VALUE && this.product.sliderPict[i].VALUE != '')
								i++;
						}
						this.setCurrentImg(this.product.sliderPict[i], false);
					}
				}
				
				this.currentIsSet = true;
			} else {
				this.errorCode = -1;
			}
		},

		initOffersData: function() {
			if(this.params.OFFERS && BX.type.isArray(this.params.OFFERS)) {
				this.offers = this.params.OFFERS;
				this.offerNum = 0;

				if(this.params.OFFER_SELECTED) {
					this.offerNum = parseInt(this.params.OFFER_SELECTED, 10) || 0;
				}

				if(this.params.TREE_PROPS) {
					this.treeProps = this.params.TREE_PROPS;
				}

				if(this.params.DEFAULT_PICTURE) {
					this.defaultPict.preview = this.params.DEFAULT_PICTURE.PREVIEW_PICTURE;
					this.defaultPict.detail = this.params.DEFAULT_PICTURE.DETAIL_PICTURE;
				}

				if(this.params.PRODUCT && typeof this.params.PRODUCT === 'object') {
					this.product.id = parseInt(this.params.PRODUCT.ID, 10);
					this.product.name = this.params.PRODUCT.NAME;
					this.product.category = this.params.PRODUCT.CATEGORY;
				}
			} else {
				this.errorCode = -1;
			}
		},

		initBasketData: function() {
			if(this.params.BASKET && typeof this.params.BASKET === 'object') {
				if(this.params.BASKET.QUANTITY) {
					this.basketData.quantity = this.params.BASKET.QUANTITY;
				}

				if(this.params.BASKET.PROPS) {
					this.basketData.props = this.params.BASKET.PROPS;
				}

				if(this.params.BASKET.BASKET_URL) {
					this.basketData.basketUrl = this.params.BASKET.BASKET_URL;
				}

				if(this.productType === 3) {
					if(this.params.BASKET.SKU_PROPS) {
						this.basketData.sku_props = this.params.BASKET.SKU_PROPS;
					}
				}

				if(this.params.BASKET.ADD_URL_TEMPLATE) {
					this.basketData.add_url = this.params.BASKET.ADD_URL_TEMPLATE;
				}

				if(this.params.BASKET.BUY_URL_TEMPLATE) {
					this.basketData.buy_url = this.params.BASKET.BUY_URL_TEMPLATE;
				}

				if(this.basketData.add_url === '' && this.basketData.buy_url === '') {
					this.errorCode = -1024;
				}
			}
		},
			
		initDelayData: function() {		
			if(this.params.DELAY && typeof this.params.DELAY === 'object') {
				if(this.params.DELAY.DELAY_PATH) {
					this.delayData.delayPath = this.params.DELAY.DELAY_PATH;
				}
			}
		},

		initCompareData: function() {
			if(this.config.useCompare) {
				if(this.params.COMPARE && typeof this.params.COMPARE === 'object') {
					if(this.params.COMPARE.COMPARE_PATH) {
						this.compareData.comparePath = this.params.COMPARE.COMPARE_PATH;
					}

					if(this.params.COMPARE.COMPARE_URL_TEMPLATE) {
						this.compareData.compareUrl = this.params.COMPARE.COMPARE_URL_TEMPLATE;
					} else {
						this.config.useCompare = false;
					}

					if(this.params.COMPARE.COMPARE_DELETE_URL_TEMPLATE) {
						this.compareData.compareDeleteUrl = this.params.COMPARE.COMPARE_DELETE_URL_TEMPLATE;
					} else {
						this.config.useCompare = false;
					}
				} else {
					this.config.useCompare = false;
				}
			}
		},

		initObjectData: function() {
			if(this.params.OBJECT && typeof this.params.OBJECT === 'object') {
				if(this.params.OBJECT.ID) {
					this.object.id = this.params.OBJECT.ID;
				}

				if(this.params.OBJECT.NAME) {
					this.object.name = this.params.OBJECT.NAME;
				}

				if(this.params.OBJECT.ADDRESS) {
					this.object.address = this.params.OBJECT.ADDRESS;
				}

				if(this.params.OBJECT.TIMEZONE) {
					this.object.timezone = this.params.OBJECT.TIMEZONE;
				}

				if(this.params.OBJECT.WORKING_HOURS) {
					this.object.workingHours = this.params.OBJECT.WORKING_HOURS;
				}

				if(this.params.OBJECT.PHONE) {
					this.object.phone = this.params.OBJECT.PHONE.VALUE;
					this.object.phoneDescription = this.params.OBJECT.PHONE.DESCRIPTION;
				}
				
				if(this.params.OBJECT.EMAIL) {
					this.object.email = this.params.OBJECT.EMAIL.VALUE;
					this.object.emailDescription = this.params.OBJECT.EMAIL.DESCRIPTION;
				}
				
				if(this.params.OBJECT.SKYPE) {
					this.object.skype = this.params.OBJECT.SKYPE.VALUE;
					this.object.skypeDescription = this.params.OBJECT.SKYPE.DESCRIPTION;
				}
				
				if(this.params.OBJECT.CALLBACK_FORM) {
					this.object.callbackForm = this.params.OBJECT.CALLBACK_FORM;
				}
			}
		},

		initMoreProductsData: function() {
			if(this.params.MORE_PRODUCTS && typeof this.params.MORE_PRODUCTS === 'object') {
				if(this.params.MORE_PRODUCTS.PRODUCTS_IDS) {
					this.moreProducts.ids = this.params.MORE_PRODUCTS.PRODUCTS_IDS;
				}
			}
		},

		initSlider: function() {
			if(this.node.sliderProgressBar) {
				if(this.slider.progress) {
					this.resetProgress();
				} else {
					this.slider.progress = new BX.easing({
						transition: BX.easing.transitions.linear,
						step: BX.delegate(function(state){
							this.node.sliderProgressBar.style.width = state.width / 10 + '%';
						}, this)
					});
				}
			}

			this.cycleSlider();
		},

		setAnalyticsDataLayer: function(action) {
			if(!this.config.useEnhancedEcommerce || !this.config.dataLayerName)
				return;

			var item = {},
				info = {},
				variants = [],
				i, k, j, propId, skuId, propValues;

			switch(this.productType) {
				case 0: //no catalog
				case 1: //product
				case 2: //set
					item = {
						'id': this.product.id,
						'name': this.product.name,
						'price': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].PRICE,
						'category': this.product.category,
						'brand': BX.type.isArray(this.config.brandProperty) ? this.config.brandProperty.join('/') : this.config.brandProperty
					};
					break;
				case 3: //sku
					for(i in this.offers[this.offerNum].TREE) {
						if(this.offers[this.offerNum].TREE.hasOwnProperty(i)) {
							propId = i.substring(5);
							skuId = this.offers[this.offerNum].TREE[i];

							for(k in this.treeProps) {
								if(this.treeProps.hasOwnProperty(k) && this.treeProps[k].ID == propId) {
									for(j in this.treeProps[k].VALUES) {
										propValues = this.treeProps[k].VALUES[j];
										if(propValues.ID == skuId) {
											variants.push(propValues.NAME);
											break;
										}
									}
								}
							}
						}
					}

					item = {
						'id': this.offers[this.offerNum].ID,
						'name': this.offers[this.offerNum].NAME,
						'price': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].PRICE,
						'category': this.product.category,
						'brand': BX.type.isArray(this.config.brandProperty) ? this.config.brandProperty.join('/') : this.config.brandProperty,
						'variant': variants.join('/')
					};
					break;
			}

			switch(action) {
				case 'showDetail':
					info = {
						'event': 'showDetail',
						'ecommerce': {
							'currencyCode': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].CURRENCY || '',
							'detail': {
								'products': [{
									'name': item.name || '',
									'id': item.id || '',
									'price': item.price || 0,
									'brand': item.brand || '',
									'category': item.category || '',
									'variant': item.variant || ''
								}]
							}
						}
					};
					break;
				case 'addToCart':
					info = {
						'event': 'addToCart',
						'ecommerce': {
							'currencyCode': this.currentPrices[this.currentPriceSelected] && this.currentPrices[this.currentPriceSelected].CURRENCY || '',
							'add': {
								'products': [{
									'name': item.name || '',
									'id': item.id || '',
									'price': item.price || 0,
									'brand': item.brand || '',
									'category': item.category || '',
									'variant': item.variant || ''
								}]
							}
						}
					};

					if(this.config.showQuantity) {
						if(this.obQuantity && !this.obPcQuantity && !this.obSqMQuantity) {
							info.ecommerce.add.products[0].quantity = this.obQuantity.value;
						} else if(this.obPcQuantity && this.obSqMQuantity) {
							if(this.currentMeasure.SYMBOL_INTL == 'pc. 1' || this.currentMeasure.SYMBOL_INTL == 'm2') {
								info.ecommerce.add.products[0].quantity = this.currentPrices[this.currentPriceSelected].SQ_M_PRICE ? this.obPcQuantity.value : this.obSqMQuantity.value;
							} else {
								info.ecommerce.add.products[0].quantity = this.obQuantity.value;
							}
						}
					} else {						
						info.ecommerce.add.products[0].quantity = this.currentPrices[this.currentPriceSelected] ? this.currentPrices[this.currentPriceSelected].MIN_QUANTITY : '';
					}
					break;
			}

			window[this.config.dataLayerName] = window[this.config.dataLayerName] || [];
			window[this.config.dataLayerName].push(info);
		},

		initTabs: function() {
			var tabsList = this.obTabs.querySelector('.product-item-detail-tabs-list'),
				tabs = this.getEntities(this.obTabs, 'tab'),
				tabValue, targetTab, haveActive = false;
			
			if(!!tabs.length > 0) {
				if(!!tabsList) {
					BX.addClass(tabsList, 'owl-carousel');
					$(tabsList).owlCarousel({								
						autoWidth: true,
						nav: true,
						navText: ['<i class=\"icon-arrow-left\"></i>', '<i class=\"icon-arrow-right\"></i>'],
						navContainer: '.product-item-detail-tabs-scroll',
						dots: false,			
					});
				}
			
				for(var i in tabs) {
					if(tabs.hasOwnProperty(i) && BX.type.isDomNode(tabs[i])) {
						tabValue = tabs[i].getAttribute('data-value');
						if(tabValue) {
							targetTab = this.obTabContainers.querySelector('[data-value="' + tabValue + '"]');
							if(BX.type.isDomNode(targetTab)) {
								BX.bind(tabs[i], 'click', BX.proxy(this.changeTab, this));							

								if(!haveActive) {
									BX.addClass(tabs[i], 'active');
									haveActive = true;
								} else {
									BX.removeClass(tabs[i], 'active');
								}

								if(window.location.hash.indexOf(tabValue) > -1) {
									tabs[i].click();
									window.history.pushState("", document.title, window.location.pathname + window.location.search);
								}
							}
						}
					}
				}
			}
		},

		checkTouch: function(event) {
			if(!event || !event.changedTouches)
				return false;

			return event.changedTouches[0].identifier === this.touch.identifier;
		},

		touchStartEvent: function(event) {
			if(event.touches.length != 1)
				return;

			this.touch = event.changedTouches[0];
		},

		touchEndEvent: function(event) {
			if(!this.checkTouch(event))
				return;

			var deltaX = this.touch.pageX - event.changedTouches[0].pageX,
				deltaY = this.touch.pageY - event.changedTouches[0].pageY;

			if(Math.abs(deltaX) >= Math.abs(deltaY) + 10) {
				if(deltaX > 0) {
					this.slideNext();
				}

				if(deltaX < 0) {
					this.slidePrev();
				}
			}
		},

		cycleSlider: function(event) {
			event || (this.slider.paused = false);

			this.slider.interval && clearInterval(this.slider.interval);

			if(this.config.sliderInterval && !this.slider.paused) {
				if(this.slider.progress) {
					this.slider.progress.stop();

					var width = parseInt(this.node.sliderProgressBar.style.width);

					this.slider.progress.options.duration = this.config.sliderInterval * (100 - width) / 100;
					this.slider.progress.options.start = {width: width * 10};
					this.slider.progress.options.finish = {width: 1000};
					this.slider.progress.options.complete = BX.delegate(function(){
						this.slider.interval = true;
						this.slideNext();
					}, this);
					this.slider.progress.animate();
				} else {
					this.slider.interval = setInterval(BX.proxy(this.slideNext, this), this.config.sliderInterval);
				}
			}
		},

		stopSlider: function(event) {
			event || (this.slider.paused = true);

			this.slider.interval && (this.slider.interval = clearInterval(this.slider.interval));

			if(this.slider.progress) {
				this.slider.progress.stop();

				var width = parseInt(this.node.sliderProgressBar.style.width);

				this.slider.progress.options.duration = this.config.sliderInterval * width / 200;
				this.slider.progress.options.start = {width: width * 10};
				this.slider.progress.options.finish = {width: 0};
				this.slider.progress.options.complete = null;
				this.slider.progress.animate();
			}
		},

		resetProgress: function() {
			this.slider.progress && this.slider.progress.stop();
			this.node.sliderProgressBar.style.width = 0;
		},

		slideNext: function() {
			return this.slide('next');
		},

		slidePrev: function() {
			return this.slide('prev');
		},

		slide: function(type) {
			if(!this.product.slider || !this.product.slider.CONT)
				return;

			var active = this.getEntity(this.product.slider.CONT, 'slider-control', '.active'),
				next = this.getItemForDirection(type, active);

			BX.removeClass(active, 'active');
			this.selectSliderImg(next);

			this.slider.interval && this.cycleSlider();
		},

		getItemForDirection: function(direction, active) {
			var activeIndex = this.getItemIndex(active),
				delta = direction === 'prev' ? -1 : 1,
				itemIndex = (activeIndex + delta) % this.product.slider.COUNT;

			return this.eq(this.product.slider.ITEMS, itemIndex);
		},

		getItemIndex: function(item) {
			return BX.util.array_values(this.product.slider.ITEMS).indexOf(item);
		},

		eq: function(obj, i) {
			var len = obj.length,
				j = +i + (i < 0 ? len : 0);

			return j >= 0 && j < len ? obj[j] : {};
		},
		
		checkTopTabsBlockScroll: function() {
			var topPanel = document.querySelector('.top-panel'),
				topPanelHeight = 0,				
				topPanelThead = !!topPanel && topPanel.querySelector('.top-panel__thead'),
				topPanelTfoot = !!topPanel && topPanel.querySelector('.top-panel__tfoot'),				
				tabsPanelContainerTop = BX.pos(this.obTabs).top,
				tabsPanel = this.obTabsBlock,				
				tabsPanelHeight = tabsPanel.offsetHeight,
				scrollTop = BX.GetWindowScrollPos().scrollTop;
			
			if(window.innerWidth < 992) {
				if(!!topPanelThead && !!BX.hasClass(topPanelThead, 'fixed')) {
					topPanelHeight = topPanelThead.offsetHeight;
					if(!!topPanelTfoot && !!BX.hasClass(topPanelTfoot, 'visible'))
						topPanelHeight += topPanelTfoot.offsetHeight;
				}
				
				if(scrollTop + topPanelHeight >= tabsPanelContainerTop) {
					if(!this.tabsPanelFixed) {
						this.tabsPanelFixed = true;
						BX.style(this.obTabs, 'height', tabsPanelHeight + 'px');				
						BX.style(tabsPanel, 'top', topPanelHeight + 'px');	
						BX.addClass(tabsPanel, 'fixed');
					} else {
						if(!this.tabsPanelScrolled && topPanelHeight > 0 && scrollTop < this.lastScrollTop) {
							this.tabsPanelScrolled = true;
							var tabsPanelScrolled = this.tabsPanelScrolled;
							new BX.easing({
								duration: 300,
								start: {top: Math.abs(parseInt(BX.style(tabsPanel, 'top'), 10))},
								finish: {top: topPanelHeight},
								transition: BX.easing.transitions.linear,
								step: function(state) {
									if(!!tabsPanelScrolled)
										BX.style(tabsPanel, 'top', state.top + 'px');								
								}
							}).animate();
						} else if(!!this.tabsPanelScrolled && topPanelHeight > 0 && scrollTop > this.lastScrollTop) {								
							this.tabsPanelScrolled = false;
							new BX.easing({
								duration: 300,
								start: {top: Math.abs(parseInt(BX.style(tabsPanel, 'top'), 10))},
								finish: {top: topPanelHeight},
								transition: BX.easing.transitions.linear,
								step: function(state) {
									BX.style(tabsPanel, 'top', state.top + 'px');								
								}
							}).animate();
						}
					}
				} else if(!!this.tabsPanelFixed && (scrollTop + topPanelHeight < tabsPanelContainerTop)) {
					this.tabsPanelFixed = false;
					this.tabsPanelScrolled = false;
					this.obTabs.removeAttribute('style');
					tabsPanel.removeAttribute('style');
					BX.removeClass(tabsPanel, 'fixed');
				}
			} else {
				if(!!topPanel && !!BX.hasClass(topPanel, 'fixed'))
					topPanelHeight = topPanel.offsetHeight;
				
				if(!this.tabsPanelFixed && (scrollTop + topPanelHeight >= tabsPanelContainerTop)) {
					this.tabsPanelFixed = true;
					BX.style(this.obTabs, 'height', tabsPanelHeight + 'px');
					BX.style(tabsPanel, 'top', topPanelHeight + 'px');
					BX.addClass(tabsPanel, 'fixed');
				} else if(!!this.tabsPanelFixed && (scrollTop + topPanelHeight < tabsPanelContainerTop)) {
					this.tabsPanelFixed = false;
					this.obTabs.removeAttribute('style');
					tabsPanel.removeAttribute('style');
					BX.removeClass(tabsPanel, 'fixed');
				}
			}
			this.lastScrollTop = scrollTop;
		},

		checkTopTabsBlockResize: function() {
			if(!!BX.hasClass(this.obTabsBlock, 'fixed')) {
				var topPanel = document.querySelector('.top-panel'),
					topPanelHeight = 0,
					topPanelThead = !!topPanel && topPanel.querySelector('.top-panel__thead'),
					topPanelTfoot = !!topPanel && topPanel.querySelector('.top-panel__tfoot');					
				
				if(window.innerWidth < 992) {
					if(!!topPanelThead && !!BX.hasClass(topPanelThead, 'fixed')) {
						topPanelHeight = topPanelThead.offsetHeight;
						if(!!topPanelTfoot && !!BX.hasClass(topPanelTfoot, 'visible'))
							topPanelHeight += topPanelTfoot.offsetHeight;
					}
				} else {
					if(!!topPanel && !!BX.hasClass(topPanel, 'fixed'))
						topPanelHeight = topPanel.offsetHeight;
					this.tabsPanelScrolled = false;
				}
				
				BX.style(this.obTabsBlock, 'top', topPanelHeight + 'px');
			}
		},

		checkActiveTabsBlock: function() {
			var topPanel = document.querySelector('.top-panel'),
				topPanelHeight = 0,
				topPanelThead = !!topPanel && topPanel.querySelector('.top-panel__thead'),
				topPanelTfoot = !!topPanel && topPanel.querySelector('.top-panel__tfoot'),
				tabsPanel = this.obTabsBlock,
				tabsPanelHeight = 0,				
				containers = this.getEntities(this.obTabContainers, 'tab-container'),
				tabs = this.getEntities(this.obTabs, 'tab'),
				scrollTop = BX.GetWindowScrollPos().scrollTop;

			if(!!containers && !!tabs) {
				if(window.innerWidth < 992) {
					if(!!topPanelThead && !!BX.hasClass(topPanelThead, 'fixed')) {
						topPanelHeight = topPanelThead.offsetHeight;
						if(!!topPanelTfoot && !!BX.hasClass(topPanelTfoot, 'visible'))
							topPanelHeight += topPanelTfoot.offsetHeight;
					}
				} else {
					if(!!topPanel && !!BX.hasClass(topPanel, 'fixed'))
						topPanelHeight = topPanel.offsetHeight;
				}

				if(!!tabsPanel && !!BX.hasClass(tabsPanel, 'fixed'))
					tabsPanelHeight = tabsPanel.offsetHeight;

				var fullScrollTop = scrollTop + topPanelHeight + tabsPanelHeight;
				
				var containersLength = Object.keys(containers).length;
				for(var i in containers) {
					if(containers.hasOwnProperty(i) && BX.type.isDomNode(containers[i])) {
						var containerValue = containers[i].getAttribute('data-value');
						if(containerValue) {
							if(fullScrollTop >= BX.pos(containers[i]).top && fullScrollTop <= BX.pos(containers[containersLength - 1]).bottom) {
								for(var j in tabs) {
									if(tabs.hasOwnProperty(j) && BX.type.isDomNode(tabs[j])) {
										var tabValue = tabs[j].getAttribute('data-value');
										if(tabValue) {
											if(tabValue === containerValue)
												BX.addClass(tabs[j], 'active');
											else
												BX.removeClass(tabs[j], 'active');
										}
									}
								}
							} else if(fullScrollTop > BX.pos(containers[containersLength - 1]).bottom) {
								for(var j in tabs) {
									if(tabs.hasOwnProperty(j) && BX.type.isDomNode(tabs[j]))
										BX.removeClass(tabs[j], 'active');
								}
							}
						}
					}
				}
			}
		},
		
		changeTab: function(event) {
			BX.PreventDefault(event);

			BX.unbind(window, 'scroll', BX.proxy(this.checkActiveTabsBlock, this));
			
			var targetTabValue = BX.proxy_context && BX.proxy_context.getAttribute('data-value'),
				containers, tabs;

			if(!!targetTabValue) {
				containers = this.getEntities(this.obTabContainers, 'tab-container');
				for(var i in containers) {
					if(containers.hasOwnProperty(i) && BX.type.isDomNode(containers[i])) {
						if(containers[i].getAttribute('data-value') === targetTabValue) {
							var topPanel = document.querySelector('.top-panel'),
								topPanelHeight = 0,
								topPanelThead = !!topPanel && topPanel.querySelector('.top-panel__thead'),
								topPanelTfoot = !!topPanel && topPanel.querySelector('.top-panel__tfoot'),
								tabContainerTop = BX.pos(containers[i]).top,
								scrollTop = BX.GetWindowScrollPos().scrollTop;

							if(window.innerWidth < 992) {
								if(!!topPanelThead) {
									topPanelHeight = topPanelThead.offsetHeight;
									if(scrollTop + this.obTabsBlock.offsetHeight + topPanelHeight > tabContainerTop) {
										if(!!topPanelTfoot)
											topPanelHeight += topPanelTfoot.offsetHeight;
									}
								}
							} else {
								if(!!topPanel)
									topPanelHeight = topPanel.offsetHeight;
							}
							
							new BX.easing({
								duration: 500,
								start: {scroll: scrollTop},
								finish: {scroll: tabContainerTop - this.obTabsBlock.offsetHeight - topPanelHeight},
								transition: BX.easing.makeEaseOut(BX.easing.transitions.quint),
								step: BX.delegate(function(state) {
									window.scrollTo(0, state.scroll);
								}, this),
								complete: BX.delegate(function() {
									BX.bind(window, 'scroll', BX.proxy(this.checkActiveTabsBlock, this));
								}, this)
							}).animate();
						}
					}
				}

				tabs = this.getEntities(this.obTabs, 'tab');
				for(var i in tabs) {
					if(tabs.hasOwnProperty(i) && BX.type.isDomNode(tabs[i])) {
						if(tabs[i].getAttribute('data-value') === targetTabValue)
							BX.addClass(tabs[i], 'active');
						else
							BX.removeClass(tabs[i], 'active');
					}
				}
			}
		},

		checkTopPayBlockScroll: function() {
			var topPanel = document.querySelector('.top-panel'),
				topPanelHeight = 0,
				tabsPanel = this.obTabsBlock,
				tabsPanelHeight = 0,
				ghostTop = this.obProduct.querySelector('.product-item-detail-ghost-top'),
				ghostBottom = this.obProduct.querySelector('.product-item-detail-ghost-bottom'),
				ghostBottomTop = !!ghostBottom && BX.pos(ghostBottom).top,
				productContainer = this.getEntity(this.obProduct, 'product-container'),
				productContainerBottom = !!productContainer && BX.pos(productContainer).bottom,
				payBlock = this.obPayBlock,
				payBlockWidth = payBlock.offsetWidth,
				payBlockHeight = payBlock.offsetHeight - (!!this.obQuickOrder ? this.obQuickOrder.offsetHeight : 0),
				scrollTop = BX.GetWindowScrollPos().scrollTop;
			
			if(!!ghostTop && !!ghostBottomTop && !!productContainerBottom) {
				if(window.innerWidth >= 992) {
					if(productContainerBottom - ghostBottomTop >= (!this.payBlockFixed ? payBlockHeight + 68 : payBlockHeight)) {
						if(!!topPanel && !!BX.hasClass(topPanel, 'fixed'))
							topPanelHeight = topPanel.offsetHeight;

						if(!!tabsPanel && !!BX.hasClass(tabsPanel, 'fixed'))
							tabsPanelHeight = tabsPanel.offsetHeight;

						var fullScrollTop = scrollTop + topPanelHeight + tabsPanelHeight;
						
						if(fullScrollTop >= ghostBottomTop) {
							if(!this.payBlockFixed) {
								this.payBlockFixed = true;
								BX.style(ghostTop, 'paddingTop', payBlockHeight + 'px');
								BX.style(payBlock, 'top', topPanelHeight + tabsPanelHeight + 40 + 'px');
								BX.style(payBlock, 'width', payBlockWidth + 'px');
								BX.addClass(payBlock, 'product-item-detail-pay-block-fixed');
							} else {
								if(!this.payBlockHidden && (fullScrollTop > productContainerBottom - payBlockHeight - 40)) {
									this.payBlockHidden = true;
									BX.addClass(this.obPayBlock, 'product-item-detail-pay-block-hidden');
								} else if(!!this.payBlockHidden && (fullScrollTop <= productContainerBottom - payBlockHeight - 40)) {
									this.payBlockHidden = false;
									BX.removeClass(this.obPayBlock, 'product-item-detail-pay-block-hidden');
								}
							}
						} else if(!!this.payBlockFixed && (fullScrollTop < ghostBottomTop)) {
							this.payBlockFixed = false;
							this.payBlockHidden = false;
							ghostTop.removeAttribute('style');
							this.obPayBlock.removeAttribute('style');
							BX.removeClass(this.obPayBlock, 'product-item-detail-pay-block-fixed');
							BX.removeClass(this.obPayBlock, 'product-item-detail-pay-block-hidden');
						}
					}
				}
			}
		},

		checkTopPayBlockResize: function() {
			var insertNode = this.obProduct.querySelector('.product-item-detail-blocks'),
				ghostTop = this.obProduct.querySelector('.product-item-detail-ghost-top');
			
			if(window.innerWidth < 992) {
				if(!this.payBlockMoved && !!insertNode) {
					this.payBlockMoved = true;
					BX.prepend(this.obPayBlock, insertNode);
					
					if(!!this.payBlockFixed) {
						this.payBlockFixed = false;
						this.payBlockHidden = false;
						!!ghostTop && ghostTop.removeAttribute('style');
						this.obPayBlock.removeAttribute('style');
						BX.removeClass(this.obPayBlock, 'product-item-detail-pay-block-fixed');
						BX.removeClass(this.obPayBlock, 'product-item-detail-pay-block-hidden');
					}
				}
			} else {
				if(!!this.payBlockMoved && !!ghostTop) {
					this.payBlockMoved = false;
					BX.insertAfter(this.obPayBlock, ghostTop);
					
					this.checkTopPayBlockScroll();
				}
			}
		},

		initPopup: function() {
			if(this.config.usePopup) {
				this.node.videoImageContainer.style.cursor = 'zoom-in';
				BX.bind(this.node.videoImageContainer, 'click', BX.delegate(this.toggleMainPictPopup, this));
				BX.bind(this.node.sliderMagnifier, 'click', BX.delegate(this.toggleMainPictPopup, this));
				BX.bind(document, 'keyup', BX.proxy(this.closeByEscape, this));
				BX.bind(
					this.getEntity(this.obBigSlider, 'close-popup'),
					'click',
					BX.proxy(this.hideMainPictPopup, this)
				);
			}
		},

		checkSliderControls: function(count) {
			var display = count > 1 ? '' : 'none';
			
			this.node.sliderControlLeft && (this.node.sliderControlLeft.style.display = display);
			this.node.sliderControlRight && (this.node.sliderControlRight.style.display = display);
		},

		setCurrentImg: function(img, showImage, showShortCardImage) {
			var videos,
				images,
				videosImages = [],
				i = 0,
				j = 0,
				l;

			this.currentImg.id = img.ID;
			this.currentImg.src = img.SRC;
			this.currentImg.width = img.WIDTH;
			this.currentImg.height = img.HEIGHT;
			
			if(showImage && this.node.videoImageContainer) { 
				videos = this.getEntities(this.node.videoImageContainer, 'video');
				for(i = 0; i < videos.length; i++) {
					videosImages[j] = videos[i];
					j++
				}
				
				images = this.getEntities(this.node.videoImageContainer, 'image');
				for(i = 0; i < images.length; i++) {
					videosImages[j] = images[i];
					j++
				}
				
				l = videosImages.length;
				while (l--) {
					if(videosImages[l].getAttribute('data-id') == img.ID) {
						if(!BX.hasClass(videosImages[l], 'active')) {
							this.node.sliderProgressBar && this.resetProgress();
						}

						BX.addClass(videosImages[l], 'active');
					} else if(BX.hasClass(videosImages[l], 'active')) {
						BX.removeClass(videosImages[l], 'active');
						var iframe = BX.findChild(videosImages[l], {tagName: 'iframe'}, true, false);
						if(!!iframe)
							iframe.contentWindow.postMessage('{"event": "command", "func": "pauseVideo", "args": ""}', '*');
					}
				}
			}

			if(showShortCardImage && this.shortCardNodes.picture)
				this.shortCardNodes.picture.setAttribute('src', this.currentImg.src);
			
			if(this.config.useMagnifier && !this.isTouchDevice) {
				this.setMagnifierParams();

				if(showImage) {
					this.disableMagnifier(true);
				}
			}
		},
		
		setMagnifierParams: function() {
			var images = this.getEntities(this.node.videoImageContainer, 'image'),
				l = images.length,
				current;

			while(l--) {
				//disable image title show
				current = images[l].querySelector('img');
				if(!!current) {
					current.setAttribute('data-title', current.getAttribute('title') || '');
					current.removeAttribute('title');

					if(images[l].getAttribute('data-id') == this.currentImg.id) {
						BX.unbind(this.currentImg.node, 'mouseover', BX.proxy(this.enableMagnifier, this));

						this.currentImg.node = current;
						this.currentImg.node.style.backgroundImage = 'url(' + this.currentImg.src + ')';
						this.currentImg.node.style.backgroundSize = '100% auto';

						BX.bind(this.currentImg.node, 'mouseover', BX.proxy(this.enableMagnifier, this));
					}
				}
			}
		},

		enableMagnifier: function() {
			BX.bind(document, 'mousemove', BX.proxy(this.moveMagnifierArea, this));
		},

		disableMagnifier: function(animateSize) {
			if(!this.magnify.enabled)
				return;

			clearTimeout(this.magnify.timer);
			BX.removeClass(this.obBigSlider, 'magnified');
			this.magnify.enabled = false;

			this.currentImg.node.style.backgroundSize = '100% auto';
			if(animateSize) {
				//set initial size for css animation
				this.currentImg.node.style.height = this.magnify.height + 'px';
				this.currentImg.node.style.width = this.magnify.width + 'px';

				this.magnify.timer = setTimeout(
					BX.delegate(function(){
						this.currentImg.node.src = this.currentImg.src;
						this.currentImg.node.style.height = '';
						this.currentImg.node.style.width = '';
					}, this),
					250
				);
			} else {
				this.currentImg.node.src = this.currentImg.src;
				this.currentImg.node.style.height = '';
				this.currentImg.node.style.width = '';
			}

			BX.unbind(document, 'mousemove', BX.proxy(this.moveMagnifierArea, this));
		},

		moveMagnifierArea: function(e) {
			var posBigImg = BX.pos(this.currentImg.node),
				currentPos = this.inRect(e, posBigImg);

			if(this.inBound(posBigImg, currentPos)) {
				var posPercentX = (currentPos.X / this.currentImg.node.width) * 100,
					posPercentY = (currentPos.Y / this.currentImg.node.height) * 100,
					resolution, sliderWidth, w, h, zoomPercent;

				this.currentImg.node.style.backgroundPosition = posPercentX + '% ' + posPercentY + '%';

				if(!this.magnify.enabled) {
					clearTimeout(this.magnify.timer);
					BX.addClass(this.obBigSlider, 'magnified');

					//set initial size for css animation
					this.currentImg.node.style.height = (this.magnify.height = this.currentImg.node.clientHeight) + 'px';
					this.currentImg.node.style.width = (this.magnify.width = this.currentImg.node.offsetWidth) + 'px';

					resolution = this.currentImg.width / this.currentImg.height;
					sliderWidth = this.obBigSlider.offsetWidth;

					if(sliderWidth > this.currentImg.width && !BX.hasClass(this.obBigSlider, 'popup')) {
						w = sliderWidth;
						h = w / resolution;
						zoomPercent = 100;
					} else {
						w = this.currentImg.width;
						h = this.currentImg.height;
						zoomPercent = this.config.magnifierZoomPercent > 100 ? this.config.magnifierZoomPercent : 100;
					}

					//base64 transparent pixel
					this.currentImg.node.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQI12P4zwAAAgEBAKrChTYAAAAASUVORK5CYII=';
					this.currentImg.node.style.backgroundSize = zoomPercent + '% auto';

					//set target size
					this.magnify.timer = setTimeout(BX.delegate(function(){
							this.currentImg.node.style.height = h + 'px';
							this.currentImg.node.style.width = w + 'px';
						}, this),
						10
					);
				}

				this.magnify.enabled = true;
			} else {
				this.disableMagnifier(true);
			}
		},

		inBound: function(rect, point) {
			return (
				(point.Y >= 0 && rect.height >= point.Y)
				&& (point.X >= 0 && rect.width >= point.X)
			);
		},

		inRect: function(e, rect) {
			var wndSize = BX.GetWindowSize(),
				currentPos = {
					X: 0,
					Y: 0,
					globalX: 0,
					globalY: 0
				};

			currentPos.globalX = e.clientX + wndSize.scrollLeft;

			if(e.offsetX && e.offsetX < 0) {
				currentPos.globalX -= e.offsetX;
			}

			currentPos.X = currentPos.globalX - rect.left;
			currentPos.globalY = e.clientY + wndSize.scrollTop;

			if(e.offsetY && e.offsetY < 0) {
				currentPos.globalY -= e.offsetY;
			}

			currentPos.Y = currentPos.globalY - rect.top;

			return currentPos;
		},

		setProductMainPict: function(intPict) {
			var indexPict = -1,				
				i = 0,
				j = 0,
				value = '';
			
			if(this.product.sliderCount) {
				for(j = 0; j < this.product.sliderPict.length; j++) {
					if(intPict === this.product.sliderPict[j].ID) {
						indexPict = j;
						break;
					}
				}
				
				if(indexPict > -1) {
					if(this.product.sliderPict[indexPict]) {
						this.setCurrentImg(this.product.sliderPict[indexPict], true);
					}
					
					for(i = 0; i < this.product.slider.ITEMS.length; i++) {
						value = this.product.slider.ITEMS[i].getAttribute('data-value');

						if(value === intPict) {
							BX.addClass(this.product.slider.ITEMS[i], 'active');
						} else if(BX.hasClass(this.product.slider.ITEMS[i], 'active')) {
							BX.removeClass(this.product.slider.ITEMS[i], 'active');
						}
					}
				}
			}
		},
		
		selectSliderImg: function(target) {
			var strValue = '',
				arItem = [];

			target = BX.type.isDomNode(target) ? target : BX.proxy_context;

			if(target && target.hasAttribute('data-value')) {
				strValue = target.getAttribute('data-value');

				if(strValue.indexOf('_') !== -1) {
					arItem = strValue.split('_');
					this.setMainPict(arItem[0], arItem[1]);
				} else {
					this.setProductMainPict(strValue);
				}
			}
		},

		setMainPict: function(intSlider, intPict, shortCardPict) {
			var index = -1,
				indexPict = -1,
				i,
				j,
				value = '',
				strValue = '';

			for(i = 0; i < this.offers.length; i++) {
				if(intSlider === this.offers[i].ID) {
					index = i;
					break;
				}
			}

			if(index > -1) {
				if(this.offers[index].SLIDER_COUNT > 0) {
					for(j = 0; j < this.offers[index].SLIDER.length; j++) {
						if(intPict === this.offers[index].SLIDER[j].ID) {
							indexPict = j;
							break;
						}
					}

					if(indexPict > -1) {
						if(this.offers[index].SLIDER[indexPict]) {
							this.setCurrentImg(this.offers[index].SLIDER[indexPict], true, shortCardPict);
						}

						strValue = intSlider + '_' + intPict;

						for(i = 0; i < this.product.slider.ITEMS.length; i++) {
							value = this.product.slider.ITEMS[i].getAttribute('data-value');

							if(value === strValue) {
								BX.addClass(this.product.slider.ITEMS[i], 'active');
							} else if(BX.hasClass(this.product.slider.ITEMS[i], 'active')) {
								BX.removeClass(this.product.slider.ITEMS[i], 'active');
							}
						}
					}
				}
			}
		},

		setMainPictFromItem: function(index) {
			if(this.node.videoImageContainer) {
				var boolSet = false,
					obNewPict = {};

				if(this.offers[index]) {
					if(this.offers[index].DETAIL_PICTURE) {
						obNewPict = this.offers[index].DETAIL_PICTURE;
						boolSet = true;
					} else if(this.offers[index].PREVIEW_PICTURE) {
						obNewPict = this.offers[index].PREVIEW_PICTURE;
						boolSet = true;
					}
				}

				if(!boolSet) {
					if(this.defaultPict.detail) {
						obNewPict = this.defaultPict.detail;
						boolSet = true;
					} else if(this.defaultPict.preview) {
						obNewPict = this.defaultPict.preview;
						boolSet = true;
					}
				}

				if(boolSet) {
					this.setCurrentImg(obNewPict, true, true);
				}
			}
		},

		toggleMainPictPopup: function() {
			if(BX.hasClass(this.obBigSlider, 'popup')) {
				this.hideMainPictPopup();
			} else {
				this.showMainPictPopup();
			}
		},

		showMainPictPopup: function() {
			this.config.useMagnifier && this.disableMagnifier(false);
			BX.addClass(this.obBigSlider, 'popup');
			this.node.videoImageContainer.style.cursor = '';
			//remove double scroll bar
			document.body.style.overflow = 'hidden';
		},

		hideMainPictPopup: function() {
			this.config.useMagnifier && this.disableMagnifier(false);
			BX.removeClass(this.obBigSlider, 'popup');
			this.node.videoImageContainer.style.cursor = 'zoom-in';
			//remove double scroll bar
			document.body.style.overflow = '';
		},

		closeByEscape: function(event) {
			event = event || window.event;

			if(event.keyCode == 27) {
				this.hideMainPictPopup();
			}
		},

		quantityUp: function() {
			var curValue = 0,
				curPcValue = 0,
				curSqMValue = 0,
				boolSet = true,
				boolPcSet = true,
				boolSqMSet = true;

			if(this.errorCode === 0 && this.config.showQuantity && this.canBuy) {
				if(this.obQuantity) {
					curValue = this.isDblQuantity ? parseFloat(this.obQuantity.value) : parseInt(this.obQuantity.value, 10);
					if(!isNaN(curValue)) {
						curValue += this.stepQuantity;
						if(this.checkQuantity) {
							if(curValue > this.maxQuantity)
								boolSet = false;
						}

						if(boolSet) {
							if(this.isDblQuantity)
								curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;
							
							this.obQuantity.value = curValue;

							this.setPrice();
						}
					}
				}
				
				if(this.obPcQuantity && this.obSqMQuantity) {
					curPcValue = parseInt(this.obPcQuantity.value, 10);
					if(!isNaN(curPcValue)) {
						curPcValue += this.stepPcQuantity;
						if(this.checkQuantity) {
							if(curPcValue > this.maxPcQuantity)
								boolPcSet = false;
						}
						
						if(boolPcSet)
							this.obPcQuantity.value = curPcValue;
					}
					
					curSqMValue = parseFloat(this.obSqMQuantity.value);
					if(!isNaN(curSqMValue)) {
						curSqMValue += this.stepSqMQuantity;
						if(this.checkQuantity) {
							if(curSqMValue > this.maxSqMQuantity)
								boolSqMSet = false;
						}
						
						if(boolSqMSet) {
							curSqMValue = Math.round(curSqMValue * this.precisionFactor) / this.precisionFactor;

							this.obSqMQuantity.value = curSqMValue;
						}
					}

					if(boolPcSet && boolSqMSet)
						this.setPrice();
				}
			}
		},
			
		quantityDown: function() {
			var curValue = 0,
				curPcValue = 0,
				curSqMValue = 0,
				boolSet = true,
				boolPcSet = true,
				boolSqMSet = true;				

			if(this.errorCode === 0 && this.config.showQuantity && this.canBuy) {
				if(this.obQuantity) {
					curValue = (this.isDblQuantity ? parseFloat(this.obQuantity.value) : parseInt(this.obQuantity.value, 10));
					if(!isNaN(curValue)) {
						curValue -= this.stepQuantity;

						this.checkPriceRange(curValue);

						if(curValue < this.minQuantity)
							boolSet = false;
						
						if(boolSet) {
							if(this.isDblQuantity)
								curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;
							
							this.obQuantity.value = curValue;

							this.setPrice();
						}
					}
				}
				
				if(this.obPcQuantity && this.obSqMQuantity) {
					curPcValue = parseInt(this.obPcQuantity.value, 10);
					if(!isNaN(curPcValue)) {
						curPcValue -= this.stepPcQuantity;

						if(!this.obQuantity && this.currentPrices[this.currentPriceSelected].SQ_M_PRICE)
							this.checkPriceRange(curPcValue);
						
						if(curPcValue < this.minPcQuantity)
							boolPcSet = false;
						
						if(boolPcSet)
							this.obPcQuantity.value = curPcValue;
					}
				
					curSqMValue = parseFloat(this.obSqMQuantity.value);
					if(!isNaN(curSqMValue)) {
						curSqMValue -= this.stepSqMQuantity;

						if(!this.obQuantity && !this.currentPrices[this.currentPriceSelected].SQ_M_PRICE)
							this.checkPriceRange(curSqMValue);
						
						if(curSqMValue < this.minSqMQuantity)
							boolSqMSet = false;
						
						if(boolSqMSet) {
							curSqMValue = Math.round(curSqMValue * this.precisionFactor) / this.precisionFactor;

							this.obSqMQuantity.value = curSqMValue;
						}
					}

					if(boolPcSet && boolSqMSet)
						this.setPrice();
				}
			}
		},
		
		quantityChange: function() {
			var curValue = 0,
				curPcValue = 0,
				curSqMValue = 0,
				intCount,
				intPcCount,
				intSqMCount;

			if(this.errorCode === 0 && this.config.showQuantity) {
				if(this.canBuy) {
					curValue = this.isDblQuantity ? parseFloat(this.obQuantity.value) : Math.round(this.obQuantity.value);
					if(!isNaN(curValue)) {
						if(this.checkQuantity) {
							if(curValue > this.maxQuantity)
								curValue = this.maxQuantity;
						}

						this.checkPriceRange(curValue);

						if(curValue < this.minQuantity) {
							curValue = this.minQuantity;
						} else {
							intCount = Math.round(Math.round(curValue * this.precisionFactor / this.stepQuantity) / this.precisionFactor) || 1;
							curValue = (intCount <= 1 ? this.stepQuantity : intCount * this.stepQuantity);
							curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;
						}

						this.obQuantity.value = curValue;
					} else {
						this.obQuantity.value = this.minQuantity;
					}

					if(this.obPcQuantity && this.obSqMQuantity) {
						curPcValue = this.currentMeasure.SYMBOL_INTL == 'pc. 1' ? this.obQuantity.value : Math.round(this.obQuantity.value / this.stepQuantity);
						if(!isNaN(curPcValue)) {
							if(this.checkQuantity) {
								if(curPcValue > this.maxPcQuantity)
									curPcValue = this.maxPcQuantity;
							}
							
							if(curPcValue < this.minPcQuantity) {
								curPcValue = this.minPcQuantity;
							} else {
								intPcCount = Math.round(Math.round(curPcValue * this.precisionFactor / this.stepPcQuantity) / this.precisionFactor) || 1;
								curPcValue = (intPcCount <= 1 ? this.stepPcQuantity : intPcCount * this.stepPcQuantity);
								curPcValue = Math.round(curPcValue * this.precisionFactor) / this.precisionFactor;
							}

							this.obPcQuantity.value = curPcValue;
						} else {
							this.obPcQuantity.value = this.minPcQuantity;
						}
						
						curSqMValue = this.currentMeasure.SYMBOL_INTL == 'm2' ? this.obQuantity.value : parseFloat((this.obQuantity.value * this.stepSqMQuantity) / this.stepQuantity);
						if(!isNaN(curSqMValue)) {
							if(this.checkQuantity) {
								if(curSqMValue > this.maxSqMQuantity)
									curSqMValue = this.maxSqMQuantity;
							}
							
							if(curSqMValue < this.minSqMQuantity) {
								curSqMValue = this.minSqMQuantity;
							} else {
								intSqMCount = Math.round(Math.round(curSqMValue * this.precisionFactor / this.stepSqMQuantity) / this.precisionFactor) || 1;
								curSqMValue = (intSqMCount <= 1 ? this.stepSqMQuantity : intSqMCount * this.stepSqMQuantity);
								curSqMValue = Math.round(curSqMValue * this.precisionFactor) / this.precisionFactor;
							}

							this.obSqMQuantity.value = curSqMValue;
						} else {
							this.obSqMQuantity.value = this.minSqMQuantity;
						}
					}
				} else {
					this.obQuantity.value = this.minQuantity;
					if(this.obPcQuantity && this.obSqMQuantity) {
						this.obPcQuantity.value = this.minPcQuantity;
						this.obSqMQuantity.value = this.minSqMQuantity;
					}
				}

				this.setPrice();
			}
		},

		pcQuantityChange: function() {
			var curPcValue = 0,
				curSqMValue = 0,
				curValue = 0,
				intPcCount,
				intSqMCount,
				intCount;

			if(this.errorCode === 0 && this.config.showQuantity) {
				if(this.canBuy) {
					curPcValue = Math.round(this.obPcQuantity.value);
					if(!isNaN(curPcValue)) {
						if(this.checkQuantity) {
							if(curPcValue > this.maxPcQuantity)
								curPcValue = this.maxPcQuantity;
						}

						if(!this.obQuantity && this.currentPrices[this.currentPriceSelected].SQ_M_PRICE)
							this.checkPriceRange(curPcValue);

						if(curPcValue < this.minPcQuantity) {
							curPcValue = this.minPcQuantity;
						} else {
							intPcCount = Math.round(Math.round(curPcValue * this.precisionFactor / this.stepPcQuantity) / this.precisionFactor) || 1;
							curPcValue = (intPcCount <= 1 ? this.stepPcQuantity : intPcCount * this.stepPcQuantity);
							curPcValue = Math.round(curPcValue * this.precisionFactor) / this.precisionFactor;
						}

						this.obPcQuantity.value = curPcValue;
					} else {
						this.obPcQuantity.value = this.minPcQuantity;
					}

					if(this.obSqMQuantity) {
						curSqMValue = parseFloat(this.obPcQuantity.value * this.stepSqMQuantity);
						if(!isNaN(curSqMValue)) {
							if(this.checkQuantity) {
								if(curSqMValue > this.maxSqMQuantity)
									curSqMValue = this.maxSqMQuantity;
							}

							if(!this.obQuantity && !this.currentPrices[this.currentPriceSelected].SQ_M_PRICE)
								this.checkPriceRange(curSqMValue);
							
							if(curSqMValue < this.minSqMQuantity) {
								curSqMValue = this.minSqMQuantity;
							} else {
								intSqMCount = Math.round(Math.round(curSqMValue * this.precisionFactor / this.stepSqMQuantity) / this.precisionFactor) || 1;
								curSqMValue = (intSqMCount <= 1 ? this.stepSqMQuantity : intSqMCount * this.stepSqMQuantity);
								curSqMValue = Math.round(curSqMValue * this.precisionFactor) / this.precisionFactor;
							}

							this.obSqMQuantity.value = curSqMValue;
						} else {
							this.obSqMQuantity.value = this.minSqMQuantity;
						}
					}

					if(this.obQuantity) {
						curValue = this.isDblQuantity ? parseFloat(this.currentPrices[this.currentPriceSelected].SQ_M_PRICE ? this.obPcQuantity.value : this.obSqMQuantity.value) 
							: Math.round(this.currentPrices[this.currentPriceSelected].SQ_M_PRICE ? this.obPcQuantity.value : this.obSqMQuantity.value);
						if(!isNaN(curValue)) {
							if(this.checkQuantity) {
								if(curValue > this.maxQuantity)
									curValue = this.maxQuantity;
							}

							this.checkPriceRange(curValue);

							if(curValue < this.minQuantity) {
								curValue = this.minQuantity;
							} else {
								intCount = Math.round(Math.round(curValue * this.precisionFactor / this.stepQuantity) / this.precisionFactor) || 1;
								curValue = (intCount <= 1 ? this.stepQuantity : intCount * this.stepQuantity);
								curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;
							}

							this.obQuantity.value = curValue;
						} else {
							this.obQuantity.value = this.minQuantity;
						}
					}
				} else {
					this.obPcQuantity.value = this.minPcQuantity;
					if(this.obSqMQuantity)
						this.obSqMQuantity.value = this.minSqMQuantity;
					if(this.obQuantity)
						this.obQuantity.value = this.minQuantity;
				}

				this.setPrice();
			}
		},

		sqMQuantityChange: function() {
			var curSqMValue = 0,
				curPcValue = 0,
				curValue = 0,
				intSqMCount,
				intPcCount,
				intCount;

			if(this.errorCode === 0 && this.config.showQuantity) {
				if(this.canBuy) {
					curSqMValue = parseFloat(this.obSqMQuantity.value);
					if(!isNaN(curSqMValue)) {
						if(this.checkQuantity) {
							if(curSqMValue > this.maxSqMQuantity)
								curSqMValue = this.maxSqMQuantity;
						}

						if(!this.obQuantity && !this.currentPrices[this.currentPriceSelected].SQ_M_PRICE)
							this.checkPriceRange(curSqMValue);
						
						if(curSqMValue < this.minSqMQuantity) {
							curSqMValue = this.minSqMQuantity;
						} else {
							intSqMCount = Math.round(Math.round(curSqMValue * this.precisionFactor / this.stepSqMQuantity) / this.precisionFactor) || 1;
							curSqMValue = (intSqMCount <= 1 ? this.stepSqMQuantity : intSqMCount * this.stepSqMQuantity);
							curSqMValue = Math.round(curSqMValue * this.precisionFactor) / this.precisionFactor;
						}

						this.obSqMQuantity.value = curSqMValue;
					} else {
						this.obSqMQuantity.value = this.minSqMQuantity;
					}
					
					if(this.obPcQuantity) {
						curPcValue = Math.round((this.obSqMQuantity.value * this.stepPcQuantity) / this.stepSqMQuantity);
						if(!isNaN(curPcValue)) {
							if(this.checkQuantity) {
								if(curPcValue > this.maxPcQuantity)
									curPcValue = this.maxPcQuantity;
							}

							if(!this.obQuantity && this.currentPrices[this.currentPriceSelected].SQ_M_PRICE)
								this.checkPriceRange(curPcValue);
							
							if(curPcValue < this.minPcQuantity) {
								curPcValue = this.minPcQuantity;
							} else {
								intPcCount = Math.round(Math.round(curPcValue * this.precisionFactor / this.stepPcQuantity) / this.precisionFactor) || 1;
								curPcValue = (intPcCount <= 1 ? this.stepPcQuantity : intPcCount * this.stepPcQuantity);
								curPcValue = Math.round(curPcValue * this.precisionFactor) / this.precisionFactor;
							}

							this.obPcQuantity.value = curPcValue;
						} else {
							this.obPcQuantity.value = this.minPcQuantity;
						}
					}

					if(this.obQuantity) {
						curValue = this.isDblQuantity ? parseFloat(this.currentPrices[this.currentPriceSelected].SQ_M_PRICE ? this.obPcQuantity.value : this.obSqMQuantity.value) 
							: Math.round(this.currentPrices[this.currentPriceSelected].SQ_M_PRICE ? this.obPcQuantity.value : this.obSqMQuantity.value);
						if(!isNaN(curValue)) {
							if(this.checkQuantity) {
								if(curValue > this.maxQuantity)
									curValue = this.maxQuantity;
							}

							this.checkPriceRange(curValue);

							if(curValue < this.minQuantity) {
								curValue = this.minQuantity;
							} else {
								intCount = Math.round(Math.round(curValue * this.precisionFactor / this.stepQuantity) / this.precisionFactor) || 1;
								curValue = (intCount <= 1 ? this.stepQuantity : intCount * this.stepQuantity);
								curValue = Math.round(curValue * this.precisionFactor) / this.precisionFactor;
							}

							this.obQuantity.value = curValue;
						} else {
							this.obQuantity.value = this.minQuantity;
						}
					}
				} else {
					this.obSqMQuantity.value = this.minSqMQuantity;
					if(this.obPcQuantity)
						this.obPcQuantity.value = this.minPcQuantity;
					if(this.obQuantity)
						this.obQuantity.value = this.minQuantity;
				}

				this.setPrice();
			}
		},

		quantitySet: function(index) {
			var strLimit, resetQuantity, resetPcQuantity, resetSqMQuantity;
			
			var newOffer = this.offers[index],
				oldOffer = this.offers[this.offerNum];

			if(this.errorCode === 0) {
				this.canBuy = newOffer.CAN_BUY;

				this.currentPriceMode = newOffer.ITEM_PRICE_MODE;
				this.currentPrices = newOffer.ITEM_PRICES;
				this.currentPriceSelected = newOffer.ITEM_PRICE_SELECTED;
				this.currentQuantityRanges = newOffer.ITEM_QUANTITY_RANGES;
				this.currentQuantityRangeSelected = newOffer.ITEM_QUANTITY_RANGE_SELECTED;
				this.currentMeasure = newOffer.ITEM_MEASURE;

				var price = this.currentPrices[this.currentPriceSelected];

				if(this.canBuy) {
					if(price && price.PRICE > 0) {
						this.obDelay && BX.style(this.obDelay, 'display', '');
						this.node.quantity && BX.style(this.node.quantity, 'display', '');
						this.obBuyBtn && BX.adjust(this.obBuyBtn, {props: {disabled: false}});
						this.obAddToBasketBtn && BX.adjust(this.obAddToBasketBtn, {props: {disabled: false}});
						this.obAskPrice && BX.style(this.obAskPrice, 'display', 'none');
						if(this.obQuickOrder) {
							BX.style(this.obQuickOrder, 'display', '');							
							var quickOrderProductId = this.obQuickOrder.querySelector('[name="PRODUCT_ID"]');
							if(!!quickOrderProductId)
								quickOrderProductId.value = newOffer.ID;
						}
					} else {
						this.obDelay && BX.style(this.obDelay, 'display', 'none');
						this.node.quantity && BX.style(this.node.quantity, 'display', 'none');
						this.obBuyBtn && BX.adjust(this.obBuyBtn, {props: {disabled: true}});
						this.obAddToBasketBtn && BX.adjust(this.obAddToBasketBtn, {props: {disabled: true}});
						this.obAskPrice && BX.style(this.obAskPrice, 'display', '');
						this.obQuickOrder && BX.style(this.obQuickOrder, 'display', 'none');
					}
					this.obNotAvail && BX.style(this.obNotAvail, 'display', 'none');
					this.obSubscribe && BX.style(this.obSubscribe, 'display', 'none');
				} else {
					this.obDelay && BX.style(this.obDelay, 'display', 'none');
					this.node.quantity && BX.style(this.node.quantity, 'display', 'none');
					this.obBuyBtn && BX.adjust(this.obBuyBtn, {props: {disabled: true}});
					this.obAddToBasketBtn && BX.adjust(this.obAddToBasketBtn, {props: {disabled: true}});
					this.obAskPrice && BX.style(this.obAskPrice, 'display', 'none');
					this.obNotAvail && BX.style(this.obNotAvail, 'display', '');
					this.obQuickOrder && BX.style(this.obQuickOrder, 'display', 'none');
					if(this.obSubscribe) {
						if(newOffer.CATALOG_SUBSCRIBE === 'Y') {
							BX.style(this.obSubscribe, 'display', '');
							this.obSubscribe.setAttribute('data-item', newOffer.ID);
							BX(this.visual.SUBSCRIBE_LINK + '_hidden').click();
						} else {
							BX.style(this.obSubscribe, 'display', 'none');
						}
					}
				}

				this.isDblQuantity = newOffer.QUANTITY_FLOAT;
				this.checkQuantity = newOffer.CHECK_QUANTITY;

				if(this.isDblQuantity) {
					this.stepQuantity = Math.round(parseFloat(newOffer.STEP_QUANTITY) * this.precisionFactor) / this.precisionFactor;
					this.maxQuantity = parseFloat(newOffer.MAX_QUANTITY);
					this.minQuantity = this.currentPriceMode === 'Q' ? parseFloat(this.currentPrices[this.currentPriceSelected].MIN_QUANTITY) : this.stepQuantity;
				} else {
					this.stepQuantity = parseInt(newOffer.STEP_QUANTITY, 10);
					this.maxQuantity = parseInt(newOffer.MAX_QUANTITY, 10);
					this.minQuantity = this.currentPriceMode === 'Q' ? parseInt(this.currentPrices[this.currentPriceSelected].MIN_QUANTITY) : this.stepQuantity;
				}
				this.stepPcQuantity = parseInt(newOffer.PC_STEP_QUANTITY, 10);
				this.maxPcQuantity = parseInt(newOffer.PC_MAX_QUANTITY, 10);
				this.minPcQuantity = this.stepPcQuantity;
				this.stepSqMQuantity = Math.round(parseFloat(newOffer.SQ_M_STEP_QUANTITY) * this.precisionFactor) / this.precisionFactor;
				this.maxSqMQuantity = parseFloat(newOffer.SQ_M_MAX_QUANTITY);
				this.minSqMQuantity = this.currentPriceMode === 'Q' ? parseFloat(this.currentPrices[this.currentPriceSelected].SQ_M_MIN_QUANTITY) : this.stepSqMQuantity;
				
				if(this.config.showQuantity) {
					if(this.obQuantity) {
						var isDifferentMinQuantity = oldOffer.ITEM_PRICES.length
							&& oldOffer.ITEM_PRICES[oldOffer.ITEM_PRICE_SELECTED]
							&& oldOffer.ITEM_PRICES[oldOffer.ITEM_PRICE_SELECTED].MIN_QUANTITY != this.minQuantity;

						if(this.isDblQuantity) {
							resetQuantity = Math.round(parseFloat(oldOffer.STEP_QUANTITY) * this.precisionFactor) / this.precisionFactor !== this.stepQuantity
								|| isDifferentMinQuantity
								|| oldOffer.MEASURE !== newOffer.MEASURE
								|| (
									this.checkQuantity
									&& parseFloat(oldOffer.MAX_QUANTITY) > this.maxQuantity
									&& parseFloat(this.obQuantity.value) > this.maxQuantity
								);
						} else {
							resetQuantity = parseInt(oldOffer.STEP_QUANTITY, 10) !== this.stepQuantity
								|| isDifferentMinQuantity
								|| oldOffer.MEASURE !== newOffer.MEASURE
								|| (
									this.checkQuantity
									&& parseInt(oldOffer.MAX_QUANTITY, 10) > this.maxQuantity
									&& parseInt(this.obQuantity.value, 10) > this.maxQuantity
								);
						}

						this.obQuantity.disabled = !this.canBuy;

						if(resetQuantity) {
							this.obQuantity.value = this.minQuantity;
						}
					}

					if(this.obMeasure) {
						if(newOffer.MEASURE) {
							BX.adjust(this.obMeasure, {html: newOffer.MEASURE});
						} else {
							BX.adjust(this.obMeasure, {html: ''});
						}
					}
					
					if(this.obPriceMeasure) {
						if(newOffer.MEASURE) {
							BX.adjust(this.obPriceMeasure, {html: '/' + newOffer.MEASURE});
						} else {
							BX.adjust(this.obPriceMeasure, {html: ''});
						}
					}
					
					if(this.obPcQuantity && this.obSqMQuantity) {
						if(this.currentMeasure.SYMBOL_INTL == 'pc. 1' || this.currentMeasure.SYMBOL_INTL == 'm2') {
							if(price.SQ_M_PRICE) {
								var isDifferentMinPcQuantity = oldOffer.ITEM_PRICES.length
									&& oldOffer.ITEM_PRICES[oldOffer.ITEM_PRICE_SELECTED]
									&& oldOffer.ITEM_PRICES[oldOffer.ITEM_PRICE_SELECTED].PC_MIN_QUANTITY != this.minPcQuantity;

								resetPcQuantity = parseInt(oldOffer.PC_STEP_QUANTITY, 10) !== this.stepPcQuantity
									|| isDifferentMinPcQuantity
									|| oldOffer.MEASURE !== newOffer.MEASURE
									|| (
										this.checkQuantity
										&& parseInt(oldOffer.PC_MAX_QUANTITY, 10) > this.maxPcQuantity
										&& parseInt(this.obPcQuantity.value, 10) > this.maxPcQuantity
									);
							} else {
								var isDifferentMinSqMQuantity = oldOffer.ITEM_PRICES.length
									&& oldOffer.ITEM_PRICES[oldOffer.ITEM_PRICE_SELECTED]
									&& oldOffer.ITEM_PRICES[oldOffer.ITEM_PRICE_SELECTED].SQ_M_MIN_QUANTITY != this.minSqMQuantity;

								resetSqMQuantity = Math.round(parseFloat(oldOffer.SQ_M_STEP_QUANTITY) * this.precisionFactor) / this.precisionFactor !== this.stepSqMQuantity
									|| isDifferentMinSqMQuantity
									|| oldOffer.MEASURE !== newOffer.MEASURE
									|| (
										this.checkQuantity
										&& parseFloat(oldOffer.SQ_M_MAX_QUANTITY) > this.maxSqMQuantity
										&& parseFloat(this.obSqMQuantity.value) > this.maxSqMQuantity
									);
							}

							this.obPcQuantity.disabled = !this.canBuy;
							this.obSqMQuantity.disabled = !this.canBuy;

							if(resetPcQuantity || resetSqMQuantity) {
								this.obPcQuantity.value = this.minPcQuantity;
								this.obSqMQuantity.value = this.minSqMQuantity;
							}
							
							if(this.obPriceMeasure)
								BX.adjust(this.obPriceMeasure, {html: '/' + BX.message('SQ_M_MESSAGE')});
							
							BX.style(this.obPcQuantity.parentNode, 'display', '');
							BX.style(this.obSqMQuantity.parentNode, 'display', '');
							BX.style(this.obQuantity.parentNode, 'display', 'none');
						} else {
							BX.style(this.obPcQuantity.parentNode, 'display', 'none');
							BX.style(this.obSqMQuantity.parentNode, 'display', 'none');
							BX.style(this.obQuantity.parentNode, 'display', '');
						}
					}
				}
				
				if(this.obQuantityLimit.all && this.obQuantityLimitNotAvl.all) {					
					if(this.canBuy) {
						if(!this.checkQuantity) {
							BX.adjust(this.obQuantityLimit.value, {html: ''});												
						} else {
							if(this.config.showMaxQuantity === 'M') {
								strLimit = (this.maxQuantity / this.stepQuantity >= this.config.relativeQuantityFactor)
									? BX.message('RELATIVE_QUANTITY_MANY')
									: BX.message('RELATIVE_QUANTITY_FEW');
							} else {
								strLimit = this.maxQuantity;
							}
							BX.adjust(this.obQuantityLimit.value, {html: strLimit});							
						}
						BX.adjust(this.obQuantityLimit.all, {style: {display: ''}});
						BX.adjust(this.obQuantityLimitNotAvl.all, {style: {display: 'none'}});
					} else {
						BX.adjust(this.obQuantityLimit.value, {html: ''});
						BX.adjust(this.obQuantityLimit.all, {style: {display: 'none'}});
						BX.adjust(this.obQuantityLimitNotAvl.all, {style: {display: ''}});
					}
				}

				if(this.config.usePriceRanges && this.obPriceRanges) {
					if(this.currentPriceMode === 'Q' && newOffer.PRICE_RANGES_HTML) {
						var rangesBody = this.getEntity(this.obPriceRanges, 'price-ranges-body');

						if(rangesBody) {
							rangesBody.innerHTML = newOffer.PRICE_RANGES_HTML;
						}
						
						this.obPriceRanges.style.display = '';
					} else {
						this.obPriceRanges.style.display = 'none';
					}

				}
			}
		},

		selectOfferProp: function() {
			var i = 0,
				strTreeValue = '',
				arTreeItem = [],
				rowItems = null,
				target = BX.proxy_context;

			if(target && target.hasAttribute('data-treevalue')) {
				if(BX.hasClass(target, 'selected'))
					return;

				if(typeof document.activeElement === 'object') {
					document.activeElement.blur();
				}

				strTreeValue = target.getAttribute('data-treevalue');
				arTreeItem = strTreeValue.split('_');
				this.searchOfferPropIndex(arTreeItem[0], arTreeItem[1]);
				rowItems = BX.findChildren(target.parentNode, {tagName: 'li'}, false);

				if(rowItems && rowItems.length) {
					for(i = 0; i < rowItems.length; i++) {
						BX.removeClass(rowItems[i], 'selected');
					}
				}

				BX.addClass(target, 'selected');
			}
		},

		searchOfferPropIndex: function(strPropID, strPropValue) {
			var strName = '',
				arShowValues = false,
				arCanBuyValues = [],
				allValues = [],
				index = -1,
				i, j,
				arFilter = {},
				tmpFilter = [];

			for(i = 0; i < this.treeProps.length; i++) {
				if(this.treeProps[i].ID === strPropID) {
					index = i;
					break;
				}
			}

			if(index > -1) {
				for(i = 0; i < index; i++) {
					strName = 'PROP_' + this.treeProps[i].ID;
					arFilter[strName] = this.selectedValues[strName];
				}

				strName = 'PROP_' + this.treeProps[index].ID;
				arFilter[strName] = strPropValue;

				for(i = index + 1; i < this.treeProps.length; i++) {
					strName = 'PROP_' + this.treeProps[i].ID;
					arShowValues = this.getRowValues(arFilter, strName);

					if(!arShowValues)
						break;

					allValues = [];

					if(this.config.showAbsent) {
						arCanBuyValues = [];
						tmpFilter = [];
						tmpFilter = BX.clone(arFilter, true);

						for(j = 0; j < arShowValues.length; j++) {
							tmpFilter[strName] = arShowValues[j];
							allValues[allValues.length] = arShowValues[j];
							if(this.getCanBuy(tmpFilter))
								arCanBuyValues[arCanBuyValues.length] = arShowValues[j];
						}
					} else {
						arCanBuyValues = arShowValues;
					}

					if(this.selectedValues[strName] && BX.util.in_array(this.selectedValues[strName], arCanBuyValues)) {
						arFilter[strName] = this.selectedValues[strName];
					} else {
						if(this.config.showAbsent) {
							arFilter[strName] = (arCanBuyValues.length ? arCanBuyValues[0] : allValues[0]);
						} else {
							arFilter[strName] = arCanBuyValues[0];
						}
					}

					this.updateRow(i, arFilter[strName], arShowValues, arCanBuyValues);
				}

				this.selectedValues = arFilter;
				this.changeInfo();
			}
		},

		updateRow: function(intNumber, activeId, showId, canBuyId) {
			var i = 0,
				value = '',
				isCurrent = false,
				rowItems = null;

			var lineContainer = this.getEntities(this.obTree, 'sku-line-block');

			if(intNumber > -1 && intNumber < lineContainer.length) {
				rowItems = lineContainer[intNumber].querySelectorAll('li');
				for(i = 0; i < rowItems.length; i++) {
					value = rowItems[i].getAttribute('data-onevalue');
					isCurrent = value === activeId;

					if(isCurrent) {
						BX.addClass(rowItems[i], 'selected');
					} else {
						BX.removeClass(rowItems[i], 'selected');
					}

					if(BX.util.in_array(value, canBuyId)) {
						BX.removeClass(rowItems[i], 'notallowed');
					} else {
						BX.addClass(rowItems[i], 'notallowed');
					}

					rowItems[i].style.display = BX.util.in_array(value, showId) ? '' : 'none';

					if(isCurrent) {
						lineContainer[intNumber].style.display = (value == 0 && canBuyId.length == 1) ? 'none' : '';
					}
				}
			}
		},

		getRowValues: function(arFilter, index) {
			var arValues = [],
				i = 0,
				j = 0,
				boolSearch = false,
				boolOneSearch = true;

			if(arFilter.length === 0) {
				for(i = 0; i < this.offers.length; i++) {
					if(!BX.util.in_array(this.offers[i].TREE[index], arValues)) {
						arValues[arValues.length] = this.offers[i].TREE[index];
					}
				}
				boolSearch = true;
			} else {
				for(i = 0; i < this.offers.length; i++) {
					boolOneSearch = true;

					for(j in arFilter) {
						if(arFilter[j] !== this.offers[i].TREE[j]) {
							boolOneSearch = false;
							break;
						}
					}

					if(boolOneSearch) {
						if(!BX.util.in_array(this.offers[i].TREE[index], arValues)) {
							arValues[arValues.length] = this.offers[i].TREE[index];
						}

						boolSearch = true;
					}
				}
			}

			return (boolSearch ? arValues : false);
		},

		getCanBuy: function(arFilter) {
			var i,
				j = 0,
				boolOneSearch = true,
				boolSearch = false;

			for(i = 0; i < this.offers.length; i++) {
				boolOneSearch = true;

				for(j in arFilter) {
					if(arFilter[j] !== this.offers[i].TREE[j]) {
						boolOneSearch = false;
						break;
					}
				}

				if(boolOneSearch) {
					if(this.offers[i].CAN_BUY) {
						boolSearch = true;
						break;
					}
				}
			}

			return boolSearch;
		},

		setCurrent: function() {
			var i,
				j = 0,
				strName = '',
				arShowValues = false,
				arCanBuyValues = [],
				arFilter = {},
				tmpFilter = [],
				current = this.offers[this.offerNum].TREE;

			for(i = 0; i < this.treeProps.length; i++) {
				strName = 'PROP_' + this.treeProps[i].ID;
				arShowValues = this.getRowValues(arFilter, strName);

				if(!arShowValues)
					break;

				if(BX.util.in_array(current[strName], arShowValues)) {
					arFilter[strName] = current[strName];
				} else {
					arFilter[strName] = arShowValues[0];
					this.offerNum = 0;
				}

				if(this.config.showAbsent) {
					arCanBuyValues = [];
					tmpFilter = [];
					tmpFilter = BX.clone(arFilter, true);

					for(j = 0; j < arShowValues.length; j++) {
						tmpFilter[strName] = arShowValues[j];

						if(this.getCanBuy(tmpFilter)) {
							arCanBuyValues[arCanBuyValues.length] = arShowValues[j];
						}
					}
				} else {
					arCanBuyValues = arShowValues;
				}

				this.updateRow(i, arFilter[strName], arShowValues, arCanBuyValues);
			}

			this.selectedValues = arFilter;
			this.changeInfo();
		},

		changeInfo: function() {
			var index = -1,
				j = 0,
				boolOneSearch = true,
				eventData = {
					currentId: (this.offerNum > -1 ? this.offers[this.offerNum].ID : 0),
					newId: 0
				};

			var i, offerGroupNode;

			for(i = 0; i < this.offers.length; i++) {
				boolOneSearch = true;

				for(j in this.selectedValues) {
					if(this.selectedValues[j] !== this.offers[i].TREE[j]) {
						boolOneSearch = false;
						break;
					}
				}

				if(boolOneSearch) {
					index = i;
					break;
				}
			}

			if(index > -1) {
				this.drawImages(this.offers[index].SLIDER);
				this.checkSliderControls(this.offers[index].SLIDER_COUNT);

				for(i = 0; i < this.offers.length; i++) {
					if(this.slider.controls[i].ID) {
						if(i === index) {
							this.product.slider = this.slider.controls[i];
							this.slider.controls[i].CONT && BX.show(this.slider.controls[i].CONT);
						} else {
							this.slider.controls[i].CONT && BX.hide(this.slider.controls[i].CONT);
						}
					} else if(i === index) {
						this.product.slider = {};
					}
				}

				if(this.offers[index].SLIDER_COUNT > 0) {					
					j = 0;
					for(i = 0; i < this.offers[index].SLIDER.length; i++) {
						if(!!this.offers[index].SLIDER[i].VALUE && this.offers[index].SLIDER[i].VALUE != '')
							j++;
					}
					this.setMainPict(this.offers[index].ID, this.offers[index].SLIDER[j].ID, true);
				} else {
					this.setMainPictFromItem(index);
				}

				if(this.offers[index].SLIDER_COUNT > 1) {
					this.initSlider();
				} else {
					this.stopSlider();
				}

				if(this.obArticle.all) {
					BX.adjust(this.obArticle.value, {html: this.offers[index].ARTICLE});
				}

				if(this.config.showSkuProps) {
					if(this.obSkuProps) {
						var skuProps = this.obSkuProps.querySelectorAll('[data-entity="sku-props"]');
						for(var i in skuProps) {
							if(skuProps.hasOwnProperty(i)) {
								this.obSkuProps.removeChild(skuProps[i]);
							}
						}
						if(this.offers[index].DISPLAY_PROPERTIES) {
							BX.adjust(this.obSkuProps, {
								html: this.obSkuProps.innerHTML + this.offers[index].DISPLAY_PROPERTIES
							});
						}
					}

					if(this.obMainSkuProps) {
						var mainSkuProps = this.obMainSkuProps.querySelectorAll('[data-entity="sku-props"]');
						for(var i in mainSkuProps) {
							if(mainSkuProps.hasOwnProperty(i)) {
								this.obMainSkuProps.removeChild(mainSkuProps[i]);
							}
						}
						if(this.offers[index].DISPLAY_PROPERTIES_MAIN_BLOCK) {
							BX.adjust(this.obMainSkuProps, {
								html: this.obMainSkuProps.innerHTML + this.offers[index].DISPLAY_PROPERTIES_MAIN_BLOCK
							});
						}
					}
				}

				this.quantitySet(index);
				this.setPrice();				
				this.setDelayed(this.offers[index].DELAYED);
				this.setBuyedAdded(this.offers[index].BUYED_ADDED);
				this.setCompared(this.offers[index].COMPARED);
				this.setConstructor(index);
				
				this.offerNum = index;				
				this.setAnalyticsDataLayer('showDetail');
				this.incViewedCounter();

				eventData.newId = this.offers[this.offerNum].ID;
				//only for compatible catalog.store.amount custom templates
				BX.onCustomEvent('onCatalogStoreProductChange', [this.offers[this.offerNum].ID]);
				//new event
				BX.onCustomEvent('onCatalogElementChangeOffer', eventData);
				eventData = null;
			}
		},

		drawImages: function(images) {
			if(!this.node.videoImageContainer)
				return;

			var i, j = 0, img, entities = this.getEntities(this.node.videoImageContainer, 'image');
			for(i in entities) {
				if(entities.hasOwnProperty(i) && BX.type.isDomNode(entities[i])) {
					BX.remove(entities[i]);
				}
			}

			for(i = 0; i < images.length; i++) {
				if(!!images[i].VALUE && images[i].VALUE != '') {
					j++;
				} else {
					img = BX.create('IMG', {
						props: {
							src: images[i].SRC,
							alt: this.config.alt,
							title: this.config.title
						}
					});

					if(i == j) {
						img.setAttribute('itemprop', 'image');
					}

					this.node.videoImageContainer.appendChild(
						BX.create('DIV', {
							attrs: {
								'data-entity': 'image',
								'data-id': images[i].ID
							},
							props: {
								className: 'product-item-detail-slider-image' + (i == j ? ' active' : '')
							},
							children: [img]
						})
					);
				}
			}
		},
		
		checkPriceRange: function(quantity) {
			if(typeof quantity === 'undefined'|| this.currentPriceMode != 'Q')
				return;

			var range, found = false;

			for(var i in this.currentQuantityRanges) {
				if(this.currentQuantityRanges.hasOwnProperty(i)) {
					range = this.currentQuantityRanges[i];

					if(
						parseInt(quantity) >= parseInt(range.SORT_FROM)
						&& (
							range.SORT_TO == 'INF'
							|| parseInt(quantity) <= parseInt(range.SORT_TO)
						)
					) {
						found = true;
						this.currentQuantityRangeSelected = range.HASH;
						break;
					}
				}
			}

			if(!found && (range = this.getMinPriceRange())) {
				this.currentQuantityRangeSelected = range.HASH;
			}

			for(var k in this.currentPrices) {
				if(this.currentPrices.hasOwnProperty(k)) {
					if(this.currentPrices[k].QUANTITY_HASH == this.currentQuantityRangeSelected) {
						this.currentPriceSelected = k;
						break;
					}
				}
			}
		},

		getMinPriceRange: function() {
			var range;

			for(var i in this.currentQuantityRanges) {
				if(this.currentQuantityRanges.hasOwnProperty(i)) {
					if(
						!range
						|| parseInt(this.currentQuantityRanges[i].SORT_FROM) < parseInt(range.SORT_FROM)
					) {
						range = this.currentQuantityRanges[i];
					}
				}
			}

			return range;
		},

		checkQuantityControls: function() {
			if(this.obQuantity) {
				var reachedTopLimit = this.checkQuantity && parseFloat(this.obQuantity.value) + this.stepQuantity > this.maxQuantity,
					reachedBottomLimit = parseFloat(this.obQuantity.value) - this.stepQuantity < this.minQuantity;

				if(reachedTopLimit) {
					BX.addClass(this.obQuantityUp, 'product-item-detail-amount-btn-disabled');
				} else if(BX.hasClass(this.obQuantityUp, 'product-item-detail-amount-btn-disabled')) {
					BX.removeClass(this.obQuantityUp, 'product-item-detail-amount-btn-disabled');
				}

				if(reachedBottomLimit) {
					BX.addClass(this.obQuantityDown, 'product-item-detail-amount-btn-disabled');
				} else if(BX.hasClass(this.obQuantityDown, 'product-item-detail-amount-btn-disabled')) {
					BX.removeClass(this.obQuantityDown, 'product-item-detail-amount-btn-disabled');
				}

				if(reachedTopLimit && reachedBottomLimit) {
					this.obQuantity.setAttribute('disabled', 'disabled');
				} else {
					this.obQuantity.removeAttribute('disabled');
				}
			}
			
			if(this.obPcQuantity && this.obSqMQuantity) {
				var reachedPcTopLimit = this.checkQuantity && parseFloat(this.obPcQuantity.value) + this.stepPcQuantity > this.maxPcQuantity,
					reachedPcBottomLimit = parseFloat(this.obPcQuantity.value) - this.stepPcQuantity < this.minPcQuantity;

				if(reachedPcTopLimit) {
					BX.addClass(this.obPcQuantityUp, 'product-item-detail-amount-btn-disabled');
				} else if(BX.hasClass(this.obPcQuantityUp, 'product-item-detail-amount-btn-disabled')) {
					BX.removeClass(this.obPcQuantityUp, 'product-item-detail-amount-btn-disabled');
				}

				if(reachedPcBottomLimit) {
					BX.addClass(this.obPcQuantityDown, 'product-item-detail-amount-btn-disabled');
				} else if(BX.hasClass(this.obPcQuantityDown, 'product-item-detail-amount-btn-disabled')) {
					BX.removeClass(this.obPcQuantityDown, 'product-item-detail-amount-btn-disabled');
				}

				if(reachedPcTopLimit && reachedPcBottomLimit) {
					this.obPcQuantity.setAttribute('disabled', 'disabled');
				} else {
					this.obPcQuantity.removeAttribute('disabled');
				}
			
				var reachedSqMTopLimit = this.checkQuantity && parseFloat(this.obSqMQuantity.value) + this.stepSqMQuantity > this.maxSqMQuantity,
					reachedSqMBottomLimit = parseFloat(this.obSqMQuantity.value) - this.stepSqMQuantity < this.minSqMQuantity;

				if(reachedSqMTopLimit) {
					BX.addClass(this.obSqMQuantityUp, 'product-item-detail-amount-btn-disabled');
				} else if(BX.hasClass(this.obSqMQuantityUp, 'product-item-detail-amount-btn-disabled')) {
					BX.removeClass(this.obSqMQuantityUp, 'product-item-detail-amount-btn-disabled');
				}

				if(reachedSqMBottomLimit) {
					BX.addClass(this.obSqMQuantityDown, 'product-item-detail-amount-btn-disabled');
				} else if(BX.hasClass(this.obSqMQuantityDown, 'product-item-detail-amount-btn-disabled')) {
					BX.removeClass(this.obSqMQuantityDown, 'product-item-detail-amount-btn-disabled');
				}

				if(reachedSqMTopLimit && reachedSqMBottomLimit) {
					this.obSqMQuantity.setAttribute('disabled', 'disabled');
				} else {
					this.obSqMQuantity.removeAttribute('disabled');
				}
			}
		},

		setPrice: function() {
			var economyInfo = '',
				price;
			
			if(this.obQuantity && !this.obPcQuantity && !this.obSqMQuantity) {
				this.checkPriceRange(this.obQuantity.value);
			} else if(this.obPcQuantity && this.obSqMQuantity) {
				if(this.currentMeasure.SYMBOL_INTL == 'pc. 1' || this.currentMeasure.SYMBOL_INTL == 'm2') {
					this.checkPriceRange(this.currentPrices[this.currentPriceSelected].SQ_M_PRICE ? this.obPcQuantity.value : this.obSqMQuantity.value);
				} else {
					this.checkPriceRange(this.obQuantity.value);
				}
			}
			
			this.checkQuantityControls();

			price = this.currentPrices[this.currentPriceSelected];
			
			if(this.obPrice) {
				if(price) {
					if(this.obPriceCurrent) {
						if(price.SQ_M_PRICE) {
							BX.adjust(this.obPriceCurrent, {
								html: BX.Currency.currencyFormat(price.SQ_M_PRICE, price.CURRENCY, true),
								style: {display: price.SQ_M_PRICE > 0 ? '' : 'none'}
							});
						} else {
							BX.adjust(this.obPriceCurrent, {
								html: BX.Currency.currencyFormat(price.PRICE, price.CURRENCY, true),
								style: {display: price.PRICE > 0 ? '' : 'none'}
							});
						}
					}
					if(this.obPriceNotSet) {
						if(price.SQ_M_PRICE)
							BX.adjust(this.obPriceNotSet, {style: {display: price.SQ_M_PRICE > 0 ? 'none' : ''}});
						else
							BX.adjust(this.obPriceNotSet, {style: {display: price.PRICE > 0 ? 'none' : ''}});
					}
				} else {
					if(this.obPriceCurrent)
						BX.adjust(this.obPriceCurrent, {html: '', style: {display: 'none'}});
					if(this.obPriceNotSet)
						BX.adjust(this.obPriceNotSet, {style: {display: 'none'}});
				}
				
				if(price && price.PRICE !== price.BASE_PRICE) {
					if(this.config.showOldPrice) {
						this.obPriceOld && BX.adjust(this.obPriceOld, {
							style: {display: ''},
							html: BX.Currency.currencyFormat(price.SQ_M_BASE_PRICE ? price.SQ_M_BASE_PRICE : price.BASE_PRICE, price.CURRENCY, true)
						});
						
						if(this.obPriceDiscount) {
							economyInfo = BX.message('ECONOMY_INFO_MESSAGE');
							economyInfo = economyInfo.replace('#ECONOMY#', BX.Currency.currencyFormat(price.SQ_M_DISCOUNT ? price.SQ_M_DISCOUNT : price.DISCOUNT, price.CURRENCY, true));
							BX.adjust(this.obPriceDiscount, {style: {display: ''}, html: economyInfo});
						}
					}

					if(this.config.showPercent) {
						this.obPricePercent && BX.removeClass(this.obPricePercent, 'product-item-detail-marker-container-hidden');
						this.obPricePercentVal && BX.adjust(this.obPricePercentVal, {html: -price.PERCENT + '%'});
					}
				} else {
					if(this.config.showOldPrice) {
						this.obPriceOld && BX.adjust(this.obPriceOld, {style: {display: 'none'}, html: ''});						
						this.obPriceDiscount && BX.adjust(this.obPriceDiscount, {style: {display: 'none'}, html: ''});
					}

					if(this.config.showPercent) {
						this.obPricePercent && BX.addClass(this.obPricePercent, 'product-item-detail-marker-container-hidden');
						this.obPricePercentVal && BX.adjust(this.obPricePercentVal, {html: ''});
					}
				}
			}

			if(this.obTotalCost) {
				if(this.obQuantity && !this.obPcQuantity && !this.obSqMQuantity) {
					if(price && price.PRICE > 0) {
						if(this.obQuantity.value != 1) {
							BX.adjust(this.obTotalCost, {style: {display: ''}});
							this.obTotalCostVal && BX.adjust(this.obTotalCostVal, {html: BX.Currency.currencyFormat(price.PRICE * this.obQuantity.value, price.CURRENCY, true)});
						} else {
							BX.adjust(this.obTotalCost, {style: {display: 'none'}});
							this.obTotalCostVal && BX.adjust(this.obTotalCostVal, {html: ''});
						}
					} else {
						BX.adjust(this.obTotalCost, {style: {display: 'none'}});
						this.obTotalCostVal && BX.adjust(this.obTotalCostVal, {html: ''});
					}
				} else if(this.obPcQuantity && this.obSqMQuantity) {
					if(price && price.PRICE > 0) {
						if(this.currentMeasure.SYMBOL_INTL == 'pc. 1' || this.currentMeasure.SYMBOL_INTL == 'm2') {
							if(this.obPcQuantity.value != 1 || this.obSqMQuantity.value != 1) {
								BX.adjust(this.obTotalCost, {style: {display: ''}});
								this.obTotalCostVal && BX.adjust(this.obTotalCostVal, {html: BX.Currency.currencyFormat(price.PRICE * (price.SQ_M_PRICE ? this.obPcQuantity.value : this.obSqMQuantity.value), price.CURRENCY, true)});
							} else {
								BX.adjust(this.obTotalCost, {style: {display: 'none'}});
								this.obTotalCostVal && BX.adjust(this.obTotalCostVal, {html: ''});
							}
						} else {
							if(this.obQuantity.value != 1) {
								BX.adjust(this.obTotalCost, {style: {display: ''}});
								this.obTotalCostVal && BX.adjust(this.obTotalCostVal, {html: BX.Currency.currencyFormat(price.PRICE * this.obQuantity.value, price.CURRENCY, true)});
							} else {
								BX.adjust(this.obTotalCost, {style: {display: 'none'}});
								this.obTotalCostVal && BX.adjust(this.obTotalCostVal, {html: ''});
							}
						}
					} else {
						BX.adjust(this.obTotalCost, {style: {display: 'none'}});
						this.obTotalCostVal && BX.adjust(this.obTotalCostVal, {html: ''});
					}
				}
			}
		},
			
		delay: function() {
			var isDelayed = BX.hasClass(this.obDelay, 'product-item-detail-delayed'),
				productId,
				quantity;
			
			switch(this.productType) {
				case 0: //no catalog
				case 1: //product
				case 2: //set
					productId = this.product.id;
					break;
				case 3: //sku
					productId = this.offers[this.offerNum].ID;
					break;
			}

			if(this.config.showQuantity) {
				if(this.obQuantity && !this.obPcQuantity && !this.obSqMQuantity) {
					quantity = this.obQuantity.value;
				} else if(this.obPcQuantity && this.obSqMQuantity) {
					if(this.currentMeasure.SYMBOL_INTL == 'pc. 1' || this.currentMeasure.SYMBOL_INTL == 'm2') {
						quantity = this.currentPrices[this.currentPriceSelected].SQ_M_PRICE ? this.obPcQuantity.value : this.obSqMQuantity.value;
					} else {
						quantity = this.obQuantity.value;
					}
				}
			} else {						
				quantity = this.currentPrices[this.currentPriceSelected] ? this.currentPrices[this.currentPriceSelected].MIN_QUANTITY : '';
			}

			BX.ajax({
				method: 'POST',				
				dataType: 'json',				
				url: this.delayData.delayPath,
				data: {
					siteId: BX.message('SITE_ID'),
					action: !isDelayed ? 'ADD_TO_DELAY' : 'DELETE_FROM_DELAY',
					id: productId,
					quantity: quantity
				},
				onsuccess: BX.proxy(this.delayResult, this)
			});
		},
		
		delayResult: function(arResult) {
			if(!!arResult.STATUS) {
				if(arResult.STATUS === 'ADDED') {				
					this.setDelayed(true);
					this.setBuyedAdded(false);
					if(this.offers.length > 0) {
						this.offers[this.offerNum].DELAYED = true;
						this.offers[this.offerNum].BUYED_ADDED = false;
					}
				} else if(arResult.STATUS === 'DELETED') {
					this.setDelayed(false);
					if(this.offers.length > 0) {
						this.offers[this.offerNum].DELAYED = false;
					}
				}
				BX.onCustomEvent('OnBasketDelayChange');
			}
		},
		
		setDelayed: function(state) {
			if(!this.obDelay)
				return;

			var icon = this.getEntity(this.obDelay, 'delay-icon');
			
			BX.adjust(this.obDelay, {
				props: {
					className: 'product-item-detail-delay' + (state ? 'ed' : ''),
					title: state ? BX.message('DELAY_OK_MESSAGE') : BX.message('DELAY_MESSAGE')
				}
			});
			if(icon) {
				BX.adjust(icon, {
					props: {
						className: state ? 'icon-star-s' : 'icon-star'
					}
				});
			}
		},

		setDelayInfo: function(delayedIds) {
			if(!BX.type.isArray(delayedIds))
				return;

			for(var i in this.offers) {
				if(this.offers.hasOwnProperty(i)) {
					this.offers[i].DELAYED = BX.util.in_array(this.offers[i].ID, delayedIds);
				}
			}
		},
			
		setBuyedAdded: function(state) {
			var buyAddBtn = BX.util.in_array('BUY', this.config.basketAction) ? this.obBuyBtn : this.obAddToBasketBtn;
			if(!buyAddBtn)
				return;

			if(state) {
				BX.adjust(buyAddBtn, {
					props: {
						className: 'btn btn-buy-ok'
					},
					html: '<i class="icon-ok-b"></i><span>' + BX.message('ADD_BASKET_OK_MESSAGE') + '</span>'
				});
				BX.unbindAll(buyAddBtn);
				BX.bind(buyAddBtn, "click", BX.delegate(this.basketRedirect, this));				
			} else {
				BX.adjust(buyAddBtn, {
					props: {
						className: 'btn btn-buy'
					},
					html: '<i class="icon-cart"></i><span>В корзину</span>'
					//html: '<i class="icon-cart"></i><span>' + BX.message('ADD_BASKET_MESSAGE') + '</span>'
				});
				BX.unbindAll(buyAddBtn);
				BX.bind(buyAddBtn, "click", BX.proxy(BX.util.in_array('BUY', this.config.basketAction) ? this.buyBasket : this.add2Basket, this));
			}
		},

		setBuyAddInfo: function(buyedAddedIds) {
			if(!BX.type.isArray(buyedAddedIds))
				return;

			for(var i in this.offers) {
				if(this.offers.hasOwnProperty(i)) {
					this.offers[i].BUYED_ADDED = BX.util.in_array(this.offers[i].ID, buyedAddedIds);
				}
			}
		},

		compare: function(event) {
			var checkbox = this.getEntity(this.obCompare, 'compare-checkbox'),
				target = BX.getEventTarget(event),
				checked = true;
			
			if(!!checkbox)
				checked = target === checkbox ? checkbox.checked : !checkbox.checked;
			
			var url = checked ? this.compareData.compareUrl : this.compareData.compareDeleteUrl,
				compareLink;
			
			if(!!url) {
				if(target !== checkbox) {
					BX.PreventDefault(event);
					this.setCompared(checked);
				}
				
				switch(this.productType) {
					case 0: // no catalog
					case 1: // product
					case 2: // set
						compareLink = url.replace('#ID#', this.product.id.toString());
						break;
					case 3: // sku
						compareLink = url.replace('#ID#', this.offers[this.offerNum].ID);
						break;
				}

				BX.ajax({
					method: 'POST',
					dataType: checked ? 'json' : 'html',
					url: compareLink + (compareLink.indexOf('?') !== -1 ? '&' : '?') + 'ajax_action=Y',
					onsuccess: checked ? BX.proxy(this.compareResult, this) : BX.proxy(this.compareDeleteResult, this)
				});
			}
		},

		compareResult: function(result) {
			if(!BX.type.isPlainObject(result))
				return;
			
			if(this.offers.length > 0)
				this.offers[this.offerNum].COMPARED = result.STATUS === 'OK';
			
			if(result.STATUS === 'OK')
				BX.onCustomEvent('OnCompareChange');
		},

		compareDeleteResult: function() {
			BX.onCustomEvent('OnCompareChange');

			if(this.offers && this.offers.length)
				this.offers[this.offerNum].COMPARED = false;
		},

		setCompared: function(state) {
			if(!this.obCompare)
				return;

			var checkbox = this.getEntity(this.obCompare, 'compare-checkbox');
			if(!!checkbox)
				checkbox.checked = state;
			
			var title = this.getEntity(this.obCompare, 'compare-title');
			if(!!title)
				title.innerHTML = state ? BX.message('COMPARE_OK_MESSAGE') : BX.message('COMPARE_MESSAGE');
		},

		setCompareInfo: function(comparedIds) {
			if(!BX.type.isArray(comparedIds))
				return;

			for(var i in this.offers) {
				if(this.offers.hasOwnProperty(i))
					this.offers[i].COMPARED = BX.util.in_array(this.offers[i].ID, comparedIds);
			}
		},

		checkDeletedCompare: function(id) {
			switch(this.productType) {
				case 0: // no catalog
				case 1: // product
				case 2: // set
					if(this.product.id == id)
						this.setCompared(false);
					break;
				case 3: // sku
					var i = this.offers.length;
					while(i--) {
						if(this.offers[i].ID == id) {
							this.offers[i].COMPARED = false;
							if(this.offerNum == i)
								this.setCompared(false);
							break;
						}
					}
			}
		},

		setConstructor: function(index) {
			if(!this.obConstructor)
				return;

			var newOffer = this.offers[index],
				oldOffer = this.offers[this.offerNum];

			if(newOffer.ID == oldOffer.ID)
				return;

			this.obConstructor.style.opacity = 0.2;
			BX.ajax.post(
				BX.message('CATALOG_ELEMENT_TEMPLATE_PATH') + '/ajax.php',
				{							
					action: 'setConstructor',
					siteId: BX.message('SITE_ID'),
					parameters: BX.message('CATALOG_ELEMENT_PARAMETERS'),
					productId: newOffer.ID
				},
				BX.delegate(function(result) {
					this.obConstructor.innerHTML = result;
					new BX.easing({
						duration: 2000,
						start: {opacity: 20},
						finish: {opacity: 100},
						transition: BX.easing.makeEaseOut(BX.easing.transitions.quad),
						step: BX.delegate(function(state) {
							this.obConstructor.style.opacity = state.opacity / 100;
						}, this),
						complete: BX.delegate(function() {
							this.obConstructor.removeAttribute('style');
						}, this)
					}).animate();
				}, this)
			);
		},

		initBasketUrl: function() {
			this.basketUrl = (this.basketMode === 'ADD' ? this.basketData.add_url : this.basketData.buy_url);

			switch(this.productType) {
				case 1: //product
				case 2: //set
					this.basketUrl = this.basketUrl.replace('#ID#', this.product.id.toString());
					break;
				case 3: //sku
					this.basketUrl = this.basketUrl.replace('#ID#', this.offers[this.offerNum].ID);
					break;
			}

			this.basketParams = {
				'ajax_basket': 'Y'
			};

			if(this.config.showQuantity) {
				if(this.obQuantity && !this.obPcQuantity && !this.obSqMQuantity) {
					this.basketParams[this.basketData.quantity] = this.obQuantity.value;
				} else if(this.obPcQuantity && this.obSqMQuantity) {
					if(this.currentMeasure.SYMBOL_INTL == 'pc. 1' || this.currentMeasure.SYMBOL_INTL == 'm2') {
						this.basketParams[this.basketData.quantity] = this.currentPrices[this.currentPriceSelected].SQ_M_PRICE ? this.obPcQuantity.value : this.obSqMQuantity.value;
					} else {
						this.basketParams[this.basketData.quantity] = this.obQuantity.value;
					}
				}
			} else {						
				this.basketParams[this.basketData.quantity] = this.currentPrices[this.currentPriceSelected] ? this.currentPrices[this.currentPriceSelected].MIN_QUANTITY : '';
			}
			
			if(this.basketData.sku_props) {
				this.basketParams[this.basketData.sku_props_var] = this.basketData.sku_props;
			}
		},

		fillBasketProps: function() {
			if(!this.visual.BASKET_PROP_DIV)
				return;

			var i = 0,
				propCollection = null,
				foundValues = false,
				obBasketProps = null;
			
			obBasketProps = BX(this.visual.BASKET_PROP_DIV);
			if(!!obBasketProps) {
				propCollection = obBasketProps.getElementsByTagName('input');
				if(propCollection && propCollection.length) {
					for(i = 0; i < propCollection.length; i++) {
						if(!propCollection[i].disabled) {
							switch(propCollection[i].type.toLowerCase()) {
								case 'hidden':
									this.basketParams[propCollection[i].name] = propCollection[i].value;
									foundValues = true;
									break;
								case 'radio':
									if(propCollection[i].checked) {
										this.basketParams[propCollection[i].name] = propCollection[i].value;
										foundValues = true;
									}
									break;
								default:
									break;
							}
						}
					}
				}
			}

			if(!foundValues) {
				this.basketParams[this.basketData.props] = [];
				this.basketParams[this.basketData.props][0] = 0;
			}
		},

		showBasketPropsDropDownPopup: function(element, popupId) {
			var contentNode = element.querySelector('[data-entity="dropdownContent"]');

			if(!!this.obPopupWin)
				this.obPopupWin.close();

			this.obPopupWin = BX.PopupWindowManager.create('basketPropsDropDown' + popupId + '_' + this.visual.ID, element, {
				autoHide: true,
				offsetLeft: 0,
				offsetTop: 3,
				overlay : false,
				draggable: {restrict: true},
				closeByEsc: true,
				className: 'bx-drop-down-popup-window',
				content: BX.clone(contentNode)
			});	
			contentNode.parentNode.appendChild(BX('basketPropsDropDown' + popupId + '_' + this.visual.ID));
			this.obPopupWin.show();
		},

		selectBasketPropsDropDownPopupItem: function(element, valueId) {
			var wrapContainer = BX.findParent(element, {className: 'product-item-detail-basket-props-drop-down'}, false),
				currentValue = wrapContainer.querySelector('INPUT'),
				currentOption = wrapContainer.querySelector('[data-entity="current-option"]');
			currentValue.value = valueId;
			currentOption.innerHTML = element.innerHTML;
			BX.PopupWindowManager.getCurrentPopup().close();
		},
		
		sendToBasket: function() {
			if(!this.canBuy)
				return;

			this.initBasketUrl();
			this.fillBasketProps();
			BX.ajax({
				method: 'POST',
				dataType: 'json',
				url: this.basketUrl,
				data: this.basketParams,
				onsuccess: BX.proxy(this.basketResult, this)
			});
		},

		add2Basket: function() {
			this.basketMode = 'ADD';
			this.basket();			
		},

		buyBasket: function() {
			this.basketMode = 'BUY';
			this.basket();
		},

		basket: function() {
			if(!this.canBuy)
				return;
			
			this.sendToBasket();
		},

		basketResult: function(arResult) {
			if(arResult.STATUS === 'OK') {
				if(this.basketMode == 'BUY') {
					this.basketRedirect();
				} else {				
					var productPict,
						productPictContainer = document.querySelector('[data-entity="videos-images-container"]'),
						productPictContainerWidth = productPictContainer.offsetWidth;

					switch(this.productType) {
						case 1: //product
						case 2: //set
							productPict = this.product.pict.SRC;
							break;
						case 3: //sku
							productPict = this.offers[this.offerNum].DETAIL_PICTURE
								? this.offers[this.offerNum].DETAIL_PICTURE.SRC
								: this.defaultPict.pict.SRC;
							break;
					}
					
					if(!!productPict) {
						document.body.appendChild(
							BX.create('IMG', {
								props: {
									className: 'animated-image'
								},
								style: {
									width: productPictContainerWidth + 'px',								
									position: 'absolute',
									'z-index': '1100'
								},
								attrs: {
									src: productPict
								}
							})
						);
					}
					
					var animatedImg = document.body.querySelector('.animated-image');
					if(!!animatedImg) {
						var topPanel = document.querySelector('.top-panel'),
							miniCart = topPanel.querySelector('.mini-cart__cart');
						
						new BX.easing({
							duration: 500,
							start: {
								width: productPictContainerWidth,
								left: BX.pos(productPictContainer).left,
								top: BX.pos(productPictContainer).top
							},
							finish: {
								width: 70,							
								left: BX.pos(miniCart).left,
								top: BX.pos(miniCart).top
							},
							transition: BX.easing.transitions.linear,
							step: BX.delegate(function(state) {
								animatedImg.style.width = state.width + 'px';							
								animatedImg.style.left = state.left + 'px';
								animatedImg.style.top = state.top + 'px';
							}, this),
							complete: BX.delegate(function() {
								BX.remove(animatedImg);
								this.setBuyedAdded(true);
								this.setDelayed(false);
								if(this.offers.length > 0) {
									this.offers[this.offerNum].BUYED_ADDED = true;
									this.offers[this.offerNum].DELAYED = false;
								}
								BX.onCustomEvent('OnBasketChange');
								this.setAnalyticsDataLayer('addToCart');
							}, this)
						}).animate();
					}
				}
			}
		},

		getObjectWorkingHoursToday: function() {
			BX.ajax({
				url: BX.message('CATALOG_ELEMENT_TEMPLATE_PATH') + '/ajax.php',
				method: 'POST',
				dataType: 'json',
				timeout: 60,
				data: {							
					action: 'objectWorkingHoursToday',						
					timezone: this.object.timezone,
					workingHours: this.object.workingHours
				},
				onsuccess: BX.delegate(function(result) {
					if(!!result.today)
						this.object.workingHoursToday = result.today;
					BX.onCustomEvent(this, 'objectWorkingHoursTodayReceived');
				}, this)
			});
		},

		adjustObjectContacts: function() {
			var content = '';
			
			if(this.object.address || Object.keys(this.object.workingHours).length > 0 || this.object.workingHoursToday || Object.keys(this.object.phone).length > 0 || Object.keys(this.object.email).length > 0 || Object.keys(this.object.skype).length > 0) {
				content += '<div class="slide-panel__contacts" id="' + this.visual.ID + '_contacts">';

					if(this.object.address) {
						content += '<div class="slide-panel__contacts-item">';
							content += '<div class="slide-panel__contacts-item__block">';
								content += '<div class="slide-panel__contacts-item__icon"><i class="icon-map-maker"></i></div>';
								content += '<div class="slide-panel__contacts-item__text">' + this.object.address + '</div>';
							content += '</div>';
						content += '</div>';
					}

					if(this.object.workingHoursToday) {
						for(var i in this.object.workingHoursToday) {
							if(this.object.workingHoursToday.hasOwnProperty(i)) {
								content += '<div class="slide-panel__contacts-item" data-entity="working-hours-today">';
									content += '<div class="slide-panel__contacts-item__hours-today">';
										content += '<div class="slide-panel__contacts-item__today-container">';
											content += '<div class="slide-panel__contacts-item__today">';
												content += '<span class="slide-panel__contacts-item__today-icon"><i class="icon-clock"></i></span>';
												content += '<span class="slide-panel__contacts-item__today-title">' + BX.message('CATALOG_ELEMENT_OBJECT_TODAY') + '</span>';
												if(this.object.workingHoursToday[i].STATUS) {
													content += '<span class="slide-panel__contacts-item__today-status slide-panel__contacts-item__today-status-' + (this.object.workingHoursToday[i].STATUS == 'OPEN' ? 'open' : 'closed') + '"></span>';
												}
											content += '</div>';
										content += '</div>';
										content += '<div class="slide-panel__contacts-item__hours-break">';
											content += '<div class="slide-panel__contacts-item__hours slide-panel__contacts-item__hours-first">';
												content += '<span class="slide-panel__contacts-item__hours-title">';
													if(this.object.workingHoursToday[i].WORK_START && this.object.workingHoursToday[i].WORK_END) {
														if(this.object.workingHoursToday[i].WORK_START != this.object.workingHoursToday[i].WORK_END) {
															content += this.object.workingHoursToday[i].WORK_START + ' - ' + this.object.workingHoursToday[i].WORK_END;
														} else {
															content += BX.message('CATALOG_ELEMENT_OBJECT_24_HOURS');
														}
													} else {
														content += BX.message('CATALOG_ELEMENT_OBJECT_OFF');
													}
												content += '</span>';
												content += '<span class="slide-panel__contacts-item__hours-icon"><i class="icon-arrow-down"></i></span>';
											content += '</div>';
											if(this.object.workingHoursToday[i].WORK_START && this.object.workingHoursToday[i].WORK_END) {
												if(this.object.workingHoursToday[i].WORK_START != this.object.workingHoursToday[i].WORK_END) {
													if(this.object.workingHoursToday[i].BREAK_START && this.object.workingHoursToday[i].BREAK_END) {
														if(this.object.workingHoursToday[i].BREAK_START != this.object.workingHoursToday[i].BREAK_END) {
															content += '<div class="slide-panel__contacts-item__break">';
																content += BX.message('CATALOG_ELEMENT_OBJECT_BREAK') + ' ' + this.object.workingHoursToday[i].BREAK_START + ' - ' + this.object.workingHoursToday[i].BREAK_END;
															content += '</div>';
														}
													}
												}
											}
										content += '</div>';
									content += '</div>';
								content += '</div>';
							}
						}
					}

					if(Object.keys(this.object.workingHours).length > 0) {
						content += '<div class="slide-panel__contacts-item" data-entity="working-hours"' + (this.object.workingHoursToday ? 'style="display: none;"' : '') + '>';
							var key = 0;
							for(var i in this.object.workingHours) {
								if(this.object.workingHours.hasOwnProperty(i)) {										
									content += '<div class="slide-panel__contacts-item__hours-today">';
										content += '<div class="slide-panel__contacts-item__today-container">';
											content += '<div class="slide-panel__contacts-item__today">';
												if(key == 0) {
													content += '<span class="slide-panel__contacts-item__today-icon"><i class="icon-clock"></i></span>';
												}
												content += '<span class="slide-panel__contacts-item__today-title">' + (this.object.workingHoursToday && this.object.workingHoursToday.hasOwnProperty(i) ? BX.message('CATALOG_ELEMENT_OBJECT_TODAY') : this.object.workingHours[i].NAME) + '</span>';
												if(this.object.workingHoursToday && this.object.workingHoursToday.hasOwnProperty(i) && this.object.workingHoursToday[i].STATUS) {
													content += '<span class="slide-panel__contacts-item__today-status slide-panel__contacts-item__today-status-' + (this.object.workingHoursToday[i].STATUS == 'OPEN' ? 'open' : 'closed') + '"></span>';
												}
											content += '</div>';
										content += '</div>';
										content += '<div class="slide-panel__contacts-item__hours-break">';
											content += '<div class="slide-panel__contacts-item__hours' + (key == 0 ? ' slide-panel__contacts-item__hours-first' : '') + '">';
												content += '<span class="slide-panel__contacts-item__hours-title">';
													if(this.object.workingHours[i].WORK_START && this.object.workingHours[i].WORK_END) {
														if(this.object.workingHours[i].WORK_START != this.object.workingHours[i].WORK_END) {
															content += this.object.workingHours[i].WORK_START + ' - ' + this.object.workingHours[i].WORK_END;
														} else {
															content += BX.message('CATALOG_ELEMENT_OBJECT_24_HOURS');
														}
													} else {
														content += BX.message('CATALOG_ELEMENT_OBJECT_OFF');
													}
												content += '</span>';
												if(this.object.workingHoursToday && key == 0) {
													content += '<span class="slide-panel__contacts-item__hours-icon"><i class="icon-arrow-up"></i></span>';
												}
											content += '</div>';
											if(this.object.workingHours[i].WORK_START && this.object.workingHours[i].WORK_END) {
												if(this.object.workingHours[i].WORK_START != this.object.workingHours[i].WORK_END) {
													if(this.object.workingHours[i].BREAK_START && this.object.workingHours[i].BREAK_END) {
														if(this.object.workingHours[i].BREAK_START != this.object.workingHours[i].BREAK_END) {
															content += '<div class="slide-panel__contacts-item__break">';
																content += BX.message('CATALOG_ELEMENT_OBJECT_BREAK') + ' ' + this.object.workingHours[i].BREAK_START + ' - ' + this.object.workingHours[i].BREAK_END;
															content += '</div>';
														}
													}
												}
											}
										content += '</div>';
									content += '</div>';
									key++;
								}
							}
						content += '</div>';
					}
					
					if(Object.keys(this.object.phone).length > 0) {
						for(var i in this.object.phone) {
							if(this.object.phone.hasOwnProperty(i)) {
								content += '<div class="slide-panel__contacts-item">';
									content += '<div class="slide-panel__contacts-item__block">';
										content += '<div class="slide-panel__contacts-item__icon"><i class="icon-phone"></i></div>';
										content += '<div class="slide-panel__contacts-item__text">';
											content += '<a class="slide-panel__contacts-item__phone slide-panel__contacts-item__link" href="tel:' + this.object.phone[i].replace(/[^\d\+]/g,'') + '">' + this.object.phone[i] + '</a>';
											if(this.object.phoneDescription.hasOwnProperty(i) && this.object.phoneDescription[i].length > 0) {
												content += '<span class="slide-panel__contacts-item__descr">' + this.object.phoneDescription[i] + '</span>';
											}
										content += '</div>';
									content += '</div>';
								content += '</div>';
							}
						}
					}
					
					if(Object.keys(this.object.email).length > 0) {
						for(var i in this.object.email) {
							if(this.object.email.hasOwnProperty(i)) {
								content += '<div class="slide-panel__contacts-item">';
									content += '<div class="slide-panel__contacts-item__block">';
										content += '<div class="slide-panel__contacts-item__icon"><i class="icon-mail"></i></div>';
										content += '<div class="slide-panel__contacts-item__text">';
											content += '<a class="slide-panel__contacts-item__link" href="mailto:' + this.object.email[i] + '">' + this.object.email[i] + '</a>';
											if(this.object.emailDescription.hasOwnProperty(i) && this.object.emailDescription[i].length > 0) {
												content += '<span class="slide-panel__contacts-item__descr">' + this.object.emailDescription[i] + '</span>';
											}
										content += '</div>';
									content += '</div>';
								content += '</div>';
							}
						}
					}

					if(Object.keys(this.object.skype).length > 0) {
						for(var i in this.object.skype) {
							if(this.object.skype.hasOwnProperty(i)) {
								content += '<div class="slide-panel__contacts-item">';
									content += '<div class="slide-panel__contacts-item__block">';
										content += '<div class="slide-panel__contacts-item__icon"><i class="fa fa-skype"></i></div>';
										content += '<div class="slide-panel__contacts-item__text">';
											content += '<a class="slide-panel__contacts-item__link" href="skype:' + this.object.skype[i] + '?chat">' + this.object.skype[i] + '</a>';
											if(this.object.skypeDescription.hasOwnProperty(i) && this.object.skypeDescription[i].length > 0) {
												content += '<span class="slide-panel__contacts-item__descr">' + this.object.skypeDescription[i] + '</span>';
											}
										content += '</div>';
									content += '</div>';
								content += '</div>';
							}
						}
					}
				
				content += '</div>';
				
				this.object.content = content;
				
				BX.onCustomEvent(this, 'objectContactsAdjusted');
			}
		},

		objectContactsForm: function() {
			if(!!this.sPanel) {				
				BX.ajax.post(
					BX.message('SITE_DIR') + 'ajax/slide_panel.php',
					{
						action: 'callback_objects'
					},
					BX.delegate(function(result) {
						this.sPanel.appendChild(
							BX.create('DIV', {
								props: {
									className: 'slide-panel__title-wrap'
								},
								children: [
									BX.create('I', {
										props: {
											className: 'icon-phone-call'
										}
									}),						
									BX.create('SPAN', {
										props: {
											className: 'slide-panel__title'
										},
										html: this.object.name
									}),
									BX.create('SPAN', {
										props: {
											className: 'slide-panel__close'
										},
										children: [
											BX.create('I', {
												props: {
													className: 'icon-close'
												}
											})
										]
									})
								]
							})
						);

						this.sPanel.appendChild(
							BX.create('DIV', {
								props: {
									className: 'slide-panel__content scrollbar-inner'
								},
								html: this.object.content + result
							})
						);

						var sPanelContent = this.sPanel.querySelector('.slide-panel__content');
						if(!!sPanelContent)
							$(sPanelContent).scrollbar();

						var sPanelFormObjectIdInput = this.sPanel.querySelector('[name="OBJECT_ID"]');
						if(!!sPanelFormObjectIdInput)
							sPanelFormObjectIdInput.value = this.object.id;

						var scrollWidth = window.innerWidth - document.body.clientWidth;
						if(scrollWidth > 0) {
							BX.style(document.body, 'padding-right', scrollWidth + 'px');

							var pageBg = document.querySelector('.page-bg');
							if(!!pageBg)
								BX.style(pageBg, 'margin-right', scrollWidth + 'px');
							
							var topPanel = document.querySelector('.top-panel');
							if(!!topPanel) {
								if(BX.hasClass(topPanel, 'fixed'))
									BX.style(topPanel, 'padding-right', scrollWidth + 'px');
								
								var topPanelThead = topPanel.querySelector('.top-panel__thead');
								if(!!topPanelThead && BX.hasClass(topPanelThead, 'fixed'))
									BX.style(topPanelThead, 'padding-right', scrollWidth + 'px');
								
								var topPanelTfoot = topPanel.querySelector('.top-panel__tfoot');
								if(!!topPanelTfoot && BX.hasClass(topPanelTfoot, 'fixed'))
									BX.style(topPanelTfoot, 'padding-right', scrollWidth + 'px');
							}

							if(!!this.obTabsBlock && !!this.tabsPanelFixed)
								BX.style(this.obTabsBlock, 'padding-right', scrollWidth + 'px');
						}

						var scrollTop = BX.GetWindowScrollPos().scrollTop;
						if(!!scrollTop && scrollTop > 0)
							BX.style(document.body, 'top', '-' + scrollTop + 'px');

						BX.addClass(document.body, 'slide-panel-active')
						BX.addClass(this.sPanel, 'active');

						document.body.appendChild(
							BX.create('DIV', {
								props: {
									className: 'modal-backdrop slide-panel__backdrop fadeInBig'
								}
							})
						);
					}, this)
				);
			}
		},

		objectContacts: function() {
			if(!!this.sPanel) {				
				this.sPanel.appendChild(
					BX.create('DIV', {
						props: {
							className: 'slide-panel__title-wrap'
						},
						children: [
							BX.create('I', {
								props: {
									className: 'icon-phone-call'
								}
							}),						
							BX.create('SPAN', {
								props: {
									className: 'slide-panel__title'
								},
								html: this.object.name
							}),
							BX.create('SPAN', {
								props: {
									className: 'slide-panel__close'
								},
								children: [
									BX.create('I', {
										props: {
											className: 'icon-close'
										}
									})
								]
							})
						]
					})
				);

				this.sPanel.appendChild(
					BX.create('DIV', {
						props: {
							className: 'slide-panel__content scrollbar-inner'
						},
						html: this.object.content
					})
				);

				var sPanelContent = this.sPanel.querySelector('.slide-panel__content');
				if(!!sPanelContent)
					$(sPanelContent).scrollbar();

				var scrollWidth = window.innerWidth - document.body.clientWidth;
				if(scrollWidth > 0) {
					BX.style(document.body, 'padding-right', scrollWidth + 'px');

					var pageBg = document.querySelector('.page-bg');
					if(!!pageBg)
						BX.style(pageBg, 'margin-right', scrollWidth + 'px');
					
					var topPanel = document.querySelector('.top-panel');
					if(!!topPanel) {
						if(BX.hasClass(topPanel, 'fixed'))
							BX.style(topPanel, 'padding-right', scrollWidth + 'px');
						
						var topPanelThead = topPanel.querySelector('.top-panel__thead');
						if(!!topPanelThead && BX.hasClass(topPanelThead, 'fixed'))
							BX.style(topPanelThead, 'padding-right', scrollWidth + 'px');
						
						var topPanelTfoot = topPanel.querySelector('.top-panel__tfoot');
						if(!!topPanelTfoot && BX.hasClass(topPanelTfoot, 'fixed'))
							BX.style(topPanelTfoot, 'padding-right', scrollWidth + 'px');
					}

					if(!!this.obTabsBlock && !!this.tabsPanelFixed)
						BX.style(this.obTabsBlock, 'padding-right', scrollWidth + 'px');
				}

				var scrollTop = BX.GetWindowScrollPos().scrollTop;
				if(!!scrollTop && scrollTop > 0)
					BX.style(document.body, 'top', '-' + scrollTop + 'px');
				
				BX.addClass(document.body, 'slide-panel-active')
				BX.addClass(this.sPanel, 'active');
				
				document.body.appendChild(
					BX.create('DIV', {
						props: {
							className: 'modal-backdrop slide-panel__backdrop fadeInBig'
						}
					})
				);
			}
		},

		partnerSiteRedirect: function() {
			var newTab = window.open('', '_blank');
			BX.ajax({
				url: BX.message('CATALOG_ELEMENT_TEMPLATE_PATH') + '/ajax.php',
				method: 'POST',
				dataType: 'json',
				timeout: 60,
				data: {							
					action: 'partnerSiteRedirect',
					productId: this.product.id
				},
				onsuccess: function(result) {
					if(!!result.partnersUrl)
						newTab.location = result.partnersUrl;
				}
			});
		},

		sPanelForm: function() {
			var target = BX.proxy_context && BX.proxy_context.getAttribute('id'),
				action,
				iconClass,
				productId = this.product.id,
				offerId,
				productName,
				productLink = window.location.href.split('?')[0],
				objectId = this.object.id;
			
			if(target == this.visual.ASK_PRICE_LINK) {
				action = !!objectId ? 'ask_price_objects' : 'ask_price';
				iconClass = 'icon-comment';
			} else if(target == this.visual.NOT_AVAILABLE_MESS) {
				action = !!objectId ? 'not_available_objects' : 'not_available';
				iconClass = 'icon-clock';
			} else if(target == this.visual.WHOLESALER_LINK) {
				action = 'wholesaler';
				iconClass = 'icon-comment';
			} else if(target == this.visual.CONSULTATION_LINK) {
				action = 'consultation';
				iconClass = 'icon-comment';
			}
			else if(target == this.visual.ONECLICK_LINK) {
				action = 'oneclick';
				iconClass = 'icon-comment';
			}
			else if(target == this.visual.REQUEST_PRICE_LINK) {
				action = 'request_price';
				iconClass = 'icon-comment';
			}
			
			switch(this.productType) {
				case 1: //product
				case 2: //set
					productName = this.product.name;
					break;
				case 3: //sku
					offerId = this.offers[this.offerNum].ID;
					productName = this.offers[this.offerNum].NAME;
					productLink += '?OFFER_ID=' + offerId; 
					break;
			}
			
			if(!!this.sPanel) {
				BX.ajax.post(
					BX.message('SITE_DIR') + 'ajax/slide_panel.php',
					{
						action: action
					},
					BX.delegate(function(result) {
						this.sPanel.appendChild(
							BX.create('DIV', {
								props: {
									className: 'slide-panel__title-wrap'
								},
								children: [
									BX.create('I', {
										props: {
											className: iconClass
										}
									}),						
									BX.create('SPAN', {
										props: {
											className: 'slide-panel__title'
										}
									}),
									BX.create('SPAN', {
										props: {
											className: 'slide-panel__close'
										},
										children: [
											BX.create('I', {
												props: {
													className: 'icon-close'
												}
											})
										]
									})
								]
							})
						);
						
						this.sPanel.appendChild(
							BX.create('DIV', {
								props: {
									className: 'slide-panel__content scrollbar-inner'
								},
								html: result
							})
						);
						
						var sPanelContent = this.sPanel.querySelector('.slide-panel__content');
						if(!!sPanelContent)
							$(sPanelContent).scrollbar();
						
						var sPanelTitle = this.sPanel.querySelector('.slide-panel__title'),
							sPanelFormTitle = this.sPanel.querySelector('.slide-panel__form-title');
						if(!!sPanelFormTitle) {
							sPanelTitle.innerHTML = sPanelFormTitle.innerHTML;
							BX.remove(sPanelFormTitle);
						}

						var sPanelFormObjectIdInput = this.sPanel.querySelector('[name="OBJECT_ID"]');
						if(!!sPanelFormObjectIdInput && !!objectId)
							sPanelFormObjectIdInput.value = objectId;
						
						var sPanelFormProductIdInput = this.sPanel.querySelector('[name="PRODUCT_ID"]');
						if(!!sPanelFormProductIdInput && !!productId)
							sPanelFormProductIdInput.value = productId;

						var sPanelFormOfferIdInput = this.sPanel.querySelector('[name="OFFER_ID"]');
						if(!!sPanelFormOfferIdInput && !!offerId)
							sPanelFormOfferIdInput.value = offerId;

						var sPanelFormProductLinkInput = this.sPanel.querySelector('[name="PRODUCT_LINK"]');
						if(!!sPanelFormProductLinkInput && !!productLink)
							sPanelFormProductLinkInput.value = productLink;
						
						var sPanelFormMessageInput = this.sPanel.querySelector('[name="MESSAGE"]');
						if(!!sPanelFormMessageInput && !!productName) {
							var sPanelFormMessageInputPh = sPanelFormMessageInput.getAttribute('placeholder');
							BX.adjust(sPanelFormMessageInput, {
								attrs: {
									'placeholder': !!sPanelFormMessageInputPh ? (sPanelFormMessageInputPh + ' "' + productName + '"') : productName
								}
							});
						}

						var scrollWidth = window.innerWidth - document.body.clientWidth;
						if(scrollWidth > 0) {
							BX.style(document.body, 'padding-right', scrollWidth + 'px');

							var pageBg = document.querySelector('.page-bg');
							if(!!pageBg)
								BX.style(pageBg, 'margin-right', scrollWidth + 'px');
							
							var topPanel = document.querySelector('.top-panel');
							if(!!topPanel) {
								if(BX.hasClass(topPanel, 'fixed'))
									BX.style(topPanel, 'padding-right', scrollWidth + 'px');
								
								var topPanelThead = topPanel.querySelector('.top-panel__thead');
								if(!!topPanelThead && BX.hasClass(topPanelThead, 'fixed'))
									BX.style(topPanelThead, 'padding-right', scrollWidth + 'px');
								
								var topPanelTfoot = topPanel.querySelector('.top-panel__tfoot');
								if(!!topPanelTfoot && BX.hasClass(topPanelTfoot, 'fixed'))
									BX.style(topPanelTfoot, 'padding-right', scrollWidth + 'px');
							}
							
							if(!!this.obTabsBlock && !!this.tabsPanelFixed)
								BX.style(this.obTabsBlock, 'padding-right', scrollWidth + 'px');
						}

						var scrollTop = BX.GetWindowScrollPos().scrollTop;
						if(!!scrollTop && scrollTop > 0)
							BX.style(document.body, 'top', '-' + scrollTop + 'px');
						
						BX.addClass(document.body, 'slide-panel-active')
						BX.addClass(this.sPanel, 'active');
						
						var pageTitle = $('title').text();
						$("input[name=PAGE]").val(pageTitle + ' - ' + location.href);
					
						document.body.appendChild(
							BX.create('DIV', {
								props: {
									className: 'modal-backdrop slide-panel__backdrop fadeInBig'
								}
							})
						);
					},
					this)
				);
			}
		},
		
		changeMoreProductsSectionLink: function(event) {
			var target = event.target;
			if(target.getAttribute('data-entity') == 'moreProductsSectionsLink' && !BX.hasClass(target, 'active')) {
				var moreProductsSectionLink = this.getEntities(this.obMoreProductsSectionsLinks, 'moreProductsSectionsLink');
				for(var i in moreProductsSectionLink) {
					if(moreProductsSectionLink.hasOwnProperty(i) && BX.type.isDomNode(moreProductsSectionLink[i])) {
						BX.removeClass(moreProductsSectionLink[i], 'active');
					}
				}

				BX.addClass(target, 'active');
				
				var sectionId = target.getAttribute('data-section-id');
				if(sectionId) {
					var itemProductsContainer = this.obProduct.querySelector('.product-item-detail-more-products');
					if(!!itemProductsContainer) {					
						itemProductsContainer.style.opacity = 0.2;
						BX.ajax.post(
							BX.message('CATALOG_ELEMENT_TEMPLATE_PATH') + '/ajax.php',
							{							
								action: 'changeMoreProductsSectionLink',
								REQUEST_URI: location.href,
								siteId: BX.message('SITE_ID'),
								parameters: BX.message('CATALOG_ELEMENT_PARAMETERS'),
								productIds: this.moreProducts.ids,
								sectionId: sectionId
							},
							BX.delegate(function(result) {
								itemProductsContainer.innerHTML = result;
								new BX.easing({
									duration: 2000,
									start: {opacity: 20},
									finish: {opacity: 100},
									transition: BX.easing.makeEaseOut(BX.easing.transitions.quad),
									step: function(state) {
										itemProductsContainer.style.opacity = state.opacity / 100;
									},
									complete: function() {
										itemProductsContainer.removeAttribute('style');
									}
								}).animate();
							}, this)
						);
					}
				}
			}
		},

		removeNodes: function() {
			var sectionPic = document.body.querySelector('.catalog-section-pic');
			if(!!sectionPic)
				BX.remove(sectionPic);

			var sectionPanelWrapper = document.body.querySelector('.catalog-section-panel-wrapper');
			if(!!sectionPanelWrapper)
				BX.remove(sectionPanelWrapper);
		},
			
		basketRedirect: function() {
			location.href = (this.basketData.basketUrl ? this.basketData.basketUrl : BX.message('BASKET_URL'));
		},
		
		incViewedCounter: function() {
			if(this.currentIsSet && !this.updateViewedCount) {
				switch(this.productType) {
					case 1:
					case 2:
						this.viewedCounter.params.PRODUCT_ID = this.product.id;
						this.viewedCounter.params.PARENT_ID = this.product.id;
						break;
					case 3:
						this.viewedCounter.params.PARENT_ID = this.product.id;
						this.viewedCounter.params.PRODUCT_ID = this.offers[this.offerNum].ID;
						break;
					default:
						return;
				}

				this.viewedCounter.params.SITE_ID = BX.message('SITE_ID');
				this.updateViewedCount = true;
				BX.ajax.post(
					this.viewedCounter.path,
					this.viewedCounter.params,
					BX.delegate(function() {
						this.updateViewedCount = false;
					}, this)
				);
			}
		},

		allowViewedCount: function(update) {
			this.currentIsSet = true;

			if(update) {
				this.incViewedCounter();
			}
		}
	}
})(window);