<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;




$this->setFrameMode(true);

global $arSettings;

$this->addExternalCss(SITE_TEMPLATE_PATH."/js/owlCarousel/owl.carousel.css");
$this->addExternalJS(SITE_TEMPLATE_PATH."/js/owlCarousel/owl.carousel.min.js");

$templateLibrary = array("popup", "fx");
$currencyList = "";

if(!empty($arResult["CURRENCIES"])) {
	$templateLibrary[] = "currency";
	$currencyList = CUtil::PhpToJSObject($arResult["CURRENCIES"], false, true, true);
}

$templateData = array(	
	"TEMPLATE_LIBRARY" => $templateLibrary,
	"CURRENCIES" => $currencyList,
	"ITEM" => array(
		"ID" => $arResult["ID"],
		"IBLOCK_ID" => $arResult["IBLOCK_ID"],
		"OFFERS_SELECTED" => $arResult["OFFERS_SELECTED"],
		"JS_OFFERS" => $arResult["JS_OFFERS"]
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult["ID"]);
$itemIds = array(
	"ID" => $mainId,
	"DISCOUNT_PERCENT_ID" => $mainId."_dsc_pict",	
	"BIG_SLIDER_ID" => $mainId."_big_slider",	
	"SLIDER_CONT_ID" => $mainId."_slider_cont",
	"ARTICLE_ID" => $mainId."_article",
	"OLD_PRICE_ID" => $mainId."_old_price",
	"PRICE_ID" => $mainId."_price",
	"DISCOUNT_PRICE_ID" => $mainId."_price_discount",	
	"SLIDER_CONT_OF_ID" => $mainId."_slider_cont_",
	"QUANTITY_ID" => $mainId."_quantity",
	"QUANTITY_DOWN_ID" => $mainId."_quant_down",
	"QUANTITY_UP_ID" => $mainId."_quant_up",	
	"PC_QUANTITY_ID" => $mainId."_pc_quantity",
	"PC_QUANTITY_DOWN_ID" => $mainId."_pc_quant_down",
	"PC_QUANTITY_UP_ID" => $mainId."_pc_quant_up",	
	"SQ_M_QUANTITY_ID" => $mainId."_sq_m_quantity",
	"SQ_M_QUANTITY_DOWN_ID" => $mainId."_sq_m_quant_down",
	"SQ_M_QUANTITY_UP_ID" => $mainId."_sq_m_quant_up",
	"QUANTITY_MEASURE" => $mainId."_quant_measure",
	"QUANTITY_LIMIT" => $mainId."_quant_limit",
	"QUANTITY_LIMIT_NOT_AVAILABLE" => $mainId."_quant_limit_not_avl",
	"TOTAL_COST_ID" => $mainId."_total_cost",
	"BUY_LINK" => $mainId."_buy_link",
	"ADD_BASKET_LINK" => $mainId."_add_basket_link",	
	"BASKET_ACTIONS_ID" => $mainId."_basket_actions",
	"PARTNERS_LINK" => $mainId."_partners_link",
	"ASK_PRICE_LINK" => $mainId."_ask_price",
	"WHOLESALER_LINK" => $mainId."_wholesaler",
	"CONSULTATION_LINK" => $mainId."_consultation",
	"ONECLICK_LINK" => $mainId."_oneclick",
	"REQUEST_PRICE_LINK" => $mainId."_request_price",
	"NOT_AVAILABLE_MESS" => $mainId."_not_avail",
	"COMPARE_LINK" => $mainId."_compare_link",
	"QUICK_ORDER_LINK" => $mainId."_quick_order",	
	"DELAY_LINK" => $mainId."_delay_link",
	"TREE_ID" => $mainId."_skudiv",
	"DISPLAY_PROP_DIV" => $mainId."_sku_prop",
	"DISPLAY_MAIN_PROP_DIV" => $mainId."_main_sku_prop",	
	"BASKET_PROP_DIV" => $mainId."_basket_prop",
	"SUBSCRIBE_LINK" => $mainId."_subscribe",
	"TABS_ID" => $mainId."_tabs",
	"TAB_CONTAINERS_ID" => $mainId."_tab_containers",
	"CONSTRUCTOR_ID" => $mainId."_constructor"
);
$obName = $templateData["JS_OBJ"] = "ob".preg_replace("/[^a-zA-Z0-9_]/", "x", $mainId);

$name = !empty($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"])
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
	: $arResult["NAME"];

$title = !empty($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"])
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
	: $arResult["NAME"];

$alt = !empty($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"])
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
	: $arResult["NAME"];

$haveOffers = !empty($arResult["OFFERS"]);
if($haveOffers) {
	$actualItem = isset($arResult["OFFERS"][$arResult["OFFERS_SELECTED"]])
		? $arResult["OFFERS"][$arResult["OFFERS_SELECTED"]]
		: reset($arResult["OFFERS"]);
	
	$showSliderControls = false;
	foreach($arResult["OFFERS"] as $offer) {
		if($offer["MORE_PHOTO_COUNT"] > 1) {
			$showSliderControls = true;
			break;
		}
	}
} else {
	$actualItem = $arResult;
	$showSliderControls = $arResult["MORE_PHOTO_COUNT"] > 1;
}

$skuProps = array();       

$price = $actualItem["ITEM_PRICES"][$actualItem["ITEM_PRICE_SELECTED"]];
$measureRatio = $actualItem["ITEM_MEASURE_RATIOS"][$actualItem["ITEM_MEASURE_RATIO_SELECTED"]]["RATIO"];
$showDiscount = $price["PERCENT"] > 0;

$isMeasurePc = $isMeasureSqM = false;
if($actualItem["ITEM_MEASURE"]["SYMBOL_INTL"] == "pc. 1")
	$isMeasurePc = true;
elseif($actualItem["ITEM_MEASURE"]["SYMBOL_INTL"] == "m2")
	$isMeasureSqM = true;

$showDescription = !empty($arResult["DETAIL_TEXT"]);
$showBuyBtn = in_array("BUY", $arParams["ADD_TO_BASKET_ACTION"]);
$showAddBtn = in_array("ADD", $arParams["ADD_TO_BASKET_ACTION"]);
$showSubscribe = $arParams["PRODUCT_SUBSCRIPTION"] === "Y" && ($arResult["CATALOG_SUBSCRIBE"] === "Y" || $haveOffers);

$object = !empty($arResult["PROPERTIES"]["OBJECT"]["FULL_VALUE"]) ? $arResult["PROPERTIES"]["OBJECT"]["FULL_VALUE"] : false;
$partnersUrl = !empty($arResult["PROPERTIES"]["PARTNERS_URL"]["VALUE"]) ? $arResult["PROPERTIES"]["PARTNERS_URL"]["VALUE"] : false;

$moreProductsIds = !empty($arResult["PROPERTIES"]["MORE_PRODUCTS"]["VALUE"]) ? $arResult["PROPERTIES"]["MORE_PRODUCTS"]["VALUE"] : false;

$arParams["MESS_BTN_BUY"] = $arParams["MESS_BTN_BUY"] ?: Loc::getMessage("CT_BCE_CATALOG_BUY");
$arParams["MESS_BTN_ADD_TO_BASKET"] = $arParams["MESS_BTN_ADD_TO_BASKET"] ?: Loc::getMessage("CT_BCE_CATALOG_ADD");
$arParams["MESS_NOT_AVAILABLE"] = $arParams["MESS_NOT_AVAILABLE"] ?: Loc::getMessage("CT_BCE_CATALOG_NOT_AVAILABLE");
$arParams["MESS_BTN_COMPARE"] = $arParams["MESS_BTN_COMPARE"] ?: Loc::getMessage("CT_BCE_CATALOG_COMPARE");
$arParams["MESS_BTN_DELAY"] = $arParams["MESS_BTN_DELAY"] ?: Loc::getMessage("CT_BCE_CATALOG_DELAY");
$arParams["MESS_PRICE_RANGES_TITLE"] = $arParams["MESS_PRICE_RANGES_TITLE"] ?: Loc::getMessage("CT_BCE_CATALOG_PRICE_RANGES_TITLE");
$arParams["MESS_DESCRIPTION_TAB"] = $arParams["MESS_DESCRIPTION_TAB"] ?: Loc::getMessage("CT_BCE_CATALOG_DESCRIPTION_TAB");
$arParams["MESS_PROPERTIES_TAB"] = $arParams["MESS_PROPERTIES_TAB"] ?: Loc::getMessage("CT_BCE_CATALOG_PROPERTIES_TAB");
$arParams["MESS_REVIEWS_TAB"] = $arParams["MESS_REVIEWS_TAB"] ?: Loc::getMessage("CT_BCE_CATALOG_REVIEWS_TAB");
$arParams["MESS_SHOW_MAX_QUANTITY"] = $arParams["MESS_SHOW_MAX_QUANTITY"] ?: Loc::getMessage("CT_BCE_CATALOG_SHOW_MAX_QUANTITY");
$arParams["MESS_RELATIVE_QUANTITY_MANY"] = $arParams["MESS_RELATIVE_QUANTITY_MANY"] ?: Loc::getMessage("CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY");
$arParams["MESS_RELATIVE_QUANTITY_FEW"] = $arParams["MESS_RELATIVE_QUANTITY_FEW"] ?: Loc::getMessage("CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW");?>

<div class="bx-catalog-element" id="<?=$itemIds['ID']?>" itemscope itemtype="http://schema.org/Product">
	<?//TABS//?>
	<div class="product-item-detail-tabs-container" id="<?=$itemIds['TABS_ID']?>">
		<div class="product-item-detail-tabs-block" data-entity="tabs">					
			<div class="product-item-detail-tabs-scroll">
				<ul class="product-item-detail-tabs-list">
					<?//TAB_DESCRIPTION//?>
					<li class="product-item-detail-tab active" data-entity="tab" data-value="description"><?=$arParams["MESS_DESCRIPTION_TAB"]?></li>
					<?//TAB_PROPERTIES//
					if(!empty($arResult["DISPLAY_PROPERTIES"]) || $arResult["SHOW_OFFERS_PROPS"]) {?>
						<li class="product-item-detail-tab" data-entity="tab" data-value="properties"><?=$arParams["MESS_PROPERTIES_TAB"]?></li>
					<?}
					//TAB_FREE_TAB//
					if(!empty($arResult["PROPERTIES"]["FREE_TAB"]["VALUE"])) {?>
						<li class="product-item-detail-tab" data-entity="tab" data-value="free-tab"><?=$arResult["PROPERTIES"]["FREE_TAB"]["NAME"]?></li>
					<?}
					//TAB_FILES_DOCS//
					if(!empty($arResult["PROPERTIES"]["FILES_DOCS"]["FULL_VALUE"])) {?>
						<li class="product-item-detail-tab" data-entity="tab" data-value="files-docs"><?=$arResult["PROPERTIES"]["FILES_DOCS"]["NAME"]?><span><?=count($arResult["PROPERTIES"]["FILES_DOCS"]["FULL_VALUE"])?></span></li>
					<?}
					//TAB_MORE_PRODUCTS//
					if($moreProductsIds) {?>
						<li class="product-item-detail-tab" data-entity="tab" data-value="more-products"><?=$arResult["PROPERTIES"]["MORE_PRODUCTS"]["NAME"]?><span><?=count($moreProductsIds)?></span></li>
					<?}
					//TAB_REVIEWS//
					if(isset($arResult["REVIEWS_COUNT"])) {?>
						<li class="product-item-detail-tab" data-entity="tab" data-value="reviews"><?=$arParams["MESS_REVIEWS_TAB"]?><span><?=$arResult["REVIEWS_COUNT"]?></span></li>
					<?}?>
				</ul>
			</div>
		</div>
	</div>
	<div class="product-item-detail-tabs-content" id="<?=$itemIds['TAB_CONTAINERS_ID']?>">
		<div class="row">
			<div class="col-xs-12 col-md-9" data-entity="product-container">
				<div class="row" data-entity="tab-container" data-value="description">
					<div class="col-xs-12 col-md-7">
						<?//SLIDER//?>
						<div class="product-item-detail-slider-container<?=($showSliderControls ? ' full' : '')?>" id="<?=$itemIds['BIG_SLIDER_ID']?>">
							<span class="product-item-detail-slider-close" data-entity="close-popup"><i class="icon-close"></i></span>
							<div class="product-item-detail-slider-block<?=($arParams['IMAGE_RESOLUTION'] === '1by1' ? ' product-item-detail-slider-block-square' : '')?>">
								<span class="product-item-detail-slider-left" data-entity="slider-control-left" style="display: none;"><i class="icon-arrow-left"></i></span>
								<span class="product-item-detail-slider-right" data-entity="slider-control-right" style="display: none;"><i class="icon-arrow-right"></i></span>
								<?//MARKERS//?>
								<div class="product-item-detail-markers">
									<?if($arParams["SHOW_DISCOUNT_PERCENT"] === "Y") {?>
										<span class="product-item-detail-marker-container<?=($showDiscount ? '' : ' product-item-detail-marker-container-hidden')?>" id="<?=$itemIds['DISCOUNT_PERCENT_ID']?>">
											<span class="product-item-detail-marker product-item-detail-marker-discount product-item-detail-marker-14px"><span data-entity="dsc-perc-val"><?=-$price["PERCENT"]?>%</span></span>
										</span>
									<?}
									if(!empty($arResult["PROPERTIES"]["MARKER"]["FULL_VALUE"])) {
										foreach($arResult["PROPERTIES"]["MARKER"]["FULL_VALUE"] as $key => $arMarker) {
											if($key <= 4) {?>
												<span class="product-item-detail-marker-container">
													<span class="product-item-detail-marker<?=(!empty($arMarker['FONT_SIZE']) ? ' product-item-detail-marker-'.$arMarker['FONT_SIZE'] : '')?>"<?=(!empty($arMarker["BACKGROUND_1"]) && !empty($arMarker["BACKGROUND_2"]) ? " style='background: ".$arMarker["BACKGROUND_2"]."; background: -webkit-linear-gradient(left, ".$arMarker["BACKGROUND_1"].", ".$arMarker["BACKGROUND_2"]."); background: -moz-linear-gradient(left, ".$arMarker["BACKGROUND_1"].", ".$arMarker["BACKGROUND_2"]."); background: -o-linear-gradient(left, ".$arMarker["BACKGROUND_1"].", ".$arMarker["BACKGROUND_2"]."); background: -ms-linear-gradient(left, ".$arMarker["BACKGROUND_1"].", ".$arMarker["BACKGROUND_2"]."); background: linear-gradient(to right, ".$arMarker["BACKGROUND_1"].", ".$arMarker["BACKGROUND_2"].");'" : (!empty($arMarker["BACKGROUND_1"]) && empty($arMarker["BACKGROUND_2"]) ? " style='background: ".$arMarker["BACKGROUND_1"].";'" : (empty($arMarker["BACKGROUND_1"]) && !empty($arMarker["BACKGROUND_2"]) ? " style='background: ".$arMarker["BACKGROUND_2"].";'" : "")))?>><?=(!empty($arMarker["ICON"]) ? "<i class='".$arMarker["ICON"]."'></i>" : "")?><span><?=$arMarker["NAME"]?></span></span>
												</span>
											<?} else {
												break;
											}
										}
										unset($key, $arMarker);
									}?>
								</div>
								<?//MAGNIFIER_DELAY//?>					
								<div class="product-item-detail-icons-container">
									<div class="product-item-detail-magnifier">
										<i class="icon-scale-plus" data-entity="slider-magnifier"></i>
									</div>
									<?if(!$object && !$partnersUrl) {?>
										<div class="product-item-detail-delay" id="<?=$itemIds['DELAY_LINK']?>" title="<?=$arParams['MESS_BTN_DELAY']?>" style="display: <?=($actualItem['CAN_BUY'] && $price['RATIO_PRICE'] > 0 ? '' : 'none')?>;">
											<i class="icon-star" data-entity="delay-icon"></i>
										</div>
									<?}?>
								</div>
								<?//SLIDER_IMAGES//?>
                                <meta itemprop="name" content="<?=$arResult["NAME"]?>" />
								<div class="product-item-detail-slider-videos-images-container" data-entity="videos-images-container">
									<?if(!empty($actualItem["MORE_PHOTO"])) {
										$activeKey = 0;
										foreach($actualItem["MORE_PHOTO"] as $key => $photo) {
											if(!empty($photo["VALUE"])) {
												$activeKey++;?>
												<div class="product-item-detail-slider-video" data-entity="video" data-id="<?=$photo['ID']?>">
													<iframe width="640" height="480" src="<?=$arResult['SCHEME']?>://www.youtube.com/embed/<?=$photo['VALUE']?>?rel=0&showinfo=0&enablejsapi=1" frameborder="0" allowfullscreen></iframe>
												</div>
											<?} else {?>
												<div class="product-item-detail-slider-image<?=($key == $activeKey ? ' active' : '')?>" data-entity="image" data-id="<?=$photo['ID']?>">
													<img src="<?=$photo['SRC']?>" alt="<?=$alt?>" title="<?=$title?>"<?=($key == $activeKey ? " itemprop='image'" : "")?>>
												</div>
											<?}
										}
									}
									unset($activeKey);
									//SLIDER_PROGRESS//
									if($arParams["SLIDER_PROGRESS"] === "Y") {?>
										<div class="product-item-detail-slider-progress-bar" data-entity="slider-progress-bar" style="width: 0;"></div>
									<?}?>
								</div>
								<?//BRAND//
								if(!empty($arResult["PROPERTIES"]["BRAND"]["FULL_VALUE"]["PREVIEW_PICTURE"])) {?>
									<div class="product-item-detail-brand" itemprop="brand" itemscope itemtype="http://schema.org/Brand">
									<meta itemprop="name" content="<?=$arResult['PROPERTIES']['BRAND']['FULL_VALUE']['NAME']?>" />
										<img itemprop="logo" src="<?=$arResult['PROPERTIES']['BRAND']['FULL_VALUE']['PREVIEW_PICTURE']['SRC']?>" width="<?=$arResult['PROPERTIES']['BRAND']['FULL_VALUE']['PREVIEW_PICTURE']['WIDTH']?>" height="<?=$arResult['PROPERTIES']['BRAND']['FULL_VALUE']['PREVIEW_PICTURE']['HEIGHT']?>" alt="<?=$arResult['PROPERTIES']['BRAND']['FULL_VALUE']['NAME']?>" title="<?=$arResult['PROPERTIES']['BRAND']['FULL_VALUE']['NAME']?>" />
									</div>
								<?}?>
							</div>
							<?//SLIDER_CONTROLS//
							if($showSliderControls) {
								if($haveOffers) {
									foreach($arResult["OFFERS"] as $keyOffer => $offer) {
										if(!isset($offer["MORE_PHOTO_COUNT"]) || $offer["MORE_PHOTO_COUNT"] <= 0)
											continue;
										$strVisible = $arResult["OFFERS_SELECTED"] == $keyOffer ? "" : "none";?>
										<div class="hidden-xs hidden-sm product-item-detail-slider-controls-block" id="<?=$itemIds['SLIDER_CONT_OF_ID'].$offer['ID']?>" style="display: <?=$strVisible?>;">
											<?$activeKeyPhoto = 0;
											foreach($offer["MORE_PHOTO"] as $keyPhoto => $photo) {
												if(!empty($photo["VALUE"])) {
													$activeKeyPhoto++;?>
													<div class="product-item-detail-slider-controls-video" data-entity="slider-control" data-value="<?=$offer['ID'].'_'.$photo['ID']?>">
														<div class="product-item-detail-slider-controls-video-image" style="background-image: url('<?=$arResult["SCHEME"]?>://img.youtube.com/vi/<?=$photo["VALUE"]?>/default.jpg');"></div>
														<div class="product-item-detail-slider-controls-video-play"><i class="fa fa-play" aria-hidden="true"></i></div>
													</div>
												<?} else {?>
													<div class="product-item-detail-slider-controls-image<?=($keyPhoto == $activeKeyPhoto ? ' active' : '')?>" data-entity="slider-control" data-value="<?=$offer['ID'].'_'.$photo['ID']?>">
														<img src="<?=$photo['SRC']?>">
													</div>
												<?}
											}?>
										</div>
									<?}
									unset($activeKeyPhoto);
								} else {?>
									<div class="hidden-xs hidden-sm product-item-detail-slider-controls-block" id="<?=$itemIds['SLIDER_CONT_ID']?>">
										<?if(!empty($actualItem["MORE_PHOTO"])) {
											$activeKey = 0;
											foreach($actualItem["MORE_PHOTO"] as $key => $photo) {
												if(!empty($photo["VALUE"])) {
													$activeKey++;?>
													<div class="product-item-detail-slider-controls-video" data-entity="slider-control" data-value="<?=$photo['ID']?>">
														<div class="product-item-detail-slider-controls-video-image" style="background-image: url('<?=$arResult["SCHEME"]?>://img.youtube.com/vi/<?=$photo["VALUE"]?>/default.jpg');"></div>
														<div class="product-item-detail-slider-controls-video-play"><i class="fa fa-play" aria-hidden="true"></i></div>
													</div>
												<?} else {?>
													<div class="product-item-detail-slider-controls-image<?=($key == $activeKey ? ' active' : '')?>" data-entity="slider-control" data-value="<?=$photo['ID']?>">
														<img src="<?=$photo['SRC']?>">
													</div>
												<?}
											}
										}
										unset($activeKey);?>
									</div>
								<?}
							}?>
							<div class="icons-block">
								<?if($arResult["LED_ICON"]){?><img src="<?=$arResult["LED_ICON"]?>" alt="Источником света являются светодиоды" title="Источником света являются светодиоды"><?}?>
								<?if($arResult["ECO_ICON"]){?><img src="<?=$arResult["ECO_ICON"]?>" alt="Светильник не требует специальной утилизации" title="Светильник не требует специальной утилизации"><?}?>
								<?if($arResult["5YEAR_ICON"]){?><img src="<?=$arResult["5YEAR_ICON"]?>" alt="Гарантия 5 лет" title="Гарантия 5 лет"><?}?>
								<?if($arResult["7YEAR_ICON"]){?><img src="<?=$arResult["7YEAR_ICON"]?>" alt="Гарантия 7 лет" title="Гарантия 7 лет"><?}?>
								<?if($arResult["HOME_ICON"]){?><img src="<?=$arResult["HOME_ICON"]?>" alt="Для внутренних помещений" title="Для внутренних помещений"><?}?>
								<?if($arResult["BLOB_ICON"]){?><img src="<?=$arResult["BLOB_ICON"]?>" alt="Может применяться в помещениях с повышенной влажностью" title="Может применяться в помещениях с повышенной влажностью"><?}?>
								<?if($arResult["SNOW_ICON"]){?><img src="<?=$arResult["SNOW_ICON"]?>" alt="Работает при низких температурах" title="Работает при низких температурах"><?}?>
								<?if($arResult["CLIMATE_ICON"]){?><img src="<?=$arResult["CLIMATE_ICON"]?>" alt="Для уличного освещения" title="Для уличного освещения"><?}?>
								<?if($arResult["STREAM_ICON"]){?><img src="<?=$arResult["STREAM_ICON"]?>" alt="<?=$arResult["STREAM_ICON_TITLE"]?>" title="<?=$arResult["STREAM_ICON_TITLE"]?>"><?}?>
								<?if($arResult["TEMP_COLOR_ICON"]){?><img src="<?=$arResult["TEMP_COLOR_ICON"]?>" alt="<?=$arResult["TEMP_COLOR_ICON_TITLE"]?>" title="<?=$arResult["TEMP_COLOR_ICON_TITLE"]?>"><?}?>
								<?if($arResult["ANGLE_ICON"]){?>
									<?foreach($arResult["ANGLE_ICON"] as $arIcon){?>
									<img src="<?=$arIcon["ICON"]?>" alt="<?=$arIcon["ICON_TITLE"]?>" title="<?=$arIcon["ICON_TITLE"]?>">
									<?}?>
								<?}?>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-5 product-item-detail-blocks">
						<?//ARTICLE//
						/*if($haveOffers) {?>
							<div class="product-item-detail-article" id="<?=$itemIds['ARTICLE_ID']?>">
								<?=Loc::getMessage("CT_BCE_CATALOG_ARTICLE");?>
								<span data-entity="article-value"></span>
							</div>
						<?} else {?>
							<div class="product-item-detail-article">
								<?=Loc::getMessage("CT_BCE_CATALOG_ARTICLE");
								$article = $arResult["PROPERTIES"]["ARTNUMBER"]["VALUE"];?>
								<span><?=(!empty($article) ? $article : "-");?></span>
							</div>
						<?}*/?>
						<?if($USER->IsAdmin()){?>
						<div>Кол-во просмотров: <span data-cid="<?=$arResult['ID']?>"></span></div>
						<?}?>
						<?//RATING//
						if(isset($arResult["REVIEWS_COUNT"]) && $arResult["REVIEWS_COUNT"] > 0) {?>							
							<div class="product-item-detail-rating">
								<?if($arResult["RATING_VALUE"] > 0) {?>
									<div class="product-item-detail-rating-val"<?=($arResult["RATING_VALUE"] <= 4.4 ? " data-rate='".intval($arResult["RATING_VALUE"])."'" : "")?>><?=$arResult["RATING_VALUE"]?></div>
								<?}
								$arReviewsDeclension = new Bitrix\Main\Grid\Declension(Loc::getMessage("CT_BCE_CATALOG_REVIEW"), Loc::getMessage("CT_BCE_CATALOG_REVIEWS_1"), Loc::getMessage("CT_BCE_CATALOG_REVIEWS_2"));?>
								<div class="product-item-detail-rating-reviews-count"><?=($arResult["REVIEWS_COUNT"] > 0 ? $arResult["REVIEWS_COUNT"]." ".$arReviewsDeclension->get($arResult["REVIEWS_COUNT"]) : Loc::getMessage("CT_BCE_CATALOG_NO_REVIEWS"))?></div>
								<?unset($arReviewsDeclension);?>
							</div>
							<?if($arResult["REVIEWS_COUNT"] > 0) {?>
								<span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
									<meta itemprop="ratingValue" content="<?=$arResult['RATING_VALUE']?>" />
									<meta itemprop="reviewCount" content="<?=$arResult['REVIEWS_COUNT']?>" />
								</span>
							<?}
						}
						//PREVIEW_TEXT//
						if(!empty($arResult["PREVIEW_TEXT"])) {?>
							<div class="product-item-detail-preview" itemprop="description"><?=$arResult["PREVIEW_TEXT"]?></div>
						<?}
						//PROPERTIES//
						$isMainProps = false;
						if(!empty($arResult["DISPLAY_PROPERTIES"])) {
							foreach($arResult["DISPLAY_PROPERTIES"] as $property) {
								if(isset($arParams["MAIN_BLOCK_PROPERTY_CODE"][$property["CODE"]])) {
									$isMainProps = true;
									break;
								}
							}
						}
						if(!!$isMainProps || $arResult["SHOW_OFFERS_PROPS"]) {?>
							<div class="product-item-detail-main-properties-container">					
								<div class="product-item-detail-properties-block"<?=($arResult["SHOW_OFFERS_PROPS"] ? " id='".$itemIds["DISPLAY_MAIN_PROP_DIV"]."'" : "");?>>
									<?if(!empty($arResult["DISPLAY_PROPERTIES"])) {
										foreach($arResult["DISPLAY_PROPERTIES"] as $property) {
											if(isset($arParams["MAIN_BLOCK_PROPERTY_CODE"][$property["CODE"]])) {?>
												<div class="product-item-detail-properties">
													<div class="product-item-detail-properties-name"><?=$property["NAME"]?></div>
													<div class="product-item-detail-properties-val"><?=$property["DISPLAY_VALUE"]?></div>
												</div>
											<?}
										}
										unset($property);
									}?>
								</div>
							</div>
						<?}
						unset($isMainProps);
						//ADVANTAGES//			
						if(!empty($arResult["PROPERTIES"]["ADVANTAGES"]["FULL_VALUE"])) {?>
							<div class="product-item-detail-advantages">
								<?foreach($arResult["PROPERTIES"]["ADVANTAGES"]["FULL_VALUE"] as $arItem) {
									if(!empty($arItem["PREVIEW_PICTURE"])) {?>
										<div class="product-item-detail-advantages-item">
											<div class="product-item-detail-advantages-item-pic">
												<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" width="<?=$arItem['PREVIEW_PICTURE']['WIDTH']?>" height="<?=$arItem['PREVIEW_PICTURE']['HEIGHT']?>" alt="<?=$arItem['NAME']?>" />
											</div>
											<div class="visible-md visible-lg product-item-detail-advantages-item-tooltip"><?=$arItem["NAME"]?></div>
										</div>
									<?}
								}
								unset($arItem);?>
							</div>
						<?}?>
					</div>
					<?//SET_CONSTRUCTOR//
					if($arResult["MODULES"]["catalog"] && $arResult["OFFER_GROUP"]) {?>
						<div class="col-xs-12 product-item-detail-set-constructor" id="<?=$itemIds['CONSTRUCTOR_ID']?>">
							<?$APPLICATION->IncludeComponent("altop:catalog.set.constructor.enext", ".default",
								array(
									"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
									"IBLOCK_ID" => $arParams["IBLOCK_ID"],
									"ELEMENT_ID" => $actualItem["ID"],
									"BASKET_URL" => $arParams["BASKET_URL"],
									"PRICE_CODE" => $arParams["PRICE_CODE"],
									"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
									"CACHE_TYPE" => $arParams["CACHE_TYPE"],
									"CACHE_TIME" => $arParams["CACHE_TIME"],
									"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
									"BUNDLE_ITEMS_COUNT" => !empty($arResult["PROPERTIES"]["SET_ITEMS_COUNT"]["VALUE"]) ? $arResult["PROPERTIES"]["SET_ITEMS_COUNT"]["VALUE"] : $arParams["SET_ITEMS_COUNT"],
									"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
									"CURRENCY_ID" => $arParams["CURRENCY_ID"],
									"ADD_PROPERTIES_TO_BASKET" => $arParams["ADD_PROPERTIES_TO_BASKET"],
									"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
									"PARTIAL_PRODUCT_PROPERTIES" => $arParams["PARTIAL_PRODUCT_PROPERTIES"],
									"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
									"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"]
								),
								$component,
								array("HIDE_ICONS" => "Y")
							);?>
						</div>
					<?}
					//SET_ITEMS//
					if(!empty($arResult["SET_ITEMS"])) {?>
						<div class="col-xs-12 product-item-detail-set-items">
							<div class="h2"><?=Loc::getMessage("CT_BCE_CATALOG_SET_ITEMS")?></div>
							<?$counter = 0;
							foreach($arResult["SET_ITEMS"] as $arSetItem) {
								if($counter % 3 == 0) {?>
									<div class="row product-item-detail-set-item-row">
								<?}?>
								<div class="col-xs-12 col-md-4">
									<a class="product-item-detail-set-item" href="<?=$arSetItem['DETAIL_PAGE_URL']?>" title="<?=$arSetItem['NAME']?>">
										<span class="product-item-detail-set-item-image">
											<?if(is_array($arSetItem["PREVIEW_PICTURE"])) {?>
												<img src="<?=$arSetItem['PREVIEW_PICTURE']['SRC']?>" width="<?=$arSetItem['PREVIEW_PICTURE']['WIDTH']?>" height="<?=$arSetItem['PREVIEW_PICTURE']['HEIGHT']?>" alt="<?=$arSetItem['NAME']?>" title="<?=$arSetItem['NAME']?>" />
											<?} else {?>
												<img src="<?=SITE_TEMPLATE_PATH?>/images/no_photo.png" width="222" height="222" alt="<?=$arSetItem['NAME']?>" title="<?=$arSetItem['NAME']?>" />
											<?}
											if(!empty($arSetItem["BRAND"]["PREVIEW_PICTURE"])) {?>
												<span class="product-item-detail-set-item-brand">
													<img src="<?=$arSetItem['BRAND']['PREVIEW_PICTURE']['SRC']?>" width="<?=$arSetItem['BRAND']['PREVIEW_PICTURE']['WIDTH']?>" height="<?=$arSetItem['BRAND']['PREVIEW_PICTURE']['HEIGHT']?>" alt="<?=$arSetItem['BRAND']['NAME']?>" title="<?=$arSetItem['BRAND']['NAME']?>" />
												</span>
											<?}?>
										</span>
										<span class="product-item-detail-set-item-title"><?=$arSetItem["NAME"]?></span>
										<span class="product-item-detail-set-item-quantity"><?=$arSetItem["QUANTITY"]." ".$arSetItem["MEASURE"]?></span>
									</a>
								</div>
								<?if($counter % 3 == 2 || $counter == count($arResult["SET_ITEMS"]) - 1) {?>
									</div>
								<?}
								$counter++;
							}
							unset($arSetItem, $counter);?>
						</div>
					<?}
					//GIFTS//
					if($arResult["CATALOG"] && $arParams["USE_GIFTS_DETAIL"] == "Y" && Bitrix\Main\ModuleManager::isModuleInstalled("sale")) {?>
						<div class="col-xs-12 product-item-detail-gifts" data-entity="parent-container" style="display: none;">
							<?if($arParams["GIFTS_DETAIL_HIDE_BLOCK_TITLE"] !== "Y") {?>
								<div class="h2" data-entity="header" data-showed="false" style="display: none; opacity: 0;"><?=($arParams["GIFTS_DETAIL_BLOCK_TITLE"] ?: Loc::getMessage("CT_BCE_CATALOG_GIFTS"))?></div>
							<?}
							CBitrixComponent::includeComponentClass("bitrix:sale.products.gift");?>
							<?$APPLICATION->IncludeComponent("bitrix:sale.products.gift", ".default",
								array(
									"CUSTOM_SITE_ID" => isset($arParams["CUSTOM_SITE_ID"]) ? $arParams["CUSTOM_SITE_ID"] : null,
									"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
									"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
									"PRODUCT_ROW_VARIANTS" => "",
									"PAGE_ELEMENT_COUNT" => 0,
									"DEFERRED_PRODUCT_ROW_VARIANTS" => Bitrix\Main\Web\Json::encode(SaleProductsGiftComponent::predictRowVariants(3, $arParams["GIFTS_DETAIL_PAGE_ELEMENT_COUNT"])),
									"DEFERRED_PAGE_ELEMENT_COUNT" => $arParams["GIFTS_DETAIL_PAGE_ELEMENT_COUNT"],
									"PRODUCT_DISPLAY_MODE" => $arParams["PRODUCT_DISPLAY_MODE"],
									"TEXT_LABEL_GIFT" => $arParams["GIFTS_DETAIL_TEXT_LABEL_GIFT"],
									"ADD_TO_BASKET_ACTION" => (isset($arParams["ADD_TO_BASKET_ACTION"]) ? $arParams["ADD_TO_BASKET_ACTION"] : ""),
									"MESS_BTN_BUY" => $arParams["~GIFTS_MESS_BTN_BUY"],
									"MESS_BTN_ADD_TO_BASKET" => $arParams["~GIFTS_MESS_BTN_BUY"],
									"MESS_BTN_DETAIL" => $arParams["~MESS_BTN_DETAIL"],
									"MESS_BTN_SUBSCRIBE" => $arParams["~MESS_BTN_SUBSCRIBE"],
									"SHOW_PRODUCTS_".$arParams["IBLOCK_ID"] => "Y",
									"PROPERTY_CODE_".$arParams["IBLOCK_ID"] => $arParams["LIST_PROPERTY_CODE"],
									"PROPERTY_CODE_".$arResult["OFFERS_IBLOCK"] => $arParams["OFFER_TREE_PROPS"],
									"OFFER_TREE_PROPS_".$arResult["OFFERS_IBLOCK"] => $arParams["OFFER_TREE_PROPS"],
									"CART_PROPERTIES_".$arResult["OFFERS_IBLOCK"] => $arParams["OFFERS_CART_PROPERTIES"],
									"ADDITIONAL_PICT_PROP_".$arParams["IBLOCK_ID"] => (isset($arParams["ADD_PICT_PROP"]) ? $arParams["ADD_PICT_PROP"] : ""),
									"ADDITIONAL_PICT_PROP_".$arResult["OFFERS_IBLOCK"] => (isset($arParams["OFFER_ADD_PICT_PROP"]) ? $arParams["OFFER_ADD_PICT_PROP"] : ""),
									"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
									"HIDE_NOT_AVAILABLE_OFFERS" => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
									"PRODUCT_SUBSCRIPTION" => $arParams["PRODUCT_SUBSCRIPTION"],
									"PRICE_CODE" => $arParams["PRICE_CODE"],
									"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
									"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
									"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
									"CURRENCY_ID" => $arParams["CURRENCY_ID"],
									"BASKET_URL" => $arParams["BASKET_URL"],
									"ADD_PROPERTIES_TO_BASKET" => $arParams["ADD_PROPERTIES_TO_BASKET"],
									"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
									"PARTIAL_PRODUCT_PROPERTIES" => $arParams["PARTIAL_PRODUCT_PROPERTIES"],
									"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
									"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
									"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
									"POTENTIAL_PRODUCT_TO_BUY" => array(
										"ID" => isset($arResult["ID"]) ? $arResult["ID"] : null,
										"MODULE" => isset($arResult["MODULE"]) ? $arResult["MODULE"] : "catalog",
										"PRODUCT_PROVIDER_CLASS" => isset($arResult["~PRODUCT_PROVIDER_CLASS"]) ? $arResult["~PRODUCT_PROVIDER_CLASS"] : "\Bitrix\Catalog\Product\CatalogProvider",
										"QUANTITY" => isset($arResult["QUANTITY"]) ? $arResult["QUANTITY"] : null,
										"IBLOCK_ID" => isset($arResult["IBLOCK_ID"]) ? $arResult["IBLOCK_ID"] : null,
										"PRIMARY_OFFER_ID" => isset($arResult["OFFERS"][$arResult["OFFERS_SELECTED"]]["ID"]) ? $arResult["OFFERS"][$arResult["OFFERS_SELECTED"]]["ID"] : null,
										"SECTION" => array(
											"ID" => isset($arResult["SECTION"]["ID"]) ? $arResult["SECTION"]["ID"] : null,
											"IBLOCK_ID" => isset($arResult["SECTION"]["IBLOCK_ID"]) ? $arResult["SECTION"]["IBLOCK_ID"] : null,
											"LEFT_MARGIN" => isset($arResult["SECTION"]["LEFT_MARGIN"]) ? $arResult["SECTION"]["LEFT_MARGIN"] : null,
											"RIGHT_MARGIN" => isset($arResult["SECTION"]["RIGHT_MARGIN"]) ? $arResult["SECTION"]["RIGHT_MARGIN"] : null,
										)
									),
									"USE_ENHANCED_ECOMMERCE" => $arParams["USE_ENHANCED_ECOMMERCE"],
									"DATA_LAYER_NAME" => $arParams["DATA_LAYER_NAME"],
									"BRAND_PROPERTY" => $arParams["BRAND_PROPERTY"]
								),
								$component,
								array("HIDE_ICONS" => "Y")
							);?>
						</div>
					<?}
					//DETAIL_TEXT
					if($showDescription) {?>
						<div class="col-xs-12 product-item-detail-description">
							<div class="h2"><?=$arParams["MESS_DESCRIPTION_TAB"]?></div>
							<?=$arResult["DETAIL_TEXT"]?>
						</div>
					<?}?>
				</div>
				<?//PROPERTIES
				if(!empty($arResult["DISPLAY_PROPERTIES"]) || $arResult["SHOW_OFFERS_PROPS"]) {?>			
					<div class="product-item-detail-tab-content" data-entity="tab-container" data-value="properties">
						<div class="product-item-detail-properties-container">
							<div class="h2"><?=Loc::getMessage("CT_BCE_CATALOG_PROPERTIES")?></div>
							<div class="product-item-detail-properties-block"<?=($arResult["SHOW_OFFERS_PROPS"] ? " id='".$itemIds["DISPLAY_PROP_DIV"]."'" : "");?>>
								<?if(!empty($arResult["DISPLAY_PROPERTIES"])) {
									foreach($arResult["DISPLAY_PROPERTIES"] as $property) {?>
										<?if($property['CODE'] == 'LENGTH_LIGHT' && $arResult["PROPERTIES"]["ANGLE"]['VALUE'] || $property['CODE'] == 'DESCRIPTION'){?><?continue;?><?}?>
										<div class="product-item-detail-properties">
											<div class="product-item-detail-properties-name"><?=$property["NAME"]?></div>
											<div class="product-item-detail-properties-val"><?=$property["DISPLAY_VALUE"]?></div>
										</div>
									<?}
									unset($property);
								}?>
							</div>
						</div>
					</div>
				<?}
				//FREE_TAB
				if(!empty($arResult["PROPERTIES"]["FREE_TAB"]["VALUE"])) {?>
					<div class="product-item-detail-tab-content" data-entity="tab-container" data-value="free-tab">
						<div class="h2"><?=$arResult["PROPERTIES"]["FREE_TAB"]["NAME"]?></div>
						<?=$arResult["PROPERTIES"]["FREE_TAB"]["~VALUE"]["TEXT"];?>
					</div>
				<?}
				//FILES_DOCS
				if(!empty($arResult["PROPERTIES"]["FILES_DOCS"]["FULL_VALUE"])) {?>
					<div class="product-item-detail-tab-content" data-entity="tab-container" data-value="files-docs">
						<div class="h2"><?=$arResult["PROPERTIES"]["FILES_DOCS"]["NAME"]?></div>
						<div class="row product-item-detail-files-docs">
							<?foreach($arResult["PROPERTIES"]["FILES_DOCS"]["FULL_VALUE"] as $key => $arDoc) {?><!--
							 --><div class="col-xs-12 col-md-4">
									<a class="product-item-detail-files-docs-item" href="<?=$arDoc['SRC']?>" target="_blank">
										<div class="product-item-detail-files-docs-icon">
											<?if($arDoc["TYPE"] == "doc" || $arDoc["TYPE"] == "docx" || $arDoc["TYPE"] == "rtf") {?>
												<i class="fa fa-file-word-o"></i>
											<?} elseif($arDoc["TYPE"] == "xls" || $arDoc["TYPE"] == "xlsx") {?>
												<i class="fa fa-file-excel-o"></i>
											<?} elseif($arDoc["TYPE"] == "pdf") {?>
												<i class="fa fa-file-pdf-o"></i>
											<?} elseif($arDoc["TYPE"] == "rar" || $arDoc["TYPE"] == "zip" || $arDoc["TYPE"] == "gzip") {?>
												<i class="fa fa-file-archive-o"></i>
											<?} elseif($arDoc["TYPE"] == "jpg" || $arDoc["TYPE"] == "jpeg" || $arDoc["TYPE"] == "png" || $arDoc["TYPE"] == "gif") {?>
												<i class="fa fa-file-image-o"></i>
											<?} elseif($arDoc["TYPE"] == "ppt" || $arDoc["TYPE"] == "pptx") {?>
												<i class="fa fa-file-powerpoint-o"></i>
											<?} elseif($arDoc["TYPE"] == "txt") {?>
												<i class="fa fa-file-text-o"></i>
											<?} else {?>
												<i class="fa fa-file-o"></i>
											<?}?>
										</div>
										<div class="product-item-detail-files-docs-block">
											<span class="product-item-detail-files-docs-name"><?=!empty($arDoc["DESCRIPTION"]) ? $arDoc["DESCRIPTION"] : $arDoc["NAME"]?></span>
											<span class="product-item-detail-files-docs-size"><?=Loc::getMessage("CT_BCE_CATALOG_SIZE").$arDoc["SIZE"]?></span>
										</div>
									</a>
								</div><!--
						 --><?}?>
						</div>
					</div>
				<?}?>				
			</div>
			<div class="col-xs-12 col-md-3">			
				<div class="product-item-detail-ghost-top"></div>				
				<div class="product-item-detail-pay-block">
					<?//SHORT_CARD//?>
					<div class="product-item-detail-short-card">
						<div class="product-item-detail-short-card-image">
							<img src="" data-entity="short-card-picture" />
						</div>
						<div class="product-item-detail-short-card-title"><?=$name?></div>
					</div>
					<?//PRICE//?>
					<div class="product-item-detail-info-container">					
						<div id="<?=$itemIds['PRICE_ID']?>">
												
    						<span class="product-item-detail-price-not-set" data-entity="price-current-not-set"<?=($price["SQ_M_PRICE"] > 0 ? " style='display:none;'" : ($price["PRICE"] > 0 ? " style='display:none;'" : ""))?>><?=Loc::getMessage("CT_BCE_CATALOG_PRICE_NOT_SET")?></span>
    						
    						<!-- оптовая цена -->
    						<? if (!empty($arResult["PROPERTIES"]["OPT_PRICE"]["VALUE"])) { ?>    						
        						<div>        							
        							<span class="product-item-detail-rating-reviews-count"><?=Loc::getMessage("CT_BCE_CATALOG_PRICE_WHOLESALE")?></span>							
        							
        							<span class="product-item-detail-price-current">
        								
										<?=number_format($arResult["PROPERTIES"]["OPT_PRICE"]["VALUE"], 0, ' ', ' ');?>
										<?//=$arResult["PROPERTIES"]["OPT_PRICE"]["VALUE"]?> 
        								<?=str_replace("# ", "", $arResult["CURRENCIES"][0]["FORMAT"]["FORMAT_STRING"]);?>
        							</span>
        							 
        							<span class="product-item-detail-price-measure" style="display:none" data-entity="price-measure"<?=($price["SQ_M_PRICE"] > 0 ? "" : ($price["PRICE"] > 0 ? "" : " style='display:none;'"))?>>/<?=($price["SQ_M_PRICE"] > 0 ? Loc::getMessage("CT_BCE_CATALOG_MEASURE_SQ_M") : $actualItem["ITEM_MEASURE"]["TITLE"])?></span>
        						</div>        						
    						<? } ?>
    						 
    						<div>	
    							<!-- розничная цена -->	
    							<? if (!empty($arResult["PROPERTIES"]["OPT_PRICE"]["VALUE"])) { ?>
    								<span class="product-item-detail-rating-reviews-count"><?=Loc::getMessage("CT_BCE_CATALOG_PRICE_RETAIL")?></span>
    							<? } ?>
    							
    							<span class="product-item-detail-price-current <? if (!empty($arResult["PROPERTIES"]["OPT_PRICE"]["VALUE"])) { ?>double_price<? } ?>" 
    							data-entity="price-current" <?=($price["SQ_M_PRICE"] > 0 ? "" : ($price["PRICE"] > 0 ? "" : " style='display:none;'"))?>>
								<?=($price["SQ_M_PRICE"] > 0 ? $price["SQ_M_PRINT_PRICE"] : $price["PRINT_PRICE"])?>
								</span>
    							<span class="product-item-detail-price-measure" style="display:none" data-entity="price-measure"<?=($price["SQ_M_PRICE"] > 0 ? "" : ($price["PRICE"] > 0 ? "" : " style='display:none;'"))?>>/<?=($price["SQ_M_PRICE"] > 0 ? Loc::getMessage("CT_BCE_CATALOG_MEASURE_SQ_M") : $actualItem["ITEM_MEASURE"]["TITLE"])?></span>
							</div>
							
							<? if (!empty($arResult["PROPERTIES"]["OPT_PRICE"]["VALUE"])) {  } 	 else { ?>
								
								<?if($USER->IsAdmin()){?>
								<!-- Временно отключили -->
								<!--p><br/></p>
								<button type="button" class="btn btn-buy" id="
								<? //$itemIds['REQUEST_PRICE_LINK']
								?>">
									<i class="icon-ok-b"></i>
									<span>Узнать оптовую цену</span>
								</button-->
								<? } ?>
								
							<? } ?>
							
						</div>
						<?if($arParams["SHOW_OLD_PRICE"] === "Y") {?>
							<div class="product-item-detail-price-old" id="<?=$itemIds['OLD_PRICE_ID']?>"<?=($showDiscount ? "" : " style='display:none;'")?>><?=($showDiscount ? ($price["SQ_M_BASE_PRICE"] > 0 ? $price["SQ_M_PRINT_BASE_PRICE"] : $price["PRINT_BASE_PRICE"]) : "")?></div>
							<div class="product-item-detail-price-economy" id="<?=$itemIds['DISCOUNT_PRICE_ID']?>"<?=($showDiscount ? "" : " style='display:none;'")?>><?=($showDiscount ? Loc::getMessage("CT_BCE_CATALOG_ECONOMY_INFO2", array("#ECONOMY#" => ($price["SQ_M_DISCOUNT"] > 0 ? $price["SQ_M_PRINT_DISCOUNT"] : $price["PRINT_DISCOUNT"]))) : "")?></div>
						<?}?>
						<div class="product-item-article">Артикул: <?=$arResult["PROPERTIES"]["ARTNUMBER_V"]["VALUE"]?></div>
					</div>
					<?//QUANTITY_LIMIT
					if($arParams["SHOW_MAX_QUANTITY"] !== "N"){
						if($haveOffers) {?>
							<div class="product-item-detail-info-container" id="<?=$itemIds['QUANTITY_LIMIT']?>" style="display: none;">
								<div class="product-item-detail-quantity">
									<i class="icon-ok-b product-item-detail-quantity-icon"></i>
									<span class="product-item-detail-quantity-val">
										<?=$arParams["MESS_SHOW_MAX_QUANTITY"]."&nbsp;"?>
										<span data-entity="quantity-limit-value"></span>
									</span>
								</div>
							</div>
							<div class="product-item-detail-info-container" id="<?=$itemIds['QUANTITY_LIMIT_NOT_AVAILABLE']?>" style="display: none;">
								<div class="product-item-detail-quantity product-item-detail-quantity-not-avl">
									<i class="icon-close-b product-item-detail-quantity-icon"></i>
									<span class="product-item-detail-quantity-val"><?=$arParams["MESS_NOT_AVAILABLE"]?></span>
								</div>
							</div>
						<?} else {?>
							<div class="product-item-detail-info-container" id="<?=$itemIds['QUANTITY_LIMIT']?>">
								<div class="product-item-detail-quantity<?=($actualItem['CAN_BUY'] ? '' : ' product-item-detail-quantity-not-avl')?>">
									<i class="icon-<?=($actualItem['CAN_BUY'] ? 'ok' : 'close')?>-b product-item-detail-quantity-icon"></i>
									<span class="product-item-detail-quantity-val">
										<?if($actualItem["CAN_BUY"]) {
											echo $arParams["MESS_SHOW_MAX_QUANTITY"]."&nbsp;";
											if($measureRatio && (float)$actualItem["CATALOG_QUANTITY"] > 0 && $actualItem["CATALOG_QUANTITY_TRACE"] === "Y" && $actualItem["CATALOG_CAN_BUY_ZERO"] === "N") {
												if($arParams["SHOW_MAX_QUANTITY"] === "M") {
													if((float)$actualItem["CATALOG_QUANTITY"] / $measureRatio >= $arParams["RELATIVE_QUANTITY_FACTOR"]) {
														echo $arParams["MESS_RELATIVE_QUANTITY_MANY"];
													} else {
														echo $arParams["MESS_RELATIVE_QUANTITY_FEW"];
													}
												} else {
													echo $actualItem["CATALOG_QUANTITY"];
												}
											}?>
										<?} else {
											echo $arParams["MESS_NOT_AVAILABLE"];
										}?>
									</span>
								</div>
							</div>
						<?}
					}
					//PRICE_RANGES//
					if($arParams["USE_PRICE_COUNT"]) {
						$showRanges = !$haveOffers && count($actualItem["ITEM_QUANTITY_RANGES"]) > 1;
						$useRatio = $arParams["USE_RATIO_IN_RANGES"] === "Y";?>
						<div class="product-item-detail-info-container"<?=($showRanges ? "" : " style='display: none;'");?> data-entity="price-ranges-block">
							<div class="product-item-detail-properties-block" data-entity="price-ranges-body">
								<?if($showRanges) {
									foreach($actualItem["ITEM_QUANTITY_RANGES"] as $range) {
										if($range["HASH"] !== "ZERO-INF") {
											$itemPrice = false;
											foreach($arResult["ITEM_PRICES"] as $itemPrice) {
												if($itemPrice["QUANTITY_HASH"] === $range["HASH"]){
													break;
												}
											}
											if($itemPrice) {?>
												<div class="product-item-detail-properties">
													<div class="product-item-detail-properties-name">													
														<?if(is_infinite($range["SORT_TO"])) {
															echo Loc::getMessage("CT_BCE_CATALOG_RANGE_FROM", array("#FROM#" => $range["SORT_FROM"]." ".$actualItem["ITEM_MEASURE"]["TITLE"]));
														} else {
															echo $range["SORT_FROM"].($range["SORT_TO"] != $range["SORT_FROM"] ? " - ".$range["SORT_TO"] : "")." ".$actualItem["ITEM_MEASURE"]["TITLE"];
														}?>
													</div>
													<div class="product-item-detail-properties-val">
														<?=($useRatio ? $itemPrice["PRINT_RATIO_PRICE"] : $itemPrice["PRINT_PRICE"])?>
													</div>
												</div>
											<?}
										}
									}
								}?>
							</div>
						</div>
						<?unset($showRanges, $useRatio, $itemPrice, $range);
					}
					//SKU//
					if($haveOffers && !empty($arResult["OFFERS_PROP"])) {?>
						<div class="product-item-detail-scu-container" id="<?=$itemIds['TREE_ID']?>">
							<?foreach($arResult["SKU_PROPS"] as $skuProperty) {
								if(!isset($arResult["OFFERS_PROP"][$skuProperty["CODE"]]))
									continue;
								$propertyId = $skuProperty["ID"];
								$skuProps[] = array(
									"ID" => $propertyId,
									"SHOW_MODE" => $skuProperty["SHOW_MODE"],
									"VALUES" => $skuProperty["VALUES"],
									"VALUES_COUNT" => $skuProperty["VALUES_COUNT"]
								);?>
								<div class="product-item-detail-info-container" data-entity="sku-line-block">
									<div class="product-item-detail-scu-title"><?=htmlspecialcharsEx($skuProperty["NAME"])?></div>
									<div class="product-item-detail-scu-block">
										<div class="product-item-detail-scu-list">
											<ul class="product-item-detail-scu-item-list">
												<?foreach($skuProperty["VALUES"] as &$value) {
													$value["NAME"] = htmlspecialcharsbx($value["NAME"]);
													if($skuProperty["SHOW_MODE"] === "PICT") {?>
														<li class="product-item-detail-scu-item-color" title="<?=$value['NAME']?>" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>" style="<?=(!empty($value['CODE']) ? 'background-color: #'.$value['CODE'].';' : (!empty($value['PICT']) ? 'background-image: url('.$value['PICT']['SRC'].');' : ''));?>"></li>
													<?} else {?>
														<li class="product-item-detail-scu-item-text" title="<?=$value['NAME']?>" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
															<?=$value["NAME"]?>
														</li>
													<?}
												}?>
											</ul>											
										</div>
									</div>
								</div>
							<?}?>
						</div>
					<?}
					//BASKET_PROPERTIES//
					if(!$haveOffers) {
						$emptyProductProperties = empty($arResult["PRODUCT_PROPERTIES"]);					
						if($arParams["ADD_PROPERTIES_TO_BASKET"] === "Y" && !$emptyProductProperties) {?>
							<div class="product-item-detail-info-container" id="<?=$itemIds['BASKET_PROP_DIV']?>">
								<?if(!empty($arResult["PRODUCT_PROPERTIES_FILL"])) {
									foreach($arResult["PRODUCT_PROPERTIES_FILL"] as $propId => $propInfo) {?>
										<input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=htmlspecialcharsbx($propInfo['ID'])?>" />
										<?unset($arResult["PRODUCT_PROPERTIES"][$propId]);
									}
								}
								$emptyProductProperties = empty($arResult["PRODUCT_PROPERTIES"]);
								if(!$emptyProductProperties) {
									foreach($arResult["PRODUCT_PROPERTIES"] as $propId => $propInfo) {?>
										<div class="product-item-detail-basket-props-container">
											<div class="product-item-detail-basket-props-title"><?=$arResult["PROPERTIES"][$propId]["NAME"]?></div>
											<div class="product-item-detail-basket-props-block">
												<?if($arResult["PROPERTIES"][$propId]["PROPERTY_TYPE"] === "L" && $arResult["PROPERTIES"][$propId]["LIST_TYPE"] === "C") {?>
													<div class="product-item-detail-basket-props-input-radio">
														<?foreach($propInfo["VALUES"] as $valueId => $value) {?>
															<label>
																<input type="radio" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=$valueId?>"<?=($valueId == $propInfo["SELECTED"] ? " checked='checked'" : "");?> />
																<span class="check-container">
																	<span class="check"><i class="fa fa-check"></i></span>
																</span>
																<span class="text" title="<?=$value?>"><?=$value?></span>
															</label>
														<?}?>
													</div>
												<?} else {?>
													<div class="product-item-detail-basket-props-drop-down" onclick="<?=$obName?>.showBasketPropsDropDownPopup(this, '<?=$propId?>');">
														<?$currId = $currVal = false;
														foreach($propInfo["VALUES"] as $valueId => $value) {
															if($valueId == $propInfo["SELECTED"]) {
																$currId = $valueId;
																$currVal = $value;
															}
														}
														unset($value);?>
														<input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=(!empty($currId) ? $currId : '');?>" />
														<div class="drop-down-text" data-entity="current-option"><?=(!empty($currVal) ? $currVal : "");?></div>
														<?unset($currVal, $currId);?>
														<div class="drop-down-arrow"><i class="icon-arrow-down"></i></div>
														<div class="drop-down-popup" data-entity="dropdownContent" style="display: none;">
															<ul>
																<?foreach($propInfo["VALUES"] as $valueId => $value) {?>
																	<li><span onclick="<?=$obName?>.selectBasketPropsDropDownPopupItem(this, '<?=$valueId?>');"><?=$value?></span></li>
																<?}
																unset($value);?>
															</ul>
														</div>
													</div>
												<?}?>
											</div>
										</div>
									<?}
								}?>
							</div>
						<?}
						unset($emptyProductProperties);
					}
					//QUANTITY//
					if($arParams["USE_PRODUCT_QUANTITY"] && (!$object || ($object && ($object["PHONE_SMS"] || $object["EMAIL_EMAIL"])) || $partnersUrl)) {?>
						<div class="product-item-detail-info-container" style="<?=(!$actualItem['CAN_BUY'] ? 'display: none;' : '')?>" data-entity="quantity-block">
							<?if(!empty($arResult["PROPERTIES"]["M2_COUNT"]["VALUE"])) {?>
								<div class="product-item-detail-amount"<?=($isMeasurePc || $isMeasureSqM ? "" : " style='display: none;'")?>>
									<a class="product-item-detail-amount-btn-minus" id="<?=$itemIds['PC_QUANTITY_DOWN_ID']?>" href="javascript:void(0)" rel="nofollow">-</a>
									<input class="product-item-detail-amount-input" id="<?=$itemIds['PC_QUANTITY_ID']?>" type="tel" value="<?=$price['PC_MIN_QUANTITY']?>" />
									<a class="product-item-detail-amount-btn-plus" id="<?=$itemIds['PC_QUANTITY_UP_ID']?>" href="javascript:void(0)" rel="nofollow">+</a>
									<div class="product-item-detail-amount-measure"><?=Loc::getMessage("CT_BCE_CATALOG_MEASURE_PC")?></div>
								</div>
								<div class="product-item-detail-amount"<?=($isMeasurePc || $isMeasureSqM ? "" : " style='display: none;'")?>>
									<a class="product-item-detail-amount-btn-minus" id="<?=$itemIds['SQ_M_QUANTITY_DOWN_ID']?>" href="javascript:void(0)" rel="nofollow">-</a>
									<input class="product-item-detail-amount-input" id="<?=$itemIds['SQ_M_QUANTITY_ID']?>" type="tel" value="<?=$price['SQ_M_MIN_QUANTITY']?>" />
									<a class="product-item-detail-amount-btn-plus" id="<?=$itemIds['SQ_M_QUANTITY_UP_ID']?>" href="javascript:void(0)" rel="nofollow">+</a>
									<div class="product-item-detail-amount-measure"><?=Loc::getMessage("CT_BCE_CATALOG_MEASURE_SQ_M")?></div>
								</div>
								<?if($haveOffers) {?>
									<div class="product-item-detail-amount"<?=($isMeasurePc || $isMeasureSqM ? " style='display: none;'" : "")?>>
										<a class="product-item-detail-amount-btn-minus" id="<?=$itemIds['QUANTITY_DOWN_ID']?>" href="javascript:void(0)" rel="nofollow">-</a>
										<input class="product-item-detail-amount-input" id="<?=$itemIds['QUANTITY_ID']?>" type="tel" value="<?=$price['MIN_QUANTITY']?>" />
										<a class="product-item-detail-amount-btn-plus" id="<?=$itemIds['QUANTITY_UP_ID']?>" href="javascript:void(0)" rel="nofollow">+</a>
										<div class="product-item-detail-amount-measure" id="<?=$itemIds['QUANTITY_MEASURE']?>"><?=$actualItem["ITEM_MEASURE"]["TITLE"]?></div>
									</div>
								<?}?>
								<div class="product-item-detail-total-cost" id="<?=$itemIds['TOTAL_COST_ID']?>"<?=($price["MIN_QUANTITY"] != 1 || $price["PC_MIN_QUANTITY"] != 1 || $price["SQ_M_MIN_QUANTITY"] != 1 ? "" : " style='display:none;'")?>><?=Loc::getMessage("CT_BCE_CATALOG_TOTAL_COST")?><span data-entity="total-cost"><?=($price["MIN_QUANTITY"] != 1 || $price["PC_MIN_QUANTITY"] != 1 || $price["SQ_M_MIN_QUANTITY"] != 1 ? $price["PRINT_RATIO_PRICE"] : "")?></span></div>
							<?} else {?>
								<div class="product-item-detail-amount">								
									<a class="product-item-detail-amount-btn-minus" id="<?=$itemIds['QUANTITY_DOWN_ID']?>" href="javascript:void(0)" rel="nofollow">-</a>
									<input class="product-item-detail-amount-input" id="<?=$itemIds['QUANTITY_ID']?>" type="tel" value="<?=$price['MIN_QUANTITY']?>" />
									<a class="product-item-detail-amount-btn-plus" id="<?=$itemIds['QUANTITY_UP_ID']?>" href="javascript:void(0)" rel="nofollow">+</a>
									<div class="product-item-detail-amount-measure" id="<?=$itemIds['QUANTITY_MEASURE']?>"><?=$actualItem["ITEM_MEASURE"]["TITLE"]?></div>
								</div>
								<div class="product-item-detail-total-cost" id="<?=$itemIds['TOTAL_COST_ID']?>"<?=($price["MIN_QUANTITY"] != 1 ? "" : " style='display:none;'")?>><?=Loc::getMessage("CT_BCE_CATALOG_TOTAL_COST")?><span data-entity="total-cost"><?=($price["MIN_QUANTITY"] != 1 ? $price["PRINT_RATIO_PRICE"] : "")?></span></div>
							<?}?>
						</div>
					<?}
					//BUTTONS//?>
					<div class="product-item-detail-button-container" data-entity="main-button-container">
						<?//BUY//
						if(!$arParams["DISABLE_BASKET"] && (!$object || $partnersUrl)) {
							if(!$partnersUrl) {?>
								<div id="<?=$itemIds['BASKET_ACTIONS_ID']?>">
									<?if($showAddBtn) {?>
										<button type="button" class="btn btn-buy" id="<?=$itemIds['ADD_BASKET_LINK']?>"<?=($actualItem["CAN_BUY"] && $price["RATIO_PRICE"] > 0 ? "" : " disabled='disabled'")?>><i class="icon-cart"></i><span><?=$arParams["MESS_BTN_ADD_TO_BASKET"]?></span></button>
									<?}
									if($showBuyBtn) {?>
										<button type="button" class="btn btn-buy" id="<?=$itemIds['BUY_LINK']?>"<?=($actualItem["CAN_BUY"] && $price["RATIO_PRICE"] > 0 ? "" : " disabled='disabled'")?>><i class="icon-cart"></i><span><?=$arParams["MESS_BTN_BUY"]?></span></button>
									<?}?>
								</div>
							<?} else {?>
								<button type="button" class="btn btn-buy" id="<?=$itemIds['PARTNERS_LINK']?>"<?=($actualItem["CAN_BUY"] && $price["RATIO_PRICE"] > 0 ? "" : " disabled='disabled'")?>><i class="icon-cart"></i><span><?=$arParams["MESS_BTN_BUY"]?></span></button>
								<?if(!empty($arSettings["PARTNERS_INFO_MESSAGE"]["VALUE"])) {?>
									<div class="product-item-detail-info-message"><?=$arSettings["PARTNERS_INFO_MESSAGE"]["VALUE"]?></div>
								<?}
							}?>
						<?}
						//ASK_PRICE//?>
						<button type="button" class="btn btn-default" id="<?=$itemIds['ASK_PRICE_LINK']?>" style="display: <?=($actualItem['CAN_BUY'] && $price['RATIO_PRICE'] <= 0 ? '' : 'none')?>;"><i class="icon-comment"></i><span><?=Loc::getMessage("CT_BCE_CATALOG_ASK_PRICE")?></span></button>
						<?//UNDER_ORDER//?>
						<button type="button" class="btn btn-default" id="<?=$itemIds['NOT_AVAILABLE_MESS']?>" style="display: <?=(!$actualItem['CAN_BUY'] ? '' : 'none')?>;"><i class="icon-clock"></i><span><?=Loc::getMessage("CT_BCE_CATALOG_UNDER_ORDER")?></span></button>
						<?//SUBSCRIBE//
						if($showSubscribe) {?>
							<?$APPLICATION->IncludeComponent("bitrix:catalog.product.subscribe", "",
								array(
									"PRODUCT_ID" => $actualItem["ID"],
									"BUTTON_ID" => $itemIds["SUBSCRIBE_LINK"],
									"BUTTON_CLASS" => "btn btn-default",
									"DEFAULT_DISPLAY" => !$actualItem["CAN_BUY"],
									"MESS_BTN_SUBSCRIBE" => $arParams["~MESS_BTN_SUBSCRIBE"]
								),
								$component,
								array("HIDE_ICONS" => "Y")
							);?>
						<?}?>
						<button type="button" class="btn btn-buy" id="<?=$itemIds['ONECLICK_LINK']?>">
							<i class="icon-ok-b"></i>
							<span>Купить в 1 клик</span>
						</button>
	
						
						<button type="button" class="btn btn-buy" id="<?=$itemIds['WHOLESALER_LINK']?>">
							<i class="ef ef-truck"></i>
							<span>Я оптовик</span>
						</button>
						<?php
							/* calcs */
							preg_match_all('|\d+|', $arResult["PROPERTIES"]["LIGHT_STREAM"]["VALUE"], $matches);
							
							$stram = $matches[0]; 

							$res = 1;
							
							if(!empty($stram[0])) {
								
								$res = $stram[0];
								
						?>
								
								<button type="button" class="btn btn-buy" data-toggle="modal" data-target="#modalCalc" id="<?=$itemIds['WHOLESALER_LINK']?>">
									<i class="icon_rasschet"></i>
									<span>Рассчитать кол-во</span>
								</button>
						
						<?php
						
							} else {
								
							}
						?>
						
						
						<button type="button" class="btn btn-buy" id="<?=$itemIds['CONSULTATION_LINK']?>">
							<i class="icon-comment"></i>
							<span>Консультация</span>
						</button>
						<!--button type="button" class="btn btn-buy" id="<?=$itemIds['WHOLESALER_LINK']?>">
							<i class="ef ef-truck"></i>
							<span>Проконссультировать</span>
						</button-->
					</div>
					<?//COMPARE//
					if($arParams["DISPLAY_COMPARE"]) {?>
						<div class="product-item-detail-compare">
							<label id="<?=$itemIds['COMPARE_LINK']?>">
								<input type="checkbox" data-entity="compare-checkbox">
								<span class="product-item-detail-compare-checkbox"><i class="icon-ok-b"></i></span>
								<span class="product-item-detail-compare-title" data-entity="compare-title"><?=$arParams["MESS_BTN_COMPARE"]?></span>
							</label>
						</div>
					<? } ?>					
					
					<div class="product-item-detail-methods">
						<div class="product-item-detail-method"><i class="icon-delivery"></i><span><?=Loc::getMessage("CT_BCE_CATALOG_DETAIL_METHODS_1")?></span></div>
						<div class="product-item-detail-method"><i class="icon-cards"></i><span><?=Loc::getMessage("CT_BCE_CATALOG_DETAIL_METHODS_2")?></span></div>
					</div>					
					
					<?//QUICK_ORDER//
					/*if($arParams["QUICK_ORDER"] && (!$object || ($object && ($object["PHONE_SMS"] || $object["EMAIL_EMAIL"]))) && !$partnersUrl) {
						$quickOrderQuantityId = $itemIds["QUANTITY_ID"];
						if(!$haveOffers && !empty($arResult["PROPERTIES"]["M2_COUNT"]["VALUE"])) {
							if($isMeasurePc)
								$quickOrderQuantityId = $itemIds["PC_QUANTITY_ID"];
							elseif($isMeasureSqM)
								$quickOrderQuantityId = $itemIds["SQ_M_QUANTITY_ID"];
						}?>
						<?$APPLICATION->IncludeComponent("altop:quick.order.enext", "",
							array(
								"MODE" => "PRODUCT",
								"PRODUCT_ID" => $actualItem["ID"],							
								"CONTAINER_ID" => $itemIds["QUICK_ORDER_LINK"],
								"CONTAINER_CLASS" => "product-item-detail-quick-order",
								"DEFAULT_DISPLAY" => $actualItem["CAN_BUY"] && $price["RATIO_PRICE"] > 0,
								"QUANTITY_ID" => $quickOrderQuantityId,
								"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
								"PARTIAL_PRODUCT_PROPERTIES" => $arParams["PARTIAL_PRODUCT_PROPERTIES"],
								"CART_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
								"BASKET_PROPS_ID" => $itemIds["BASKET_PROP_DIV"],														
								"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
								"BASKET_SKU_PROPS" => $arResult["OFFERS_PROP_CODES"],
								"OBJECT_ID" => $object ? $object["ID"] : "" 
							),
							$component,
							array("HIDE_ICONS" => "Y")
						);?>
						<?unset($quickOrderQuantityId);
					} */?>
				</div>
				<?//OBJECT//
				if($object) {?>
					<div class="product-item-detail-object-container">
						<a target="_blank" class="product-item-detail-object" href="<?=$object['DETAIL_PAGE_URL']?>">
							<span class="product-item-detail-object-image">
								<?if(is_array($object["PREVIEW_PICTURE"])) {?>									
									<img src="<?=$object['PREVIEW_PICTURE']['SRC']?>" width="<?=$object['PREVIEW_PICTURE']['WIDTH']?>" height="<?=$object['PREVIEW_PICTURE']['HEIGHT']?>" alt="<?=$object['NAME']?>" />
								<?}?>
							</span>
							<span class="product-item-detail-object-text"><?=$object["NAME"]?></span>
						</a>
						<div class="product-item-detail-object-contacts">
							<button type="button" class="product-item-detail-object-btn"><i class="icon-phone-call"></i></button>
						</div>
					</div>
				<?}?>
				<div class="product-item-detail-ghost-bottom"></div>
			</div>
			<?//SECTIONS_MORE_PRODUCTS//
			if($moreProductsIds) {?>
				<div class="col-xs-12 product-item-detail-tab-content" data-entity="tab-container" data-value="more-products">
					<div class="h2"><?=$arResult["PROPERTIES"]["MORE_PRODUCTS"]["NAME"]?></div>					
					<?//SECTIONS//?>
					<div class="product-item-detail-more-products-sections-links" data-entity="moreProductsSectionsLinks">
						<div class="product-item-detail-more-products-section-link active" data-entity="moreProductsSectionsLink" data-section-id="0"><?=Loc::getMessage("CT_BCE_CATALOG_SECTIONS_ALL")?><span><?=count($moreProductsIds)?></span></div>
						<?if(!empty($arResult["PROPERTIES"]["MORE_PRODUCTS"]["SECTIONS"])) {
							foreach($arResult["PROPERTIES"]["MORE_PRODUCTS"]["SECTIONS"] as $arSection) {?>
								<div class="product-item-detail-more-products-section-link" data-entity="moreProductsSectionsLink" data-section-id="<?=$arSection['ID']?>"><?=$arSection["NAME"]?><span><?=$arSection["COUNT"]?></span></div>
							<?}
							unset($arSection);
						}?>
					</div>
					<?//MORE_PRODUCTS//?>
					<div class="product-item-detail-more-products">
						<?$GLOBALS["arMoreProductsFilter"] = array("ID" => $moreProductsIds);?>
						<?$APPLICATION->IncludeComponent("bitrix:catalog.section", ".default", 
							array(
								"COMPONENT_TEMPLATE" => ".default",
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"SECTION_ID" => "",
								"SECTION_CODE" => "",
								"SECTION_USER_FIELDS" => array(),
								"FILTER_NAME" => "arMoreProductsFilter",
								"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
								"SHOW_ALL_WO_SECTION" => "Y",
								"CUSTOM_FILTER" => "",
								"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
								"HIDE_NOT_AVAILABLE_OFFERS" => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
								"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
								"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
								"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
								"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
								"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
								"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
								"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
								"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
								"PAGE_ELEMENT_COUNT" => "8",
								"LINE_ELEMENT_COUNT" => "4",
								"PROPERTY_CODE" => array(),
								"OFFERS_FIELD_CODE" => array(),
								"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
								"OFFERS_LIMIT" => "0",
								"BACKGROUND_IMAGE" => "-",
								"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false}]",
								"PRODUCT_DISPLAY_MODE" => $arParams["PRODUCT_DISPLAY_MODE"],
								"OFFER_TREE_PROPS" => $arParams["OFFER_TREE_PROPS"],
								"PRODUCT_SUBSCRIPTION" => $arParams["PRODUCT_SUBSCRIPTION"],
								"SHOW_DISCOUNT_PERCENT" => $arParams["SHOW_DISCOUNT_PERCENT"],
								"SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"],
								"SHOW_MAX_QUANTITY" => $arParams["SHOW_MAX_QUANTITY"],
								"MESS_SHOW_MAX_QUANTITY" => $arParams["MESS_SHOW_MAX_QUANTITY"],
								"RELATIVE_QUANTITY_FACTOR" => $arParams["RELATIVE_QUANTITY_FACTOR"],
								"MESS_RELATIVE_QUANTITY_MANY" => $arParams["MESS_RELATIVE_QUANTITY_MANY"],
								"MESS_RELATIVE_QUANTITY_FEW" => $arParams["MESS_RELATIVE_QUANTITY_FEW"],
								"MESS_BTN_BUY" => $arParams["MESS_BTN_BUY"],
								"MESS_BTN_ADD_TO_BASKET" => $arParams["MESS_BTN_ADD_TO_BASKET"],
								"MESS_BTN_SUBSCRIBE" => $arParams["MESS_BTN_SUBSCRIBE"],
								"MESS_BTN_DETAIL" => $arParams["MESS_BTN_DETAIL"],
								"MESS_NOT_AVAILABLE" => $arParams["MESS_NOT_AVAILABLE"],
								"RCM_TYPE" => "personal",
								"RCM_PROD_ID" => "",
								"SHOW_FROM_SECTION" => "N",
								"SECTION_URL" => "",
								"DETAIL_URL" => "",
								"SECTION_ID_VARIABLE" => "SECTION_ID",
								"SEF_MODE" => "N",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"BROWSER_TITLE" => "-",
								"SET_META_KEYWORDS" => "N",
								"META_KEYWORDS" => "-",
								"SET_META_DESCRIPTION" => "N",
								"META_DESCRIPTION" => "-",
								"SET_LAST_MODIFIED" => "N",
								"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"] ? "Y" : "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"CACHE_FILTER" => $arParams["CACHE_FILTER"],
								"USE_REVIEW" => $arParams["USE_REVIEW"],
								"REVIEWS_IBLOCK_TYPE" => $arParams["REVIEWS_IBLOCK_TYPE"],
								"REVIEWS_IBLOCK_ID" => $arParams["REVIEWS_IBLOCK_ID"],
								"ACTION_VARIABLE" => "action",
								"PRODUCT_ID_VARIABLE" => "id",								
								"PRICE_CODE" => $arParams["PRICE_CODE"],
								"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"] ? "Y" : "N",
								"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"] ? "Y" : "N",
								"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"] ? "Y" : "N",
								"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
								"CURRENCY_ID" => $arParams["CURRENCY_ID"],
								"BASKET_URL" => $arParams["BASKET_URL"],
								"USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"] ? "Y" : "N",
								"PRODUCT_QUANTITY_VARIABLE" => "quantity",
								"ADD_PROPERTIES_TO_BASKET" => $arParams["ADD_PROPERTIES_TO_BASKET"],
								"PRODUCT_PROPS_VARIABLE" => "prop",
								"PARTIAL_PRODUCT_PROPERTIES" => $arParams["PARTIAL_PRODUCT_PROPERTIES"],
								"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
								"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
								"ADD_TO_BASKET_ACTION" => $showBuyBtn ? "BUY" : "ADD",
								"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"] ? "Y" : "N",
								"COMPARE_PATH" => $arParams["COMPARE_PATH"],
								"MESS_BTN_COMPARE" => $arParams["MESS_BTN_COMPARE"],
								"COMPARE_NAME" => $arParams["COMPARE_NAME"],
								"USE_ENHANCED_ECOMMERCE" => "N",
								"PAGER_TEMPLATE" => "arrows",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"LAZY_LOAD" => "Y",
								"LOAD_ON_SCROLL" => "N",
								"SET_STATUS_404" => "N",
								"SHOW_404" => "N",
								"MESSAGE_404" => "",
								"COMPATIBLE_MODE" => "N",
								"DISABLE_INIT_JS_IN_COMPONENT" => "N"
							),
							$component,
							array("HIDE_ICONS" => "Y")
						);?>
					</div>
				</div>
			<?}
			//REVIEWS//
			if(isset($arResult["REVIEWS_COUNT"])) {?>
				<div class="col-xs-12 product-item-detail-tab-content" data-entity="tab-container" data-value="reviews">
					<div class="h2"><?=$arParams["MESS_REVIEWS_TAB"]?></div>
					<div class="product-item-detail-reviews">
						<?$GLOBALS["arReviewsFilter"] = array("PROPERTY_PRODUCT_ID" => $arResult["ID"]);?>
						<?$APPLICATION->IncludeComponent("bitrix:news.list", "reviews",
							array(
								"IBLOCK_TYPE" => $arParams["REVIEWS_IBLOCK_TYPE"],
								"IBLOCK_ID" => $arParams["REVIEWS_IBLOCK_ID"],
								"NEWS_COUNT" => $arParams["REVIEWS_NEWS_COUNT"],
								"SORT_BY1" => $arParams["REVIEWS_SORT_BY1"],
								"SORT_ORDER1" => $arParams["REVIEWS_SORT_ORDER1"],
								"SORT_BY2" => $arParams["REVIEWS_SORT_BY2"],
								"SORT_ORDER2" => $arParams["REVIEWS_SORT_ORDER2"],
								"FILTER_NAME" => "arReviewsFilter",
								"FIELD_CODE" => array(),
								"PROPERTY_CODE" => $arParams["REVIEWS_PROPERTY_CODE"],
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "",
								"AJAX_OPTION_SHADOW" => "",
								"AJAX_OPTION_JUMP" => "",
								"AJAX_OPTION_STYLE" => "",
								"AJAX_OPTION_HISTORY" => "",
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"CACHE_FILTER" => $arParams["CACHE_FILTER"],
								"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => $arParams["REVIEWS_ACTIVE_DATE_FORMAT"],
								"DISPLAY_PANEL" => "",
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_STATUS_404" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "",
								"HIDE_LINK_WHEN_NO_DETAIL" => "",
								"PARENT_SECTION" => "",
								"PARENT_SECTION_CODE" => "",
								"DISPLAY_NAME" => "",
								"DISPLAY_DATE" => "",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "Y",
								"PAGER_SHOW_ALWAYS" => "",
								"PAGER_TEMPLATE" => "arrows",
								"PAGER_DESC_NUMBERING" => "",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "",
								"PAGER_SHOW_ALL" => "",
								"AJAX_OPTION_ADDITIONAL" => ""
							),
							$component,
							array("HIDE_ICONS" => "Y")
						);?>
					</div>
				</div>
			<?}?>
		</div>
	</div>
	<?//PREDICTION//
	if($arResult["CATALOG"] && (!$object || $partnersUrl) && $actualItem["CAN_BUY"] && Bitrix\Main\ModuleManager::isModuleInstalled("sale")) {?>
		<?$APPLICATION->IncludeComponent("bitrix:sale.prediction.product.detail", ".default",
			array(
				"BUTTON_ID" => !$partnersUrl ? ($showBuyBtn ? $itemIds["BUY_LINK"] : $itemIds["ADD_BASKET_LINK"]) : $itemIds["PARTNERS_LINK"],
				"CUSTOM_SITE_ID" => isset($arParams["CUSTOM_SITE_ID"]) ? $arParams["CUSTOM_SITE_ID"] : null,
				"POTENTIAL_PRODUCT_TO_BUY" => array(
					"ID" => isset($arResult["ID"]) ? $arResult["ID"] : null,
					"MODULE" => isset($arResult["MODULE"]) ? $arResult["MODULE"] : "catalog",
					"PRODUCT_PROVIDER_CLASS" => isset($arResult["PRODUCT_PROVIDER_CLASS"]) ? $arResult["PRODUCT_PROVIDER_CLASS"] : "CCatalogProductProvider",
					"QUANTITY" => isset($arResult["QUANTITY"]) ? $arResult["QUANTITY"] : null,
					"IBLOCK_ID" => isset($arResult["IBLOCK_ID"]) ? $arResult["IBLOCK_ID"] : null,
					"PRIMARY_OFFER_ID" => isset($arResult["OFFERS"][0]["ID"]) ? $arResult["OFFERS"][0]["ID"] : null,
					"SECTION" => array(
						"ID" => isset($arResult["SECTION"]["ID"]) ? $arResult["SECTION"]["ID"] : null,
						"IBLOCK_ID" => isset($arResult["SECTION"]["IBLOCK_ID"]) ? $arResult["SECTION"]["IBLOCK_ID"] : null,
						"LEFT_MARGIN" => isset($arResult["SECTION"]["LEFT_MARGIN"]) ? $arResult["SECTION"]["LEFT_MARGIN"] : null,
						"RIGHT_MARGIN" => isset($arResult["SECTION"]["RIGHT_MARGIN"]) ? $arResult["SECTION"]["RIGHT_MARGIN"] : null,
					)
				)
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);?>
	<?}
	//META//?>
    <?if(!empty($arResult["DISPLAY_PROPERTIES"])) {
        $descr = "";
        foreach($arResult["DISPLAY_PROPERTIES"] as $property) {
            if(($property['CODE'] == 'LENGTH_LIGHT' && $arResult["PROPERTIES"]["ANGLE"]['VALUE'] || $property['CODE'] == 'DESCRIPTION') || substr(trim($property["DISPLAY_VALUE"]), 0,2)=="<a" || $property['CODE']=="ARTNUMBER"){continue;}
            $descr.= $property["NAME"] . ": " . $property["DISPLAY_VALUE"] . ", ";
        }
        $descr = substr($descr,0, strlen($descr)-2);


        unset($property);
        ?>
            <meta itemprop="description" content="<?=$descr?>" />
        <?
    }?>

	<?if(empty($arResult["PROPERTIES"]["BRAND"]["FULL_VALUE"]["PREVIEW_PICTURE"]) && !empty($arResult["PROPERTIES"]["BRAND"]["FULL_VALUE"])) {?>
		<meta itemprop="brand" content="<?=$arResult['PROPERTIES']['BRAND']['FULL_VALUE']['NAME']?>" />
	<?}?>
	<meta itemprop="category" content="<?=!empty($arResult['CATEGORY_PATH']) ? $arResult['CATEGORY_PATH'] : GetMessage("CT_BCE_CATALOG_LAMPS_CATEGORY")?>" />
	<?if($haveOffers) {
		foreach($arResult["JS_OFFERS"] as $offer) {
			$currentOffersList = array();
			if(!empty($offer["TREE"]) && is_array($offer["TREE"])) {
				foreach($offer["TREE"] as $propName => $skuId) {
					$propId = (int)substr($propName, 5);
					foreach($skuProps as $prop) {
						if($prop["ID"] == $propId) {
							foreach($prop["VALUES"] as $propId => $propValue) {
								if($propId == $skuId) {
									$currentOffersList[] = $propValue["NAME"];
									break;
								}
							}
						}
					}
				}
			}
			$offerPrice = $offer["ITEM_PRICES"][$offer["ITEM_PRICE_SELECTED"]];?>
			<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<!--<meta itemprop="sku" content="<?=htmlspecialcharsbx(implode('/', $currentOffersList))?>" />-->
				<meta itemprop="price" content="<?=$offerPrice['RATIO_PRICE']?>" />
				<meta itemprop="priceCurrency" content="<?=$offerPrice['CURRENCY']?>" />
				<link itemprop="availability" href="http://schema.org/<?=($offer['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
			</span>
		<?}
		unset($offerPrice, $currentOffersList);
	} else {?>
		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<meta itemprop="price" content="<?=(empty($arResult["PROPERTIES"]["OPT_PRICE"]["VALUE"]) ? $price['RATIO_PRICE'] : $arResult["PROPERTIES"]["OPT_PRICE"]["VALUE"])?>" />
			<meta itemprop="priceCurrency" content="<?=$price['CURRENCY']?>" />
			<link itemprop="availability" href="http://schema.org/<?=($actualItem['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
		</span>
    <?}?>
    <meta itemprop="sku" content="<?=$name?>" />
</div>


<div class="modal fade" id="modalCalc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Рассчитать количество</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form >
			<label for="c_area">
				<div class="f_title">Площадь помещения(ий)</div>
				<input type="number" step="any" name="c_area" class="form-control" id="c_area" value="" />
				<span> м<sup>2</sup></span>
			</label>
			<label for="c_illum">
				<div class="f_title">Требуемая норма освещенности</div>
				<input type="number" step="any" name="c_illum" id="c_illum" class="form-control" value="" />
				<span> Лк</span>
			</label>
			<label for="c_floor">
				<div class="f_title">Высота от замера до светильника</div>
				<input type="number" step="any" name="c_floor" id="c_floor" class="form-control" value="" />
				<span> м</span>
			</label>
			<label for="c_coef">
				<div class="f_title">Коэффициент эксплуатации <em> (не меняйте если не уверены)</em></div>
				<input type="number" step="0.1" name="c_coef" id="c_coef" class="form-control" value="0.8" />
				<span></span>
			</label>
			<button type="button" class="calcResult btn btn-buy"><span>Рассчитать *</span></button>
		</form>
		<div class="calcResultBlock"></div>
      </div>
      <div class="modal-footer">
		<p>*Внимание, данный подсчет хоть и с достаточной точностью отражает необходимое количество светильников к установке, но не учитывает многие важные факторы, правильно рассчитать которые могут лишь проектные специалисты. Для получения точных результатов, просьба обратиться к специалистам Компании Винчи.</p>
	  </div>
    </div>
  </div>
</div>
 

<script>


$(document).on('click','.calcResult',function(){
	
	var V = $('#c_floor').val();
	var kp = $('#c_coef').val();
	var P  = $('#c_area').val();
	var N  = $('#c_illum').val();

	if(V == '') { $('#c_floor').addClass('error'); }
	if(kp == '') { $('#c_coef').addClass('error'); }
	if(P == '') { $('#c_area').addClass('error'); }
	if(N == '') { $('#c_illum').addClass('error'); }
	
	if($('#modalCalc form input.error').length) {
		return false;
	}
	
	var k = 1;
	if(V < 2.7) {k = 1;}
	if(V >= 2.7 && V < 3) {k = 1.2;}
	if(V >= 3 && V < 3.5) {k = 1.5;}
	if(V >= 3.5) {k = 2;}
	
	var sV = <?=$res; ?>;
	var result = ((P*N*k)/sV)/kp;
	result = Math.ceil(result);


	$(this).closest('.modal-body').find('.calcResultBlock').html('<p>Необходимое количество светильников: <b>'+ result +'</b> шт.</p>');
	$(this).closest('.modal-body').find('.calcResultBlock').show();

	return false;
})

$(document).on('click','#modalCalc form input.error',function(){
	$(this).removeClass('error');
})
$(document).on('focus','#modalCalc form input.error',function(){
	$(this).removeClass('error');
})
$(document).on('click','#modalCalc .close',function(){
	var elem = this;
	setTimeout(modalHide(elem), 5000);
})


function modalHide(elem) {
	$(elem).closest('#modalCalc').find('form').trigger('reset');//.show();
	$(elem).closest('#modalCalc').find('.calcResultBlock').hide();
}

</script>
<?if($haveOffers) {
	$offerIds = array();
	$offerCodes = array();

	$useRatio = $arParams["USE_RATIO_IN_RANGES"] === "Y";

	foreach($arResult["JS_OFFERS"] as $ind => &$jsOffer) {
		$offerIds[] = (int)$jsOffer["ID"];
		$offerCodes[] = $jsOffer["CODE"];

		$fullOffer = $arResult["OFFERS"][$ind];
		$measureName = $fullOffer["ITEM_MEASURE"]["TITLE"];

		$strAllProps = "";
		$strMainProps = "";
		$strPriceRangesRatio = "";
		$strPriceRanges = "";

		if($arResult["SHOW_OFFERS_PROPS"]) {
			if(!empty($jsOffer["DISPLAY_PROPERTIES"])) {
				foreach($jsOffer["DISPLAY_PROPERTIES"] as $property) {
					$current = "
						<div class='product-item-detail-properties' data-entity='sku-props'>
							<div class='product-item-detail-properties-name'>".$property["NAME"]."</div>
							<div class='product-item-detail-properties-val'>".(is_array($property["VALUE"]) ? implode(" / ", $property["VALUE"]) : $property["VALUE"])."</div>
						</div>
					";
					$strAllProps .= $current;
					if(isset($arParams["MAIN_BLOCK_OFFERS_PROPERTY_CODE"][$property["CODE"]])) {
						$strMainProps .= $current;
					}
				}
				unset($current);
			}
		}

		if($arParams["USE_PRICE_COUNT"] && count($jsOffer["ITEM_QUANTITY_RANGES"]) > 1) {
			foreach($jsOffer["ITEM_QUANTITY_RANGES"] as $range) {
				if($range["HASH"] !== "ZERO-INF") {
					$itemPrice = false;
					foreach($jsOffer["ITEM_PRICES"] as $itemPrice) {
						if($itemPrice["QUANTITY_HASH"] === $range["HASH"]) {
							break;
						}
					}
					if($itemPrice) {
						$strPriceRanges .= "<div class='product-item-detail-properties'><div class='product-item-detail-properties-name'>";
						if(is_infinite($range["SORT_TO"])) {
							$strPriceRanges .= Loc::getMessage("CT_BCE_CATALOG_RANGE_FROM", array("#FROM#" => $range["SORT_FROM"]." ".$measureName));
						} else {
							$strPriceRanges .= $range["SORT_FROM"].($range["SORT_TO"] != $range["SORT_FROM"] ? " - ".$range["SORT_TO"] : "")." ".$measureName;
						}
						$strPriceRanges .= "</div><div class='product-item-detail-properties-val'>".($useRatio ? $itemPrice["PRINT_RATIO_PRICE"] : $itemPrice["PRINT_PRICE"])."</div></div>";
					}
				}
			}
			unset($range, $itemPrice);
		}
		
		$jsOffer["ARTICLE"] = !empty($arResult["OFFERS"][$ind]["PROPERTIES"]["ARTNUMBER"]["VALUE"])
			? $arResult["OFFERS"][$ind]["PROPERTIES"]["ARTNUMBER"]["VALUE"]
			: "-";
		$jsOffer["DISPLAY_PROPERTIES"] = $strAllProps;
		$jsOffer["DISPLAY_PROPERTIES_MAIN_BLOCK"] = $strMainProps;
		$jsOffer["PRICE_RANGES_HTML"] = $strPriceRanges;
	}
	
	$templateData["OFFER_IDS"] = $offerIds;
	$templateData["OFFER_CODES"] = $offerCodes;
	unset($jsOffer, $strAllProps, $strMainProps, $strPriceRanges, $strPriceRangesRatio, $useRatio);
	
	$jsParams = array(
		"CONFIG" => array(
			"USE_CATALOG" => $arResult["CATALOG"],
			"SHOW_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
			"SHOW_PRICE" => true,
			"SHOW_DISCOUNT_PERCENT" => $arParams["SHOW_DISCOUNT_PERCENT"] === "Y",
			"SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"] === "Y",
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
			"SHOW_SKU_PROPS" => $arResult["SHOW_OFFERS_PROPS"],
			"OFFER_GROUP" => $arResult["OFFER_GROUP"],
			"MAIN_PICTURE_MODE" => $arParams["DETAIL_PICTURE_MODE"],
			"ADD_TO_BASKET_ACTION" => $arParams["ADD_TO_BASKET_ACTION"],			
			"SHOW_MAX_QUANTITY" => $arParams["SHOW_MAX_QUANTITY"],
			"RELATIVE_QUANTITY_FACTOR" => $arParams["RELATIVE_QUANTITY_FACTOR"],
			"USE_SUBSCRIBE" => $showSubscribe,
			"SHOW_SLIDER" => $arParams["SHOW_SLIDER"],
			"SLIDER_INTERVAL" => $arParams["SLIDER_INTERVAL"],
			"ALT" => $alt,
			"TITLE" => $title,
			"MAGNIFIER_ZOOM_PERCENT" => 200,
			"USE_ENHANCED_ECOMMERCE" => $arParams["USE_ENHANCED_ECOMMERCE"],
			"DATA_LAYER_NAME" => $arParams["DATA_LAYER_NAME"],
			"BRAND_PROPERTY" => !empty($arResult["DISPLAY_PROPERTIES"][$arParams["BRAND_PROPERTY"]])
				? $arResult["DISPLAY_PROPERTIES"][$arParams["BRAND_PROPERTY"]]["DISPLAY_VALUE"]
				: null
		),
		"PRODUCT_TYPE" => $arResult["CATALOG_TYPE"],
		"VISUAL" => $itemIds,
		"DEFAULT_PICTURE" => array(
			"PREVIEW_PICTURE" => $arResult["DEFAULT_PICTURE"],
			"DETAIL_PICTURE" => $arResult["DEFAULT_PICTURE"]
		),
		"PRODUCT" => array(
			"ID" => $arResult["ID"],
			"ACTIVE" => $arResult["ACTIVE"],
			"NAME" => $arResult["~NAME"],
			"CATEGORY" => $arResult["CATEGORY_PATH"]
		),
		"BASKET" => array(
			"QUANTITY" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"SKU_PROPS" => $arResult["OFFERS_PROP_CODES"],
			"ADD_URL_TEMPLATE" => $arResult["~ADD_URL_TEMPLATE"],
			"BUY_URL_TEMPLATE" => $arResult["~BUY_URL_TEMPLATE"]
		),
		"OFFERS" => $arResult["JS_OFFERS"],
		"OFFER_SELECTED" => $arResult["OFFERS_SELECTED"],
		"TREE_PROPS" => $skuProps
	);
} else {
	$jsParams = array(
		"CONFIG" => array(
			"USE_CATALOG" => $arResult["CATALOG"],
			"SHOW_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
			"SHOW_PRICE" => !empty($arResult["ITEM_PRICES"]),
			"SHOW_DISCOUNT_PERCENT" => $arParams["SHOW_DISCOUNT_PERCENT"] === "Y",
			"SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"] === "Y",
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
			"MAIN_PICTURE_MODE" => $arParams["DETAIL_PICTURE_MODE"],
			"ADD_TO_BASKET_ACTION" => $arParams["ADD_TO_BASKET_ACTION"],			
			"SHOW_MAX_QUANTITY" => $arParams["SHOW_MAX_QUANTITY"],
			"RELATIVE_QUANTITY_FACTOR" => $arParams["RELATIVE_QUANTITY_FACTOR"],
			"USE_SUBSCRIBE" => $showSubscribe,
			"SHOW_SLIDER" => $arParams["SHOW_SLIDER"],
			"SLIDER_INTERVAL" => $arParams["SLIDER_INTERVAL"],
			"ALT" => $alt,
			"TITLE" => $title,
			"MAGNIFIER_ZOOM_PERCENT" => 200,
			"USE_ENHANCED_ECOMMERCE" => $arParams["USE_ENHANCED_ECOMMERCE"],
			"DATA_LAYER_NAME" => $arParams["DATA_LAYER_NAME"],
			"BRAND_PROPERTY" => !empty($arResult["DISPLAY_PROPERTIES"][$arParams["BRAND_PROPERTY"]])
				? $arResult["DISPLAY_PROPERTIES"][$arParams["BRAND_PROPERTY"]]["DISPLAY_VALUE"]
				: null
		),
		"VISUAL" => $itemIds,
		"PRODUCT_TYPE" => $arResult["CATALOG_TYPE"],
		"PRODUCT" => array(
			"ID" => $arResult["ID"],
			"ACTIVE" => $arResult["ACTIVE"],
			"PICT" => is_array($arResult["DETAIL_PICTURE"]) ? $arResult["DETAIL_PICTURE"] : $arResult["DEFAULT_PICTURE"],
			"NAME" => $arResult["~NAME"],
			"SUBSCRIPTION" => true,
			"ITEM_PRICE_MODE" => $arResult["ITEM_PRICE_MODE"],
			"ITEM_PRICES" => $arResult["ITEM_PRICES"],
			"ITEM_PRICE_SELECTED" => $arResult["ITEM_PRICE_SELECTED"],
			"ITEM_QUANTITY_RANGES" => $arResult["ITEM_QUANTITY_RANGES"],
			"ITEM_QUANTITY_RANGE_SELECTED" => $arResult["ITEM_QUANTITY_RANGE_SELECTED"],
			"ITEM_MEASURE_RATIOS" => $arResult["ITEM_MEASURE_RATIOS"],
			"ITEM_MEASURE_RATIO_SELECTED" => $arResult["ITEM_MEASURE_RATIO_SELECTED"],
			"ITEM_MEASURE" => $arResult["ITEM_MEASURE"],
			"SLIDER_COUNT" => $arResult["MORE_PHOTO_COUNT"],
			"SLIDER" => $arResult["MORE_PHOTO"],			
			"CAN_BUY" => $arResult["CAN_BUY"],
			"CHECK_QUANTITY" => $arResult["CHECK_QUANTITY"],
			"QUANTITY_FLOAT" => is_float($measureRatio),
			"MAX_QUANTITY" => $arResult["CATALOG_QUANTITY"],
			"STEP_QUANTITY" => $measureRatio,
			"CATEGORY" => $arResult["CATEGORY_PATH"]
		),
		"BASKET" => array(			
			"QUANTITY" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"PROPS" => $arParams["PRODUCT_PROPS_VARIABLE"],			
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ADD_URL_TEMPLATE" => $arResult["~ADD_URL_TEMPLATE"],
			"BUY_URL_TEMPLATE" => $arResult["~BUY_URL_TEMPLATE"]
		)
	);
	
	if(!empty($arResult["PROPERTIES"]["M2_COUNT"]["VALUE"])) {		
		if($isMeasurePc) {
			$jsParams["PRODUCT"]["PC_MAX_QUANTITY"] = $arResult["CATALOG_QUANTITY"];
			$jsParams["PRODUCT"]["PC_STEP_QUANTITY"] = $measureRatio;

			$jsParams["PRODUCT"]["SQ_M_MAX_QUANTITY"] = round($arResult["CATALOG_QUANTITY"] / str_replace(",", ".", $arResult["PROPERTIES"]["M2_COUNT"]["VALUE"]), 2);			
			$jsParams["PRODUCT"]["SQ_M_STEP_QUANTITY"] = round($measureRatio / str_replace(",", ".", $arResult["PROPERTIES"]["M2_COUNT"]["VALUE"]), 2);
		} elseif($isMeasureSqM) {
			$jsParams["PRODUCT"]["PC_MAX_QUANTITY"] = floor($arResult["CATALOG_QUANTITY"] / $measureRatio);			
			$jsParams["PRODUCT"]["PC_STEP_QUANTITY"] = 1;

			$jsParams["PRODUCT"]["SQ_M_MAX_QUANTITY"] = $arResult["CATALOG_QUANTITY"];
			$jsParams["PRODUCT"]["SQ_M_STEP_QUANTITY"] = $measureRatio;
		}
	}
}

$jsParams["DELAY"] = array(
	"DELAY_PATH" => $templateFolder."/ajax.php"
);

if($arParams["DISPLAY_COMPARE"]) {
	$jsParams["COMPARE"] = array(
		"COMPARE_URL_TEMPLATE" => $arResult["~COMPARE_URL_TEMPLATE"],
		"COMPARE_DELETE_URL_TEMPLATE" => $arResult["~COMPARE_DELETE_URL_TEMPLATE"],
		"COMPARE_PATH" => $arParams["COMPARE_PATH"]
	);
}

if($object) {
	$jsParams["OBJECT"] = array(
		"ID" => $object["ID"],
		"NAME" => $object["NAME"],
		"ADDRESS" => $object["ADDRESS"],
		"TIMEZONE" => $object["TIMEZONE"],
		"WORKING_HOURS" => $object["WORKING_HOURS"],		
		"PHONE" => $object["PHONE"],						
		"EMAIL" => $object["EMAIL"],
		"SKYPE" => $object["SKYPE"],
		"CALLBACK_FORM" => $object["PHONE_SMS"] || $object["EMAIL_EMAIL"] ? true : false
	);
}

if($moreProductsIds) {
	$jsParams["MORE_PRODUCTS"] = array(
		"PRODUCTS_IDS" => $moreProductsIds
	);
}

$signer = new Bitrix\Main\Security\Sign\Signer;
$signedParams = $signer->sign(base64_encode(serialize($arResult["ORIGINAL_PARAMETERS"])), "catalog.element");?>

<script>
	BX.message({
		SQ_M_MESSAGE: '<?=GetMessageJS("CT_BCE_CATALOG_MEASURE_SQ_M")?>',
		ECONOMY_INFO_MESSAGE: '<?=GetMessageJS("CT_BCE_CATALOG_ECONOMY_INFO2")?>',
		ADD_BASKET_MESSAGE: '<?=($showBuyBtn ? $arParams["MESS_BTN_BUY"] : $arParams["MESS_BTN_ADD_TO_BASKET"])?>',
		ADD_BASKET_OK_MESSAGE: '<?=GetMessageJS("CT_BCE_CATALOG_ADD_OK")?>',		
		DELAY_MESSAGE: '<?=$arParams["MESS_BTN_DELAY"]?>',
		DELAY_OK_MESSAGE: '<?=GetMessageJS("CT_BCE_CATALOG_DELAY_OK")?>',		
		RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams["MESS_RELATIVE_QUANTITY_MANY"])?>',
		RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams["MESS_RELATIVE_QUANTITY_FEW"])?>',
		COMPARE_MESSAGE: '<?=$arParams["MESS_BTN_COMPARE"]?>',
		COMPARE_OK_MESSAGE: '<?=GetMessageJS("CT_BCE_CATALOG_COMPARE_OK")?>',		
		CATALOG_ELEMENT_OBJECT_TODAY: '<?=GetMessageJS("CT_BCE_CATALOG_OBJECT_TODAY")?>',
		CATALOG_ELEMENT_OBJECT_24_HOURS: '<?=GetMessageJS("CT_BCE_CATALOG_OBJECT_24_HOURS")?>',
		CATALOG_ELEMENT_OBJECT_OFF: '<?=GetMessageJS("CT_BCE_CATALOG_OBJECT_OFF")?>',
		CATALOG_ELEMENT_OBJECT_BREAK: '<?=GetMessageJS("CT_BCE_CATALOG_OBJECT_BREAK")?>',
		CATALOG_ELEMENT_TEMPLATE_PATH: '<?=$templateFolder?>',
		CATALOG_ELEMENT_PARAMETERS: '<?=CUtil::JSEscape($signedParams)?>'
	});
	var <?=$obName?> = new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
</script>


<?unset($actualItem, $itemIds, $jsParams);