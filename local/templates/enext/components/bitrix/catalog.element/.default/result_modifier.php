<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

//DISABLE_BASKET//
global $arSettings;
$arParams["DISABLE_BASKET"] = false;
if($arSettings["DISABLE_BASKET"]["VALUE"] == "Y") {
	$arParams["DISABLE_BASKET"] = true;
	if($arParams["USE_PRODUCT_QUANTITY"])
		$arParams["USE_PRODUCT_QUANTITY"] = false;
}

//SHOW_SUBSCRIBE//
if($arParams["PRODUCT_SUBSCRIPTION"] == "Y") {
	$saleNotifyOption = Bitrix\Main\Config\Option::get("sale", "subscribe_prod");
	if(strlen($saleNotifyOption) > 0)
		$saleNotifyOption = unserialize($saleNotifyOption);
	$saleNotifyOption = is_array($saleNotifyOption) ? $saleNotifyOption : array();
	foreach($saleNotifyOption as $siteId => $data) {
		if($siteId == SITE_ID && $data["use"] != "Y")
			$arParams["PRODUCT_SUBSCRIPTION"] = "N";
	}
}

//QUICK_ORDER//
$arParams["QUICK_ORDER"] = true;
if($arSettings["QUICK_ORDER"]["VALUE"] != "Y")
	$arParams["QUICK_ORDER"] = false;

//SCHEME//
$arResult["SCHEME"] = CMain::IsHTTPS() ? "https" : "http";

//OFFERS_IBLOCK//
if(!isset($arResult["OFFERS_IBLOCK"])) {
	$mxResult = CCatalogSKU::GetInfoByProductIBlock($arParams["IBLOCK_ID"]);
	if(is_array($mxResult))
		$arResult["OFFERS_IBLOCK"] = $mxResult["IBLOCK_ID"];
}

//MEASURE//
$measureIds = $arMeasureList = array();

if(!empty($arResult["OFFERS"])) {
	foreach($arResult["OFFERS"] as $arOffer) {
		$measureIds[] = $arOffer["ITEM_MEASURE"]["ID"];
	}
	unset($arOffer);
} else {
	$measureIds[] = $arResult["ITEM_MEASURE"]["ID"];
}

if(count($measureIds) > 0) {
	$rsMeasures = CCatalogMeasure::getList(array(), array("ID" => array_unique($measureIds)), false, false, array("ID", "SYMBOL_INTL"));
	while($arMeasure = $rsMeasures->GetNext()) {
		$arMeasureList[$arMeasure["ID"]] = $arMeasure["SYMBOL_INTL"];
	}
	unset($arMeasure, $rsMeasures);

	if(!empty($arResult["OFFERS"])) {
		foreach($arResult["OFFERS"] as $key => &$arOffer) {
			if(array_key_exists($arOffer["ITEM_MEASURE"]["ID"], $arMeasureList)) {
				$arOffer["ITEM_MEASURE"]["SYMBOL_INTL"] = $arMeasureList[$arOffer["ITEM_MEASURE"]["ID"]];
				$arResult["JS_OFFERS"][$key]["ITEM_MEASURE"] = $arOffer["ITEM_MEASURE"];
			}
		}
		unset($key, $arOffer);
	} else {
		if(array_key_exists($arResult["ITEM_MEASURE"]["ID"], $arMeasureList))
			$arResult["ITEM_MEASURE"]["SYMBOL_INTL"] = $arMeasureList[$arResult["ITEM_MEASURE"]["ID"]];
	}
}
unset($arMeasureList, $measureIds);

//PRICE//
if(!empty($arResult["PROPERTIES"]["M2_COUNT"]["VALUE"])) {
	$sqMCount = str_replace(",", ".", $arResult["PROPERTIES"]["M2_COUNT"]["VALUE"]);	
	if(!empty($arResult["OFFERS"])) {
		foreach($arResult["OFFERS"] as $key => &$arOffer) {
			$measureRatio = $arOffer["ITEM_MEASURE_RATIOS"][$arOffer["ITEM_MEASURE_RATIO_SELECTED"]]["RATIO"];
			if($arOffer["ITEM_MEASURE"]["SYMBOL_INTL"] == "pc. 1") {
				$arOffer["PC_MAX_QUANTITY"] = $arOffer["CATALOG_QUANTITY"];
				$arOffer["PC_STEP_QUANTITY"] = $measureRatio;				
				$arOffer["SQ_M_MAX_QUANTITY"] = round($arOffer["CATALOG_QUANTITY"] / $sqMCount, 2);
				$arOffer["SQ_M_STEP_QUANTITY"] = round($measureRatio / $sqMCount, 2);

				$arResult["JS_OFFERS"][$key]["PC_MAX_QUANTITY"] = $arOffer["PC_MAX_QUANTITY"];
				$arResult["JS_OFFERS"][$key]["PC_STEP_QUANTITY"] = $arOffer["PC_STEP_QUANTITY"];				
				$arResult["JS_OFFERS"][$key]["SQ_M_MAX_QUANTITY"] = $arOffer["SQ_M_MAX_QUANTITY"];
				$arResult["JS_OFFERS"][$key]["SQ_M_STEP_QUANTITY"] = $arOffer["SQ_M_STEP_QUANTITY"];
				
				if(Bitrix\Main\Loader::includeModule("catalog") && Bitrix\Main\Loader::includeModule("currency")) {
					foreach($arOffer["ITEM_PRICES"] as $keyPrice => &$arPrice) {
						$arPrice["SQ_M_BASE_PRICE"] = Bitrix\Catalog\Product\Price::roundPrice($arPrice["PRICE_TYPE_ID"], $arPrice["BASE_PRICE"] * $sqMCount, $arPrice["CURRENCY"]);
						$arPrice["SQ_M_PRINT_BASE_PRICE"] = CCurrencyLang::CurrencyFormat($arPrice["BASE_PRICE"] * $sqMCount, $arPrice["CURRENCY"], true);
						$arPrice["SQ_M_PRICE"] = Bitrix\Catalog\Product\Price::roundPrice($arPrice["PRICE_TYPE_ID"], $arPrice["PRICE"] * $sqMCount, $arPrice["CURRENCY"]);	
						$arPrice["SQ_M_PRINT_PRICE"] = CCurrencyLang::CurrencyFormat($arPrice["PRICE"] * $sqMCount, $arPrice["CURRENCY"], true);
						$arPrice["SQ_M_DISCOUNT"] = Bitrix\Catalog\Product\Price::roundPrice($arPrice["PRICE_TYPE_ID"], $arPrice["BASE_PRICE"] * $sqMCount - $arPrice["PRICE"] * $sqMCount, $arPrice["CURRENCY"]);
						$arPrice["SQ_M_PRINT_DISCOUNT"] = CCurrencyLang::CurrencyFormat($arPrice["BASE_PRICE"] * $sqMCount - $arPrice["PRICE"] * $sqMCount, $arPrice["CURRENCY"], true);
						$arPrice["PC_MIN_QUANTITY"] = $arPrice["MIN_QUANTITY"];
						$arPrice["SQ_M_MIN_QUANTITY"] = round($arPrice["MIN_QUANTITY"] / $sqMCount, 2);

						$arResult["JS_OFFERS"][$key]["ITEM_PRICES"][$keyPrice]["SQ_M_BASE_PRICE"] = $arPrice["SQ_M_BASE_PRICE"];
						$arResult["JS_OFFERS"][$key]["ITEM_PRICES"][$keyPrice]["SQ_M_PRINT_BASE_PRICE"] = $arPrice["SQ_M_PRINT_BASE_PRICE"];
						$arResult["JS_OFFERS"][$key]["ITEM_PRICES"][$keyPrice]["SQ_M_PRICE"] = $arPrice["SQ_M_PRICE"];
						$arResult["JS_OFFERS"][$key]["ITEM_PRICES"][$keyPrice]["SQ_M_PRINT_PRICE"] = $arPrice["SQ_M_PRINT_PRICE"];
						$arResult["JS_OFFERS"][$key]["ITEM_PRICES"][$keyPrice]["SQ_M_DISCOUNT"] = $arPrice["SQ_M_DISCOUNT"];
						$arResult["JS_OFFERS"][$key]["ITEM_PRICES"][$keyPrice]["SQ_M_PRINT_DISCOUNT"] = $arPrice["SQ_M_PRINT_DISCOUNT"];
						$arResult["JS_OFFERS"][$key]["ITEM_PRICES"][$keyPrice]["PC_MIN_QUANTITY"] = $arPrice["PC_MIN_QUANTITY"];
						$arResult["JS_OFFERS"][$key]["ITEM_PRICES"][$keyPrice]["SQ_M_MIN_QUANTITY"] = $arPrice["SQ_M_MIN_QUANTITY"];
					}
					unset($keyPrice, $arPrice);
				}
			} elseif($arOffer["ITEM_MEASURE"]["SYMBOL_INTL"] == "m2") {
				$arOffer["PC_MAX_QUANTITY"] = floor($arOffer["CATALOG_QUANTITY"] / $measureRatio);
				$arOffer["PC_STEP_QUANTITY"] = 1;
				$arOffer["SQ_M_MAX_QUANTITY"] = $arOffer["CATALOG_QUANTITY"];
				$arOffer["SQ_M_STEP_QUANTITY"] = $measureRatio;

				$arResult["JS_OFFERS"][$key]["PC_MAX_QUANTITY"] = $arOffer["PC_MAX_QUANTITY"];
				$arResult["JS_OFFERS"][$key]["PC_STEP_QUANTITY"] = $arOffer["PC_STEP_QUANTITY"];
				$arResult["JS_OFFERS"][$key]["SQ_M_MAX_QUANTITY"] = $arOffer["SQ_M_MAX_QUANTITY"];
				$arResult["JS_OFFERS"][$key]["SQ_M_STEP_QUANTITY"] = $arOffer["SQ_M_STEP_QUANTITY"];

				foreach($arOffer["ITEM_PRICES"] as $keyPrice => &$arPrice) {
					$arPrice["PC_MIN_QUANTITY"] = 1;
					$arPrice["SQ_M_MIN_QUANTITY"] = $arPrice["MIN_QUANTITY"];

					$arResult["JS_OFFERS"][$key]["ITEM_PRICES"][$keyPrice]["PC_MIN_QUANTITY"] = $arPrice["PC_MIN_QUANTITY"];
					$arResult["JS_OFFERS"][$key]["ITEM_PRICES"][$keyPrice]["SQ_M_MIN_QUANTITY"] = $arPrice["SQ_M_MIN_QUANTITY"];
				}
				unset($keyPrice, $arPrice);
			}
		}
		unset($measureRatio, $key, $arOffer);
	} else {
		if($arResult["ITEM_MEASURE"]["SYMBOL_INTL"] == "pc. 1") {
			if(Bitrix\Main\Loader::includeModule("catalog") && Bitrix\Main\Loader::includeModule("currency")) {
				foreach($arResult["ITEM_PRICES"] as &$arPrice) {
					$arPrice["SQ_M_BASE_PRICE"] = Bitrix\Catalog\Product\Price::roundPrice($arPrice["PRICE_TYPE_ID"], $arPrice["BASE_PRICE"] * $sqMCount, $arPrice["CURRENCY"]);
					$arPrice["SQ_M_PRINT_BASE_PRICE"] = CCurrencyLang::CurrencyFormat($arPrice["BASE_PRICE"] * $sqMCount, $arPrice["CURRENCY"], true);
					$arPrice["SQ_M_PRICE"] = Bitrix\Catalog\Product\Price::roundPrice($arPrice["PRICE_TYPE_ID"], $arPrice["PRICE"] * $sqMCount, $arPrice["CURRENCY"]);	
					$arPrice["SQ_M_PRINT_PRICE"] = CCurrencyLang::CurrencyFormat($arPrice["PRICE"] * $sqMCount, $arPrice["CURRENCY"], true);
					$arPrice["SQ_M_DISCOUNT"] = Bitrix\Catalog\Product\Price::roundPrice($arPrice["PRICE_TYPE_ID"], $arPrice["BASE_PRICE"] * $sqMCount - $arPrice["PRICE"] * $sqMCount, $arPrice["CURRENCY"]);
					$arPrice["SQ_M_PRINT_DISCOUNT"] = CCurrencyLang::CurrencyFormat($arPrice["BASE_PRICE"] * $sqMCount - $arPrice["PRICE"] * $sqMCount, $arPrice["CURRENCY"], true);
					$arPrice["PC_MIN_QUANTITY"] = $arPrice["MIN_QUANTITY"];
					$arPrice["SQ_M_MIN_QUANTITY"] = round($arPrice["MIN_QUANTITY"] / $sqMCount, 2);
				}
				unset($arPrice);
			}
		} elseif($arResult["ITEM_MEASURE"]["SYMBOL_INTL"] == "m2") {
			foreach($arResult["ITEM_PRICES"] as &$arPrice) {
				$arPrice["PC_MIN_QUANTITY"] = 1;
				$arPrice["SQ_M_MIN_QUANTITY"] = $arPrice["MIN_QUANTITY"];
			}
			unset($arPrice);
		}
	}
}
unset($sqMCount);

//PROPERTIES//
foreach($arResult["PROPERTIES"] as &$arProp) {
	//MARKERS//
	if($arProp["CODE"] == "MARKER" && !empty($arProp["VALUE"])) {
		$rsElement = CIBlockElement::GetList(array(), array("ID" => $arProp["VALUE"], "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"]), false, false, array("ID", "IBLOCK_ID", "NAME", "SORT"));	
		while($obElement = $rsElement->GetNextElement()) {
			$arElement = $obElement->GetFields();
			$arElement["PROPERTIES"] = $obElement->GetProperties();

			$arProp["FULL_VALUE"][] = array(
				"NAME" => $arElement["NAME"],
				"SORT" => $arElement["SORT"],
				"BACKGROUND_1" => $arElement["PROPERTIES"]["BACKGROUND_1"]["VALUE"],
				"BACKGROUND_2" => $arElement["PROPERTIES"]["BACKGROUND_2"]["VALUE"],
				"ICON" => $arElement["PROPERTIES"]["ICON"]["VALUE"],
				"FONT_SIZE" => $arElement["PROPERTIES"]["FONT_SIZE"]["VALUE_XML_ID"]
			);
		}
		unset($arElement, $obElement, $rsElement);

		if(!empty($arProp["FULL_VALUE"]))
			Bitrix\Main\Type\Collection::sortByColumn($arProp["FULL_VALUE"], array("SORT" => SORT_NUMERIC, "NAME" => SORT_ASC));	
	//MORE_PHOTO//
	} elseif($arProp["CODE"] == "MORE_PHOTO" && !empty($arProp["VALUE"])) {
		if(!empty($arResult["OFFERS"])) {
			$arResult["MORE_PHOTO"] = array();
			foreach($arProp["VALUE"] as $FILE) {
				$FILE = CFile::GetFileArray($FILE);
				if(is_array($FILE))
					$arResult["MORE_PHOTO"][] = $FILE;
			}
			unset($FILE);
			foreach($arResult["OFFERS"] as &$offer) {		
				if(!empty($offer["DETAIL_PICTURE"])) {
					$offer["MORE_PHOTO_COUNT"] += count($arResult["MORE_PHOTO"]);
					$offer["MORE_PHOTO"] = array_merge($offer["MORE_PHOTO"], $arResult["MORE_PHOTO"]);
				}
			}
			unset($offer);
			foreach($arResult["JS_OFFERS"] as &$offer) {		
				if(!empty($offer["DETAIL_PICTURE"])) {
					$offer["SLIDER_COUNT"] += count($arResult["MORE_PHOTO"]);
					$offer["SLIDER"] = array_merge($offer["SLIDER"], $arResult["MORE_PHOTO"]);
				}
			}
			unset($offer);
		}
	//BRAND//
	} elseif($arProp["CODE"] == "BRAND" && !empty($arProp["VALUE"])) {
		$rsElement = CIBlockElement::GetList(array(), array("ID" => $arProp["VALUE"], "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"]), false, false, array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE"));
		while($arElement = $rsElement->GetNext()) {
			$arProp["FULL_VALUE"] = array(
				"NAME" => $arElement["NAME"],
				"PREVIEW_PICTURE" => $arElement["PREVIEW_PICTURE"] > 0 ? CFile::GetFileArray($arElement["PREVIEW_PICTURE"]) : array()
			);
		}
		unset($arElement, $rsElement);
	//OBJECT//
	} elseif($arProp["CODE"] == "OBJECT" && !empty($arProp["VALUE"])) {
		$arDays = array("MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN");
		$rsElement = CIBlockElement::GetList(array(), array("ID" => $arProp["VALUE"], "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"]), false, false, array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL"));
		while($obElement = $rsElement->GetNextElement()) {
			$arElement = $obElement->GetFields();
			$arElement["PROPERTIES"] = $obElement->GetProperties();
			
			$arProp["FULL_VALUE"] = array(
				"ID" => $arElement["ID"],
				"NAME" => $arElement["NAME"],
				"PREVIEW_PICTURE" => $arElement["PREVIEW_PICTURE"] > 0 ? CFile::GetFileArray($arElement["PREVIEW_PICTURE"]) : false,
				"DETAIL_PAGE_URL" => $arElement["DETAIL_PAGE_URL"]
			);

			foreach($arElement["PROPERTIES"] as $arElProp) {
				//OBJECT_ADDRESS//
				if($arElProp["CODE"] == "ADDRESS" && !empty($arElProp["VALUE"])) {
					$arProp["FULL_VALUE"][$arElProp["CODE"]] = $arElProp["VALUE"];
				//OBJECT_TIMEZONE//
				} elseif($arElProp["CODE"] == "TIMEZONE" && !empty($arElProp["VALUE"])) {
					$rsTZElement = CIBlockElement::GetList(array(), array("ID" => $arElProp["VALUE"], "IBLOCK_ID" => $arElProp["LINK_IBLOCK_ID"]), false, false, array("ID", "IBLOCK_ID"));	
					while($obTZElement = $rsTZElement->GetNextElement()) {
						$arTZElement = $obTZElement->GetFields();
						$arTZElement["PROPERTIES"] = $obTZElement->GetProperties();

						$arProp["FULL_VALUE"][$arElProp["CODE"]] = $arTZElement["PROPERTIES"]["OFFSET"]["VALUE"];
					}
					unset($arTZElement, $obTZElement, $rsTZElement);
				//OBJECT_WORKING_HOURS//
				} elseif(in_array($arElProp["CODE"], $arDays) && !empty($arElProp["VALUE"])) {
					$workingHoursIds[] = $arElProp["VALUE"];
				//OBJECT_PHONE_EMAIL_SKYPE_LINKS//
				} elseif(($arElProp["CODE"] == "PHONE" || $arElProp["CODE"] == "EMAIL" || $arElProp["CODE"] == "SKYPE") && !empty($arElProp["VALUE"])) {
					$arProp["FULL_VALUE"][$arElProp["CODE"]] = array(
						"VALUE" => $arElProp["VALUE"],
						"DESCRIPTION" => $arElProp["DESCRIPTION"]
					);
				//OBJECT_PHONE_SMS_EMAIL_EMAIL//
				} elseif(($arElProp["CODE"] == "PHONE_SMS" || $arElProp["CODE"] == "EMAIL_EMAIL") && !empty($arElProp["VALUE"])) {
					$arProp["FULL_VALUE"][$arElProp["CODE"]] = true;
				}
			}
			unset($arElProp);
			
			//OBJECT_WORKING_HOURS//
			if(!empty($workingHoursIds)) {	
				$rsWHElements = CIBlockElement::GetList(array(), array("ID" => array_unique($workingHoursIds)), false, false, array("ID", "IBLOCK_ID"));	
				while($obWHElement = $rsWHElements->GetNextElement()) {
					$arWHElement = $obWHElement->GetFields();
					$arWHElement["PROPERTIES"] = $obWHElement->GetProperties();

					$arWorkingHours[$arWHElement["ID"]] = array(
						"WORK_START" => strtotime($arWHElement["PROPERTIES"]["WORK_START"]["VALUE"]) ? $arWHElement["PROPERTIES"]["WORK_START"]["VALUE"] : "",
						"WORK_END" => strtotime($arWHElement["PROPERTIES"]["WORK_END"]["VALUE"]) ? $arWHElement["PROPERTIES"]["WORK_END"]["VALUE"] : "",
						"BREAK_START" => strtotime($arWHElement["PROPERTIES"]["BREAK_START"]["VALUE"]) ? $arWHElement["PROPERTIES"]["BREAK_START"]["VALUE"] : "",
						"BREAK_END" => strtotime($arWHElement["PROPERTIES"]["BREAK_END"]["VALUE"]) ? $arWHElement["PROPERTIES"]["BREAK_END"]["VALUE"] : ""
					);
				}
				unset($arWHElement, $obWHElement, $rsWHElements);
				
				if(!empty($arWorkingHours)) {
					foreach($arElement["PROPERTIES"] as $arElProp) {
						if(in_array($arElProp["CODE"], $arDays) && !empty($arElProp["VALUE"])) {
							if(array_key_exists($arElProp["VALUE"], $arWorkingHours)) {
								$arProp["FULL_VALUE"]["WORKING_HOURS"][$arElProp["CODE"]] = $arWorkingHours[$arElProp["VALUE"]];
								$arProp["FULL_VALUE"]["WORKING_HOURS"][$arElProp["CODE"]]["NAME"] = $arElProp["NAME"];
							}
						}
					}
					unset($arElProp);
				}
				unset($arWorkingHours);
			}
			unset($workingHoursIds);
		}
		unset($arElement, $obElement, $rsElement);
	//ADVANTAGES//
	} elseif($arProp["CODE"] == "ADVANTAGES") {
		if(empty($arProp["VALUE"])) {		
			foreach($arResult["SECTION"]["PATH"] as $arSectionPath) {
				$sectionIds[] = $arSectionPath["ID"];
			}
			unset($arSectionPath);
			
			if(!empty($sectionIds)) {		
				$rsSections = CIBlockSection::GetList(array("DEPTH_LEVEL" => "DESC"), array("ID" => $sectionIds, "IBLOCK_ID" => $arParams["IBLOCK_ID"]), false, array("ID", "UF_ADVANTAGES"));
				while($arSection = $rsSections->GetNext()) {
					if(empty($arProp["VALUE"]) && !empty($arSection["UF_ADVANTAGES"]))
						$arProp["VALUE"] = $arSection["UF_ADVANTAGES"];
				}
				unset($arSection, $rsSections);
			}
			unset($sectionIds);
		}
		
		if(!empty($arProp["VALUE"])) {
			$rsElements = CIBlockElement::GetList(array("SORT" => "ASC", "ACTIVE_FROM" => "DESC", "ID" => "DESC"), array("ID" => $arProp["VALUE"], "ACTIVE" => "Y", "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"]), false, false, array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE"));
			while($arElement = $rsElements->GetNext()) {
				if($arElement["PREVIEW_PICTURE"] > 0)
					$arElement["PREVIEW_PICTURE"] = CFile::GetFileArray($arElement["PREVIEW_PICTURE"]);
				$arProp["FULL_VALUE"][] = $arElement;
			}
			unset($arElement, $rsElements);	
		}
	//YOUTUBE_ID//
	} elseif($arProp["CODE"] == "YOUTUBE_ID" && !empty($arProp["VALUE"])) {
		if(!empty($arResult["OFFERS"])) {
			foreach($arResult["OFFERS"] as &$offer) {
				foreach($arProp["VALUE"] as $k => $v) {
					$arYouTube[$k]["ID"] = $arProp["PROPERTY_VALUE_ID"][$k];
					$arYouTube[$k]["VALUE"] = $v;				
					$offer["MORE_PHOTO_COUNT"]++;
				}
				unset($k, $v);

				$offer["MORE_PHOTO"] = array_merge($arYouTube, $offer["MORE_PHOTO"]);			
			}
			unset($offer, $arYouTube);
			foreach($arResult["JS_OFFERS"] as &$offer) {
				foreach($arProp["VALUE"] as $k => $v) {
					$arYouTube[$k]["ID"] = $arProp["PROPERTY_VALUE_ID"][$k];
					$arYouTube[$k]["VALUE"] = $v;				
					$offer["SLIDER_COUNT"]++;
				}
				unset($k, $v);

				$offer["SLIDER"] = array_merge($arYouTube, $offer["SLIDER"]);	
			}
			unset($offer, $arYouTube);
		} else {
			foreach($arProp["VALUE"] as $k => $v) {	
				$arYouTube[$k]["ID"] = $arProp["PROPERTY_VALUE_ID"][$k];
				$arYouTube[$k]["VALUE"] = $v;			
				$arResult["MORE_PHOTO_COUNT"] ++;
			}
			unset($k, $v);

			$arResult["MORE_PHOTO"] = array_merge($arYouTube, $arResult["MORE_PHOTO"]);
			unset($arYouTube);
		}
	//FILES_DOCS//
	} elseif($arProp["CODE"] == "FILES_DOCS" && !empty($arProp["VALUE"])) {
		foreach($arProp["VALUE"] as $arDocId) {
			$arDocFile = CFile::GetFileArray($arDocId);
			
			$fileTypePos = strrpos($arDocFile["FILE_NAME"], ".");		
			$fileType = substr($arDocFile["FILE_NAME"], $fileTypePos + 1);
			$fileTypeFull = substr($arDocFile["FILE_NAME"], $fileTypePos);
			
			$fileName = str_replace($fileTypeFull, "", $arDocFile["ORIGINAL_NAME"]);		
			
			$fileSize = $arDocFile["FILE_SIZE"];
			$metrics = array(
				0 => Loc::getMessage("CT_BCE_CATALOG_SIZE_B"),
				1 => Loc::getMessage("CT_BCE_CATALOG_SIZE_KB"),
				2 => Loc::getMessage("CT_BCE_CATALOG_SIZE_MB"),
				3 => Loc::getMessage("CT_BCE_CATALOG_SIZE_GB")
			);
			$metric = 0;
			while(floor($fileSize / 1024) > 0) {
				$metric ++;
				$fileSize /= 1024;
			}
			$fileSizeFormat = round($fileSize, 1)." ".$metrics[$metric];

			$arProp["FULL_VALUE"][] = array(
				"NAME" => $fileName,
				"DESCRIPTION" => $arDocFile["DESCRIPTION"],
				"TYPE" => $fileType,
				"SIZE" => $fileSizeFormat,
				"SRC" => $arDocFile["SRC"]			
			);
		}
		unset($arDocId);
	//MORE_PRODUCTS//
	} elseif($arProp["CODE"] == "MORE_PRODUCTS" && !empty($arProp["VALUE"])) {
		$rsElements = CIBlockElement::GetList(array(), array("ID" => $arProp["VALUE"], "ACTIVE" => "Y", "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"], "SECTION_GLOBAL_ACTIVE" => "Y"), false, false, array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID"));	
		while($arElement = $rsElements->GetNext()) {	
			if(!empty($arElement["IBLOCK_SECTION_ID"]))
				$sectionIds[] = $arElement["IBLOCK_SECTION_ID"];
		}
		unset($arElement, $rsElements);

		if(!empty($sectionIds)) {
			$arCount = array_count_values($sectionIds);
			$rsSections = CIBlockSection::GetList(array("NAME" => "ASC"), array("ID" => array_unique($sectionIds)), false, array("ID", "IBLOCK_ID", "NAME"));	
			while($arSection = $rsSections->GetNext()) {
				$arProp["SECTIONS"][] = array(
					"ID" => $arSection["ID"],
					"NAME" => $arSection["NAME"],
					"COUNT" => $arCount[$arSection["ID"]]
				);
			}
		}
		unset($arCount, $sectionIds);
	}
}
unset($arProp);

//DISPLAY_PROPERTIES//
if(!empty($arResult["DISPLAY_PROPERTIES"])) {
	foreach($arResult["DISPLAY_PROPERTIES"] as &$property) {
		if($property["CODE"] == "BRAND") {
			continue;
		} elseif($property["CODE"] == "COLLECTION") {			
			$property["DISPLAY_VALUE"] = strip_tags($property["DISPLAY_VALUE"]);
			$rsElements = CIBlockElement::GetList(array(), array("ID" => $property["VALUE"], "IBLOCK_ID" => $property["LINK_IBLOCK_ID"]), false, false, array("ID", "IBLOCK_ID", "CODE", "NAME"));	
			while($obElement = $rsElements->GetNextElement()) {
				$arElement = $obElement->GetFields();
				$arElement["PROPERTIES"] = $obElement->GetProperties();
				foreach($arElement["PROPERTIES"] as $arCollectProp) {
					if($arCollectProp["CODE"] == "BRAND" && !empty($arCollectProp["VALUE"])) {
						$rsBrand = CIBlockElement::GetList(array(), array("ID" => $arCollectProp["VALUE"], "IBLOCK_ID" => $arCollectProp["LINK_IBLOCK_ID"]), false, false, array("ID", "IBLOCK_ID", "DETAIL_PAGE_URL"));
						if($arBrand = $rsBrand->GetNext()) {
							$property["DISPLAY_VALUE"] = "<a href='".$arBrand["~DETAIL_PAGE_URL"].$arElement["CODE"]."/'>".$arElement["NAME"]."</a>";
						}
						unset($arBrand, $rsBrand);
					}
				}
				unset($arCollectProp);
			}
			unset($arElement, $obElement, $rsElements);
		} else {
			$property["DISPLAY_VALUE"] = is_array($property["DISPLAY_VALUE"]) ? implode(" / ", $property["DISPLAY_VALUE"]) : strip_tags($property["DISPLAY_VALUE"]);
		}
	}
	unset($property);
}

//UF_CODE//
if(!empty($arResult["OFFERS"]) && !empty($arResult["OFFERS_PROP"])) {	
	foreach($arResult["SKU_PROPS"] as &$skuProperty) {
		if($skuProperty["SHOW_MODE"] == "PICT") {
			$entity = $skuProperty["USER_TYPE_SETTINGS"]["ENTITY"];
			if(!($entity instanceof Bitrix\Main\Entity\Base))
				continue;
			
			$entityFields = $entity->getFields();
			if(!array_key_exists("UF_CODE", $entityFields))
				continue;
			
			$entityDataClass = $entity->getDataClass();
			
			$directorySelect = array("ID", "UF_CODE");
			$directoryOrder = array();
			
			$entityGetList = array(
				"select" => $directorySelect,
				"order" => $directoryOrder
			);
			$propEnums = $entityDataClass::getList($entityGetList);
			while($oneEnum = $propEnums->fetch()) {
				$values[$oneEnum["ID"]] = $oneEnum["UF_CODE"];
			}

			foreach($skuProperty["VALUES"] as &$val) {				
				if(isset($values[$val["ID"]]))
					$val["CODE"] = $values[$val["ID"]];
			}
			unset($val, $values);
		}
	}
	unset($skuProperty);
}

//RATING_REVIEWS_COUNT//
if($arParams["USE_REVIEW"] != "N" && intval($arParams["REVIEWS_IBLOCK_ID"]) > 0) {
	$ratingSum = $reviewsCount = 0;
	$rsElements = CIBlockElement::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => $arParams["REVIEWS_IBLOCK_ID"], "PROPERTY_PRODUCT_ID" => $arResult["ID"]), false, false, array("ID", "IBLOCK_ID"));
	while($obElement = $rsElements->GetNextElement()) {
		$arElement = $obElement->GetFields();
		$arProps = $obElement->GetProperties();
		
		$ratingSum += $arProps["RATING"]["VALUE_XML_ID"];
		
		$reviewsCount++;
	}
	unset($arProps, $arElement, $obElement, $rsElements);
	
	$arResult["RATING_VALUE"] = $reviewsCount > 0 ? sprintf("%.1f", round($ratingSum / $reviewsCount, 1)) : 0;
	$arResult["REVIEWS_COUNT"] = $reviewsCount;
}

//SETS//
if(CCatalogProductSet::isProductInSet($arResult["ID"])) {
	$allSets = CCatalogProductSet::getAllSetsByProduct($arResult["ID"], CCatalogProductSet::TYPE_SET);
	if(!empty($allSets)) {
		foreach($allSets as $oneSet) {
			if($oneSet["ACTIVE"] == "Y") {
				$arSet = $oneSet;
				break;
			}
		}
		unset($oneSet);
	}
	unset($allSets);

	if(!empty($arSet["ITEMS"])) {
		Bitrix\Main\Type\Collection::sortByColumn($arSet["ITEMS"], array("SORT" => SORT_ASC));
		
		$arSetItemsIds = $arSetItemsLinks = array();
		foreach($arSet["ITEMS"] as $key => $arSetItem) {			
			$arSetItemsIds[] = $arSetItem["ITEM_ID"];
			$arSetItemsLinks[$arSetItem["ITEM_ID"]] = $key;
		}
		unset($key, $arSetItem);

		if(!empty($arSetItemsIds)) {
			$arSetRatioMeasureList = Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arSetItemsIds);
			foreach($arSet["ITEMS"] as &$arSetItem) {
				if(array_key_exists($arSetItem["ITEM_ID"], $arSetRatioMeasureList)) {
					$arSetItem["MEASURE"] = $arSetRatioMeasureList[$arSetItem["ITEM_ID"]]["MEASURE"]["SYMBOL"];
					$arSetItem["QUANTITY"] = round($arSetItem["QUANTITY"] * $arSetRatioMeasureList[$arSetItem["ITEM_ID"]]["RATIO"], 2);
				}
			}
			unset($arSetItem);

			$setItemsList = $setOffersList = array();
			$rsElements = CIBlockElement::GetList(array(), array("ID" => $arSetItemsIds, "ACTIVE" => "Y"), false, false, array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "CATALOG_TYPE"));
			while($obElement = $rsElements->GetNextElement()) {
				$arElement = $obElement->GetFields();
				
				$setItemsList[$arElement["ID"]]["ID"] = $arElement["ID"];
				$setItemsList[$arElement["ID"]]["NAME"] = $arElement["NAME"];

				if($arElement["PREVIEW_PICTURE"] > 0)
					$setItemsList[$arElement["ID"]]["PREVIEW_PICTURE"] = CFile::GetFileArray($arElement["PREVIEW_PICTURE"]);

				$setItemsList[$arElement["ID"]]["DETAIL_PAGE_URL"] = $arElement["DETAIL_PAGE_URL"];

				$arProps = $obElement->GetProperties();
				foreach($arProps as $arProp) {
					if($arProp["CODE"] == "BRAND" && !empty($arProp["VALUE"])) {
						$rsBrandElement = CIBlockElement::GetList(array(), array("ID" => $arProp["VALUE"], "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"]), false, false, array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE"));
						while($arBrandElement = $rsBrandElement->GetNext()) {
							$setItemsList[$arElement["ID"]]["BRAND"] = array(
								"NAME" => $arBrandElement["NAME"],
								"PREVIEW_PICTURE" => $arBrandElement["PREVIEW_PICTURE"] > 0 ? CFile::GetFileArray($arBrandElement["PREVIEW_PICTURE"]) : array()
							);
						}
						unset($arBrandElement, $rsBrandElement);
					}
				}
				unset($arProp);

				if($arElement["CATALOG_TYPE"] == Bitrix\Catalog\ProductTable::TYPE_OFFER)
					$setOffersList[$arElement["ID"]] = $arElement["ID"];
			}
			unset($arProps, $arElement, $obElement, $rsElements);
		}
		unset($arSetItemsIds);

		if(!empty($setItemsList)) {
			if(!empty($setOffersList)) {
				$setItemsParents = CCatalogSku::getProductList($setOffersList);
				if(!empty($setItemsParents)) {
					$offersMap = array();
					foreach($setItemsParents as $offerId => $parentData) {
						if(!isset($offersMap[$parentData["ID"]]))
							$offersMap[$parentData["ID"]] = array();
						$offersMap[$parentData["ID"]][$offerId] = $offerId;
					}
					unset($offerId, $parentData);
					
					if(!empty($offersMap)) {
						$rsElements = CIBlockElement::GetList(array(), array("ID" => array_keys($offersMap), "ACTIVE" => "Y"), false, false, array("ID", "IBLOCK_ID", "PREVIEW_PICTURE"));
						while($obElement = $rsElements->GetNextElement()) {
							$arElement = $obElement->GetFields();
							$arProps = $obElement->GetProperties();
							
							foreach($offersMap[$arElement["ID"]] as $itemId) {
								unset($setOffersList[$itemId]);
								
								if(!isset($setItemsList[$itemId]["PREVIEW_PICTURE"]) && $arElement["PREVIEW_PICTURE"] > 0)
									$setItemsList[$itemId]["PREVIEW_PICTURE"] = CFile::GetFileArray($arElement["PREVIEW_PICTURE"]);

								if(!isset($setItemsList[$itemId]["DETAIL_PAGE_URL"]))
									$setItemsList[$itemId]["DETAIL_PAGE_URL"] = $arElement["DETAIL_PAGE_URL"];

								if(!isset($setItemsList[$itemId]["BRAND"])) {
									foreach($arProps as $arProp) {
										if($arProp["CODE"] == "BRAND" && !empty($arProp["VALUE"])) {
											$rsBrandElement = CIBlockElement::GetList(array(), array("ID" => $arProp["VALUE"], "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"]), false, false, array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE"));
											while($arBrandElement = $rsBrandElement->GetNext()) {
												$setItemsList[$itemId]["BRAND"] = array(
													"NAME" => $arBrandElement["NAME"],
													"PREVIEW_PICTURE" => $arBrandElement["PREVIEW_PICTURE"] > 0 ? CFile::GetFileArray($arBrandElement["PREVIEW_PICTURE"]) : array()
												);
											}
											unset($arBrandElement, $rsBrandElement);
										}
									}
									unset($arProp);
								}
							}
							unset($itemId);
						}
						unset($arProps, $arElement, $obElement, $rsElements);
					}
					unset($offersMap);
				}
				unset($setItemsParents);
				
				if(!empty($setOffersList)) {
					foreach($setOffersList as $setOfferId)
						unset($setItemsList[$setOfferId]);
					unset($setOfferId);
				}
			}
			unset($setOffersList);
			
			foreach($setItemsList as $setItem) {
				if(array_key_exists($setItem["ID"], $arSetItemsLinks))
					$arSet["ITEMS"][$arSetItemsLinks[$setItem["ID"]]]["ITEM_DATA"] = $setItem;
			}
			unset($setItem);
		}
		unset($setItemsList);

		foreach($arSet["ITEMS"] as &$arSetItem) {
			if(!isset($arSetItem["ITEM_DATA"]))
				continue;

			$arSetItem["ITEM_DATA"]["QUANTITY"] = $arSetItem["QUANTITY"];
			$arSetItem["ITEM_DATA"]["MEASURE"] = $arSetItem["MEASURE"];

			$arResult["SET_ITEMS"][] = $arSetItem["ITEM_DATA"];
		}
		unset($arSetItem);
	}
	unset($arSet);
}

$templateFolder = $this->GetFolder();

$arResult["LED_ICON"] = false;
$arResult["ECO_ICON"] = $templateFolder.'/images/eco.jpg';
$arResult["5YEAR_ICON"] = false;
$arResult["HOME_ICON"] = false;
$arResult["BLOB_ICON"] = false;
$arResult["SNOW_ICON"] = false;
$arResult["CLIMATE_ICON"] = false;
$arResult["STREAM_ICON"] = false;
$arResult["TEMP_COLOR_ICON"] = false;

if(strtolower($arResult["PROPERTIES"]["LIGHT"]['VALUE']) == 'led' || strtolower($arResult["PROPERTIES"]["LIGHT"]['VALUE']) == 'светодиод')
{
	$arResult["LED_ICON"] = $templateFolder.'/images/led.jpg';
}

if(strtolower($arResult["PROPERTIES"]["WARRANTY"]['VALUE']) == '5 лет')
{
	$arResult["5YEAR_ICON"] = $templateFolder.'/images/а5year.jpg';
}
if(strtolower($arResult["PROPERTIES"]["WARRANTY"]['VALUE']) == '7 лет')
{
	$arResult["7YEAR_ICON"] = $templateFolder.'/images/а7year.jpg';
}

if(preg_match("/IP[ ]{0,1}([0-9]+)/", trim($arResult["PROPERTIES"]["IP_CLASS"]['VALUE']), $arMatches))
{
	if(intval($arMatches[1]) > 0 && intval($arMatches[1]) < 54)
	{
		$arResult["HOME_ICON"] = $templateFolder.'/images/vnutri.jpg';
	}
}

if(preg_match("/IP[ ]{0,1}([0-9]+)/", trim($arResult["PROPERTIES"]["IP_CLASS"]['VALUE']), $arMatches))
{
	if(intval($arMatches[1]) > 0 && intval($arMatches[1]) >= 54)
	{
		$arResult["BLOB_ICON"] = $templateFolder.'/images/vlajnost.jpg';
	}
}

if(preg_match("/\-[\ ]{0,1}([0-9]+)/", trim($arResult["PROPERTIES"]["TEMP_GROUP"]['VALUE']), $arMatches))
{
	if(intval($arMatches[1]) > 0 && intval($arMatches[1]) >= 40)
	{
		$arResult["SNOW_ICON"] = $templateFolder.'/images/snow.jpg';
	}
}

$arClimate = Array('У1', 'ХЛ1', 'УХЛ1');

if(in_array(strtoupper($arResult["PROPERTIES"]["CLIMATE"]['VALUE']), $arClimate))
{
	$arResult["CLIMATE_ICON"] = $templateFolder.'/images/streetlight.jpg';
}


if(preg_match("/([0-9]+)/", trim($arResult["PROPERTIES"]["LIGHT_STREAM"]['VALUE']), $arStreamMatches))
{
	$LIGHT_STREAM = $arStreamMatches[1];
	
	if(preg_match("/([0-9]+)/", trim($arResult["PROPERTIES"]["POWER"]['VALUE']), $arPowerMatches))
	{
		$POWER = $arPowerMatches[1];

		if($LIGHT_STREAM / $POWER >= 137)
		{
			$arResult["STREAM_ICON"] = $templateFolder.'/images/lm-vt.jpg';
			$arResult["STREAM_ICON_TITLE"] = '140 Лм/Вт';
		}
		
		if($LIGHT_STREAM / $POWER > 159)
		{
			$arResult["STREAM_ICON"] = $templateFolder.'/images/lm-vt2.jpg';
			$arResult["STREAM_ICON_TITLE"] = '160 Лм/Вт';
		}
	}
}




$arTempColorIcon = GetColorTempIcon($arResult["NAME"]);

if($arTempColorIcon)
{
	$arResult["TEMP_COLOR_ICON"] = $templateFolder.'/images/'.$arTempColorIcon['IMG'];
	$arResult["TEMP_COLOR_ICON_TITLE"] = $arTempColorIcon['TITLE'];
}

if($arResult["PROPERTIES"]["ANGLE"]['VALUE'])
{
	$arAngeIcons = GetAngleIcon($arResult["PROPERTIES"]["ANGLE"]['VALUE']);
	
	foreach($arAngeIcons as $icon)
	{
		if(file_exists($_SERVER['DOCUMENT_ROOT'].$templateFolder.'/images/angle/'.$icon))
		{
			$arResult["ANGLE_ICON"][] = array(
				"ICON" => $templateFolder.'/images/angle/'.$icon,
				"ICON_TITLE" => 'Угол раскрытия светового пучка '.str_replace(".jpg", "", $icon).'°'
			);
		}
	}
}

//CACHE_KEYS//
$haveOffers = !empty($arResult["OFFERS"]);
if($haveOffers) {
	$actualItem = isset($arResult["OFFERS"][$arResult["OFFERS_SELECTED"]])
		? $arResult["OFFERS"][$arResult["OFFERS_SELECTED"]]
		: reset($arResult["OFFERS"]);
	$arResult["DETAIL_PICTURE_EPILOG"] = $actualItem["DETAIL_PICTURE"];
} else {
	$arResult["DETAIL_PICTURE_EPILOG"] = $arResult["DETAIL_PICTURE"];
}

$arResult["BREADCRUMB_TITLE"] = $arResult["PROPERTIES"]["BREADCRUMB_TITLE"]["VALUE"];

$this->__component->SetResultCacheKeys(
	array(
		"NAME",	
		"PREVIEW_TEXT",
		"DETAIL_PICTURE_EPILOG",
		"BREADCRUMB_TITLE"
	)
);

function GetAngleIcon($anglePropertyValue)
{
	$arValues = array();
	
	if(strpos($anglePropertyValue, ',') !== false)
	{
		$arAnglePropertyValue = explode(',', $anglePropertyValue);
		foreach($arAnglePropertyValue as $value)
		{
			$value = trim($value);
			$value =  preg_replace('/[^xх0-9]/', '', $value);
			$value = str_replace(array('x','х'),'-',$value);
			$arValues[] = $value.'.jpg';
		}
	}
	else
	{
		$anglePropertyValue =  preg_replace('/[^xх0-9]/', '', $anglePropertyValue);
		$anglePropertyValue = str_replace(array('x','х'),'-',$anglePropertyValue);
		$arValues[] = $anglePropertyValue.'.jpg';
	}
	
	return $arValues;
}

function GetColorTempIcon($title)
{
	$arIcon= false;
	$title = strtolower($title);
	
	if(strpos($title, 'епл') > 0)
	{
		$arIcon['IMG'] = 'tepl.jpg';
		$arIcon['TITLE'] = 'Теплый свет';
	}
	if((strpos($title, 'невн') > 0) || (strpos($title, 'ейтральн') > 0))
	{
		$arIcon['IMG'] = 'dnev.jpg';
		$arIcon['TITLE'] = 'Дневной свет';
	}
	if(strpos($title, 'олодн') > 0)
	{
		$arIcon['IMG'] = 'holo.jpg';
		$arIcon['TITLE'] = 'Холодный свет';
	}
	if(strpos($title, 'елт') > 0)
	{
		$arIcon['IMG'] = 'zhel.jpg';
		$arIcon['TITLE'] = 'Желтый свет';
	}
	if(strpos($title, 'елен') > 0)
	{
		$modx->setPlaceholder('uzele', '1');
		$arIcon['IMG'] = 'zele.jpg';
		$arIcon['TITLE'] = 'Зеленый свет';
	}
	if(strpos($title, 'расн') > 0)
	{
		$arIcon['IMG'] = 'kras.jpg';
		$arIcon['TITLE'] = 'Красный свет';
	}
	if(strpos($title, 'ульти') > 0)
	{
		$arIcon['IMG'] = 'rgb.jpg';
		$arIcon['TITLE'] = 'Мульти RGB';
	}
	if(strpos($title, 'озов') > 0)
	{
		$arIcon['IMG'] = 'rozo.jpg';
		$arIcon['TITLE'] = 'Розовый свет';
	}
	if(strpos($title, 'нтарн') > 0)
	{
		$arIcon['IMG'] = 'yant.jpg';
		$arIcon['TITLE'] = 'Янтарный свет';
	}
	if(strpos($title, 'синий') > 0 || strpos($title, 'синяя') > 0)
	{
		$arIcon['IMG'] = 'sini.jpg';
		$arIcon['TITLE'] = 'Синий свет';
	}
	if(strpos($title, 'олуб') > 0)
	{
		$arIcon['IMG'] = 'golo.jpg';
		$arIcon['TITLE'] = 'Голубой свет';
	}
	if(strpos($title, 'льтра') > 0)
	{
		$arIcon['IMG']= 'uhol.jpg';
		$arIcon['TITLE'] = 'Ультра холодный свет';
	}
	if(strpos($title, 'RGB') > 0)
	{
		if(strpos($title, 'RGBA') > 0)
		{
			$arIcon['IMG'] = 'rgba.jpg';
			$arIcon['TITLE'] = 'Мульти RGBA';
		}
		elseif(strpos($title, 'RGBW') > 0)
		{
			$arIcon['IMG'] = 'rgbw.jpg';
			$arIcon['TITLE'] = 'Мульти RGBW';
		}
		else
		{
			$arIcon['IMG'] = 'rgb.jpg';
			$arIcon['TITLE'] = 'Мульти RGB';
		}
	}
	
	return $arIcon;
}