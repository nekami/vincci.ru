(function() {
	'use strict';

	if(!!window.JCNewsDetail)
		return;

	window.JCNewsDetail = function(params) {
		this.container = document.querySelector('[data-entity="' + params.container + '"]');
		
		BX.ready(BX.delegate(this.adjustBanner, this));
		BX.ready(BX.delegate(this.adjustGallery, this));
		BX.bind(window, 'resize', BX.proxy(this.adjustBanner, this));
		BX.bind(window, 'resize', BX.proxy(this.adjustGallery, this));
	};

	window.JCNewsDetail.prototype =	{		
		adjustBanner: function() {			
			var pageContainer = document.querySelector('.page-container'),
				containerWidth = !!pageContainer ? pageContainer.offsetWidth : document.body.clientWidth,
				banner = this.container.querySelector('.services-detail__banner'),		
				coeff = 3.33;
			
			if(!!banner) {
				BX.adjust(banner, {
					style: {
						width: containerWidth + 'px',
						height: Math.ceil(containerWidth / coeff) + 'px',						
						marginLeft: '-' + (!!pageContainer
							? this.container.getBoundingClientRect().left - pageContainer.getBoundingClientRect().left
							: this.container.getBoundingClientRect().left) + 'px'
					}
				});
			}
		},

		adjustGallery: function() {
			var galleryContainer = document.querySelector('.detail-blocks-wrapper'),
				gallery = galleryContainer.querySelector('.gallery'),
				itemsBg = !!gallery && gallery.querySelectorAll('.gallery-item__bg'),
				i;

			for(i in itemsBg) {
				if(itemsBg.hasOwnProperty(i)) {
					BX.remove(itemsBg[i].parentNode);
				}
			}

			if(!!gallery) {
				var items = gallery.querySelectorAll('.gallery-item'),
					itemsCount = items.length,
					itemsRowCount = 4,
					rowsCount = Math.ceil(itemsCount / itemsRowCount),
					itemsBgCount = (itemsRowCount * rowsCount) - itemsCount,
					itemsAll,		
					itemImage,
					itemCaption,		
					coeff = 4 / 3,
					j, k;
				
				for(j = 0; j < itemsBgCount; j++) {
					gallery.appendChild(BX.create('DIV', {
						props: {className: 'col-xs-6 col-md-3'},
						children: [
							BX.create('DIV', {
								props: {className: 'gallery-item gallery-item__bg'}
							})
						]
					}));
				}
				
				itemsAll = gallery.querySelectorAll('.gallery-item');	
				for(k in itemsAll) {
					if(itemsAll.hasOwnProperty(k)) {
						itemImage = itemsAll[k].querySelector('.gallery-item__image');
						if(!!itemImage)
							BX.style(itemImage, 'height', Math.ceil(itemsAll[k].offsetWidth / coeff) + 'px');

						itemCaption = itemsAll[k].querySelector('.gallery-item__caption-wrap');
						if(!!itemCaption)
							BX.style(itemCaption, 'height', Math.ceil(itemsAll[k].offsetWidth / coeff) + 'px');

						if(BX.hasClass(itemsAll[k], 'gallery-item__bg'))
							BX.style(itemsAll[k], 'height', Math.ceil(itemsAll[k].offsetWidth / coeff) + 'px');
					}
				}
			}
		}
	}
})();