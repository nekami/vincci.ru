<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($arResult['ID']));
$containerName = 'detail-'.$obName;?>

<div id="<?=$this->GetEditAreaId($arResult['ID'])?>" data-entity="<?=$containerName?>">
	<?//PREVIEW_TEXT//
	if(!empty($arResult["PREVIEW_TEXT"])) {?>
		<div class="services-detail__preview-text"><?=$arResult["PREVIEW_TEXT"]?></div>
	<?}
	
	//DETAIL_PICTURE//
	if(is_array($arResult["DETAIL_PICTURE"])) {?>				
		<div class="services-detail__banner lazy-load" data-src="<?=$arResult['DETAIL_PICTURE']['SRC']?>"></div>
	<?}

	//DETAIL_TEXT//
	if(!empty($arResult["DETAIL_TEXT"])) {?>
		<div class="services-detail__detail-text"><?=$arResult["DETAIL_TEXT"]?></div>
	<?}?>
</div>

<?//GALLERY_FILES_DOCS//
$this->SetViewTarget("gallery_files_docs");?>

<div class="detail-blocks-wrapper">
<?if((is_array($arResult["DISPLAY_PROPERTIES"]["GALLERY"]["FILE_VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["GALLERY"]["FILE_VALUE"]) > 0) || (is_array($arResult["DISPLAY_PROPERTIES"]["FILES_DOCS"]["FILE_VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["FILES_DOCS"]["FILE_VALUE"]) > 0)) {	
	//GALLERY//
	if(is_array($arResult["DISPLAY_PROPERTIES"]["GALLERY"]["FILE_VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["GALLERY"]["FILE_VALUE"]) > 0) {?>
		<div class="gallery-wrapper detail">			
			<div class="container">				
				<div class="row gallery<?=(!$arResult["DISPLAY_PROPERTIES"]["GALLERY_TITLE"] ? ' no_h1' : '');?>">
					<?if($arResult["DISPLAY_PROPERTIES"]["GALLERY_TITLE"]) {?>
						<div class="col-xs-12">
							<div class="h1"><?=$arResult["DISPLAY_PROPERTIES"]["GALLERY_TITLE"]["VALUE"];?></div>
						</div>
					<?}
					foreach($arResult["DISPLAY_PROPERTIES"]["GALLERY"]["FILE_VALUE"] as $key => $arFile) {?>
						<div class="col-xs-6 col-md-3">				
							<a class="gallery-item fancyimage" title="<?=$arFile['DESCRIPTION']?>" href="<?=$arFile['SRC']?>" data-fancybox-group="gallery">
								<span class="gallery-item__image lazy-load"<?=(!empty($arFile['PREVIEW']) ? " data-src='".$arFile['PREVIEW']."'" : "");?>></span>
								<?if(!empty($arFile["DESCRIPTION"])) {?>
									<span class="gallery-item__caption-wrap">
										<span class="gallery-item__caption">
											<span class="gallery-item__title"><?=$arFile["DESCRIPTION"]?></span>
										</span>
									</span>
								<?}?>
							</a>
						</div>
					<?}?>
				</div>
			</div>
		</div>	
	<?}

	//FILES_DOCS//
	if(is_array($arResult["DISPLAY_PROPERTIES"]["FILES_DOCS"]["FILE_VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["FILES_DOCS"]["FILE_VALUE"]) > 0) {?>
		<div class="files-docs-wrapper">			
			<div class="container">
				<div class="row files-docs">
					<div class="col-xs-12">
						<div class="h2"><?=$arResult["DISPLAY_PROPERTIES"]["FILES_DOCS"]["NAME"];?></div>
					</div>
					<?foreach($arResult["DISPLAY_PROPERTIES"]["FILES_DOCS"]["FILE_VALUE"] as $key => $arDoc) {?><!--
						--><div class="col-xs-12 col-md-3">
							<a class="files-docs-item" href="<?=$arDoc['SRC']?>" target="_blank">
								<div class="item-icon">
									<?if($arDoc["FILE_TYPE"] == "doc" || $arDoc["FILE_TYPE"] == "docx" || $arDoc["FILE_TYPE"] == "rtf") {?>
										<i class="fa fa-file-word-o"></i>
									<?} elseif($arDoc["FILE_TYPE"] == "xls" || $arDoc["FILE_TYPE"] == "xlsx") {?>
										<i class="fa fa-file-excel-o"></i>
									<?} elseif($arDoc["FILE_TYPE"] == "pdf") {?>
										<i class="fa fa-file-pdf-o"></i>
									<?} elseif($arDoc["FILE_TYPE"] == "rar" || $arDoc["FILE_TYPE"] == "zip" || $arDoc["FILE_TYPE"] == "gzip") {?>
										<i class="fa fa-file-archive-o"></i>
									<?} elseif($arDoc["FILE_TYPE"] == "jpg" || $arDoc["FILE_TYPE"] == "jpeg" || $arDoc["FILE_TYPE"] == "png" || $arDoc["FILE_TYPE"] == "gif") {?>
										<i class="fa fa-file-image-o"></i>
									<?} elseif($arDoc["FILE_TYPE"] == "ppt" || $arDoc["FILE_TYPE"] == "pptx") {?>
										<i class="fa fa-file-powerpoint-o"></i>
									<?} elseif($arDoc["FILE_TYPE"] == "txt") {?>
										<i class="fa fa-file-text-o"></i>
									<?} else {?>
										<i class="fa fa-file-o"></i>
									<?}?>
								</div>
								<div class="item-caption">
									<span class="item-name"><?=!empty($arDoc["DESCRIPTION"]) ? $arDoc["DESCRIPTION"] : $arDoc["FILE_NAME"]?></span>
									<span class="item-size"><?=GetMessage("SIZE").$arDoc["FILE_SIZE"]?></span>
								</div>
							</a>
						</div><!--
					--><?}?>
				</div>
			</div>
		</div>	
	<?}
}

$this->EndViewTarget();?>

<script type="text/javascript">
	var <?=$obName?> = new JCNewsDetail({
		container: '<?=$containerName?>'
	});
</script>