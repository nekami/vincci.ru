<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);

$this->addExternalCss(SITE_TEMPLATE_PATH."/js/owlCarousel/owl.carousel.css");
$this->addExternalJS(SITE_TEMPLATE_PATH."/js/owlCarousel/owl.carousel.min.js");

$mainId = $this->GetEditAreaId($arResult['ID']);
$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);?>

<div class="brands-detail" id="<?=$mainId?>" itemprop="brand" itemscope itemtype="http://schema.org/Brand">
    <?//META ?>
    <meta itemprop="name" content="<?=$arResult["NAME"]?>" />
    <?//TABS//?>
    <div class="brands-detail-tabs-container">
        <div class="brands-detail-tabs-block" data-entity="tabs">
            <div class="brands-detail-tabs-scroll">
                <ul class="brands-detail-tabs-list">
                    <li class="brands-detail-tab active" data-entity="tab" data-value="brand"><?=Loc::getMessage("BRANDS_ITEM_DETAIL_TAB_BRAND")?></li>
                    <?if(!empty($arResult["COLLECTIONS_IDS"])) {?>
                        <li class="brands-detail-tab" data-entity="tab" data-value="collections"><?=Loc::getMessage("BRANDS_ITEM_DETAIL_TAB_COLLECTIONS")?><span><?=count($arResult["COLLECTIONS_IDS"])?></span></li>
                    <?}
                    if(!empty($arResult["PRODUCTS_IDS"])) {?>
                        <li class="brands-detail-tab" data-entity="tab" data-value="products"><?=Loc::getMessage("BRANDS_ITEM_DETAIL_TAB_PRODUCTS")?><span><?=count($arResult["PRODUCTS_IDS"])?></span></li>
                    <?}
                    if(!empty($arResult["DETAIL_TEXT"])) {?>
                        <li class="brands-detail-tab" data-entity="tab" data-value="description"><?=Loc::getMessage("BRANDS_ITEM_DETAIL_TAB_DESCRIPTION")?></li>
                    <?}?>
                </ul>
            </div>
        </div>
    </div>
    <div class="brands-detail-tabs-content">
        <?//ITEM//?>
        <div class="brands-item-detail" data-entity="tab-container" data-value="brand">
            <div class="brands-item-detail-item">
                <div class="brands-item-detail-item-image">
                    <?if(is_array($arResult["DETAIL_PICTURE"])) {?>
                        <img itemprop="logo" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" width="<?=$arResult['DETAIL_PICTURE']['WIDTH']?>" height="<?=$arResult['DETAIL_PICTURE']['HEIGHT']?>" alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>" />
                    <?}?>
                </div>
                <?if(!empty($arResult["MARKER"])) {?>
                    <div class="brands-item-detail-item-markers">
                        <?foreach($arResult["MARKER"] as $key => $arMarker) {
                            if($key <= 1) {?>
                                <div class="brands-item-detail-item-marker-container">
                                    <div class="brands-item-detail-item-marker<?=(!empty($arMarker['FONT_SIZE']) ? ' brands-item-detail-item-marker-'.$arMarker['FONT_SIZE'] : '')?>"<?=(!empty($arMarker["BACKGROUND_1"]) && !empty($arMarker["BACKGROUND_2"]) ? " style='background: ".$arMarker["BACKGROUND_2"]."; background: -webkit-linear-gradient(left, ".$arMarker["BACKGROUND_1"].", ".$arMarker["BACKGROUND_2"]."); background: -moz-linear-gradient(left, ".$arMarker["BACKGROUND_1"].", ".$arMarker["BACKGROUND_2"]."); background: -o-linear-gradient(left, ".$arMarker["BACKGROUND_1"].", ".$arMarker["BACKGROUND_2"]."); background: -ms-linear-gradient(left, ".$arMarker["BACKGROUND_1"].", ".$arMarker["BACKGROUND_2"]."); background: linear-gradient(to right, ".$arMarker["BACKGROUND_1"].", ".$arMarker["BACKGROUND_2"].");'" : (!empty($arMarker["BACKGROUND_1"]) && empty($arMarker["BACKGROUND_2"]) ? " style='background: ".$arMarker["BACKGROUND_1"].";'" : (empty($arMarker["BACKGROUND_1"]) && !empty($arMarker["BACKGROUND_2"]) ? " style='background: ".$arMarker["BACKGROUND_2"].";'" : "")))?>><?=(!empty($arMarker["ICON"]) ? "<i class='".$arMarker["ICON"]."'></i>" : "")?><span><?=$arMarker["NAME"]?></span></div>
                                </div>
                            <?} else {
                                break;
                            }
                        }
                        unset($key, $arMarker);?>
                    </div>
                <?}
                if(!empty($arResult["DISPLAY_PROPERTIES"]["COUNTRY"])) {?>
                    <div class="brands-item-detail-item-text"><?=strip_tags($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"])?></div>
                <?}?>
            </div>
            <div class="brands-item-detail-preview-text"><?=(!empty($arResult["PREVIEW_TEXT"]) ? $arResult["PREVIEW_TEXT"] : "")?></div>
        </div>
        <?//COLLECTIONS//
        if(!empty($arResult["COLLECTIONS_IDS"])) {?>
            <div class="brands-detail-collections-container" data-entity="tab-container" data-value="collections">
                <div class="h2"><?=Loc::getMessage("BRANDS_ITEM_DETAIL_COLLECTIONS", array("#BRAND#" => $arResult["NAME"]))?></div>
                <div class="brands-detail-collections">
                    <?$GLOBALS["arBrandsCollectFilter"] = array("ID" => $arResult["COLLECTIONS_IDS"]);?>
                    <?$APPLICATION->IncludeComponent("bitrix:news.list", "collections",
                        array(
                            "IBLOCK_TYPE" => $arParams["COLLECTIONS_IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arParams["COLLECTIONS_IBLOCK_ID"],
                            "NEWS_COUNT" => $arParams["COLLECTIONS_NEWS_COUNT"],
                            "SORT_BY1" => $arParams["COLLECTIONS_SORT_BY1"],
                            "SORT_ORDER1" => $arParams["COLLECTIONS_SORT_ORDER1"],
                            "SORT_BY2" => $arParams["COLLECTIONS_SORT_BY2"],
                            "SORT_ORDER2" => $arParams["COLLECTIONS_SORT_ORDER2"],
                            "FILTER_NAME" => "arBrandsCollectFilter",
                            "FIELD_CODE" => array(),
                            "PROPERTY_CODE" => $arParams["COLLECTIONS_PROPERTY_CODE"],
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "AJAX_MODE" => "",
                            "AJAX_OPTION_SHADOW" => "",
                            "AJAX_OPTION_JUMP" => "",
                            "AJAX_OPTION_STYLE" => "",
                            "AJAX_OPTION_HISTORY" => "",
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "",
                            "DISPLAY_PANEL" => "",
                            "SET_TITLE" => "N",
                            "SET_BROWSER_TITLE" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "DISPLAY_NAME" => "",
                            "DISPLAY_DATE" => "",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "PAGER_SHOW_ALWAYS" => "",
                            "PAGER_TEMPLATE" => "arrows",
                            "PAGER_DESC_NUMBERING" => "",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "",
                            "PAGER_SHOW_ALL" => "",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "SHOW_MIN_PRICE" => $arParams["COLLECTIONS_SHOW_MIN_PRICE"],
                            "CATALOG_IBLOCK_TYPE" => $arParams["CATALOG_IBLOCK_TYPE"],
                            "CATALOG_IBLOCK_ID" => $arParams["CATALOG_IBLOCK_ID"],
                            "CATALOG_PRICE_CODE" => $arParams["CATALOG_PRICE_CODE"],
                            "CATALOG_PRICE_VAT_INCLUDE" => $arParams["CATALOG_PRICE_VAT_INCLUDE"],
                            "CATALOG_CONVERT_CURRENCY" => $arParams["CATALOG_CONVERT_CURRENCY"],
                            "CURRENCY_ID" => $arParams["CATALOG_CURRENCY_ID"]
                        ),
                        $component,
                        array("HIDE_ICONS" => "Y")
                    );?>
                </div>
            </div>
        <?}
        else{
            ?>
            <script>
                var templateFolder = "<?=$templateFolder?>";
            </script>
            <?
        }
        //SECTIONS_PRODUCTS//
        //DETAIL_TEXT//
        ?>

