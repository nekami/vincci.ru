<?php

namespace Sprint\Migration;


class BRANDS_IBLOCK20191223112824 extends Version
{
    protected $description = "Свойства парсера в инфоблоке брендов";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();
        $iblockId = $helper->Iblock()->getIblockIdIfExists('enext_brands_s1', 'catalog');
        $helper->Iblock()->saveProperty($iblockId, array (
          'NAME' => 'Профиль парсера',
          'ACTIVE' => 'Y',
          'SORT' => '500',
          'CODE' => 'PARSER_TEMPLATE',
          'DEFAULT_VALUE' => '',
          'PROPERTY_TYPE' => 'E',
          'ROW_COUNT' => '1',
          'COL_COUNT' => '30',
          'LIST_TYPE' => 'L',
          'MULTIPLE' => 'N',
          'XML_ID' => '',
          'FILE_TYPE' => '',
          'MULTIPLE_CNT' => '5',
          'LINK_IBLOCK_ID' => 'parser:parse_templates',
          'WITH_DESCRIPTION' => 'N',
          'SEARCHABLE' => 'N',
          'FILTRABLE' => 'N',
          'IS_REQUIRED' => 'N',
          'VERSION' => '1',
          'USER_TYPE' => NULL,
          'USER_TYPE_SETTINGS' => NULL,
          'HINT' => '',
        ));

    }

    public function down()
    {
        //your code ...
    }
}