<?php

namespace Sprint\Migration;


class NO_PARSE_CATALOG_PROP20191227115918 extends Version
{
    protected $description = "Добавление свойства \"не парсить\" к элементам каталога";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();
        $iblockId = $helper->Iblock()->getIblockIdIfExists('enext_catalog_s1', 'catalog');
        $helper->Iblock()->saveProperty($iblockId, array (
  'NAME' => 'Не парсить',
  'ACTIVE' => 'Y',
  'SORT' => '500',
  'CODE' => 'NO_PARSE',
  'DEFAULT_VALUE' => '',
  'PROPERTY_TYPE' => 'L',
  'ROW_COUNT' => '1',
  'COL_COUNT' => '30',
  'LIST_TYPE' => 'C',
  'MULTIPLE' => 'N',
  'XML_ID' => '',
  'FILE_TYPE' => '',
  'MULTIPLE_CNT' => '5',
  'LINK_IBLOCK_ID' => '0',
  'WITH_DESCRIPTION' => 'N',
  'SEARCHABLE' => 'N',
  'FILTRABLE' => 'N',
  'IS_REQUIRED' => 'N',
  'VERSION' => '1',
  'USER_TYPE' => NULL,
  'USER_TYPE_SETTINGS' => NULL,
  'HINT' => '',
  'VALUES' => 
  array (
    0 => 
    array (
      'VALUE' => 'Y',
      'DEF' => 'Y',
      'SORT' => '500',
      'XML_ID' => 'Y',
    ),
  ),
));

    }

    public function down()
    {
        //your code ...
    }
}