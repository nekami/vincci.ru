<?php

namespace Sprint\Migration;


class PROPERTY_CLEAR_SCRIPTS_PARSER20200210135752 extends Version
{
    protected $description = "Добавлено свойство очистки скриптов в шаблон парсера";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();
        $iblockId = $helper->Iblock()->getIblockIdIfExists('parse_templates', 'parser');
        $helper->Iblock()->saveProperty($iblockId, array (
  'NAME' => 'Удалять скрипты',
  'ACTIVE' => 'Y',
  'SORT' => '500',
  'CODE' => 'CLEAR_SCRIPTS',
  'DEFAULT_VALUE' => '',
  'PROPERTY_TYPE' => 'L',
  'ROW_COUNT' => '1',
  'COL_COUNT' => '30',
  'LIST_TYPE' => 'C',
  'MULTIPLE' => 'N',
  'XML_ID' => '',
  'FILE_TYPE' => '',
  'MULTIPLE_CNT' => '5',
  'LINK_IBLOCK_ID' => '0',
  'WITH_DESCRIPTION' => 'N',
  'SEARCHABLE' => 'N',
  'FILTRABLE' => 'N',
  'IS_REQUIRED' => 'N',
  'VERSION' => '2',
  'USER_TYPE' => NULL,
  'USER_TYPE_SETTINGS' => NULL,
  'HINT' => '',
  'VALUES' => 
  array (
    0 => 
    array (
      'VALUE' => 'Y',
      'DEF' => 'Y',
      'SORT' => '500',
      'XML_ID' => '0941a402fe887cc313ad49ebd84a19f9',
    ),
  ),
));

    }

    public function down()
    {
        //your code ...
    }
}