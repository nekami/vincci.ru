<?php

namespace Sprint\Migration;


class needle_elems_added_remove_tags20200302115424 extends Version
{
    protected $description = "Добавлено свойство удаления тегов из значений искомых элементов";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();
        $iblockId = $helper->Iblock()->getIblockIdIfExists('parse_elements', 'parser');
        $helper->Iblock()->saveProperty($iblockId, array (
  'NAME' => 'Вырезать теги из значения',
  'ACTIVE' => 'Y',
  'SORT' => '500',
  'CODE' => 'REMOVE_TAGS',
  'DEFAULT_VALUE' => '',
  'PROPERTY_TYPE' => 'L',
  'ROW_COUNT' => '1',
  'COL_COUNT' => '30',
  'LIST_TYPE' => 'C',
  'MULTIPLE' => 'N',
  'XML_ID' => '',
  'FILE_TYPE' => '',
  'MULTIPLE_CNT' => '5',
  'LINK_IBLOCK_ID' => '0',
  'WITH_DESCRIPTION' => 'N',
  'SEARCHABLE' => 'N',
  'FILTRABLE' => 'N',
  'IS_REQUIRED' => 'N',
  'VERSION' => '1',
  'USER_TYPE' => NULL,
  'USER_TYPE_SETTINGS' => NULL,
  'HINT' => '',
  'VALUES' => 
  array (
    0 => 
    array (
      'VALUE' => 'Y',
      'DEF' => 'N',
      'SORT' => '500',
      'XML_ID' => 'y',
    ),
  ),
));
        $helper->UserOptions()->saveElementForm($iblockId, array (
  'Элемент' => 
  array (
    'ID' => 'ID',
    'ACTIVE' => 'Активность',
    'NAME' => 'Название',
    'XML_ID' => 'Внешний код',
    'IBLOCK_ELEMENT_PROP_VALUE' => 'Значения свойств',
    'PROPERTY_CONTAINER_CLASSIFIER' => 'Классификатор контейнера',
    'PROPERTY_NODE_ATTRIBUTE' => 'Атрибут тега',
    'PROPERTY_PREVIOUS_TAG_ATTR' => 'Атрибут предыдущего тега',
    'PROPERTY_PREVIOUS_TAG_CONTENT' => 'Содержимое атрибута предыдущего тега',
    'PROPERTY_ELEMENT_PROP' => 'Код свойства элемента инфоблока',
    'PROPERTY_ELEMENT_CODE' => 'Поле элемента инфоблока',
    'PROPERTY_TYPE' => 'Тип',
    'edit1_csection1' => 'Изменение значения',
    'PROPERTY_PREFIX' => 'Префикс значения',
    'PROPERTY_POSTFIX' => 'Постфикс значения',
    'PROPERTY_REMOVE_TAGS' => 'Вырезать теги из значения',
  ),
));

    }

    public function down()
    {
        //your code ...
    }
}