<?php

namespace Sprint\Migration;


class PARSE_TEMPLATE_ENCODING_PROP20200210140014 extends Version
{
    protected $description = "Миграция свойства кодировки в шаблоне парсера";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();
        $iblockId = $helper->Iblock()->getIblockIdIfExists('parse_templates', 'parser');
        $helper->Iblock()->saveProperty($iblockId, array (
  'NAME' => 'Кодировка сайта',
  'ACTIVE' => 'Y',
  'SORT' => '500',
  'CODE' => 'ENCODING',
  'DEFAULT_VALUE' => '',
  'PROPERTY_TYPE' => 'L',
  'ROW_COUNT' => '1',
  'COL_COUNT' => '30',
  'LIST_TYPE' => 'L',
  'MULTIPLE' => 'N',
  'XML_ID' => '',
  'FILE_TYPE' => '',
  'MULTIPLE_CNT' => '5',
  'LINK_IBLOCK_ID' => '0',
  'WITH_DESCRIPTION' => 'N',
  'SEARCHABLE' => 'N',
  'FILTRABLE' => 'N',
  'IS_REQUIRED' => 'Y',
  'VERSION' => '2',
  'USER_TYPE' => NULL,
  'USER_TYPE_SETTINGS' => NULL,
  'HINT' => '',
  'VALUES' => 
  array (
    0 => 
    array (
      'VALUE' => 'UTF-8',
      'DEF' => 'Y',
      'SORT' => '500',
      'XML_ID' => 'utf8',
    ),
    1 => 
    array (
      'VALUE' => 'Windows 1251',
      'DEF' => 'N',
      'SORT' => '500',
      'XML_ID' => 'cp1251',
    ),
  ),
));

    }

    public function down()
    {
        //your code ...
    }
}