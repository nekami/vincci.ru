<?php

namespace Sprint\Migration;


class PARSE_ELEMENTS_IBLOCK_V220191223150348 extends Version
{
    protected $description = "измененный инфоблок искомых элементов ";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();
        $iblockId = $helper->Iblock()->getIblockIdIfExists('parse_elements', 'parser');
        $helper->Iblock()->saveProperty($iblockId, array (
  'NAME' => 'Поле элемента инфоблока',
  'ACTIVE' => 'Y',
  'SORT' => '500',
  'CODE' => 'ELEMENT_CODE',
  'DEFAULT_VALUE' => '',
  'PROPERTY_TYPE' => 'L',
  'ROW_COUNT' => '1',
  'COL_COUNT' => '30',
  'LIST_TYPE' => 'L',
  'MULTIPLE' => 'N',
  'XML_ID' => '',
  'FILE_TYPE' => '',
  'MULTIPLE_CNT' => '5',
  'LINK_IBLOCK_ID' => '0',
  'WITH_DESCRIPTION' => 'N',
  'SEARCHABLE' => 'N',
  'FILTRABLE' => 'N',
  'IS_REQUIRED' => 'N',
  'VERSION' => '1',
  'USER_TYPE' => NULL,
  'USER_TYPE_SETTINGS' => NULL,
  'HINT' => '',
  'VALUES' => 
  array (
    0 => 
    array (
      'VALUE' => 'Анонс',
      'DEF' => 'N',
      'SORT' => '500',
      'XML_ID' => 'PREVIEW_TEXT',
    ),
    1 => 
    array (
      'VALUE' => 'Детальное изображение',
      'DEF' => 'N',
      'SORT' => '500',
      'XML_ID' => 'DETAIL_PICTURE',
    ),
    2 => 
    array (
      'VALUE' => 'Детальное описание',
      'DEF' => 'N',
      'SORT' => '500',
      'XML_ID' => 'DETAIL_TEXT',
    ),
    3 => 
    array (
      'VALUE' => 'Изображение для анонса',
      'DEF' => 'N',
      'SORT' => '500',
      'XML_ID' => 'PREVIEW_PICTURE',
    ),
    4 => 
    array (
      'VALUE' => 'Название',
      'DEF' => 'N',
      'SORT' => '500',
      'XML_ID' => 'NAME',
    ),
    5 => 
    array (
      'VALUE' => 'Свойство',
      'DEF' => 'N',
      'SORT' => '500',
      'XML_ID' => 'PROPERTY',
    ),
    6 => 
    array (
      'VALUE' => 'Символьный код',
      'DEF' => 'N',
      'SORT' => '500',
      'XML_ID' => 'CODE',
    ),
  ),
));
        $helper->Iblock()->saveProperty($iblockId, array (
  'NAME' => 'Код свойства элемента инфоблока',
  'ACTIVE' => 'Y',
  'SORT' => '500',
  'CODE' => 'ELEMENT_PROP',
  'DEFAULT_VALUE' => '',
  'PROPERTY_TYPE' => 'S',
  'ROW_COUNT' => '1',
  'COL_COUNT' => '30',
  'LIST_TYPE' => 'L',
  'MULTIPLE' => 'N',
  'XML_ID' => '',
  'FILE_TYPE' => '',
  'MULTIPLE_CNT' => '5',
  'LINK_IBLOCK_ID' => '0',
  'WITH_DESCRIPTION' => 'N',
  'SEARCHABLE' => 'N',
  'FILTRABLE' => 'N',
  'IS_REQUIRED' => 'Y',
  'VERSION' => '1',
  'USER_TYPE' => NULL,
  'USER_TYPE_SETTINGS' => NULL,
  'HINT' => '',
));

    }

    public function down()
    {
        //your code ...
    }
}