<?
class CCaptchaCheck{

    function checkCaptcha() { //функция проверки рекапчи 3.0 на валидность

        $context = \Bitrix\Main\Application::getInstance()->getContext();

        $request = $context->getRequest();  //получаем параметры запроса

        $sidVar = isset($_REQUEST['captcha_sid']) ? "captcha_sid" : "CAPTCHA_SID";
        $wordVar = isset($_REQUEST['captcha_sid']) ? "captcha_word" : "CAPTCHA_WORD";

        $captchaResponse = $request->getPost("g_recaptcha_response");   //ответ от рекапчи
        $captchaSid = $request->getPost($sidVar);                 //и идентификатор битриксовой капчи
        if($captchaResponse && $captchaSid)    //проверяем ответ рекапчи на существование и наличие параметра битриксовой капчи
        {
            $request = array(   //собираем параметры запроса к гуглу для получения результата проверки
                "secret" => "6LdRfr8UAAAAAGBomL2-gT6xoC6puGqMpnK13MHR",
                "response" => $captchaResponse
            );
            $result = [];
            if( $curl = curl_init() ) { //отправляем curl запрос, получаем ответ
                curl_setopt($curl, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($request));
                $result = json_decode(curl_exec($curl));
                curl_close($curl);
            }
            if($result->success)    //если в результате параметр success = true
            {

                //Т.к. нет API, позволяющего получить слово капчи по ID, делаем запрос напрямую к БД, получаем ключевое слово битриксовой капчи

                $dbRes = \Bitrix\Main\Application::getConnection()->query(

                    "SELECT CODE FROM b_captcha WHERE id='".$captchaSid."'");

                if($res = $dbRes->fetch())
                {

                    if($res['CODE'])    //при его наличии подставляем в параметры запроса полученное слово в капче
                    {
                        $_REQUEST[$wordVar] = $res['CODE'];
                        $_POST[$wordVar] = $res['CODE'];
                    }

                }

            }

        }

    }

}
?>