<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

function s($var, $userID = 0)
{
    global $USER;
    
    if (($userID > 0 && $USER->GetID() == $userID) || $userID == 0) {
        if ($var === false) {
            $var = "false";
        }
        echo "<xmp>";
        print_r($var);
        echo "</xmp>";
    }
}

function p($var, $url = false, $delimiter = false)
{
    if (empty($url)) {
        $url = $_SERVER["DOCUMENT_ROOT"] . "/logs/log.txt";
    }

    if (!empty($delimiter)) {
        $delimiter = "\n\n--- " . $delimiter . " ---\n\n";
    } else {
        $delimiter = "\n\n-------------------------------------------------\n\n";
    }
    file_put_contents($url, $delimiter . print_r($var, true), FILE_APPEND);
}

/**
 * Функция для генерации image sitemap через агента для элементов инфоблока с ID = $iblockId
 *
 * @param int $iblockId - id инфоблока
 * @return string
 */
function createImageSitemap($iblockId)
{
    $elementsData = CIBlockElement::GetList(
        ["SORT" => "ASC"],
        ["ACTIVE" => "Y", "IBLOCK_ID" => $iblockId],
        false, false,
        ["DETAIL_PICTURE", "DETAIL_PAGE_URL", "NAME"]
    );
    $sitemapXML = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" />");
    $counter = 0;
    $fileIds = [];
    $http = "http";
    if ($_SERVER['HTTPS']) {
        $http = "https";
    }
    $host = "{$http}://{$_SERVER['HTTP_HOST']}";
    while ($element = $elementsData->GetNext()) {
        $sitemapXML->url[$counter]->loc = $host . $element['DETAIL_PAGE_URL'];
        $sitemapXML->url[$counter]->{"image:image"}->{"image:loc"} = $element['DETAIL_PICTURE'];
        $sitemapXML->url[$counter]->{"image:image"}->{"image:title"} = $element['NAME'];
        $fileIds[$counter] = $element['DETAIL_PICTURE'];
        $counter++;
    }
    $filesData = CFile::GetList(
        ["SORT" => "ASC"],
        ["ID" => $fileIds]
    );
    $fileIds = array_flip($fileIds);
    $fileDir = $host . "/" . COption::GetOptionString("main", "upload_dir", "upload") . "/";
    while ($file = $filesData->Fetch()) {
        $sitemapXML->url[$fileIds[$file['ID']]]->{"image:image"}->{"image:loc"} = $fileDir . $file['SUBDIR'] . "/" . $file['FILE_NAME'];
    }
    $file = fopen($_SERVER['DOCUMENT_ROOT'] . "/imageSitemap.xml", "w");
    fwrite($file, $sitemapXML->asXML());
    fclose($file);

    return "createImageSitemap($iblockId);";
}

/*Отключение ядра kernel_main*/

function deleteKernelJs(&$content) {
   global $USER, $APPLICATION;
   if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
   if($APPLICATION->GetProperty("save_kernel") == "Y") return;

   $arPatternsToRemove = Array(
      '/<script.+?src=".+?kernel_main\/kernel_main\.js\?\d+"><\/script\>/',
      '/<script.+?src=".+?bitrix\/js\/main\/core\/core[^"]+"><\/script\>/',
      '/<script.+?>BX\.(setCSSList|setJSList)\(\[.+?\]\).*?<\/script>/',
      '/<script.+?>if\(\!window\.BX\)window\.BX.+?<\/script>/',
      '/<script[^>]+?>\(window\.BX\|\|top\.BX\)\.message[^<]+<\/script>/',
   );

   $content = preg_replace($arPatternsToRemove, "", $content);
   $content = preg_replace("/\n{2,}/", "\n\n", $content);
}

function deleteKernelCss(&$content) {
   global $USER, $APPLICATION;
    
   if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
   if($APPLICATION->GetProperty("save_kernel") == "Y") return;
 
   $arPatternsToRemove = Array(
      '/<link.+?href=".+?kernel_main\/kernel_main\.css\?\d+"[^>]+>/',
   );
 
   $content = preg_replace($arPatternsToRemove, "", $content);
   $content = preg_replace("/\n{2,}/", "\n\n", $content);
}
  
function includeCssInline(&$content) {
   global $USER, $APPLICATION;
    
   if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
   if($APPLICATION->GetProperty("save_kernel") == "Y") return;
 
   preg_match('/<link.+?href="(\/bitrix\/cache\/css\/'.SITE_ID.'\/'.SITE_TEMPLATE_ID.'\/template_[^"]+)"[^>]+>/', $content, $arMatches);
   $sFilePath = "http://".$_SERVER["HTTP_HOST"].$arMatches[1];
    
   $obCache = new CPHPCache;
   $life_time = 0*60;
    
   if($obCache->InitCache($life_time, $sFilePath, "/")) {
      $vars         = $obCache->GetVars();
      $sIncludeCss      = $vars["sIncludeCss"];
      $sIncludeCssClear   = $vars["sIncludeCssClear"];
       
   } else {
      $sIncludeCss      = file_get_contents($sFilePath);
      $sIncludeCssClear   = compressCSS($sIncludeCss);
       
   }
    
   if(false) {
      ?><pre style="font-size:10px;line-height: 8px;">До: <?=strlen($sIncludeCss);?></pre><?
      ?><pre style="font-size:10px;line-height: 8px;">После: <?=strlen($sIncludeCssClear);?></pre><?
   }
    
   $content = str_replace($arMatches[0], "<style>$sIncludeCssClear</style>", $content);
    
   if($obCache->StartDataCache()) {
      $obCache->EndDataCache(array(
         "sIncludeCss" => $sIncludeCss,
         "sIncludeCssClear" => $sIncludeCssClear,
      ));
   }
}
 
function compressCSS($css, $arOptions = Array()) {
   $sResult = $css;
    
   $sResult = preg_replace("/\/\*[^*]+\*\//", "", $sResult);      // comments
   $sResult = preg_replace("/\/\**\*\//", "", $sResult);         // comments
   $sResult = preg_replace("/\s*(:|,|;|{|}|\t)\s*/", "$1", $sResult);   // whitespaces
   $sResult = preg_replace("/(\t+|\s{2,})/", " ", $sResult);      // tabs and double whitespace
   $sResult = preg_replace("/(\s|:)([\-]{0,1}0px)\s/", " 0 ", $sResult);   // zeros
   //$sResult = preg_replace("/#(\w){6};/", "#$1$1$1;", $sResult);      // #dddddd => #ddd
 
   return $sResult;
}

/*===========================*/
?>