<? /** @noinspection PhpUndefinedConstantInspection */
require_once "phpQuery/phpQuery.php";

class CatalogParser{
    private $elementId;
    private $needleParameters = array();         //   массив заполняемых элементов инфоблока
    private $searchPathTemplate;     //   шаблон ссылки на страницу поиска с
    private $searchLinkContainerId;  //   ид/класс/тег, в котором находятся ссылки на теги <a> / класс тега <a>
    private $iblockId;
    private $templateId;
    private $searchProperty;
    private $elementUrl;
    private $parsedData = [];
    private $partnerXmlId;
    private $partnerDomain;
    private $elementParseDate;
    private $siteEncoding;
    private $removeScripts;
    const PARSE_INTERVAL_POINT = 86400;

    private $hasError = false;

    function __construct($elementId){
        if(CModule::IncludeModule("iblock")){
            $elem = CIBlockElement::GetList(
                array("SORT" => "ASC"),
                array("ID" => $elementId),
                false, false,
                array("ID", "NAME", "PROPERTY_LINK_PARSER", "PROPERTY_BRAND", "PROPERTY_PARSE_DATE", "PROPERTY_NO_PARSE")
            );
            if($elem = $elem->Fetch()){
                if($elem['PROPERTY_NO_PARSE_VALUE']!="Y"){
                    $this->elementId = $elementId;
                    $this->iblockId = $elem['IBLOCK_ID'];
                    $this->partnerXmlId = $elem['PROPERTY_BRAND_VALUE'];
                    $this->elementUrl = $elem['PROPERTY_LINK_PARSER_VALUE'];
                    $this->elementParseDate = $elem['PROPERTY_PARSE_DATE_VALUE'];
                    if(!$this->setTemplate())
                        $this->hasError=true;
                    if(!$this->setUrl())
                        $this->hasError=true;
                }
                else{
                    $this->hasError=true;
                }
            }
            else{
                $this->hasError = true;
            }
        }
        else{
            $this->hasError = true;
        }
    }

    private function setUrl(){
        if(!$this->hasError){
            if(empty($this->elementUrl)){
                $searchParam = CIBlockElement::GetList(
                    array("SORT" => "ASC"),
                    array("ID" => $this->elementId),
                    false, false,
                    array("ID", $this->searchProperty)
                );
                if($param = $searchParam->Fetch()){
                    $parName = substr($this->searchProperty, 0, 9) == "PROPERTY_" ? $this->searchProperty."_VALUE" : $this->searchProperty;
                    $search = $param[$parName];
                }
                else{
                    return false;
                }
                $searchUrl = str_replace("#SEARCH#", $search, $this->searchPathTemplate);
                $domain = parse_url($searchUrl, PHP_URL_HOST);
                $http = substr($searchUrl, 0, 4) == "http" ? substr($searchUrl, 0, strpos($searchUrl, "/")+2) : "https://";

                $this->partnerDomain = $http . $domain;

                $pq = phpQuery::newDocument(file_get_contents($searchUrl));
                $index = $this->getIndexFromSelector($this->searchLinkContainerId);
                $container = $pq->find($this->searchLinkContainerId);

                if($container->elements[0]->tagName != "a"){
                    $links = $container->find("a");
                    if($index>-1){
                        $links = pq($links->elements[$index]);
                    }
                    $elemUrl = $links->attr("href");
                    if($elemUrl[0]=="/"){
                        $elemUrl = $this->partnerDomain . $elemUrl;
                    }
                }
                else{
                    if($index>-1){
                        $container = pq($container->elements[$index]);
                    }
                    $elemUrl = $container->attr("href");
                    if(!preg_match("#^{$this->partnerDomain}#")){
                        if($elemUrl[0]=="/"){
                            $elemUrl = $this->partnerDomain . $elemUrl;
                        }
                        else{
                            $elemUrl = $this->partnerDomain ."/". $elemUrl;
                        }
                    }
                }
                $this->elementUrl = $elemUrl;
            }
            else{
                $domain = parse_url($this->searchPathTemplate, PHP_URL_HOST);
                $http = substr($this->searchPathTemplate, 0, 4) == "http" ? substr($this->searchPathTemplate, 0, strpos($this->searchPathTemplate, "/")+2) : "https://";
                $this->partnerDomain = $http . $domain;
            }
            //var_dump("<pre>", $this->elementUrl);
            //die;

            return true;
        }
        return false;
    }

    private function setTemplate(){
        if(!$this->hasError){
            $brandData = CIBlockElement::GetList(
                array("SORT" => "ASC"),
                array("IBLOCK_ID" => PARSER_BRANDS_IBLOCK, "XML_ID" => $this->partnerXmlId, "ACTIVE" => "Y"),
                false, false,
                array("ID", "PROPERTY_PARSER_TEMPLATE")
            );
            if($brand = $brandData->Fetch())
                $this->templateId = $brand["PROPERTY_PARSER_TEMPLATE_VALUE"];

            $templateData = CIBlockElement::GetList(
                array("SORT" => "ASC"),
                array(
                    "IBLOCK_ID" => PARSER_TEMPLATES_IBLOCK,
                    "ID" => $this->templateId,
                    "ACTIVE" => "Y"
                ),
                false, false,
                array(
                    "ID",
                    "PROPERTY_SITE_SEARCH_URL_TEMPLATE",
                    "PROPERTY_SEARCH_CONTAINER_CLASSIFIER",
                    "PROPERTY_SEARCH_PROP",
                    "PROPERTY_NEEDLE_ELEMENTS",
                    "PROPERTY_PARSE_INTERVAL",
                    "PROPERTY_ENCODING",
                    "PROPERTY_CLEAR_SCRIPTS",
                )
            );
            if($template = $templateData->Fetch()){
                $parseDate = MakeTimeStamp($this->elementParseDate);
                if(time() > $parseDate+intval($template['PROPERTY_PARSE_INTERVAL_VALUE'])*self::PARSE_INTERVAL_POINT){
                    $this->searchPathTemplate = $template['PROPERTY_SITE_SEARCH_URL_TEMPLATE_VALUE'];
                    $this->searchLinkContainerId = $template['PROPERTY_SEARCH_CONTAINER_CLASSIFIER_VALUE'];
                    $this->searchProperty = $template['PROPERTY_SEARCH_PROP_VALUE'];
                    $propertyEncodingData = CIBlockPropertyEnum::GetList(
                        Array("SORT"=>"ASC", "VALUE"=>"ASC"),
                        Array("IBLOCK_ID" => PARSER_TEMPLATES_IBLOCK, "CODE" => "ENCODING")
                    );
                    while($encoding = $propertyEncodingData->Fetch()){
                        if($encoding['ID'] == $template['PROPERTY_ENCODING_ENUM_ID'])
                            $this->siteEncoding = $encoding['XML_ID'];
                    }
                    $this->removeScripts = $template['PROPERTY_CLEAR_SCRIPTS_VALUE']=="Y";
                    $this->setNeedleParameters($template['PROPERTY_NEEDLE_ELEMENTS_VALUE']);
                }
                else return false;
            }
            else{
                return false;
            }
            return true;
        }
        return false;
    }

    private function setNeedleParameters($ids){
        $iblockParams = [];
        $propertyParamData = CIBlockPropertyEnum::GetList(
            Array("SORT"=>"ASC", "VALUE"=>"ASC"),
            Array("IBLOCK_ID" => PARSER_PARAMETERS_IBLOCK, "CODE" => "ELEMENT_CODE")
        );
        while($param = $propertyParamData->Fetch()){
            $iblockParams[$param['ID']] = $param['XML_ID'];
        }
        $paramsData = CIBlockElement::GetList(
            array("SORT" => "ASC"),
            array(
                "IBLOCK_ID" => PARSER_PARAMETERS_IBLOCK,
                "ID" => $ids,
                "ACTIVE" => "Y"
            ),
            false, false,
            array(
                "ID",
                "PROPERTY_CONTAINER_CLASSIFIER",
                "PROPERTY_NODE_ATTRIBUTE",
                "PROPERTY_ELEMENT_CODE",
                "PROPERTY_TYPE",
                "PROPERTY_ELEMENT_PROP",
                "PROPERTY_PREVIOUS_TAG_CONTENT",
                "PROPERTY_PREVIOUS_TAG_ATTR",
                "PROPERTY_PREFIX",
                "PROPERTY_POSTFIX",
                "PROPERTY_REMOVE_TAGS",
            )
        );
        while($param = $paramsData->Fetch()){
            $param['PROPERTY_ELEMENT_CODE_VALUE'] = $iblockParams[$param['PROPERTY_ELEMENT_CODE_ENUM_ID']];
            $label = $param['PROPERTY_ELEMENT_CODE_VALUE']!="PROPERTY" ? $param['PROPERTY_ELEMENT_CODE_VALUE'] : array("PROPERTY" => $param['PROPERTY_ELEMENT_PROP_VALUE']);
            $container = trim($param['PROPERTY_CONTAINER_CLASSIFIER_VALUE']);
            $index = $this->getIndexFromSelector($container);

            $this->needleParameters[] = array(
                "container"         => $container,
                "containerIndex"    => $index,
                "nodeAttribute"     => trim($param['PROPERTY_NODE_ATTRIBUTE_VALUE']),
                "iblockLabel"       => $label,
                "type"              => trim($param['PROPERTY_TYPE_ENUM_ID']),
                "previousAttr"      => trim($param['PROPERTY_PREVIOUS_TAG_ATTR_VALUE']),
                "previousContent"   => trim($param['PROPERTY_PREVIOUS_TAG_CONTENT_VALUE']),
                "prefix"            => trim($param['PROPERTY_PREFIX_VALUE']),
                "postfix"           => trim($param['PROPERTY_POSTFIX_VALUE']),
                "removeTags"           => trim($param['PROPERTY_REMOVE_TAGS_VALUE'])=="Y",
            );
        }
        $types = [];
        $propertyTypeData = CIBlockPropertyEnum::GetList(
            Array("SORT"=>"ASC", "VALUE"=>"ASC"),
            Array("IBLOCK_ID" => PARSER_PARAMETERS_IBLOCK, "CODE" => "TYPE")
        );
        while($type = $propertyTypeData->Fetch()){
            $types[$type['ID']] = $type['XML_ID'];
        }
        foreach($this->needleParameters as &$param){
            $param['type'] = $types[$param['type']];
        }
    }

    public function parse(){
        if(!$this->hasError){
            $finallyData = array();
            $html = phpQuery::newDocument($this->getContent());
            $pq = pq($html);
            foreach ($this->needleParameters as $data){
                $container = $pq->find($data['container']);
                //var_dump($container->count(), $data['container']); echo "<br>";
                if($container->count() != 0){
                    $elem = $container;
                    if($data['previousAttr']!="" || $data['previousContent']!=""){
                        $needleElement = 0;
                        $elementFinded = false;     //обозначаем что нужный элемент еще не найден чтобы остановить присваивание в цикле после его нахождения
                        if($data['previousAttr']==""){
                            $elem->each(function($el) use ($data, &$needleElement, &$elementFinded){
                                $data['previousContent'] = trim($data['previousContent']);
                                if(substr($this->trimString(mb_convert_encoding(pq($el)->prev()->html(), "utf8", $this->siteEncoding)),0, strlen($data['previousContent'])) == $data['previousContent'] && !$elementFinded){
                                    $elementFinded = true;
                                    $needleElement = pq($el);
                                }
                            });
                        }
                        else{
                            if(!isset($data['previousContent'])) $data['previousContent'] = "";
                            $elem->each(function($el) use ($data, &$needleElement, &$elementFinded){
                                $data['previousContent'] = trim($data['previousContent']);
                                if(substr($this->trimString(mb_convert_encoding(pq($el)->prev()->attr($data['previousAttr']), "utf8", $this->siteEncoding)),0, strlen($data['previousContent'])) == $data['previousContent'] && !$elementFinded){
                                    $elementFinded = true;
                                    $needleElement = pq($el);
                                }
                            });
                        }
                        if($needleElement!=0)
                            $elem = $needleElement;
                        else{
                            continue;
                        }
                    }
                    if ($data['containerIndex'] >= 0 && $elem->count()-1 >= $data['containerIndex']){
                        $elem = pq($elem->elements[$data['containerIndex']]);
                    }


                    $value = array(
                        "value" => !empty($data['nodeAttribute'])
                            ? mb_convert_encoding($this->trimString($elem->attr($data['nodeAttribute'])), "utf8", $this->siteEncoding)  //получаем содержимое указанного атрибута тега, преобразуем в utf8
                            : str_replace(["</body>", "</html>"], null, mb_convert_encoding($this->trimString($elem->html()), "utf8", $this->siteEncoding)),  //получаем содержимое тега, преобразуем в utf8
                        "type" => $data['type']
                    );
                    if($data['removeTags']){
                        $value = preg_replace("/<(\/)?[^>]>/", null, $value);       //Удаляем теги из полученного значения
                    }
                    if(!empty($data['prefix'])) $value['value'] = $data['prefix']." ".$value['value'];  //добавляем префикс и постфикс к значению
                    if(!empty($data['postfix'])) $value['value'] = $value['value']." ".$data['postfix'];

                    if(is_array($data['iblockLabel'])){     //действия со свойствами эелемента инфоблока
                        $finallyData['PROPERTIES'][$data['iblockLabel']['PROPERTY']] = $value;
                    }
                    else{       //действия со стандартными полями элемента инфоблока
                        $finallyData[$data['iblockLabel']] = $value;
                    }
                }
            }
            $this->parsedData = $finallyData;
            return $finallyData;
        }
        else{
            $this->hasError = true;
            return false;
        }
    }

    public function save(){
        if(!empty($this->parsedData) || !$this->hasError){
            $updateArray = [];
            $properties = [];
            $removeFiles = [];
            foreach ($this->parsedData as $name => $elem){
                if($name == "PROPERTIES"){
                    foreach ($elem as $key => $prop){
                        switch($prop['type']){
                            case "option":
                                $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$this->iblockId, "CODE"=>$key, "VALUE" => $prop['value']));
                                while($enum_field = $property_enums->GetNext())
                                    CIBlockElement::SetPropertyValuesEx($this->elementId, $this->iblockId, array($key => $enum_field["ID"]));
                                break;

                            case "string":
                                CIBlockElement::SetPropertyValuesEx($this->elementId, $this->iblockId, array($key => $prop['value']));
                                break;

                            case "file":
                                unset($properties);
                                if($prop['value'][0]=="/") $prop['value'] = $this->partnerDomain . $prop['value'];
                                $property = $this->uploadFile($prop['value']);
                                $file = array('VALUE' => $property, 'DESCRIPTION' => $property['name']);
                                CIBlockElement::SetPropertyValuesEx($this->elementId, $this->iblockId, $file);
                                unlink($file['VALUE']['tmp_name']);
                                break;

                            case "text":
                                unset($properties);
                                $properties[$key] = array("VALUE" => array("TYPE" =>"TEXT", "TEXT" => $prop['value']));
                                CIBlockElement::SetPropertyValuesEx($this->elementId, $this->iblockId, $properties);
                                break;

                            case "html":
                                unset($properties);
                                $properties[$key] = array("VALUE" => array("TYPE" =>"HTML", "TEXT" => $prop['value']));
                                CIBlockElement::SetPropertyValuesEx($this->elementId, $this->iblockId, $properties);
                                break;
                        }

                    }
                }
                else{
                    switch($elem['type']){
                        case "string":
                        case "text":
                        case "html":
                            $updateArray[$name] = $elem['value'];
                            break;

                        case "file":
                            if($elem['value'][0]=="/") $elem['value'] = $this->partnerDomain . $elem['value'];
                            $updateArray[$name] = $this->uploadFile($elem['value']);
                            $removeFiles[] = $updateArray[$name]['tmp_name'];
                            break;
                    }
                }
            }
            CIBlockElement::SetPropertyValuesEx($this->elementId, $this->iblockId, array("PARSE_DATE" => ConvertTimeStamp(time())));

            $el = new CIBlockElement;
            $result = $el->Update($this->elementId, $updateArray);
            foreach ($removeFiles as $file){
                unlink($file);
            }
            return $result;
        }
        else {
            $this->hasError = true;
            return false;
        }
    }

    private function uploadFile($url){
        $filename = end(explode("/", $url));
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/upload/catalog/';
        if(!file_exists($dir)){
            mkdir($dir);
        }
        $dir .= $this->partnerXmlId;
        if(!file_exists($dir)){
            mkdir($dir);
        }
        $path = $dir . "/" . $filename;
        file_put_contents($path, file_get_contents($url));
        $arFile = CFile::MakeFileArray($path);
        $arFile['MODULE_ID'] = "iblock";
        return $arFile;
    }

    public static function checkBrandTemplate($brandXmlId){
        $brandData = CIBlockElement::GetList(
            array("SORT" => "ASC"),
            array("IBLOCK_ID" => PARSER_BRANDS_IBLOCK, "XML_ID" => $brandXmlId, "ACTIVE" => "Y"),
            false, false,
            array("ID", "PROPERTY_PARSER_TEMPLATE")
        );
        if($brand = $brandData->Fetch()){
            return !empty($brand["PROPERTY_PARSER_TEMPLATE_VALUE"]);
        }
        else{
            return false;
        }
    }
    private function trimString($string){
        return str_replace(["\r","\n"], [NULL, NULL], trim($string));
    }

    private function getContent(){
        $ch = curl_init($this->elementUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "User-Agent: ".$_SERVER["HTTP_USER_AGENT"],
        ));
        $html = curl_exec($ch);
        curl_close($ch);
        if($this->removeScripts){
            $html = preg_replace("/<script[^>]*?>(.*?\s?)+<\/script>/m", null, $html); //удаляем из кода подключение скриптов для корректного парсинга
        }

        return $html;
    }

    private function getIndexFromSelector(&$selector){
        $matches = [];
        preg_match("/\[(?<index>[0-9]+)\]/", $selector, $matches);    //получаем индекс элемента, указанный в строке классификатора в квадратных скобках
        $index = isset($matches['index']) ? intval($matches['index']) : -1;     //если индекс указан, устанавливаем его значение, иначе ставим -1 для получения данных из всех блоков указанного классификатора
        $selector = trim(preg_replace("/\[[0-9]+\]/", null, $selector));    //удаляем индекс элемента у классификатора
        return $index;
    }
}
?>