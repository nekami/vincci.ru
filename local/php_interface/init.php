<?
use Bitrix\Main\Event;
use Bitrix\Main\EventManager;
use Bitrix\Main\EventResult;
use Bitrix\Main\Loader;
use Bitrix\Sale\ResultError;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

define('googleBot', (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Lighthouse') !== false)); //Отключение метрики для бота

/*Отключение ненужных js и css для неавторизованных*/

if (googleBot) {
	AddEventHandler("main", "OnEndBufferContent", "deleteKernelJs");
	AddEventHandler("main", "OnEndBufferContent", "deleteKernelCss");
	AddEventHandler("main", "OnEndBufferContent", "includeCssInline");
}
/*=============*/


define("RECAPTCHA_SITE_KEY", "6LdRfr8UAAAAAGm79Vgh9ra1NqjFgMI8_Pn2pZkJ");
define("CACHE_TTL", 3600 * 24 * 30);
/* Объявление констант инфоблоков парсера */
define("PARSER_BRANDS_IBLOCK", 22);
define("PARSER_TEMPLATES_IBLOCK", 39);
define("PARSER_PARAMETERS_IBLOCK", 40);
define("PARSER_CATALOG_IBLOCK", 24);

require_once "include/functions.php"; // служебные функции
require_once "include/check_recaptcha.php"; // работа с reCaptcha 3.0
/* Подключение класса парсера */
require_once "include/catalogparser.php";

AddEventHandler("main", "OnPageStart", Array("CCaptchaCheck", "checkCaptcha"));


AddEventHandler('main', 'OnBeforeProlog', 'OnBeforePrologHandler');
AddEventHandler('main', 'OnEndBufferContent', 'OnEndBufferContentHandler');
AddEventHandler('main', 'OnBeforeEventAdd', 'OnBeforeEventAddHandler');

AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "OnAfterIBlockElementUpdateHandler", 1000);
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementAddHandler", 1000);

AddEventHandler("catalog", "OnPriceAdd", "OnPriceAddHandler", 1000);
AddEventHandler("catalog", "OnPriceUpdate", "OnPriceUpdateHandler", 1000);
AddEventHandler('main', 'OnEpilog', 'OnPageStartHandler');

EventManager::getInstance()->addEventHandler('sale', 'OnSaleOrderBeforeSaved', 'OnSaleOrderBeforeSavedHandler');

AddEventHandler("iblock", "OnAfterIBlockElementAdd", "ParseBrandSiteOnAddProduct"); //добавление товара
AddEventHandler("esol.importexportexcel", "OnEndImport", "ParseBrandSiteOnImport"); //импорт элементов


//функция на добавление
function ParseBrandSiteOnAddProduct($arFields){
    if($arFields['IBLOCK_ID'] == PARSER_CATALOG_IBLOCK){
        $db_props = CIBlockElement::GetList(
            array("SORT" => "ASC"),
            array("IBLOCK_ID" => $arFields['IBLOCK_ID'], "ID" => $arFields['ID']),
            false, false,
            array("ID", "PROPERTY_BRAND")
        );
        if($ar_props = $db_props->Fetch()){
            if(CatalogParser::checkBrandTemplate($ar_props['PROPERTY_BRAND_VALUE'])){
                $parser = new CatalogParser($arFields['ID']);
                $parser->parse();
                $parser->save();
            }
        }
    }
}

//функция на изменение
function ParseBrandSiteOnImport($af1, $af2){
    $db_props = CIBlockElement::GetList(
        array("SORT" => "ASC"),
        array(
            "IBLOCK_ID" => PARSER_CATALOG_IBLOCK,
            "DATE_MODIFY_FROM" => $af2['IMPORT_START_DATETIME'],
            "DATE_MODIFY_TO" => $af2['IMPORT_FINISH_DATETIME'],
        ),
        false, false,
        array(
            "ID",
            "PROPERTY_BRAND",
            "PROPERTY_PARSE_DATE"
        )
    );
    while($ar_props = $db_props->Fetch()){
        if(CatalogParser::checkBrandTemplate($ar_props['PROPERTY_BRAND_VALUE'])){
            $parser = new CatalogParser($ar_props['ID']);
            $parser->parse();
            $parser->save();
        }
    }
}


function OnSaleOrderBeforeSavedHandler(Event $event)
{
	global $USER;
	$order = $event->getParameter("ENTITY");
	$basket = $order->getBasket();

	if(!count($basket) && !$USER->IsAdmin())
	{
		return new EventResult(EventResult::ERROR, new ResultError("При оформлении заказа произошла ошибка. Ваша корзина пуста."), 'sale');
	}
}

function OnPriceAddHandler($id, $arFields)
{
	UpdateXmlPrice($id);
}

function OnPageStartHandler()
{
    global $APPLICATION;//, $_GET;
    if(!empty($_GET['CITY_ID'])){// && empty($_REQUEST['PHPSESSID'])){
        $removeParams = array(
            "CITY_ID"
        );
        $url = $APPLICATION->GetCurPageParam(
            "",
            $removeParams
        );
        LocalRedirect($url, false, "301 Moved permanently");
    }
}

function OnPriceUpdateHandler($id, $arFields)
{
	UpdateXmlPrice($id);
}

function UpdateXmlPrice($id, $iblockId = false)
{
	$xmlPrice = GetXmlPrice($id);
	
	if($id && $xmlPrice)
	{
		CIBlockElement::SetPropertyValuesEx($id, $iblockId, array('XML_PRICE' => $xmlPrice));
	}
}

function GetXmlPrice($productId)
{
	$xmlPrice = false;
	
	if(CModule::IncludeModule('iblock') && $productId)
	{
		$arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_MINIMUM_PRICE_1", "PROPERTY_OPT_PRICE");
		$arFilter = Array("IBLOCK_TYPE" => "catalog", "IBLOCK_CODE" => "enext_catalog_s1", "ID" => $productId);
		$rsProduct = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($arProduct = $rsProduct->Fetch())
		{
			if($arProduct["PROPERTY_MINIMUM_PRICE_1_VALUE"])
			{
				$xmlPrice = $arProduct["PROPERTY_MINIMUM_PRICE_1_VALUE"];
			}
			
			if($arProduct["PROPERTY_OPT_PRICE_VALUE"])
			{
				if($arProduct["PROPERTY_OPT_PRICE_VALUE"] < $arProduct["PROPERTY_MINIMUM_PRICE_1_VALUE"])
				{
					$xmlPrice = $arProduct["PROPERTY_OPT_PRICE_VALUE"];
				}
			}
		}
	}
	
	return $xmlPrice;
}

function OnAfterIBlockElementUpdateHandler(&$arFields)
{
	GenerateArticle($arFields['ID']);
	UpdateXmlPrice($arFields['ID'], $arFields['IBLOCK_ID']);
}

function OnAfterIBlockElementAddHandler(&$arFields)
{
	GenerateArticle($arFields['ID']);
	UpdateXmlPrice($arFields['ID'], $arFields['IBLOCK_ID']);
}

function GenerateArticle($productId)
{
	if(CModule::IncludeModule('iblock'))
	{
		$arSelect = Array("ID", "IBLOCK_ID");
		$arFilter = Array("IBLOCK_TYPE" => "catalog", "IBLOCK_CODE" => "enext_catalog_s1", "ID" => $productId);
		$rsProduct = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

		if($arProduct = $rsProduct->GetNext())
		{
			$nullsCount = 6 - strlen((string)$arProduct['ID']);
			CIBlockElement::SetPropertyValuesEx($arProduct['ID'], $arProduct['IBLOCK_ID'], array('ARTNUMBER_V' => 'V-1'.str_repeat("0", $nullsCount).$arProduct['ID']));
			CIBlockElement::UpdateSearch($arProduct['ID'], $bOverWrite = true);
		}
	}
}

function OnBeforeEventAddHandler($event, $lid, &$arFields)
{
	if($event == "SALE_NEW_ORDER")
	{
		$arFields['PHONE'] = GetOrderPropertyValue($arFields['ORDER_REAL_ID'], 'PHONE');
		
		$arCity = GetSelectedCityArray();
		$arFields['CITY'] = $arCity['NAME'];
		
		$dbRes = \Bitrix\Sale\Order::getList(Array('filter' => Array('ID' => $arFields['ORDER_REAL_ID']), 'select' => Array("USER_DESCRIPTION")));
		if($arOrder = $dbRes->fetch())
		{
			$arFields['COMMENT'] = $arOrder['USER_DESCRIPTION'];
		}
		
		FormatOrderList($arFields['ORDER_LIST']);

		// Отправить письмо на региональный адрес
		$arMailProps = $arFields;
		$arMailProps['ORDER_LIST'] = GetOrderList($arFields['ORDER_REAL_ID']);
		$arMailProps['OFFICE_EMAIL'] = GetSlectedCityOfficeEmail();
		
		Bitrix\Main\Mail\Event::send(array(
			'EVENT_NAME' => 'SALE_NEW_ORDER_FOR_OFIICE',
			'LID' => $lid,
			'C_FIELDS' => $arMailProps
		));
	}
}

function FormatOrderList(&$orderList)
{
	$arOrderList = explode('<br/>', $orderList);
	
	$k = 0;
	foreach($arOrderList as &$item)
	{
		if(strlen(trim($item)) > 0)
			$item = ++$k.'. '.$item;
		else
			unset($item);
	}
	
	$orderList = implode('<br/>', $arOrderList);
}

function GetOrderList($orderId)
{
	$orderList = '';
	
	if(Loader::includeModule("sale") && Loader::includeModule("catalog"))
	{
		$basket = Bitrix\Sale\Order::load($orderId)->getBasket();
		$i = 0;
		
		foreach($basket as $basketItem)
		{
			$productId = $basketItem->getProductId();
			$arProduct = CCatalogProduct::GetByIDEx($productId);
			
			if($arProduct['IBLOCK_CODE'] == 'enext_offers_s1')
			{
				$mxResult = CCatalogSku::GetProductInfo($productId);
				if(is_array($mxResult))
				{
					$arProduct = CCatalogProduct::GetByIDEx($mxResult['ID']);
				}
			}
			
			$arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($productId);

			$orderList .= ++$i.'. <a href="/'.$arProduct['CODE'].'/">'.$basketItem->getField('NAME').'</a> арт. '.$arProduct['PROPERTIES']['ARTNUMBER_V']['VALUE'].' - '.$basketItem->getQuantity().' '.$arMeasure[$productId]['MEASURE']['SYMBOL_RUS'].' x '.CurrencyFormat($basketItem->getPrice(), $basketItem->getCurrency()).'<br />';
		}
	}
	
	return $orderList;
}

function GetSlectedCityOfficeEmail()
{
	return GetSlectedCityOfficeData("PROPERTY_EMAIL_EMAIL");
}

function GetSlectedCityOfficeId()
{
	return GetSlectedCityOfficeData("ID");
}

function GetSlectedCityOfficeData($dataType)
{
	$data = 0;
	
	if(CModule::IncludeModule('iblock'))
	{
		$selectedCityId = GetSelectedCityId();
		
		$obCache = new CPHPCache();

		if($obCache->InitCache(CACHE_TTL, $selectedCityId.$dataType, '/OFFICE'))
		{
		   $arVars = $obCache->GetVars();
		   $data = $arVars['DATA'];
		}
		elseif($obCache->StartDataCache())
		{
			$arSelect = Array($dataType);
			$arFilter = Array("IBLOCK_TYPE" => "content", "IBLOCK_CODE" => "enext_objects_s1", "PROPERTY_CITY" => $selectedCityId, "ACTIVE" => "Y");
			$rsCity = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

			if($arOfficeElement = $rsCity->GetNext())
			{
				if(substr($dataType, 0, 9) == 'PROPERTY_')
					$data = $arOfficeElement[$dataType.'_VALUE'];
				else
					$data = $arOfficeElement[$dataType];
			}
			
			$obCache->EndDataCache(Array('DATA' => $data));
		}
	}
	
	return $data;
}

function OnBeforePrologHandler()
{
	$defaultCityId = 0;
	
	if(CModule::IncludeModule('iblock'))
	{
		require_once (__DIR__ . '/include/ipgeobase/ipgeobase.php');

		$ip = $_SERVER['REMOTE_ADDR'];
		$gb = new IPGeoBase();
		$data = $gb->getRecord($ip);

		if(is_array($data))
		{
			foreach($data as &$v)
			{
				$v = iconv('windows-1251', 'utf-8', $v);
			}
		}

		if(isset($data['city']))
		{
			$arFilter = Array("IBLOCK_TYPE" => "references", "IBLOCK_CODE" => "city", "ACTIVE" => "Y", "NAME" => $data['city']);
			$arSelect = Array("ID");
			$rsCity = CIBlockElement::GetList(Array(), $arFilter, false, Array ("nTopCount" => 1), $arSelect);
			if($arCityElement = $rsCity->GetNext())
			{
				$defaultCityId = $arCityElement['ID'];
			}
			else
			{
				if(isset($data['region']))
				{
					$arFilter = Array("IBLOCK_TYPE" => "references", "IBLOCK_CODE" => "city", "ACTIVE" => "Y", "PROPERTY_REGION_VALUE" => $data['region'], "PROPERTY_REGION_CENTER_VALUE" => "Y");
					$arSelect = Array("ID");
					$rsCity = CIBlockElement::GetList(Array(), $arFilter, false, Array ("nTopCount" => 1), $arSelect);
					if($arCityElement = $rsCity->GetNext())
					{
						$defaultCityId = $arCityElement['ID'];
					}
					else
					{
						if(isset($data['district']))
						{
							$arFilter = Array("IBLOCK_TYPE" => "references", "IBLOCK_CODE" => "city", "ACTIVE" => "Y", "PROPERTY_DISTRICT_VALUE" => $data['district'], "PROPERTY_DISTRICT_CENTER_VALUE" => "Y");
							$arSelect = Array("ID");
							$rsCity = CIBlockElement::GetList(Array(), $arFilter, false, Array ("nTopCount" => 1), $arSelect);
							if($arCityElement = $rsCity->GetNext())
							{
								$defaultCityId = $arCityElement['ID'];
							}
						}
					}
				}
			}
		}
	}
	
	define("DEFAULT_CITY_ID", $defaultCityId);
}

function OnEndBufferContentHandler(&$buffer)
{
	if(!CSite::InDir('/bitrix/'))
	{
		ReplaceCityPlaceHolders($buffer);
	}
}

function ReplaceCityPlaceHolders(&$content)
{
	// замена плейсхолдеров #CITY#, #CITY_PHRASE_1#, #CITY_PHRASE_2#, #CITY_PHRASE_3# на данные выбранного города
	$arCity = GetSelectedCityArray();
	$content = str_replace(
		Array("#CITY#", "#CITY_PHRASE_1#", "#CITY_PHRASE_2#", "#CITY_PHRASE_3#"),
		Array($arCity["NAME"], $arCity["CITY_PHRASE_1"], $arCity["CITY_PHRASE_2"], $arCity["CITY_PHRASE_3"]),
		$content);
}

function GetSelectedCityId()
{
	global $APPLICATION;
	
	$selectedCityId = intval($APPLICATION->get_cookie("CITY_ID"));
	
	if(isset($_GET['CITY_ID']) && !empty($_GET['CITY_ID'])){
        $APPLICATION->set_cookie("CITY_ID", intval($_GET['CITY_ID']));
        $selectedCityId = intval($_GET['CITY_ID']);
    }

	if(empty($selectedCityId))
	{
		$selectedCityId = DEFAULT_CITY_ID;
	}

    return $selectedCityId;
}

function GetSelectedCityArray() 
{
	$arCity = Array(
		"ID" => 0,
		"NAME" => "",
		"PHRASE_1" => "",
		"PHRASE_2" => "",
		"PHRASE_3" => ""
	);
	
	if(CModule::IncludeModule('iblock'))
	{
		$selectedCityId = GetSelectedCityId();
		
		$obCache = new CPHPCache();

		if($obCache->InitCache(CACHE_TTL, $selectedCityId, '/CITY'))
		{
		   $arVars = $obCache->GetVars();
		   $arCity = $arVars['CITY'];
		}
		elseif($obCache->StartDataCache())
		{
			$arSelect = Array("ID", "NAME", "PROPERTY_CITY_PHRASE_1", "PROPERTY_CITY_PHRASE_2", "PROPERTY_CITY_PHRASE_3");
			$arFilter = Array("IBLOCK_TYPE" => "references", "IBLOCK_CODE" => "city", "ID" => $selectedCityId, "ACTIVE" => "Y");
			$rsCity = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

			if($arCityElement = $rsCity->GetNext())
			{
				$arCity = Array(
					"ID" => intval($arCityElement["ID"]),
					"NAME" => $arCityElement["NAME"],
					"CITY_PHRASE_1" => $arCityElement["PROPERTY_CITY_PHRASE_1_VALUE"],
					"CITY_PHRASE_2" => $arCityElement["PROPERTY_CITY_PHRASE_2_VALUE"],
					"CITY_PHRASE_3" => $arCityElement["PROPERTY_CITY_PHRASE_3_VALUE"]
				);
			}
			
			$obCache->EndDataCache(Array('CITY' => $arCity));
		}
	}
	
	return $arCity;
}

function BrandsRedirect() // Вызывается на странице каталога
{
	global $APPLICATION;
	
	$brandCode = str_replace("/", "", $APPLICATION->GetCurPAge());
	
	if($brandCode)
	{
		$obCache = new CPHPCache();

		if($obCache->InitCache(CACHE_TTL, $brandCode, '/BRAND'))
		{
		   $arVars = $obCache->GetVars();
		   $redirect = $arVars['REDIRECT'];
		}
		elseif($obCache->StartDataCache())
		{
			$arSelect = Array("ID");
			$arFilter = Array("IBLOCK_CODE" => "enext_brands_s1", "CODE" => $brandCode, "ACTIVE" => "Y");
			$rsCity = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

			$redirect = false;
			
			if($arBrandElement = $rsCity->GetNext())
			{
				$redirect = true;
			}
			
			$obCache->EndDataCache(Array('REDIRECT' => $redirect));
		}
		
		if($redirect)
			LocalRedirect('/proizvoditeli-svetodiodnyix-svetilnikov-predlagaemyie-kompaniej-vinchi/'.$brandCode.'/', false, '301 Moved permanently');
	}
}

function GetOrderPropertyValue($ORDER_ID, $PROPERTY_CODE)
{
	$value = '';
	
	if(intval($ORDER_ID) && !empty($PROPERTY_CODE))
	{
		if(CModule::IncludeModule("sale"))
		{
			$arFilter = array("ORDER_ID" => $ORDER_ID);
			
			if(is_array($PROPERTY_CODE))
			{
				$arFilter["@CODE"] = $PROPERTY_CODE;
			}
			else
			{
				$arFilter["CODE"] = $PROPERTY_CODE;
			}
			
			$value = false;
			$db_vals = CSaleOrderPropsValue::GetList(
				array("SORT" => "ASC"),
				$arFilter
			);
			while($arVals = $db_vals->Fetch())
			{
				if(is_array($PROPERTY_CODE))
				{
					$value[$arVals["CODE"]] = $arVals;
				}
				else
				{
					$value = $arVals["VALUE"];
				}
			}
		}
	}
	
	return $value;
}
setLastModified();
function setLastModified(){
    $LastModified_unix = strtotime(date("D, d M Y H:i:s", filectime($_SERVER['SCRIPT_FILENAME'])));
    $LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
    $IfModifiedSince = false;

    if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))
        $IfModifiedSince = strtotime(substr ($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));

    if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
        $IfModifiedSince = strtotime(substr ($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));

    if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
        header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
        exit;
    }

    header('Last-Modified: '. $LastModified);
}
