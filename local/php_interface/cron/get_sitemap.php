<?
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
$pass = 'I9wnMC4o';
if(true){
    $host = COption::GetOptionString("main", "server_name");
    $link = 'https://'.$host.'/local/php_interface/cron/seo_sitemap_run.php';

    $schet = 0; //просто счетчик для ограничения
    $nextStep = 0;
    $ns = '';
    $id = 1; //ID выгрузки

    while($schet <= 15){
        $dataStep = array(
            'lang' => 'ru',
            'action' => 'sitemap_run',
            'ID' => $id,
            'value' => $nextStep,
            'pid' => $id,
            'NS' => $ns,
            'key' => $pass
        );


        $dataStepQuery = http_build_query($dataStep);
        $contextStep = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header'=> "Content-type: application/x-www-form-urlencoded",
                'content' => $dataStepQuery
            ),
        ));
        $contStart = file_get_contents($link, false, $contextStep);
        if(preg_match('/runSitemap/', $contStart)){

            //выбираем данные
            preg_match('/runSitemap\(1,\s([0-9]+),/', $contStart, $arNextStep);
            if(intval($arNextStep[1]) > 0){
                $nextStep = intval($arNextStep[1]);
                preg_match('/{(.*)}/', $contStart, $arPregNs);
                $ns = '';
                if(strlen($arPregNs[0]) > 0 &&
                    strpos($arPregNs[0], '{') !== false &&
                    strpos($arPregNs[0], '}') !== false
                ){
                    $nsJson = str_replace("'", '"', $arPregNs[0]);
                    $ns = json_decode($nsJson, true);
                }
            }
        }else{
            $schet = 15;
        }
        $schet++;
    }
}
?>