<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<div class="CityOnline">
	<div class="CityOnline__Title">
		<div class="top-panel__col top-panel__city">
			<div class="top-panel__compare-block">
				<a class="top-panel__compare-link">
					<i class="icon-map-maker" aria-hidden="true"></i>
				</a>
			</div>
		</div>
		<span class="yours-city"><?=GetMessage("FILTER_CONTACT_YOUR_CITY")?>:</span> <span data-name-city=""><?=$arResult["CITY_NAME"]?></span>
		
	</div>
	<div class="js_city_addr"></div>
	<div class="js_city_mail"></div>
	<div class="CityOnline__Body DisplayNone">
		<span class="slide-panel__close CityOnline__Close"><i class="icon-close"></i></span>
		<div><?=GetMessage("FILTER_CONTACT_YOUR_CITY")?></div>
		<div><b data-name-city=""><?=$arResult["CITY_NAME"]?>?</b></div>
		<div class="CityOnline__ButtonsPanel">
			<div class="CityOnline__Yes" <?if(!$arResult["SELECTED"]){?> data-city="<?=$arResult["CITY_ID"]?>" <?}?>>
				<?=GetMessage("FILTER_CONTACT_YES")?>
			</div>
			<div class="CityOnline__No"><?=GetMessage("FILTER_CONTACT_TRANSFORM")?></div>
		</div>
	</div>
</div>
<div class="PopupCity">
	<div class="CitySearch">
		<ul class="CitySearch__Ul">
			<?foreach($arResult["CITIES"] as $arCity){?>
				<li class="CitySearch__Li">
					<span class="CitySearch__A" data-city="<?=$arCity['ID']?>">
						<?=$arCity['NAME']?>
					</span>
				</li>
			<?}?>
		</ul>
	</div>
</div>
<?CUtil::InitJSCore(array('ajax', 'popup'));?>