$(function() {
	var PopupCity = new BX.PopupWindow(
		"PopupCity",
		null,
		{
			autoHide : true,
			offsetTop : 1,
			offsetLeft : 0,
			lightShadow : true,
			closeIcon : true,
			closeByEsc : true,
			overlay: {
				backgroundColor: 'rgba(61, 75, 82, 0.5)', opacity: '1'
			},
			events: {
               onPopupShow: function() {
                  $('.PopupCity').css({'display': 'block'});
               }
            },
			content: document.getElementsByClassName("PopupCity")[0]
		}
	);
	
	$('body').on('click', '.CityOnline__No', function() {
		PopupCity.show();
	});
	
	$('body').on('click', '.CityOnline__Close, .CityOnline__Title [data-name-city], .CityOnline__Title .top-panel__col.top-panel__city', function() {
		$('.CityOnline__Body').toggleClass('DisplayNone');
	});
	
	// при первичном заходе на сайт
	$('.CityOnline__Yes').click(function() {
		if ($(this).attr('data-city')) {
			var me = this;
			
			$.ajax({
				url: window.location.href,
				data: {'CITY_ID': $(me).attr('data-city')},
				method: 'get',
				success: function () {
					// do noting
				}
			});
		}

		$('.CityOnline__Body').toggleClass('DisplayNone');		
	});

	$('span[data-city]').click(function() {
		var me = this;
		var CityId = $(me).attr('data-city');
		
		$.ajax({
			url: window.location.href,
			data: {'CITY_ID': CityId},
			method: 'get',
			success: function () {
				BX.STOP = false;
				$('[data-name-city]').html($(me).html());
				BX.onCustomEvent("UpdateCity", []);
				PopupCity.close();
				$('.CityOnline__Body').toggleClass('DisplayNone');
			}
		});
		
		return false;
	});
});