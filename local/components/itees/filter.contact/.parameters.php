<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-"=>" "));

$arIBlocks=array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = "[".$arRes["ID"]."] ".$arRes["NAME"];

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(		
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("T_IBLOCK_FILTER_CONTACT_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,			
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("T_IBLOCK_FILTER_CONTACT_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,			
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"CITY_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("T_IBLOCK_FILTER_CONTACT_CITY"),
			"TYPE" => "STRING",			
		)		
	)
);