<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
class CFilterContactComponent extends CBitrixComponent
{	
	public function executeComponent()
	{
		global $APPLICATION;

		$arResult["SELECTED"] = false;
		if(intval($APPLICATION->get_cookie("CITY_ID")) > 0)
			$arResult["SELECTED"] = true;
		
		if(isset($_GET['CITY_ID']) && !empty($_GET['CITY_ID']))
			$APPLICATION->set_cookie("CITY_ID", intval($_GET['CITY_ID']));
		
		$arResult["CITY_ID"] = GetSelectedCityId();
		$arResult["CITIES"] = $this->getCities();
		
		foreach($arResult["CITIES"] as $arCity)
		{
			if($arCity['ID'] == $arResult["CITY_ID"])
			{
				$arResult["CITY_NAME"] = $arCity['NAME'];
				break;
			}
		}
		
		$this->arResult = $arResult;
		$this->includeComponentTemplate();
	}
	
	protected function getCities()
	{
		$obCache = new CPHPCache();

		if($obCache->InitCache(CACHE_TTL, false, '/CITY_COMPONENT'))
		{
		   $arVars = $obCache->GetVars();
		   $arCities = $arVars['CITIES'];
		}
		elseif($obCache->StartDataCache())
		{
			$arSort = Array("NAME" => "ASC");
			$arSelect = Array("ID", "NAME");
			$arFilter = Array("IBLOCK_TYPE" => "references", "IBLOCK_CODE" => "city", "ACTIVE" => "Y");
			$rsCity = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

			$arCities = Array();
			while($arCity = $rsCity->GetNext())
			{
				$arCities[] = $arCity;
			}
			
			$obCache->EndDataCache(Array('CITIES' => $arCities));
		}
		
		return $arCities;
	}
}