<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("T_IBLOCK_FILTER_CONTACT"),
	"DESCRIPTION" => GetMessage("T_IBLOCK_FILTER_CONTACT_DESC"),
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "itees",
		"NAME" => GetMessage("T_IBLOCK_DESC_ITEES"),
	)
);
?>