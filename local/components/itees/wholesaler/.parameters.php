<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
		"OVERALL" => array(
			"NAME" => "Общие настройки",
		),
		"DATA_SOURSE" => array(
			"NAME" => "Источник данных",
		),
		"SEF_PARAMS" => array(
			"NAME" => "Настройки ЧПУ",
		),
	),
	"PARAMETERS" => array(
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
		"CACHE_GROUPS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => "Учитывать права доступа",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"USE_SEF" => array(
			"PARENT" => "SEF_PARAMS",
			"NAME" => "Обрабатывать ЧПУ",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
			"REFRESH" => "Y"
		),
	),
);
if($arCurrentValues["USE_SEF"] == "Y"){
	$arComponentParameters["PARAMETERS"]["SEF_PATH"] = array(
		"PARENT" => "SEF_PARAMS",
		"NAME" => "Каталог ЧПУ относительно корня",
		"TYPE" => "STRING",
	);
	$arComponentParameters["PARAMETERS"]["SECTION_PATH"] = array(
		"PARENT" => "SEF_PARAMS",
		"NAME" => "Путь к разделу",
		"TYPE" => "STRING",
	);
	$arComponentParameters["PARAMETERS"]["ELEMENT_PATH"] = array(
		"PARENT" => "SEF_PARAMS",
		"NAME" => "Путь к элементу",
		"TYPE" => "STRING",
	);
}
?>
