(function(window) {
	'use strict';

	if(!!window.JCWholesaler)
		return;

	window.JCWholesaler = function(arParams) {		
		this.visual = {
			ID: ''
		};
		
		this.sPanel = null;
		this.sPanelContent = null;
		this.obItem = null;
		
		this.errorCode = 0;
		
		if(typeof arParams === 'object') {
			this.visual = arParams.VISUAL;

			BX.ready(BX.delegate(this.init, this));
		}
	};

	window.JCWholesaler.prototype = {
		init: function() {
			this.sPanel = document.body.querySelector('.slide-panel');
			
			this.obItem = BX(this.visual.ID);
			if(!this.obItem) {
				this.errorCode = -1;
			}

			if(this.errorCode === 0) {
				BX.bind(this.obItem, 'click', BX.proxy(this.showContacts, this));
			}
		},
		
		showContacts: function(e) {
			if(!!this.sPanel) {
				e.preventDefault();
				
				BX.ajax.post(
					BX.message('SITE_DIR') + 'ajax/slide_panel.php',
					{
						action: 'wholesaler'
					},
					BX.delegate(function(result) {
						this.sPanel.appendChild(
							BX.create('DIV', {
								props: {
									className: 'slide-panel__title-wrap'
								},
								children: [
									BX.create('I', {
										props: {
											className: 'icon-comment'
										}
									}),						
									BX.create('SPAN', {
										props: {
											className: 'slide-panel__title'
										},
									}),
									BX.create('SPAN', {
										props: {
											className: 'slide-panel__close'
										},
										children: [
											BX.create('I', {
												props: {
													className: 'icon-close'
												}
											})
										]
									})
								]
							})
						);
											
						this.sPanel.appendChild(
							BX.create('DIV', {
								props: {
									className: 'slide-panel__content scrollbar-inner'
								},
								html: result
							})
						);

						var sPanelContent = this.sPanel.querySelector('.slide-panel__content');
						if(!!sPanelContent)
							$(sPanelContent).scrollbar();
						
						var sPanelTitle = this.sPanel.querySelector('.slide-panel__title'),
							sPanelFormTitle = this.sPanel.querySelector('.slide-panel__form-title');
						if(!!sPanelFormTitle) {
							sPanelTitle.innerHTML = sPanelFormTitle.innerHTML;
							BX.remove(sPanelFormTitle);
						}
						
						var scrollWidth = window.innerWidth - document.body.clientWidth;
						if(scrollWidth > 0) {
							BX.style(document.body, 'padding-right', scrollWidth + 'px');

							var pageBg = document.querySelector('.page-bg');
							if(!!pageBg)
								BX.style(pageBg, 'margin-right', scrollWidth + 'px');
							
							var topPanel = document.querySelector('.top-panel');
							if(!!topPanel) {
								if(BX.hasClass(topPanel, 'fixed'))
									BX.style(topPanel, 'padding-right', scrollWidth + 'px');
								
								var topPanelThead = topPanel.querySelector('.top-panel__thead');
								if(!!topPanelThead && BX.hasClass(topPanelThead, 'fixed'))
									BX.style(topPanelThead, 'padding-right', scrollWidth + 'px');
								
								var topPanelTfoot = topPanel.querySelector('.top-panel__tfoot');
								if(!!topPanelTfoot && BX.hasClass(topPanelTfoot, 'fixed'))
									BX.style(topPanelTfoot, 'padding-right', scrollWidth + 'px');
							}

							var sectionPanel = document.querySelector('.catalog-section-panel');
							if(!!sectionPanel && BX.hasClass(sectionPanel, 'fixed'))
								BX.style(sectionPanel, 'padding-right', scrollWidth + 'px');

							var tabsPanel = document.querySelector('[data-entity="tabs"]');							
							if(!!tabsPanel && BX.hasClass(tabsPanel, 'fixed'))
								BX.style(tabsPanel, 'padding-right', scrollWidth + 'px');

							var objectsMap = document.querySelector('.objects-map');
							if(!!objectsMap)
								BX.style(objectsMap, 'padding-right', scrollWidth + 'px');
						}

						var scrollTop = BX.GetWindowScrollPos().scrollTop;
						if(!!scrollTop && scrollTop > 0)
							BX.style(document.body, 'top', '-' + scrollTop + 'px');

						BX.addClass(document.body, 'slide-panel-active')
						BX.addClass(this.sPanel, 'active');
						
						var pageTitle = $('title').text();
						$("input[name=PAGE]").val(pageTitle + ' - ' + location.href);

						document.body.appendChild(
							BX.create('DIV', {
								props: {
									className: 'modal-backdrop slide-panel__backdrop fadeInBig'
								}
							})
						);
					}, this)
				);
			}
		}
	};
})(window);