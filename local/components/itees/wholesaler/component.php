<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(isset($arParams["COMPONENT_ENABLE"]) && $arParams["COMPONENT_ENABLE"] === false)
	return;

// Режим разработки под админом
$bDesignMode = $GLOBALS["APPLICATION"]->GetShowIncludeAreas() && is_object($GLOBALS["USER"]) && $GLOBALS["USER"]->IsAdmin();

$arNavParams = CDBResult::GetNavParams();

//	обработка ЧПУ
if($arParams["USE_SEF"] == "Y" && strlen($arParams["SEF_PATH"]) && strlen($arParams["SECTION_PATH"]) && strlen($arParams["ELEMENT_PATH"])){
	$SEF_URL_TEMPlATES = array();
	$SEF_URL_TEMPlATES["sections"] = ""; 
	$SEF_URL_TEMPlATES["section"] = $arParams["SECTION_PATH"];
	$SEF_URL_TEMPlATES["element"] = $arParams["ELEMENT_PATH"];
	
	$VariableAliases = array();
	$arDefaultUrlTemplates404 = array(
		"sections" => "",
		"section" => "#SECTION_ID#/",
		"element" => "#SECTION_ID#/#ELEMENT_ID#/",
		"compare" => "",
	);

	$arDefaultVariableAliases404 = array();

	$arDefaultVariableAliases = array();

	$arComponentVariables = array(
		"SECTION_ID",
		"SECTION_CODE",
		"ELEMENT_ID",
		"ELEMENT_CODE",
		"action",
	);

	$arVariables = array();

	$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $SEF_URL_TEMPlATES);
	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $VariableAliases);

	$componentPage = CComponentEngine::ParseComponentPath(
		$arParams["SEF_PATH"],
		$arUrlTemplates,
		$arVariables
	);

	if(!$componentPage)
		$componentPage = "sections";
	
	CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);
	
	$arResult["VARIABLES"] = $arVariables;
	$ADDITIONAL_CACHE_ID[] = $arVariables;
}


// Дополнительно кешируем текущую страницу
$ADDITIONAL_CACHE_ID[] = $arNavParams["PAGEN"];
$ADDITIONAL_CACHE_ID[] = $arNavParams["SIZEN"];
if($arParams["CACHE_GROUPS"] === "Y"){
	global $USER;
	$ADDITIONAL_CACHE_ID[] = $USER->GetGroups();
}

$CACHE_PATH = "/".SITE_ID."/".LANGUAGE_ID.$this->__relativePath;

// Подключается файл result-modifier.php
if($this->StartResultCache($arParams["CACHE_TIME"], $ADDITIONAL_CACHE_ID, $CACHE_PATH)) 
{
	$this->IncludeComponentTemplate();
}

// Подключаем файл без кеширования
$modifier_path = $_SERVER["DOCUMENT_ROOT"].$arResult["__TEMPLATE_FOLDER"]."/component_epilog.php";
$modifier_short_path = $_SERVER["DOCUMENT_ROOT"].$arResult["__TEMPLATE_FOLDER"]."/nc.php";

if (file_exists($modifier_short_path))
{
	require_once($modifier_short_path);
	$mod_name = "nc.php";
}
elseif (file_exists($modifier_path))
{
	require_once($modifier_path);
	$mod_name = "component_epilog.php";
}

// Возвращаемое значение
if (!empty($arResult["__RETURN_VALUE"]))
	return $arResult["__RETURN_VALUE"];

?>