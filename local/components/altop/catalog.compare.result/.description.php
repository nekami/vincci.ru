<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("CCR_DEFAULT_TEMPLATE_NAME"),
	"DESCRIPTION" => GetMessage("CCR_DEFAULT_TEMPLATE_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"PATH" => array(
		"ID" => "altop",
		"NAME" => GetMessage("CCR_DEFAULT_COMPONENT_PATH_NAME"),		
	)
);?>