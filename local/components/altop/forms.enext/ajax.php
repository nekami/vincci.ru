<?define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Application,	
	Bitrix\Main\Web\Cookie,
	Bitrix\Main\Config\Option,
	Bitrix\Main\Mail\Event;

if(!Loader::IncludeModule('iblock'))
	return;

Loc::loadMessages(__FILE__);

global $APPLICATION;

$context = Application::getInstance()->getContext();
$request = $context->getRequest();

if($request->isAjaxRequest() && $request->isPost()) {
	$siteId = $request->getPost('SITE_ID') ? $request->getPost('SITE_ID') : SITE_ID;
	
	$iblockString = $request->getPost('IBLOCK_STRING');
	if(!empty($iblockString))
		$iblock = unserialize(base64_decode(strtr($iblockString, '-_,', '+/=')));

	//USER_CONSENT//
	$userConsent = $request->getPost('USER_CONSENT');
	if($userConsent == 'Y') {
		$userConsentId = (int)$request->getPost('USER_CONSENT_ID');
		$userConsentUrl = $request->getPost('USER_CONSENT_URL');
		Bitrix\Main\UserConsent\Consent::addByContext($userConsentId, null, null, array("URL" => $userConsentUrl));
	}
	
	//CAPTCHA//
	$captchaSid = $request->getPost('CAPTCHA_SID');
	if(!empty($captchaSid)) {	
		require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/classes/general/captcha.php');		
		CCaptcha::Delete($captchaSid);
	}
	
	//PROPERTIES//
	foreach($iblock['PROPERTIES'] as $arProp) {	
		$post = $request->getPost($arProp['CODE']);
		if(!empty($post)) {			
			if($arProp['CODE'] == 'OBJECT_ID' || $arProp['CODE'] == 'PRODUCT_ID' || $arProp['CODE'] == 'OFFER_ID') {
				$arProps[$arProp['CODE']] = intval($post);
			} else {
				$post = iconv('UTF-8', SITE_CHARSET, strip_tags(trim($post)));
				if($arProp['USER_TYPE'] == 'HTML') {
					$arProps[$arProp['CODE']] = array(
						'VALUE' => array(
							'TEXT' => $post,
							'TYPE' => $arProp['DEFAULT_VALUE']['TYPE']
						)
					);
				} else {
					$arProps[$arProp['CODE']] = $post;
					$cookie = new Cookie('ENEXT_FORMS_'.$arProp['CODE'], $post, time() + 32832000);
					$cookie->setDomain(SITE_SERVER_NAME);
					$cookie->setHttpOnly(false);
					$context->getResponse()->addCookie($cookie);			
					unset($cookie);
				}
			}
		}
	}
	unset($arProp);
	$context->getResponse()->flush("");

	//PRODUCT_LINK//
	$productLink = $request->getPost('PRODUCT_LINK');
	
	//NEW_ELEMENT//
	$el = new CIBlockElement;

	$arFields = array(
		'IBLOCK_ID' => $iblock['ID'],
		'ACTIVE' => 'Y',
		'NAME' => Loc::getMessage('IBLOCK_ELEMENT_NAME').ConvertTimeStamp(time(), 'FULL'),
		'PROPERTY_VALUES' => !empty($arProps) ? $arProps : array(),
	);

	if($el->Add($arFields)) {
		//MAIL_PROPERTIES//
		foreach($iblock['PROPERTIES'] as $arProp) {
			$post = $request->getPost($arProp['CODE']);
			if(!empty($post)) {
				if($arProp['CODE'] == 'OBJECT_ID' || $arProp['CODE'] == 'PRODUCT_ID' || $arProp['CODE'] == 'OFFER_ID') {
					$arSelect = array('ID', 'IBLOCK_ID', 'NAME');
					if($arProp['CODE'] == 'OBJECT_ID') {
						$arSelect[] = 'PROPERTY_PHONE_SMS';
						$arSelect[] = 'PROPERTY_EMAIL_EMAIL';
						$arSelect[] = 'PROPERTY_CITY.NAME';
						
						$code = 'enext_objects_s1';
					}
					else if($arProp['CODE'] == 'PRODUCT_ID') {
						$code = 'enext_catalog_s1';
					}
					else if($arProp['CODE'] == 'OFFER_ID') {
						$code = 'enext_offers_s1';
					}

					$rsElement = CIBlockElement::GetList(
						array(),
						array('ID' => intval($post), 'IBLOCK_CODE' => $code),
						false,
						false,
						$arSelect
					);
					if($arElement = $rsElement->GetNext()) {
						$arMailProps[$arProp['CODE']] = $arElement['NAME'];
						if($arProp['CODE'] == 'OBJECT_ID') {
							$arMailProps['PHONE_SMS'] = $arElement['PROPERTY_PHONE_SMS_VALUE'];
							$arMailProps['EMAIL_EMAIL'] = $arElement['PROPERTY_EMAIL_EMAIL_VALUE'];
							$arMailProps['CITY'] = $arElement['PROPERTY_CITY_NAME'];
						}
					}
					unset($arElement, $rsElement, $arSelect);
				} else {
					$arMailProps[$arProp['CODE']] = iconv('UTF-8', SITE_CHARSET, strip_tags(trim($post)));
				}
			}
		}
		unset($arProp);
		
		if(!empty($productLink))
			$arMailProps['PRODUCT_LINK'] = $productLink;
		
		$arMailProps['FORM_NAME'] = $iblock['NAME'];
		$arMailProps['EMAIL_TO'] = Option::get('main', 'email_from');

		//MAIL_EVENT//	
		$eventName = 'ALTOP_FORM_'.$iblock['IBLOCK_TYPE_ID'].'_'.$iblock['CODE'];

		$eventDesc = $messBody = '';		
		foreach($iblock['PROPERTIES'] as $arProp) {
			$eventDesc .= '#'.$arProp['CODE'].'# - '.$arProp['NAME']."\n";
			if($arProp['CODE'] == 'OBJECT_ID') {
				$eventDesc .= Loc::getMessage('MAIL_EVENT_DESCRIPTION_NOTIFICATION');
			}
			$messBody .= $arProp['NAME'].': #'.$arProp['CODE'].'#<br />';		
		}
		unset($arProp);
		
		if(!empty($productLink)) {
			$eventDesc .= Loc::getMessage('MAIL_EVENT_DESCRIPTION_PRODUCT_LINK');
			$messBody .= Loc::getMessage('MAIL_EVENT_MESSAGE_PRODUCT_LINK');
		}
		
		$eventDesc .= Loc::getMessage('MAIL_EVENT_DESCRIPTION');
		
		//MAIL_EVENT_TYPE//
		$arEvent = CEventType::GetByID($eventName, LANGUAGE_ID)->Fetch();
		if(empty($arEvent)) {
			$et = new CEventType;
			$arEventFields = array(
				'LID' => LANGUAGE_ID,
				'EVENT_NAME' => $eventName,
				'NAME' => Loc::getMessage('MAIL_EVENT_TYPE_NAME').' "'.$iblock['NAME'].'"',
				'DESCRIPTION' => $eventDesc
			);
			$et->Add($arEventFields);		
		}

		//MAIL_EVENT_MESSAGE//
		$arMess = CEventMessage::GetList($by = 'site_id', $order = 'desc', array('TYPE_ID' => $eventName))->Fetch();
		if(empty($arMess)) {
			$em = new CEventMessage;
			$arMess = array();
			$arMess['ID'] = $em->Add(
				array(
					'ACTIVE' => 'Y',
					'EVENT_NAME' => $eventName,
					'LID' => $siteId,
					'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
					'EMAIL_TO' => !empty($arMailProps['EMAIL_EMAIL']) ? '#EMAIL_EMAIL#' : '#EMAIL_TO#',
					'BCC' => '',
					'SUBJECT' => Loc::getMessage('MAIL_EVENT_MESSAGE_SUBJECT'),
					'BODY_TYPE' => 'html',
					'MESSAGE' => $messBody.Loc::getMessage('MAIL_EVENT_MESSAGE_FOOTER')
				)
			);		
		}

		//SEND_MAIL//
		Event::send(array(
			'EVENT_NAME' => $eventName,
			'LID' => $siteId,
			'C_FIELDS' => !empty($arMailProps) ? $arMailProps : array(),
		));
		
		$result = array(
			'status' => true,
			'captcha_code' => false
		);	
	} else {	
		$result = array(
			'status' => false,
			'captcha_code' => !empty($captchaSid) ? $APPLICATION->CaptchaGetCode() : false
		);
	}

	echo Bitrix\Main\Web\Json::encode($result);
}