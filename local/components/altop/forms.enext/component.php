<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

if(!Loader::includeModule('iblock'))
	return;

if(!isset($arParams['CACHE_TIME']))
	$arParams['CACHE_TIME'] = 36000000;

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
if($arParams['IBLOCK_ID'] <= 0)
	return;

global $USER;
$arSettings = CEnext::GetFrontParametrsValues(SITE_ID);
$arParams['USE_CAPTCHA'] = !$USER->IsAuthorized() && $arSettings['FORMS_USE_CAPTCHA'] == 'Y' ? 'Y' : 'N';

$arParams['PHONE_MASK'] = $arSettings['FORMS_PHONE_MASK'];
$arParams['VALIDATE_PHONE_MASK'] = $arSettings['FORMS_VALIDATE_PHONE_MASK'];

$arParams['USER_CONSENT'] = $arSettings['FORMS_USER_CONSENT'];
$arParams['USER_CONSENT_ID'] = $arSettings['FORMS_USER_CONSENT_ID'];
$arParams['USER_CONSENT_IS_CHECKED'] = $arSettings['FORMS_USER_CONSENT_IS_CHECKED'];
$arParams['USER_CONSENT_IS_LOADED'] = $arSettings['FORMS_USER_CONSENT_IS_LOADED'];

$officeId = GetSlectedCityOfficeId();

if($this->StartResultCache(false, $officeId)) {
	//IBLOCK//
	$rsIblock = CIBlock::GetList(
		array(
			'SORT' => 'ASC'
		),
		array(
			'ID' => $arParams['IBLOCK_ID'],
			'ACTIVE' => 'Y'
		)
	);
	if(!$arIblock = $rsIblock->Fetch()) {
		$this->abortResultCache();
		return;
	}
	
	$arResult['OFFICE_ID'] = $officeId;
	
	$arResult['IBLOCK']['ID'] = $arIblock['ID'];
	$arResult['IBLOCK']['CODE'] = $arIblock['CODE'];
	$arResult['IBLOCK']['IBLOCK_TYPE_ID'] = $arIblock['IBLOCK_TYPE_ID'];
	$arResult['IBLOCK']['NAME'] = $arIblock['NAME'];
	$arResult['IBLOCK']['DESCRIPTION'] = $arIblock['DESCRIPTION'];

	//IBLOCK_PROPS//
	$rsProps = CIBlock::GetProperties(
		$arIblock['ID'],
		array(
			'SORT' => 'ASC',
			'NAME' => 'ASC'
		),
		array(
			'ACTIVE' => 'Y',
			'PROPERTY_TYPE' => 'S'
		)
	);
	while($arProps = $rsProps->fetch()) {
		$arResult['IBLOCK']['PROPERTIES'][] = $arProps;
	}
	unset($arProps, $rsProps);

	$rsProps = CIBlock::GetProperties(
		$arIblock['ID'],
		array(
			'SORT' => 'ASC',
			'NAME' => 'ASC'
		),
		array(
			'ACTIVE' => 'Y',
			'USER_TYPE' => 'EList'
		)
	);
	while($arProps = $rsProps->fetch()) {
		$arResult['IBLOCK']['PROPERTIES'][] = $arProps;
	}
	unset($arProps, $rsProps, $arIblock);
	
	if(!isset($arResult['IBLOCK']['PROPERTIES']) || empty($arResult['IBLOCK']['PROPERTIES'])) {
		$this->abortResultCache();
		return;
	}

	$arResult['IBLOCK']['STRING'] = strtr(base64_encode(serialize($arResult['IBLOCK'])), '+/=', '-_,');
	
	$this->IncludeComponentTemplate();
}