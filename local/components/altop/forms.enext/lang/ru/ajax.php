<?
$MESS['IBLOCK_ELEMENT_NAME'] = "Сообщение формы от ";
$MESS['MAIL_EVENT_DESCRIPTION'] = "#FORM_NAME# - Название формы\n#EMAIL_TO# - E-Mail администратора сайта (из настроек главного модуля)\n";
$MESS['MAIL_EVENT_DESCRIPTION_NOTIFICATION'] = "#PHONE_SMS# - Телефон для SMS уведомлений\n#EMAIL_EMAIL# - Email для Email уведомлений\n";
$MESS['MAIL_EVENT_DESCRIPTION_PRODUCT_LINK'] = "#PRODUCT_LINK# - Ссылка на товар\n";
$MESS['MAIL_EVENT_TYPE_NAME'] = "Сообщение формы";
$MESS['MAIL_EVENT_MESSAGE_SUBJECT'] = "Новое сообщение формы \"#FORM_NAME#\" с сайта \"#SITE_NAME#\"";
$MESS['MAIL_EVENT_MESSAGE_PRODUCT_LINK'] = "Ссылка на товар: #PRODUCT_LINK#<br />";
$MESS['MAIL_EVENT_MESSAGE_FOOTER'] = "<br />Сообщение сгенерировано автоматически.";