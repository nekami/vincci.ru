<?define('NOT_CHECK_PERMISSIONS', true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Web\Json,
	Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();;

if($request->isAjaxRequest() && $request->isPost()) {
	$getCaptcha = $request->getPost('getCaptcha');
	if($getCaptcha == 'Y')
		$result['captcha'] = $APPLICATION->CaptchaGetCode();
	$props = $request->getPost('props');
	if(!empty($props)) {
		foreach($props as $arProp) {
			$result[$arProp] = $request->getCookie('ENEXT_FORMS_'.$arProp);
		}
	}
	echo Json::encode($result);
}