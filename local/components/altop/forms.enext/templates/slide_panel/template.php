<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true ) die();

$this->setFrameMode(true);

use Bitrix\Main\Localization\Loc;?>

<div class="slide-panel__form">
	<div class="slide-panel__form-title"><?=$arResult['IBLOCK']['NAME']?></div>
	<?if(!empty($arResult['IBLOCK']['DESCRIPTION'])) {?>
		<div class="slide-panel__form-caption"><?=$arResult['IBLOCK']['DESCRIPTION']?></div>
	<?}?>
	<form id="<?=$arResult['IBLOCK']['IBLOCK_TYPE_ID'].'_'.$arResult['IBLOCK']['CODE']?>_form" action="javascript:void(0)">
		<input type="hidden" name="IBLOCK_STRING" value="<?=$arResult['IBLOCK']['STRING']?>" />
		<?foreach($arResult['IBLOCK']['PROPERTIES'] as $arProp) {
			if($arProp['CODE'] == 'OBJECT_ID' || $arProp['CODE'] == 'PRODUCT_ID' || $arProp['CODE'] == 'OFFER_ID' || $arProp['CODE'] == 'PAGE') {?>
				<?if($arProp['CODE'] == 'PRODUCT_ID') {?>
					<input type="hidden" name="PRODUCT_LINK" value="" />
				<?}?>
				<?if($arProp['CODE'] == 'OBJECT_ID') {?>
					<input type="hidden" name="<?=$arProp['CODE']?>" value="<?=$arResult['OFFICE_ID']?>" />
				<?} else {?>
					<input type="hidden" name="<?=$arProp['CODE']?>" value="" />
				<?}?>
			<?} else {?>
				<div class="form-group<?=(!empty($arProp['HINT']) ? ' has-feedback' : '');?>">									
					<?if($arProp['USER_TYPE'] != 'HTML') {?>
						<input type="text" name="<?=$arProp['CODE']?>" class="form-control" placeholder="<?=(!empty($arProp['DEFAULT_VALUE']) ? $arProp['DEFAULT_VALUE'] : $arProp['NAME']);?>" />
					<?} else {?>									
						<textarea name="<?=$arProp['CODE']?>" class="form-control" rows="3" placeholder="<?=(!empty($arProp['DEFAULT_VALUE']['TEXT']) ? $arProp['DEFAULT_VALUE']['TEXT'] : $arProp['NAME']);?>" style="height:<?=$arProp['USER_TYPE_SETTINGS']['height']?>px; min-height:<?=$arProp['USER_TYPE_SETTINGS']['height']?>px; max-height:<?=$arProp['USER_TYPE_SETTINGS']['height']?>px;"></textarea>
					<?}
					if(!empty($arProp['HINT'])) {?>
						<i class="form-control-feedback fv-icon-no-has fa <?=$arProp['HINT']?>"></i>
					<?}?>
				</div>
			<?}
		}
		unset($arProp);
		if($arParams['USER_CONSENT'] == 'Y') {?>
			<input type="hidden" name="USER_CONSENT_ID" value="<?=$arParams['USER_CONSENT_ID']?>" />
			<input type="hidden" name="USER_CONSENT_URL" value="" />
			<div class="form-group form-group-checkbox">
				<div class="checkbox">
					<?$fields = array();
					foreach($arResult['IBLOCK']['PROPERTIES'] as $arProp) {
						if($arProp['CODE'] != 'OBJECT_ID' && $arProp['CODE'] != 'PRODUCT_ID' && $arProp['CODE'] != 'OFFER_ID' && $arProp['CODE'] != 'PAGE' && $arProp['USER_TYPE'] != 'HTML')
							$fields[] = $arProp['NAME'];
					}
					unset($arProp);?>
					<?$APPLICATION->IncludeComponent('bitrix:main.userconsent.request', '',
						array(
							'ID' => $arParams['USER_CONSENT_ID'],
							'INPUT_NAME' => 'USER_CONSENT',
							'IS_CHECKED' => $arParams['USER_CONSENT_IS_CHECKED'],
							'AUTO_SAVE' => 'N',
							'IS_LOADED' => $arParams['USER_CONSENT_IS_LOADED'],
							'REPLACE' => array(
								'button_caption' => GetMessage('FORMS_SLIDE_PANEL_SUBMIT'),
								'fields' => $fields
							)
						),
						$component
					);?>
					<?unset($fields);?>
				</div>
			</div>
		<?}
		if($arParams['USE_CAPTCHA'] == 'Y') {?>
			<div class="form-group captcha">
				<div class="pic" style="display:none;">								
					<img src="" width="100" height="36" alt="CAPTCHA" />
				</div>							
				<input type="text" maxlength="5" name="CAPTCHA_WORD" class="form-control" placeholder="<?=Loc::getMessage('FORMS_SLIDE_PANEL_CAPTCHA_WORD')?>" />
				<input type="hidden" name="CAPTCHA_SID" value="" />
			</div>
		<?}?>		
		<div class="form-group">
			<button type="submit" id="<?=$arResult['IBLOCK']['IBLOCK_TYPE_ID'].'_'.$arResult['IBLOCK']['CODE']?>_btn" class="btn btn-buy"><span><?=Loc::getMessage('FORMS_SLIDE_PANEL_SUBMIT')?></span></button>
		</div>		
	</form>
	<div id="<?=$arResult['IBLOCK']['IBLOCK_TYPE_ID'].'_'.$arResult['IBLOCK']['CODE']?>_alert" class="alert"></div>
</div>

<script type="text/javascript">
	BX.ready(function() {
		var form = BX('<?=$arResult["IBLOCK"]["IBLOCK_TYPE_ID"]."_".$arResult["IBLOCK"]["CODE"]?>_form'),
			btn = BX('<?=$arResult["IBLOCK"]["IBLOCK_TYPE_ID"]."_".$arResult["IBLOCK"]["CODE"]?>_btn'),
			alert = BX('<?=$arResult["IBLOCK"]["IBLOCK_TYPE_ID"]."_".$arResult["IBLOCK"]["CODE"]?>_alert'),			
			userConsent = false,
			useCaptcha = false;

		if(!!form) {	
			<?$arProps = array();
			foreach($arResult['IBLOCK']['PROPERTIES'] as $arProp) {
				if($arProp['CODE'] != 'OBJECT_ID' && $arProp['CODE'] != 'PRODUCT_ID' && $arProp['CODE'] != 'OFFER_ID' && $arProp['CODE'] != 'PAGE' && $arProp['USER_TYPE'] != 'HTML')
					$arProps[] = $arProp['CODE'];
				//MASK//
				if($arProp['CODE'] == 'PHONE' && !empty($arParams['PHONE_MASK'])) {?>
					var inputPhone = form.querySelector('[name="<?=$arProp["CODE"]?>"]');
					if(!!inputPhone)
						$(inputPhone).inputmask('<?=$arParams["PHONE_MASK"]?>');
				<?}
			}
			unset($arProp);
			
			if($arParams['USER_CONSENT'] == 'Y') {?>
				userConsent = true;
				var userConsentUrl = form.querySelector('[name="USER_CONSENT_URL"]');
				if(!!userConsentUrl)
					userConsentUrl.value = window.location.href;
				BX.UserConsent.load(form);
			<?}
			
			if($arParams['USE_CAPTCHA'] == 'Y') {?>
				useCaptcha = true;
				var captchaImg = form.querySelector('[alt="CAPTCHA"]'),
					captchaWord = form.querySelector('[name="CAPTCHA_WORD"]'),
					captchaSid = form.querySelector('[name="CAPTCHA_SID"]');
			<?}?>

			var props = <?=CUtil::PhpToJSObject($arProps)?>;
			<?unset($arProps);?>

			if(props.length > 0 || !!useCaptcha) {
				BX.ajax.post(
					'<?=$componentPath?>/get.php',
					{
						'getCaptcha': !!useCaptcha ? 'Y' : 'N',
						'props': props.length > 0 ? props : 0
					},
					function(response) {							
						if(!!response) {					
							var result = JSON.parse(response);					
							
							<?foreach($arResult['IBLOCK']['PROPERTIES'] as $arProp) {
								if($arProp['CODE'] != 'OBJECT_ID' && $arProp['CODE'] != 'PRODUCT_ID' && $arProp['CODE'] != 'OFFER_ID' && $arProp['CODE'] != 'PAGE' && $arProp['USER_TYPE'] != 'HTML') {?>
									var formInput = form.querySelector('[name="<?=$arProp["CODE"]?>"]');
									if(!!formInput && !!result.<?=$arProp["CODE"]?> && result.<?=$arProp["CODE"]?>.length > 0)
										formInput.value = result.<?=$arProp["CODE"]?>;
								<?}
							}
							unset($arProp);?>
							
							if(!!useCaptcha && !!result.captcha) {
								if(!!captchaImg) {
									BX.adjust(captchaImg, {props: {src: '/bitrix/tools/captcha.php?captcha_sid=' + result.captcha}});
									captchaImg.parentNode.style.display = '';
								}
								if(!!captchaWord)
									captchaWord.value = '';
								if(!!captchaSid)
									captchaSid.value = result.captcha;
							}
						}
					}
				);
			}

			var fields = {};

			<?foreach($arResult['IBLOCK']['PROPERTIES'] as $arProp) {
				if($arProp['CODE'] != 'OBJECT_ID' && $arProp['CODE'] != 'PRODUCT_ID' && $arProp['CODE'] != 'OFFER_ID' && $arProp['CODE'] != 'PAGE') {?>
					fields.<?=$arProp['CODE']?> = {
						row: '.form-group',
						validators: {								
							<?if($arProp['IS_REQUIRED'] == 'Y') {?>
								notEmpty: {
									message: '<?=Loc::getMessage("FORMS_SLIDE_PANEL_NOT_EMPTY_INVALID")?>'
								},
							<?}
							if($arProp['CODE'] == 'PHONE' && !empty($arParams['VALIDATE_PHONE_MASK'])) {?>
								regexp: {
									message: '<?=Loc::getMessage("FORMS_SLIDE_PANEL_REGEXP_INVALID")?>',										
									regexp: <?=$arParams['VALIDATE_PHONE_MASK']?>
								},
							<?}?>
						}
					};
				<?}
			}
			unset($arProp);?>

			if(!!userConsent) {
				fields.USER_CONSENT = {
					row: '.form-group',
					validators: {
						notEmpty: {
							message: '<?=Loc::getMessage("FORMS_SLIDE_PANEL_USER_CONSENT_NOT_EMPTY_INVALID")?>'
						}
					}
				};
			}

			if(!!useCaptcha) {
				fields.CAPTCHA_WORD = {
					row: '.form-group',
					validators: {
						notEmpty: {
							message: '<?=Loc::getMessage("FORMS_SLIDE_PANEL_NOT_EMPTY_INVALID")?>'
						},
						remote: {
							type: 'POST',
							url: '<?=$componentPath?>/check_captcha.php',
							message: '<?=Loc::getMessage("FORMS_SLIDE_PANEL_CAPTCHA_WRONG")?>',
							data: function() {
								return {
									CAPTCHA_SID: captchaSid.value
								};
							},
							delay: 1000
						}
					}
				};
			}
				
			//VALIDATION//
			$(form).formValidation({
				framework: 'bootstrap',
				icon: {
					valid: 'icon-ok-b',
					invalid: 'icon-close-b',
					validating: 'icon-repeat-b'
				},			
				fields: fields
			});

			//REVALIDATE_USER_CONSENT_FIELD//
			if(!!userConsent) {
				BX.addCustomEvent(form, 'OnFormInputUserConsentChange', function() {
					$(form).formValidation('revalidateField', 'USER_CONSENT');
				});
			}
			
			//SEND_FORM//
			$(form).on('success.form.fv', function(e) {			
				e.preventDefault();
					
				var $form = $(e.target);			
					
				//AJAX//
				$.ajax({
					url: '<?=$componentPath?>/ajax.php',
					type: 'POST',
					data: $form.serialize() + '&SITE_ID=<?=SITE_ID?>',
					dataType: 'json',
					success: function(response) {						
						//CLEAR_FORM//
						$form.formValidation('resetForm', false);
						
						if(!!response.status) {
							//DISABLE_FORM_INPUTS//
							<?foreach($arResult['IBLOCK']['PROPERTIES'] as $arProp) {
								if($arProp['CODE'] != 'OBJECT_ID' && $arProp['CODE'] != 'PRODUCT_ID' && $arProp['CODE'] != 'OFFER_ID' && $arProp['CODE'] != 'PAGE') {?>
									var formInput = form.querySelector('[name="<?=$arProp["CODE"]?>"]');
									if(!!formInput)
										BX.adjust(formInput, {props: {disabled: true}});
								<?}
							}
							unset($arProp);?>
							if(!!userConsent) {							
								var formUserConsentInput = form.querySelector('[name="USER_CONSENT"]');
								if(!!formUserConsentInput)
									BX.adjust(formUserConsentInput, {props: {disabled: true}});
							}
							if(!!useCaptcha && !!captchaWord)
								BX.adjust(captchaWord, {props: {disabled: true}});
							
							//SUBMITTED//
							if(!!btn)
								BX.adjust(btn, {
									props: {
										disabled: true
									},
									html: '<i class="icon-ok-b"></i><span><?=Loc::getMessage("FORMS_SLIDE_PANEL_SUBMITTED")?></span>'
								});
							
							//SHOW_MESSAGE//
							if(!!alert)
								BX.adjust(alert, {
									props: {
										className: 'alert alert-success'
									},
									style: {
										display: 'block'
									},
									html: '<?=Loc::getMessage("FORMS_SLIDE_PANEL_ALERT_SUCCESS")?>'
								});
						} else {
							//SUBMIT//
							if(!!btn)
								BX.adjust(btn, {
									props: {
										disabled: false
									},
									html: '<?=Loc::getMessage("FORMS_SLIDE_PANEL_SUBMIT")?>'
								});
							
							//SHOW_MESSAGE//
							if(!!alert)
								BX.adjust(alert, {
									props: {
										className: 'alert alert-error'
									},
									style: {
										display: 'block'
									},
									html: '<?=Loc::getMessage("FORMS_SLIDE_PANEL_ALERT_WARNING")?>'
								});
						}
						
						//CAPTCHA//
						if(!!useCaptcha && !!response.captcha_code) {
							if(!!captchaImg)
								BX.adjust(captchaImg, {attrs: {src: '/bitrix/tools/captcha.php?captcha_sid=' + response.captcha_code}});
							if(!!captchaWord)
								captchaWord.value = '';
							if(!!captchaSid)
								captchaSid.value = response.captcha_code;
						}
					}
				});
			});
		}
	});
</script>