<?
$MESS['FORMS_SLIDE_PANEL_CAPTCHA_WORD'] = "Введите код";
$MESS['FORMS_SLIDE_PANEL_CAPTCHA_WRONG'] = "Неверный код";
$MESS['FORMS_SLIDE_PANEL_SUBMIT'] = "Отправить";
$MESS['FORMS_SLIDE_PANEL_SUBMITTED'] = "Отправлено";
$MESS['FORMS_SLIDE_PANEL_NOT_EMPTY_INVALID'] = "Заполните поле";
$MESS['FORMS_SLIDE_PANEL_USER_CONSENT_NOT_EMPTY_INVALID'] = "Согласитесь с условиями";
$MESS['FORMS_SLIDE_PANEL_REGEXP_INVALID'] = "Неверный формат";
$MESS['FORMS_SLIDE_PANEL_ALERT_SUCCESS'] = "Ваше сообщение отправлено<br />Мы постараемся ответить вам максимально быстро";
$MESS['FORMS_SLIDE_PANEL_ALERT_WARNING'] = "Ошибка! Ваше сообщение не отправлено";