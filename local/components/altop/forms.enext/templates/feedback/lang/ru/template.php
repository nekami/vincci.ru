<?
$MESS['FORMS_FEEDBACK_CAPTCHA_WORD'] = "Введите код";
$MESS['FORMS_FEEDBACK_CAPTCHA_WRONG'] = "Неверный код";
$MESS['FORMS_FEEDBACK_SUBMIT'] = "Отправить сообщение";
$MESS['FORMS_FEEDBACK_SUBMITTED'] = "Отправлено";
$MESS['FORMS_FEEDBACK_NOT_EMPTY_INVALID'] = "Заполните поле";
$MESS['FORMS_FEEDBACK_USER_CONSENT_NOT_EMPTY_INVALID'] = "Согласитесь с условиями";
$MESS['FORMS_FEEDBACK_REGEXP_INVALID'] = "Неверный формат";
$MESS['FORMS_FEEDBACK_ALERT_SUCCESS'] = "Ваше сообщение отправлено<br />Мы постараемся ответить вам максимально быстро";
$MESS['FORMS_FEEDBACK_ALERT_WARNING'] = "Ошибка! Ваше сообщение не отправлено";