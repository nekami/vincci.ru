<?
$MESS["QUICK_ORDER_AJAX_ORDER_PAYER_NAME"] = "Гость";
$MESS["QUICK_ORDER_AJAX_ORDER_COMMENT"] = "Быстрое оформление заказа";
$MESS["QUICK_ORDER_AJAX_EVENT_NAME"] = "Новый заказ (объект)";
$MESS["QUICK_ORDER_AJAX_EVENT_DESCRIPTION"] = "#ORDER_ID# - код заказа\n#ORDER_REAL_ID# - реальный ID заказа\n#ORDER_DATE# - дата заказа\n#PRICE# - сумма заказа\n#ORDER_LIST# - состав заказа\n#NAME# - имя заказчика\n#PHONE# - телефон заказчика\n#OBJECT_PHONE_SMS# - телефон объекта для SMS уведомлений\n#OBJECT_EMAIL_EMAIL - Email объекта для Email уведомлений#\n#BCC# - Email скрытой копии\n#SALE_EMAIL# - Email отдела продаж\n";
$MESS["QUICK_ORDER_AJAX_EVENT_MESSAGE_SUBJECT"] = "Новый заказ №#ORDER_ID# от #ORDER_DATE# с сайта \"#SITE_NAME#\"";
$MESS["QUICK_ORDER_AJAX_EVENT_MESSAGE_MESSAGE"] = "Стоимость заказа: #PRICE#<br/><br/>Состав заказа:<br/>#ORDER_LIST#<br/><br/>Заказчик:<br/>Имя: #NAME#<br/>Телефон: #PHONE#<br/><br/>Сообщение сгенерировано автоматически.";
$MESS["QUICK_ORDER_AJAX_ORDER_CREATE_SUCCESS"] = "Ваш заказ №#ORDER_ID# от #ORDER_DATE# успешно создан.<br />В ближайшее время наш менеджер свяжется с вами.";
$MESS["QUICK_ORDER_AJAX_ORDER_CREATE_ERROR"] = "Ошибка создания заказа";
?>