(function() {
	'use strict';

	if(!!window.JCQuickOrderComponent)
		return;

	window.JCQuickOrderComponent = function(params) {
		this.componentPath = params.componentPath || '';
		this.props = params.jsProps || '';
		this.phoneMask = params.phoneMask || '';
		this.validatePhoneMask = params.validatePhoneMask || '';
		this.userConsent = params.userConsent || '';
		this.useCaptcha = params.useCaptcha || '';
		this.quantityId = params.quantityId || '';
		this.basketPropsId = params.basketPropsId || '';
		this.basketSkuProps = params.basketSkuProps || '';
		this.container = BX(params.container);
		
		BX.ready(BX.delegate(this.init, this));
	};

	window.JCQuickOrderComponent.prototype =	{		
		init: function() {
			this.form = this.container.querySelector('[data-entity="quickOrderForm"]');
			this.alert = this.container.querySelector('[data-entity="quickOrderAlert"]');
			
			if(!!this.props && !!this.phoneMask) {
				for(var i in this.props) {
					if(this.props.hasOwnProperty(i)) {
						if(this.props[i].CODE == 'PHONE') {
							var inputPhone = this.form.querySelector('[name="' + this.props[i].CODE + '"]');
							if(!!inputPhone)
								$(inputPhone).inputmask(this.phoneMask);
						}
					}
				}
			}
			
			if(!!this.userConsent) {
				var inputUserConsent = this.form.querySelector('[name="USER_CONSENT"]'),
					inputUserConsentUrl = this.form.querySelector('[name="USER_CONSENT_URL"]');
				
				if(!!inputUserConsentUrl)
					inputUserConsentUrl.value = window.location.href;

				BX.UserConsent.load(this.form);
			}

			if(!!this.useCaptcha) {
				var captchaImg = this.form.querySelector('[alt="CAPTCHA"]'),
					captchaWord = this.form.querySelector('[name="CAPTCHA_WORD"]'),
					captchaSid = this.form.querySelector('[name="CAPTCHA_SID"]');
			}
			
			if(!!this.props || !!this.useCaptcha) {
				var showFormHiddenInputs = false;
				BX.ajax({
					url: this.componentPath + '/ajax.php',
					method: 'POST',
					dataType: 'json',
					timeout: 60,
					data: {							
						action: 'getCaptchaProps',
						getCaptcha: !!this.useCaptcha ? true : false,
						props: !!this.props ? this.props : false
					},
					onsuccess: BX.delegate(function(result) {
						if(!!this.props) {
							for(var i in this.props) {
								if(this.props.hasOwnProperty(i)) {
									var formInput = this.form.querySelector('[name="' + this.props[i].CODE + '"]');
									if(!!formInput && !!result[this.props[i].CODE] && result[this.props[i].CODE].length > 0) {
										formInput.value = result[this.props[i].CODE];
										if(!showFormHiddenInputs)
											showFormHiddenInputs = true;
									}
								}
							}
						}
						
						if(!!showFormHiddenInputs) {
							var formHiddenInputs = this.form.querySelectorAll('.form-group-hidden');
							if(!!formHiddenInputs) {
								for(var i in formHiddenInputs) {
									if(formHiddenInputs.hasOwnProperty(i) && BX.type.isDomNode(formHiddenInputs[i])) {
										BX.removeClass(formHiddenInputs[i], 'form-group-hidden');
									}
								}
							}
						}
						
						if(!!this.useCaptcha && !!result.captcha) {
							if(!!captchaImg) {
								BX.adjust(captchaImg, {props: {src: '/bitrix/tools/captcha.php?captcha_sid=' + result.captcha}});
								captchaImg.parentNode.style.display = '';
							}
							if(!!captchaWord)
								captchaWord.value = '';
							if(!!captchaSid)
								captchaSid.value = result.captcha;
						}
					}, this)
				});
			}
			
			var fields = {};
			
			if(!!this.props) {
				for(var i in this.props) {
					if(this.props.hasOwnProperty(i)) {
						var formInput = this.form.querySelector('[name="' + this.props[i].CODE + '"]');
						if(!!formInput) {
							fields[this.props[i].CODE] = {
								row: '.form-group',
								validators: {}
							};

							if(this.props[i].REQUIRED == 'Y') {
								fields[this.props[i].CODE].validators.notEmpty = {
									message: BX.message('QUICK_ORDER_NOT_EMPTY_INVALID')
								};
							}
							
							if(this.props[i].CODE == 'PHONE' && !!this.validatePhoneMask) {
								fields[this.props[i].CODE].validators.regexp = {
									message: BX.message('QUICK_ORDER_REGEXP_INVALID'),
									regexp: this.validatePhoneMask
								};
							} else if(this.props[i].CODE == 'EMAIL') {
								fields[this.props[i].CODE].validators.emailAddress = {
									message: BX.message('QUICK_ORDER_EMAIL_ADDRESS_INVALID')
								};
							}
						}
					}
				}
			}
			
			if(!!this.userConsent && !!inputUserConsent) {
				fields.USER_CONSENT = {
					row: '.form-group',
					validators: {
						notEmpty: {
							message: BX.message('QUICK_ORDER_USER_CONSENT_NOT_EMPTY_INVALID')
						}
					}
				};
			}

			if(!!this.useCaptcha && !!captchaWord) {
				fields.CAPTCHA_WORD = {
					row: '.form-group',
					validators: {
						notEmpty: {
							message: BX.message('QUICK_ORDER_NOT_EMPTY_INVALID')
						},
						remote: {
							type: 'POST',
							url: this.componentPath + '/ajax.php',
							message: BX.message('QUICK_ORDER_CAPTCHA_WRONG'),
							data: function() {
								return {
									action: 'checkCaptcha',
									CAPTCHA_SID: !!captchaSid ? captchaSid.value : ''
								};
							},
							delay: 1000
						}
					}
				};
			}
			
			$(this.form).formValidation({
				framework: 'bootstrap',
				icon: {
					valid: 'icon-ok-b',
					invalid: 'icon-close-b',
					validating: 'icon-repeat-b'
				},
				fields: fields				
			});
				
			if(!!this.userConsent && !!inputUserConsent) {
				BX.addCustomEvent(this.form, 'OnFormInputUserConsentChange', BX.delegate(function() {
					$(this.form).formValidation('revalidateField', 'USER_CONSENT');
				}, this));
			}
			
			$(this.form).on('success.field.fv', BX.delegate(function(e) {
				e.preventDefault();
				
				var input = e.target,
					inputName = input.getAttribute('name');

				if(inputName == 'PHONE') {
					var formHiddenInputs = this.form.querySelectorAll('.form-group-hidden');
					if(!!formHiddenInputs) {
						for(var i in formHiddenInputs) {
							if(formHiddenInputs.hasOwnProperty(i) && BX.type.isDomNode(formHiddenInputs[i])) {
								BX.removeClass(formHiddenInputs[i], 'form-group-hidden');
							}
						}
					}
				}
			}, this));
			
			$(this.form).on('success.form.fv', BX.delegate(function() {
				var data = {
					action: 'createOrder',
					siteId: BX.message('SITE_ID'),
					siteCharset: BX.message('SITE_CHARSET'),
					siteServerName: BX.message('SITE_SERVER_NAME'),
					languageId: BX.message('LANGUAGE_ID')
				};
				
				var propCollection = this.form.querySelectorAll('input, textarea');
				if(!!propCollection) {
					for(var i in propCollection) {
						if(propCollection.hasOwnProperty(i) && BX.type.isDomNode(propCollection[i])) {
							data[propCollection[i].name] = propCollection[i].value;
						}
					}
				}

				var obQuantity = BX(this.quantityId);
				if(!!obQuantity)
					data['quantity'] = obQuantity.value;

				var obBasketProps = BX(this.basketPropsId);
				if(!!obBasketProps) {
					var propCollection = obBasketProps.getElementsByTagName('input');
					if(propCollection && propCollection.length) {
						for(i = 0; i < propCollection.length; i++) {
							if(!propCollection[i].disabled) {
								switch(propCollection[i].type.toLowerCase()) {
									case 'hidden':
										data[propCollection[i].name] = propCollection[i].value;
										break;
									case 'radio':
										if(propCollection[i].checked)
											data[propCollection[i].name] = propCollection[i].value;
										break;
									default:
										break;
								}
							}
						}
					}
				}

				if(!!this.basketSkuProps)
					data['basket_props'] = this.basketSkuProps;
				
				BX.ajax({
					url: this.componentPath + '/ajax.php',
					method: 'POST',
					dataType: 'json',
					timeout: 60,
					data: data,
					onsuccess: BX.delegate(function(result) {
						$(this.form).formValidation('resetForm', false);
						
						if(!!result.status) {
							BX.remove(this.form);

							if(!!this.alert && !!result.text) {
								BX.adjust(this.alert, {
									props: {
										className: 'alert alert-success alert-show'
									},
									html: result.text
								});
								
								$("#basket_form_container").html("<span class='alert alert-success alert-show'>" + result.text + "</span>");
							}
						} else {
							if(!!this.alert && !!result.text) {
								BX.adjust(this.alert, {
									props: {
										className: 'alert alert-error alert-show'
									},
									html: result.text
								});
							}
						}
						
						if(!!this.useCaptcha && !!result.captcha_code) {
							if(!!captchaImg)
								BX.adjust(captchaImg, {attrs: {src: '/bitrix/tools/captcha.php?captcha_sid=' + result.captcha_code}});
							if(!!captchaWord)
								captchaWord.value = '';
							if(!!captchaSid)
								captchaSid.value = result.captcha_code;
						}
					}, this)
				});
			}, this));
		}
	}
})();