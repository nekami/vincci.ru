<?
$MESS["QUICK_ORDER_TEMPLATE_TITLE"] = "Быстрый заказ";
$MESS["QUICK_ORDER_TEMPLATE_NAME"] = "Имя";
$MESS["QUICK_ORDER_TEMPLATE_PHONE"] = "Телефон";
$MESS["QUICK_ORDER_TEMPLATE_EMAIL"] = "Email";
$MESS["QUICK_ORDER_TEMPLATE_COMMENTS"] = "Комментарии к заказу";
$MESS["QUICK_ORDER_TEMPLATE_CAPTCHA_WORD"] = "Введите код";
$MESS["QUICK_ORDER_TEMPLATE_SUBMIT"] = "Оформить заказ";
$MESS["QUICK_ORDER_TEMPLATE_SUBMITTED"] = "Оформлено";
$MESS["QUICK_ORDER_TEMPLATE_NOT_EMPTY_INVALID"] = "Заполните поле";
$MESS["QUICK_ORDER_TEMPLATE_REGEXP_INVALID"] = "Неверный формат";
$MESS["QUICK_ORDER_TEMPLATE_EMAIL_ADDRESS_INVALID"] = "Некорректный email";
$MESS["QUICK_ORDER_TEMPLATE_USER_CONSENT_NOT_EMPTY_INVALID"] = "Согласитесь с условиями";
$MESS["QUICK_ORDER_TEMPLATE_CAPTCHA_WRONG"] = "Неверный код";
?>