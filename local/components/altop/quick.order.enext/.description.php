<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("QUICK_ORDER_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("QUICK_ORDER_COMPONENT_DESCRIPTION"),
	"ICON" => "/images/icon.gif",	
	"PATH" => array(
		"ID" => "altop",
		"NAME" => GetMessage('QUICK_ORDER_COMPONENT_PATH_NAME'),		
	),
	"COMPLEX" => "N"
);