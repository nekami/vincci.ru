<?define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader,
	Bitrix\Main\Config\Option,	
	Bitrix\Main\Localization\Loc,	
	Bitrix\Main\Web\Cookie,
	Bitrix\Iblock\Component\Base,
	Bitrix\Sale;

if(!Loader::includeModule("iblock") || !Loader::includeModule("catalog") || !Loader::includeModule("sale"))
	return;

Loc::loadMessages(__FILE__);

global $APPLICATION;

$context = Bitrix\Main\Application::getInstance()->getContext();
$request = $context->getRequest();

if($request->isAjaxRequest()) {
	$action = $request->get("action");
	//GET_CAPTCHA_PROPS//
	if($action == "getCaptchaProps") {
		$getCaptcha = $request->get("getCaptcha");
		if($getCaptcha)
			$result["captcha"] = $APPLICATION->CaptchaGetCode();
		
		$props = $request->get("props");
		if(!empty($props)) {
			foreach($props as $arProp) {
				$result[$arProp["CODE"]] = $request->getCookie("ENEXT_FORMS_".$arProp["CODE"]);
			}
			unset($arProp);
		}
		
		Base::sendJsonAnswer($result);
	//CHECK_CAPTCHA//
	} elseif($action == "checkCaptcha") {
		$resp = CEnext::CheckCaptchaCode($request->get("CAPTCHA_WORD"), $request->get("CAPTCHA_SID"));
		
		Base::sendJsonAnswer(array(
			"valid" => $resp
		));
	//CREATE_ORDER//	
	} elseif($action == "createOrder") {
		$siteId = $request->get("siteId") ?: SITE_ID;
		$siteCharset = $request->get("siteCharset") ?: SITE_CHARSET;
		$siteServerName = $request->get("siteServerName") ?: SITE_SERVER_NAME;
		$languageId = $request->get("languageId") ?: LANGUAGE_ID;
		
		$mode = $request->get("MODE");

		//USER_CONSENT//
		$userConsent = $request->get("USER_CONSENT");
		if($userConsent == "Y") {
			$userConsentId = (int)$request->get("USER_CONSENT_ID");
			$userConsentUrl = $request->get("USER_CONSENT_URL");
			Bitrix\Main\UserConsent\Consent::addByContext($userConsentId, null, null, array("URL" => $userConsentUrl));
		}
		
		//CAPTCHA//
		$captchaSid = $request->get("CAPTCHA_SID");
		if(!empty($captchaSid)) {	
			require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");		
			CCaptcha::Delete($captchaSid);
		}
		
		//PROPS//
		$name = $request->get("NAME");
		if(!empty($name)) {
			$name = iconv("UTF-8", $siteCharset, strip_tags(trim($name)));
			$cookie = new Cookie("ENEXT_FORMS_NAME", $name, time() + 32832000);
			$cookie->setDomain($siteServerName);
			$cookie->setHttpOnly(false);
			$context->getResponse()->addCookie($cookie);			
			unset($cookie);
		}
		
		$phone = $request->get("PHONE");
		$phone = iconv("UTF-8", $siteCharset, strip_tags(trim($phone)));
		$cookie = new Cookie("ENEXT_FORMS_PHONE", $phone, time() + 32832000);
		$cookie->setDomain($siteServerName);
		$cookie->setHttpOnly(false);
		$context->getResponse()->addCookie($cookie);			
		unset($cookie);
		
		$context->getResponse()->flush("");

		$email = $request->get("EMAIL");
		if(!empty($email))
			$email = iconv("UTF-8", $siteCharset, strip_tags(trim($email)));

		$comments = $request->get("COMMENTS");
		if(!empty($comments))
			$comments = iconv("UTF-8", $siteCharset, strip_tags(trim($comments)));
		
		//LOCATION//
		$ipAddress = Bitrix\Main\Service\GeoIp\Manager::getRealIp();
		$locCode = Sale\Location\GeoIp::getLocationCode($ipAddress, $languageId);

		//OBJECT//
		$objectId = (int)$request->get("OBJECT_ID");
		if($objectId > 0) {
			$rsElement = CIBlockElement::GetList(array(), array("ID" => $objectId), false, false, array("ID", "IBLOCK_ID", "PROPERTY_PHONE_SMS", "PROPERTY_EMAIL_EMAIL"));
			if($arElement = $rsElement->GetNext()) {
				$objectPhoneSms = $arElement["PROPERTY_PHONE_SMS_VALUE"];
				$objectEmailEmail = $arElement["PROPERTY_EMAIL_EMAIL_VALUE"];
			}
		}
	
		//USER//
		global $USER;
		if(!$USER->IsAuthorized()) { 
//			$rsUser = $USER->GetByLogin($phone);
			$rsUser = CUser::GetByLogin($phone);
			if($arUser = $rsUser->Fetch()) {
				$registeredUserID = $arUser["ID"];
			} else {
				$newEmail = preg_replace("/[^0-9]/", "", $phone)."@".$siteServerName;
				$newPass = randString(10);
				

				$users = new CUser;
				$arFields = Array(
					"LOGIN" => $phone,      
					"NAME" => !empty($name) ? $name : $phone,
					"EMAIL" => !empty($email) ? $email : $newEmail,
					"PASSWORD" => $newPass,
					"CONFIRM_PASSWORD" => $newPass,
					"ACTIVE" => "Y",
					"LID" => $siteId
				);

				$ids = $users->Add($arFields);

				
				$registeredUserID = $ids;//CUser::Add($arFields);
			}
			unset($arUser, $rsUser);
		} else {
			$registeredUserID = $USER->GetID();
		}

	//	return $registeredUserID;

		//BASKET//
		$basketUserID = Sale\Fuser::getId();

		Sale\DiscountCouponsManager::init();

		if($mode == "PRODUCT") {
			$basket = Sale\Basket::loadItemsForFUser($basketUserID, $siteId)->getOrderableItems();
			foreach($basket as $basketItem) {
				CSaleBasket::Delete($basketItem->getId());
			}
			unset($basketItem);

			//PRODUCT_ID//
			$productId = (int)$request->get("PRODUCT_ID");

			//PRODUCT_QUANTITY//
			$productQnt = (float)$request->get("quantity");	
			if($productQnt <= 0) {
				$ratioIterator = CCatalogMeasureRatio::getList(
					array(),
					array("PRODUCT_ID" => $productId),
					false,
					false,
					array("PRODUCT_ID", "RATIO")
				);
				if($ratio = $ratioIterator->Fetch()) {
					$intRatio = (int)$ratio["RATIO"];
					$floatRatio = (float)$ratio["RATIO"];
					$productQnt = $floatRatio > $intRatio ? $floatRatio : $intRatio;
				}
				unset($ratio, $ratioIterator);
			}
			if($productQnt <= 0) {
				$productQnt = 1;
			}
			
			$item = $basket->createItem("catalog", $productId);
			$item->setFields(array(
				"QUANTITY" => $productQnt,
				"CURRENCY" => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
				"LID" => $siteId,
				"PRODUCT_PROVIDER_CLASS" => "CCatalogProductProvider"
			));
			$basket->save();

			//PRODUCT_PROPERTIES//	
			$productProperties = array();	
			$iblockId = (int)CIBlockElement::GetIBlockByID($productId);
			if($iblockId > 0) {
				$productCatalogInfo = CCatalogSku::GetInfoByIBlock($iblockId);
				if(!empty($productCatalogInfo) && $productCatalogInfo["CATALOG_TYPE"] == CCatalogSku::TYPE_PRODUCT) {
					$productCatalogInfo = false;
				}
				if(!empty($productCatalogInfo)) {
					$productIblockId = $productCatalogInfo["CATALOG_TYPE"] == CCatalogSku::TYPE_CATALOG ? $productCatalogInfo["IBLOCK_ID"] : $productCatalogInfo["PRODUCT_IBLOCK_ID"];			
					if($productCatalogInfo["CATALOG_TYPE"] != CCatalogSku::TYPE_OFFERS) {				
						$cartProps = unserialize(base64_decode(strtr($request->get("CART_PROPERTIES"), "-_,", "+/=")));				
						if(!empty($cartProps)) {					
							$productPropsVar = $request->get("PRODUCT_PROPS_VARIABLE");
							$productProps = !empty($productPropsVar) ? $request->get($productPropsVar) : "";
							if(is_array($productProps)) {
								$partialProductProps = $request->get("PARTIAL_PRODUCT_PROPERTIES");
								$productProperties = CIBlockPriceTools::CheckProductProperties(
									$productIblockId,
									$productId,
									$cartProps,
									$productProps,
									$partialProductProps === "Y"
								);
							}
						}
					} else {																
						$skuCartProps = $request->get("OFFERS_CART_PROPERTIES") ? unserialize(base64_decode(strtr($request->get("OFFERS_CART_PROPERTIES"), "-_,", "+/="))) : "";				
						$skuAddProps = $request->get("basket_props") ? $request->get("basket_props") : "";
						if(!empty($skuCartProps) || !empty($skuAddProps)) {
							$productProperties = CIBlockPriceTools::GetOfferProperties(
								$productId,
								$productIblockId,
								$skuCartProps,
								$skuAddProps
							);
						}
					}			
				}
			}
			if(!empty($productProperties)) {
				$basketPropertyCollection = $item->getPropertyCollection();
				$basketPropertyCollection->setProperty($productProperties);
				$basketPropertyCollection->save();
			}
		}
		unset($item, $basket);

		//CREATE_ORDER//
		$newOrder = Sale\Order::create($siteId, $registeredUserID);//$registeredUserID);

		//PERSON_TYPE//
		$arPersonTypes = Sale\PersonType::load($siteId);
		reset($arPersonTypes);
		$arPersonType = current($arPersonTypes);
		$newOrder->setPersonTypeId(!empty($arPersonType) ? $arPersonType["ID"] : 1);
		unset($arPersonType, $arPersonTypes);

		//ORDER_SET_BASKET//
		$basket = Sale\Basket::loadItemsForFUser($basketUserID, $siteId)->getOrderableItems();
		$newOrder->setBasket($basket);
		unset($basket);
		
		$newOrder->doFinalAction(true);

		//ORDER_SET_PROPERTIES//
		$propertyCollection = $newOrder->getPropertyCollection();
		
		$nameProp = $propertyCollection->getPayerName();
		if(!empty($name))
			$nameProp->setValue($name);
		elseif($USER->IsAuthorized())
			$nameProp->setValue($USER->GetFullName());
		else
			$nameProp->setValue(Loc::getMessage("QUICK_ORDER_AJAX_ORDER_PAYER_NAME"));
		
		$phoneProp = $propertyCollection->getPhone();
		$phoneProp->setValue($phone);

		if(!empty($email)) {
			$emailProp = $propertyCollection->getUserEmail();
			$emailProp->setValue($email);
		}
	
		$locProp = $propertyCollection->getDeliveryLocation();
		$locProp->setValue($locCode);
		
		//ORDER_SET_FIELDS//
		$newOrder->setField("CURRENCY", Option::get("sale", "default_currency"));
		if(!empty($comments))
			$newOrder->setField("USER_DESCRIPTION", $comments);
		$newOrder->setField("COMMENTS", Loc::getMessage("QUICK_ORDER_AJAX_ORDER_COMMENT"));

		$orderResult = $newOrder->save();
	
		$orderId = $newOrder->GetId();
		if($orderId > 0) {
			//OBJECT//
			if(!empty($objectPhoneSms) || !empty($objectEmailEmail)) {
				$basketList = "";
				$basket = $newOrder->getBasket();
				if($basket) {
					$basketTextList = $basket->getListOfFormatText();
					if(!empty($basketTextList)) {
						foreach($basketTextList as $basketItemCode => $basketItemData) {
							$basketList .= $basketItemData."<br/>";
						}
					}
				}
				
				$fields = array(
					"ORDER_ID" => $newOrder->getField("ACCOUNT_NUMBER"),
					"ORDER_REAL_ID" => $newOrder->getField("ID"),
					"ORDER_DATE" => $newOrder->getDateInsert()->toString(),				
					"PRICE" => SaleFormatCurrency($newOrder->getPrice(), $newOrder->getCurrency()),
					"ORDER_LIST" => $basketList,
					"NAME" => $USER->IsAuthorized() ? $USER->GetFullName() : Loc::getMessage("QUICK_ORDER_AJAX_ORDER_PAYER_NAME"),
					"PHONE" => $phone,
					"OBJECT_PHONE_SMS" => !empty($objectPhoneSms) ? $objectPhoneSms : "",
					"OBJECT_EMAIL_EMAIL" => !empty($objectEmailEmail) ? $objectEmailEmail : "",
					"BCC" => Option::get("sale", "order_email"),
					"SALE_EMAIL" => Option::get("sale", "order_email")
				);

				$eventName = "SALE_NEW_ORDER_OBJECT";
				
				//EVENT_TYPE//
				$arEvent = CEventType::GetByID($eventName, Sale\Notify::getOrderLanguageId($newOrder))->Fetch();
				if(empty($arEvent)) {
					$et = new CEventType;
					$arEventFields = array(
						"LID" => Sale\Notify::getOrderLanguageId($newOrder),
						"EVENT_NAME" => $eventName,
						"NAME" => Loc::getMessage("QUICK_ORDER_AJAX_EVENT_NAME"),
						"DESCRIPTION" => Loc::getMessage("QUICK_ORDER_AJAX_EVENT_DESCRIPTION")
					);
					$et->Add($arEventFields);		
				}

				//EVENT_MESSAGE//
				$arMess = CEventMessage::GetList($by = "site_id", $order = "desc", array("TYPE_ID" => $eventName))->Fetch();
				if(empty($arMess)) {
					$em = new CEventMessage;
					$arMess = array();
					$arMess["ID"] = $em->Add(
						array(
							"ACTIVE" => "Y",
							"EVENT_NAME" => $eventName,
							"LID" => $newOrder->getField("LID"),
							"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
							"EMAIL_TO" => "#OBJECT_EMAIL_EMAIL#",
							"BCC" => "#BCC#",
							"SUBJECT" => Loc::getMessage("QUICK_ORDER_AJAX_EVENT_MESSAGE_SUBJECT"),
							"BODY_TYPE" => "html",
							"MESSAGE" => Loc::getMessage("QUICK_ORDER_AJAX_EVENT_MESSAGE_MESSAGE")
						)
					);		
				}

				//EVENT_SEND//
				Bitrix\Main\Mail\Event::send(array(
					"EVENT_NAME" => $eventName,
					"LID" => $newOrder->getField("LID"),
					"C_FIELDS" => $fields
				));
			}
			
			//MESSAGE//
			$result = array(
				"status" => true,
				"text" => Loc::getMessage("QUICK_ORDER_AJAX_ORDER_CREATE_SUCCESS", array("#ORDER_DATE#" => $newOrder->getDateInsert()->toString(), "#ORDER_ID#" => $newOrder->getField("ACCOUNT_NUMBER"))),
				"captcha_code" => false
			);

			unset($newOrder);
		} else {
			//MESSAGE//
			$errors = $orderResult->getErrors();
			$result = array(
				"status" => false,
				"text" => Loc::getMessage("QUICK_ORDER_AJAX_ORDER_CREATE_ERROR"),//implode($errors, ';').' ---- '.$registeredUserID, //Loc::getMessage("QUICK_ORDER_AJAX_ORDER_CREATE_ERROR"),
				"captcha_code" => !empty($captchaSid) ? $APPLICATION->CaptchaGetCode() : false
			);
		}
		unset($orderId);
		
		Base::sendJsonAnswer($result);
	}
} 