<?
$MESS["CP_SBB_TPL_PROPERTIES_RECALCULATE_BASKET"] = "Свойства, влияющие на пересчет корзины";
$MESS["CP_SBB_TPL_USE_ENHANCED_ECOMMERCE"] = "Отправлять данные электронной торговли в Google и Яндекс";
$MESS["USE_ENHANCED_ECOMMERCE_TIP"] = "Требуется дополнительная настройка в Google Analytics Enhanced 
Ecommerce и/или Яндекс.Метрике";
$MESS["CP_SBB_TPL_DATA_LAYER_NAME"] = "Имя контейнера данных";
$MESS["CP_SBB_TPL_BRAND_PROPERTY"] = "Свойство брендов";
?>