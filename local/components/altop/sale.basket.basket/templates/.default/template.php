r<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$this->addExternalCss(SITE_TEMPLATE_PATH."/js/owlCarousel/owl.carousel.css");
$this->addExternalJS(SITE_TEMPLATE_PATH."/js/owlCarousel/owl.carousel.min.js");

$curPage = $APPLICATION->GetCurPage()."?".$arParams["ACTION_VARIABLE"]."=";
$arUrls = array(
	"delete" => $curPage."delete&id=#ID#",
	"delay" => $curPage."delay&id=#ID#",
	"add" => $curPage."add&id=#ID#",
	"clear" => $curPage."clear",
	"clearDelay" => $curPage."clearDelay"
);
unset($curPage);

$arParams["USE_ENHANCED_ECOMMERCE"] = isset($arParams["USE_ENHANCED_ECOMMERCE"]) && $arParams["USE_ENHANCED_ECOMMERCE"] === "Y" ? "Y" : "N";
$arParams["DATA_LAYER_NAME"] = isset($arParams["DATA_LAYER_NAME"]) ? trim($arParams["DATA_LAYER_NAME"]) : "dataLayer";
$arParams["BRAND_PROPERTY"] = isset($arParams["BRAND_PROPERTY"]) ? trim($arParams["BRAND_PROPERTY"]) : "";

$signer = new Bitrix\Main\Security\Sign\Signer;
$signedTemplate = $signer->sign($templateName, "sale.basket.basket");
$signedParams = $signer->sign(base64_encode(serialize($arParams)), "sale.basket.basket");

$arBasketJSParams = array(
	"DISABLE_BASKET" => $arParams["DISABLE_BASKET"],
	"SALE_TOTAL_DISCOUNT" => GetMessage("SALE_TOTAL_DISCOUNT"),
	"SALE_DELETE" => GetMessage("SALE_DELETE"),
	"SALE_DELAY" => GetMessage("SALE_DELAY"),
	"SALE_TYPE" => GetMessage("SALE_TYPE"),
	"SITE_DIR" => SITE_DIR,
	"DELETE_URL" => $arUrls["delete"],
	"DELAY_URL" => $arUrls["delay"],
	"ADD_URL" => $arUrls["add"],
	"EVENT_ONCHANGE_ON_START" => (!empty($arResult["EVENT_ONCHANGE_ON_START"]) && $arResult["EVENT_ONCHANGE_ON_START"] === "Y") ? "Y" : "N",
	"USE_ENHANCED_ECOMMERCE" => $arParams["USE_ENHANCED_ECOMMERCE"],
	"DATA_LAYER_NAME" => $arParams["DATA_LAYER_NAME"],
	"BRAND_PROPERTY" => $arParams["BRAND_PROPERTY"],
	"SIGNED_TEMPLATE" => CUtil::JSEscape($signedTemplate),
	"SIGNED_PARAMS" => CUtil::JSEscape($signedParams)
);?>

<script>
	BX.message({
		MEASURE_PC: '<?=GetMessageJS("SALE_MEASURE_PC");?>',
		MEASURE_SQ_M: '<?=GetMessageJS("SALE_MEASURE_SQ_M");?>'
	});
	var basketJSParams = <?=CUtil::PhpToJSObject($arBasketJSParams);?>;
</script>

<?$this->addExternalJs($templateFolder."/script.js");

if(strlen($arResult["ERROR_MESSAGE"]) <= 0) {
	$normalCount = count($arResult["ITEMS"]["AnDelCanBuy"]);
	$delayCount = count($arResult["ITEMS"]["DelDelCanBuy"]);
	
	foreach(array_keys($arResult["GRID"]["HEADERS"]) as $id) {
		$data = $arResult["GRID"]["HEADERS"][$id];
		$headerName = (isset($data["name"]) ? (string)$data["name"] : "");
		if($headerName == "")
			$arResult["GRID"]["HEADERS"][$id]["name"] = GetMessage("SALE_".$data["id"]);
		unset($headerName, $data);
	}
	unset($id);?>

	<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="basket_form" id="basket_form">
		<div id="basket_form_container">
			<div class="bx-ordercart">
				<div class="hidden-print bx-ordercart-tabs-block" data-entity="tabs">					
					<div class="row">
						<div class="col-xs-12 col-md-8">
							<div class="bx-ordercart-tabs-scroll">
								<ul class="bx-ordercart-tabs-list">
									<?if(!$arParams["DISABLE_BASKET"]) {?>
										<li class="bx-ordercart-tab active" data-entity="tab" data-value="basket-items" onclick="showBasketItemsList()">
											<?=GetMessage("SALE_BASKET_ITEMS")?>
											<span class="bx-ordercart-tab-count"><?=$normalCount?></span>
										</li>
									<?}?>
									<li class="bx-ordercart-tab<?=($arParams['DISABLE_BASKET'] ? ' active' : '')?>" data-entity="tab" data-value="basket-items-delayed" onclick="showBasketItemsList(2)">
										<?=GetMessage("SALE_BASKET_ITEMS_DELAYED")?>
										<span class="bx-ordercart-tab-count"><?=$delayCount?></span>
									</li>
								</ul>
							</div>
						</div>
						<div class="hidden-xs hidden-sm col-md-4">
							<div class="bx-ordercart-tab-buttons">
								<a class="btn btn-default" href="javascript:window.print(); void(0);" role="button" data-entity="print" style="<?=(!$arParams['DISABLE_BASKET'] ? ($normalCount <= 0 ? 'display: none;' : '') : ($delayCount <= 0 ? 'display: none;' : ''));?>"><i class="icon-print"></i><?=GetMessage("SALE_BASKET_PRINT")?></a>
								<?if(!$arParams["DISABLE_BASKET"]) {?>
									<a class="btn btn-default" href="<?=$arUrls['clear']?>" role="button" data-entity="clear" style="<?=($normalCount <= 0 ? 'display: none;' : '');?>"><i class="fa fa-trash-o" aria-hidden="true"></i><span><?=GetMessage("SALE_BASKET_CLEAR")?></span></a>
								<?}?>
								<a class="btn btn-default" href="<?=$arUrls['clearDelay']?>" role="button" data-entity="clearDelay" style="<?=(!$arParams['DISABLE_BASKET'] ? 'display: none;' : ($delayCount <= 0 ? 'display: none;' : ''))?>"><i class="fa fa-trash-o" aria-hidden="true"></i><span><?=GetMessage("SALE_BASKET_CLEAR_DELAYED")?></span></a>
							</div>
						</div>
					</div>					
				</div>
				<div id="warning_message" class="bx-ordercart-message" style="display: none;">
					<?if(!empty($arResult["WARNING_MESSAGE"]) && is_array($arResult["WARNING_MESSAGE"])) {
						foreach($arResult["WARNING_MESSAGE"] as $v)
							$arMessage[] = $v;
						ShowNote(implode("<br />", $arMessage), "warning");
					}?>
				</div>
				<?if(!$arParams["DISABLE_BASKET"])
					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items.php");
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/basket_items_delayed.php");?>
			</div>
		</div>
		<input type="hidden" name="BasketOrder" value="BasketOrder" />
	</form>
	<?if($arParams["USE_GIFTS"] == "Y") {?>
		<div class="basket-gifts" data-entity="parent-container" style="display: none;">
			<?if($arParams["GIFTS_HIDE_BLOCK_TITLE"] !== "Y") {?>
				<div class="h2" data-entity="header" data-showed="false" style="display: none; opacity: 0;"><?=($arParams["GIFTS_BLOCK_TITLE"] ?: Loc::getMessage("SALE_GIFTS_BLOCK_TITLE"))?></div>
			<?}
			CBitrixComponent::includeComponentClass("bitrix:sale.products.gift.basket");?>
			<?$APPLICATION->IncludeComponent("bitrix:sale.products.gift.basket", ".default",
				array(
					"PRODUCT_ID_VARIABLE" => $arParams["GIFTS_PRODUCT_ID_VARIABLE"],
					"ACTION_VARIABLE" => (!empty($arParams["GIFTS_ACTION_VARIABLE"]) ? $arParams["GIFTS_ACTION_VARIABLE"] : "action")."_spgb",
					"PRODUCT_ROW_VARIANTS" => "",
					"PAGE_ELEMENT_COUNT" => 0,
					"DEFERRED_PRODUCT_ROW_VARIANTS" => Bitrix\Main\Web\Json::encode(SaleProductsGiftBasketComponent::predictRowVariants(4, $arParams["GIFTS_PAGE_ELEMENT_COUNT"])),
					"DEFERRED_PAGE_ELEMENT_COUNT" => $arParams["GIFTS_PAGE_ELEMENT_COUNT"],
					"PRODUCT_DISPLAY_MODE" => $arParams["GIFTS_PRODUCT_DISPLAY_MODE"],
					"TEXT_LABEL_GIFT" => $arParams["GIFTS_TEXT_LABEL_GIFT"],
					"ADD_TO_BASKET_ACTION" => $arParams["GIFTS_ADD_TO_BASKET_ACTION"],
					"MESS_BTN_BUY" => $arParams["~GIFTS_MESS_BTN_BUY"],
					"MESS_BTN_ADD_TO_BASKET" => $arParams["~GIFTS_MESS_BTN_ADD_TO_BASKET"],
					"MESS_BTN_DETAIL" => $arParams["~GIFTS_MESS_BTN_DETAIL"],
					"MESS_BTN_SUBSCRIBE" => $arParams["~GIFTS_MESS_BTN_SUBSCRIBE"],										
					"HIDE_NOT_AVAILABLE" => $arParams["GIFTS_HIDE_NOT_AVAILABLE"],
					"HIDE_NOT_AVAILABLE_OFFERS" => $arParams["GIFTS_HIDE_NOT_AVAILABLE_OFFERS"],
					"PRODUCT_SUBSCRIPTION" => $arParams["GIFTS_PRODUCT_SUBSCRIPTION"],				
					"PRICE_CODE" => $arParams["GIFTS_PRICE_CODE"],
					"SHOW_PRICE_COUNT" => $arParams["GIFTS_SHOW_PRICE_COUNT"],
					"PRICE_VAT_INCLUDE" => $arParams["GIFTS_PRICE_VAT_INCLUDE"],
					"CONVERT_CURRENCY" => $arParams["GIFTS_CONVERT_CURRENCY"],
					"CURRENCY_ID" => $arParams["GIFTS_CURRENCY_ID"],
					"BASKET_URL" => $APPLICATION->GetCurPage(),
					"ADD_PROPERTIES_TO_BASKET" => $arParams["GIFTS_ADD_PROPERTIES_TO_BASKET"],
					"PRODUCT_PROPS_VARIABLE" => $arParams["GIFTS_PRODUCT_PROPS_VARIABLE"],
					"PARTIAL_PRODUCT_PROPERTIES" => $arParams["GIFTS_PARTIAL_PRODUCT_PROPERTIES"],
					"PRODUCT_PROPERTIES" => $arParams["GIFTS_PRODUCT_PROPERTIES"],
					"PRODUCT_QUANTITY_VARIABLE" => $arParams["GIFTS_PRODUCT_QUANTITY_VARIABLE"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"APPLIED_DISCOUNT_LIST" => $arResult["APPLIED_DISCOUNT_LIST"],
					"FULL_DISCOUNT_LIST" => $arResult["FULL_DISCOUNT_LIST"],
					"USE_ENHANCED_ECOMMERCE" => $arParams["USE_ENHANCED_ECOMMERCE"],
					"DATA_LAYER_NAME" => $arParams["DATA_LAYER_NAME"],
					"BRAND_PROPERTY" => $arParams["BRAND_PROPERTY"]
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);?>
		</div>
	<?}
} else {
	ShowNote($arResult["ERROR_MESSAGE"], "warning");
}